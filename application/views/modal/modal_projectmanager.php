<!-- ONGOING PROJECTS -->
<div class="modal fade" id="addProject" role="dialog" >
    <div class="modal-dialog box box-warning" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("PM013"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-target="addCat" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="addprojectform">
            <!-- text input -->
            <div class="row">
                <div class="col-sm-8">
                    <div hidden>
                        <input type="text" class="form-control">
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("PM014"); ?><span style="color:red;">*</span></label>
                        <input name="name" type="text" class="form-control" placeholder="Enter ...">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UT003"); ?></label>
                        <textarea name="description" type="text" rows="4" class="form-control" placeholder="Enter ..."></textarea>
                    </div>
                </div>
                <div class="col-sm-4">
                    <label><?php echo $this->lang->line("PM005"); ?><span style="color:red;">*</span></label>
                    <div class="input-group date col-lg-12" style="display:block">
                        <input name="start_date" id="start_date_add" type="text" class="form-control pull-right datepicker" readonly="">
                        <div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>
                    </div>
                    <label><?php echo $this->lang->line("PM006"); ?><span style="color:red;">*</span></label>
                    <div class="input-group date col-lg-12" style="display:block">
                        <input name="due_date" id="due_date_add" type="text" class="form-control pull-right datepicker" readonly="">
                        <div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>
                    </div>
                    <label><?php echo $this->lang->line("PM007"); ?></label>
                    <div class="input-group date col-lg-12" style="display:block">
                        <input name="due_date_revised" id="due_date_revised_add" type="text" class="form-control pull-right datepicker" readonly="">
                        <div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
            <div style="float:right;margin-top:10px;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-add-project"class="btn btn-sm btn-warning"><?php echo $this->lang->line("submit"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL ADD -->

<div class="modal fade" id="viewProject" role="dialog" >
    <div class="modal-dialog box box-success" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("PM019"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form">
            <!-- text input -->
            <div class="row">
                <div class="col-sm-8">
                    <div hidden>
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("PM014"); ?></label>
                        <input name="name" type="text" class="form-control" readonly>
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UT003"); ?></label>
                        <textarea  name="description" type="text" rows="4" class="form-control" readonly></textarea>
                    </div>
                </div>
                <div class="col-sm-4">
                    <label><?php echo $this->lang->line("PM005"); ?></label>
                    <div class="input-group date col-lg-12">
                        <input name="start_date" type="text" class="form-control pull-right datepicker"  readonly>
                        <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                    </div>
                    <label><?php echo $this->lang->line("PM006"); ?></label>
                    <div class="input-group date col-lg-12">
                        <input name="due_date" type="text" class="form-control pull-right datepicker"  readonly>
                        <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                    </div>
                    <label><?php echo $this->lang->line("PM007"); ?></label>
                    <div class="input-group date col-lg-12">
                        <input name="due_date_revised"type="text" class="form-control pull-right datepicker"  readonly>
                        <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                    </div>
                </div>
            </div>
            <div style="float:right;margin-top:10px;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("close"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL VIEW -->

<div class="modal fade" id="editProject" role="dialog" >
    <div class="modal-dialog box box-warning" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("PM020"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id='editprojectform'>
            <!-- text input -->
            <div class="row">
                <div class="col-sm-8">
                    <div hidden>
                      <input type="text" name="id" value="" hidden>
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("PM014"); ?><span style="color:red;">*</span></label>
                        <input name="name" type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UT003"); ?></label>
                        <textarea name="description" type="text" rows="4" class="form-control"></textarea>
                    </div>
                </div>
                <div class="col-sm-4">
                    <label><?php echo $this->lang->line("PM005"); ?><span style="color:red;">*</span></label>
                    <div class="input-group date col-lg-12">
                        <input name="start_date" id="start_date_edit" type="text" class="form-control pull-right datepicker" readonly>
                        <div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>
                    </div>
                    <label><?php echo $this->lang->line("PM006"); ?><span style="color:red;">*</span></label>
                    <div class="input-group date col-lg-12">
                        <input name="due_date" id="due_date_edit" type="text" class="form-control pull-right datepicker" readonly>
                        <div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>
                    </div>
                    <label><?php echo $this->lang->line("PM007"); ?></label>
                    <div class="input-group date col-lg-12">
                        <input name="due_date_revised" id="due_date_revised_edit"type="text" class="form-control pull-right datepicker" readonly>
                        <div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>
            <div style="float:right;margin-top:10px;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-update-project" class="btn btn-sm btn-warning"><?php echo $this->lang->line("save"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL ADD -->

<div class="modal fade" id="deleteProject" role="dialog" >
    <div class="modal-dialog box box-danger" style="width:30%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("warning"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <div class="form-group text-center">
              <input type="text" name="id" value="" hidden>
              <p><?php echo $this->lang->line("del_confirm"); ?></p>
            </div>

            <div class="text-center">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-delete-project"class="btn btn-sm btn-danger"><?php echo $this->lang->line("yes"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL DELETE -->

<!-- TASK CATEGORY -->
<div class="modal fade" id="addCat" role="dialog" >
    <div class="modal-dialog box box-warning" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("PM015"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="addcatform">
            <!-- text input -->
                    <div hidden>
                        <input type="text" class="form-control">
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("PM016"); ?><span style="color:red;">*</span><i class="fa text-danger" aria-hidden="true"></i></label>
                        <input type="text" name="name" class="form-control" placeholder="Enter ...">
                    </div>
                    <div class="form-group col-lg-12">
                      <label><?php lang('role_label')?><?php echo $this->lang->line("UM011"); ?><span style="color:red;">*</span><i class="fa text-danger" aria-hidden="true"></i></label>
                      <select name="role_id_add" id="role_id_add" class="form-control">
                        <option value="" disabled selected>Select</option>
                        <?php foreach($role as $r) {
                          echo '<option value="'.$r->id.'">'.$r->name.'</option>';
                        } ?>
                      </select>
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UT003"); ?></label>
                        <textarea type="text"name="description" rows="4" class="form-control" placeholder="Enter ..."></textarea>
                    </div>

            <div style="float:right;margin-top:10px;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-add-cat" class="btn btn-sm btn-warning"><?php echo $this->lang->line("submit"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL ADD -->

<div class="modal fade" id="viewCat" role="dialog" >
    <div class="modal-dialog box box-success" style="width:40%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("PM021"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form">
            <!-- text input -->
                    <div hidden>
                        <input type="text" class="form-control">
                        <input name="id" type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("PM008"); ?></label>
                        <input type="text" name="name" class="form-control" disabled>
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UM004"); ?></label>
                        <input type="text" name="role"class="form-control" disabled>
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UT003"); ?></label>
                        <textarea type="text" name="description"rows="4" class="form-control" disabled></textarea>
                    </div>

            <div style="float:right;margin-top:10px;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("close"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL VIEW -->

<div class="modal fade" id="editCat" role="dialog" >
    <div class="modal-dialog box box-warning" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("PM022"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form id="editcatform">
            <!-- text input -->
                    <div hidden>
                        <input name="id" type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("PM016"); ?><span style="color:red;">*</span></label>
                        <input type="text" name="name_cat" class="form-control" required>
                    </div>
                    <div class="form-group col-lg-12">
                      <label><?php lang('role_label')?><?php echo $this->lang->line("UM011"); ?><span style="color:red;">*</span></label>
                      <select name="role_id_edit" id="role_id_edit" class="form-control">
                        <option value="" disabled selected>Select</option>
                        <?php foreach($role as $r) {
                          echo '<option value="'.$r->id.'">'.$r->name.'</option>';
                        } ?>
                      </select>
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UT003"); ?></label>
                        <textarea name="description_cat" type="text" rows="4" class="form-control"></textarea>
                    </div>

            <div style="float:right;margin-top:10px;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-edit-cat" class="btn btn-sm btn-warning"><?php echo $this->lang->line("save"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL ADD -->

<div class="modal fade" id="deleteCat" role="dialog" >
    <div class="modal-dialog box box-danger" style="width:30%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("warning"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <div class="form-group text-center">
              <input type="text" name="id" value="" hidden>
              <p><?php echo $this->lang->line("del_confirm"); ?></p>
            </div>

            <div class="text-center">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-delete-cat"class="btn btn-sm btn-danger"><?php echo $this->lang->line("yes"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL DELETE -->

<!-- TASK STATUS -->
<div class="modal fade" id="addStatus" role="dialog" >
    <div class="modal-dialog box box-warning" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("PM017"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="addstatusform">
            <!-- text input -->

                    <div hidden>
                        <input type="text" class="form-control">
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("PM018"); ?><span style="color:red;">*</span></label>
                        <input name="name" type="text" class="form-control" placeholder="Enter ...">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UT003"); ?></label>
                        <textarea name="description" type="text" rows="4" class="form-control" placeholder="Enter ..."></textarea>
                    </div>

            <div style="float:right;margin-top:10px;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-add-status"class="btn btn-sm btn-warning"><?php echo $this->lang->line("submit"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL ADD -->

<div class="modal fade" id="viewStatus" role="dialog" >
    <div class="modal-dialog box box-success" style="width:40%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("PM023"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form">
            <!-- text input -->

                    <div hidden>
                        <input type="text" class="form-control">
                        <input type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("PM011"); ?></label>
                        <input name="name" type="text" class="form-control" disabled>
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UT003"); ?></label>
                        <textarea name="description"type="text" rows="4" class="form-control" disabled></textarea>
                    </div>

            <div style="float:right;margin-top:10px;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("close"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL VIEW -->

<div class="modal fade" id="editStatus" role="dialog" >
    <div class="modal-dialog box box-warning" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("PM024"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="editstatusform">
            <!-- text input -->
                    <div hidden>
                        <input type="text" class="form-control">
                        <input name="id" type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("PM018"); ?><span style="color:red;">*</span></label>
                        <input name="name" type="text" class="form-control">
                    </div>
                    <div class="form-group col-lg-12">
                        <label><?php echo $this->lang->line("UT003"); ?></label>
                        <textarea name="description"type="text" rows="4" class="form-control"></textarea>
                    </div>

            <div style="float:right;margin-top:10px;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-edit-status" class="btn btn-sm btn-warning"><?php echo $this->lang->line("save"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL ADD -->

<div class="modal fade" id="deleteStatus" role="dialog" >
    <div class="modal-dialog box box-danger" style="width:30%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("warning"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <div class="form-group text-center">
              <input type="text" name="id" value="" hidden>
              <p><?php echo $this->lang->line("del_confirm"); ?></p>
            </div>

            <div class="text-center">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-delete-status"class="btn btn-sm btn-danger"><?php echo $this->lang->line("yes"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL DELETE -->
