<div class="modal fade" id="viewTask" role="dialog" >
    <div class="modal-dialog box box-success" style="width:70%">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><strong><?php echo $this->lang->line("WB018"); ?></strong></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("PM004"); ?><span style="color:red;">*</span></label>
                            <input name="project" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("PM010"); ?><span style="color:red;">*</span></label>
                            <input name="category" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB005"); ?></label>
                            <input name="pic" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB006"); ?></label>
                            <input name="excecutedby" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB004"); ?><span style="color:red;">*</span></label>
                            <input name="task_name" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB007"); ?></label>
                            <input name="start_time" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB008"); ?></label>
                            <input name="stop_time" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("PM005"); ?><span style="color:red;">*</span></label>
                            <input name="start_date" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("PM006"); ?><span style="color:red;">*</span></label>
                            <input name="due_date" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("PM007"); ?></label>
                            <input name="due_date_revised" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB010"); ?><span style="color:red;">*</span></label>
                            <input name="estimated_hour" type="number" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB011"); ?></label>
                            <input name="actual_hour"  type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB012"); ?></label>
                            <input name="task_percentage" type="number" class="form-control" placeholder="" disabled>
                        </div>
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB009"); ?><span style="color:red;">*</span></label>
                            <input name="task_status" type="text" class="form-control" placeholder="" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB013"); ?></label>
                            <textarea name="notes" class="form-control" rows="3" placeholder="" disabled></textarea>
                        </div>
                        <!-- /.form-group -->
                        <button class="btn btn-sm" data-dismiss="modal" style="background:#777;color:beige;font-weight:700;float:right;"><?php echo $this->lang->line("close"); ?></button>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.modal content -->
    </div>
</div>
<!-- END OF VIEW MODAL -->

<div class="modal fade" id="editTask" role="dialog" >
    <div class="modal-dialog box box-warning" style="width:70%">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><strong><?php echo $this->lang->line("WB019"); ?></strong></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->

        <div class="box-body">
            <div class="row">
              <form role="form" id="edittaskform">
                <div class="col-md-4">
                        <div class="form-group col-md-12">
                          <input type="text" name="id" value="" hidden>
                            <label><?php echo $this->lang->line("PM004"); ?><span style="color:red;">*</span></label>
                            <select name="project" id="project_edit" class="form-control" required>
                              <option value="" disabled selected><?php echo $this->lang->line('select') ?></option>
                              <?php foreach($project as $r) {
                                echo '<option value="'.$r->id.'">'.$r->name.'</option>';
                              } ?>
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("PM010"); ?><span style="color:red;">*</span></label>
                            <select name="category" id="category_edit" class="form-control" required>
                              <option value="" disabled selected><?php echo $this->lang->line('select') ?></option>
        
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB005"); ?></label>
                            <select name="pic" id="pic_edit" class="form-control" required>
                              <option value="" disabled selected><?php echo $this->lang->line('select') ?></option>
                              <?php foreach($pic as $r) {
                                echo '<option value="'.$r->id.'">'.$r->username.'</option>';
                              } ?>
                            </select>

                            <input name="picid" type="text" hidden>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB006"); ?></label>
                            <input name="excecutedby" type="text" class="form-control" placeholder="" disabled>
                            <input name="excecutedbyid" type="text" hidden>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB004"); ?><span style="color:red;">*</span></label>
                            <input type="text" name="task_name" class="form-control" placeholder="Enter ...">
                        </div>
                        <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB007"); ?></label>
                            <div class="input-group">
                                <input name="start_time" id="start_time" type="text" class="form-control timepicker">
                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB008"); ?></label>
                            <div class="input-group">
                                <input name="stop_time" id="stop_time" type="text" class="form-control timepicker">
                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("PM005"); ?><span style="color:red;">*</span></label>
                            <div class="input-group date">
                                <input name="start_date" id="sdatepicker" type="text" class="form-control pull-right datepicker" readonly="">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("PM006"); ?><span style="color:red;">*</span></label>
                            <div class="input-group date">
                                <input name="due_date" id="edatepicker" type="text" class="form-control pull-right datepicker" readonly="">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("PM007"); ?></label>
                            <div class="input-group date">
                                <input name="due_date_revised" id="edatepicker_rev" type="text" class="form-control pull-right datepicker" readonly="">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                        <!-- /.form-group -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB010"); ?><span style="color:red;">*</span></label>
                            <!-- <input type="text" class="form-control" placeholder="Enter ...">     -->
                            <input name="estimated_hour" type="number" class="form-control" step="0.01">
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB011"); ?></label>
                            <input name="actual_hour" id="actual_hour" type="text" class="form-control" placeholder="1.00" disabled>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB012"); ?></label>
                            <div class="input-group">
                                <input name="task_percentage" type="number" class="form-control" step="0.01" placeholder="0.00">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB009"); ?><span style="color:red;">*</span></label>
                            <select name="task_status" id="task_status_edit" class="form-control" required>
                              <option value="" disabled selected><?php echo $this->lang->line('select') ?></option>
                              <?php foreach($task_status as $r) {
                                echo '<option value="'.$r->id.'">'.$r->name.'</option>';
                              } ?>
                            </select>
                        </div>
                        <!-- /.form-group -->
                        <div class="form-group col-md-12">
                            <label><?php echo $this->lang->line("WB013"); ?></label>
                            <textarea name="notes" class="form-control" rows="3" placeholder="Enter ..."></textarea>
                        </div>
                        <!-- /.form-group -->
                        <div style="float:right;">
                            <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                            <button class="btn btn-sm" id="button-update-task" style="background:#f7b733;color:#404040;font-weight:700;"><?php echo $this->lang->line("save"); ?></button>
                        </div>
                    </form>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.modal content -->
    </div>
</div>
<!-- END OF EDIT MODAL -->

<div class="modal fade" id="deleteTask" role="dialog" >
    <div class="modal-dialog box box-danger" style="width:30%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("warning"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <div class="form-group text-center">
              <input type="text" id="id" name="id" value="" hidden>
              <p><?php echo $this->lang->line("del_confirm"); ?></p>
            </div>

            <div class="text-center">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-delete-task" class="btn btn-sm btn-danger"><?php echo $this->lang->line("yes"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL DELETE -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
