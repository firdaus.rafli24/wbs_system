<!-- ROLE / POSITION -->

<div class="modal fade" id="addRole" role="dialog" >
    <div class="modal-dialog box box-warning" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("UT004"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="addroleform">
            <!-- text input -->
            <div hidden>
              <input type="text" class="form-control">
              <input type="text" class="form-control">
            </div>
            <div class="form-group col-lg-12">
              <label><?php echo $this->lang->line("UT005"); ?><span style="color:red;">*</span></label>
              <input type="text" name="name" class="form-control" placeholder="Enter <?php echo $this->lang->line("UT005"); ?>" autofocus>
            </div>
            <div class="form-group col-lg-12">
              <label><?php echo $this->lang->line("UT003"); ?></label>
              <textarea type="text" name="description" rows="4" class="form-control" placeholder="Enter <?php echo $this->lang->line("UT003"); ?>"></textarea>
            </div>

            <div style="float:right;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="submitrole" class="btn btn-sm btn-warning"><?php echo $this->lang->line("submit"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL ADD -->

<div class="modal fade" id="viewRole" role="dialog" >
    <div class="modal-dialog box box-success" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("UT006"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form">
            <!-- text input -->
            <div hidden>
              <input type="text" class="form-control">
              <input type="text" class="form-control">
            </div>
            <div class="form-group col-lg-12">
              <label><?php echo $this->lang->line("UT005"); ?></label>
              <input type="text" name="name" class="form-control" placeholder="" disabled>
            </div>
            <div class="form-group col-lg-12">
              <label><?php echo $this->lang->line("UT003"); ?></label>
              <textarea type="text" name="description" rows="4" class="form-control" placeholder="" disabled></textarea>
            </div>

            <div style="float:right;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("close"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL VIEW -->

<div class="modal fade" id="editRole" role="dialog" >
    <div class="modal-dialog box box-warning" style="width:50%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("UT007"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="editroleform">
            <!-- text input -->
            <div hidden>
              <input class="id" type="text" name="id" class="form-control">
              <input type="text" name="user_type" class="form-control">
            </div>
            <div class="form-group col-lg-12">
              <label><?php echo $this->lang->line("UT005"); ?><span style="color:red;">*</span></label>
              <input type="text" name="name" class="form-control" placeholder="">
            </div>
            <div class="form-group col-lg-12">
              <label><?php echo $this->lang->line("UT003"); ?></label>
              <textarea type="text" name="description"rows="4" class="form-control" placeholder=""></textarea>
            </div>

            <div style="float:right;">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-update-role"class="btn btn-sm btn-warning"><?php echo $this->lang->line("save"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL EDIT -->

<div class="modal fade" id="deleteRole" role="dialog" >
    <div class="modal-dialog box box-danger" style="width:30%;">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><?php echo $this->lang->line("warning"); ?></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

            <div class="form-group text-center">
              <input type="hidden" name="id">
              <p><?php echo $this->lang->line("del_confirm"); ?></p>
            </div>

            <div class="text-center">
              <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              <button type="button" id="button-delete-role" class="btn btn-sm btn-danger"><?php echo $this->lang->line("yes"); ?></button>
            </div>
          </form>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
</div>
<!-- END OF MODAL DELETE -->
