<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WBS | Inventory Loan List</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php $this->load->view('template/style') ?>
    <!-- CUSTOM CSS UM -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
    <style type="text/css">
        .form-control[disabled],
        .form-control[readonly],
        fieldset[disabled] .form-control {
            background-color: #fff;
            opacity: 1;
        }

        .select2-container--default.select2-container--disabled .select2-selection--multiple {
            background-color: #fff;
            cursor: default;
            opacity: 1;
        }
    </style>

</head>

<body class="layout-top-nav skin-white">
    <!-- Site wrapper -->
    <div class="wrapper">

        <?php $this->load->view('template/navbar') ?>
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!-- <section class="content-header">
      <h1>
        User Management
        <small>(Administrator Only)</small>
      </h1>
    </section> -->

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <!-- DATATABLE -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped" style="width:1300px;table-layout: fixed; word-wrap:break-word;">
                            <thead>
                                <tr>
                                    <th data-data="id" class="hidden">#</th>
                                    <th data-data="start_date"><?php echo $this->lang->line("IL001"); ?></th>
                                    <th data-data="loan_duration"><?php echo $this->lang->line("IL002"); ?></th>
                                    <th data-data="username"><?php echo $this->lang->line("IL003"); ?></th>
                                    <th data-data="inventory_name"><?php echo $this->lang->line("IL004"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php $this->load->view('template/footer') ?>
    </div>
    <!-- ./wrapper -->

    <?php $this->load->view('template/localization') ?>
    <?php $this->load->view('template/js') ?>

    <script>
        $(document).ready(function() {
            $('.sidebar-menu').tree()
        })
    </script>
    <script src="<?php echo base_url('assets/js/'); ?>inventory_list_view.js"></script>
</body>

</html>