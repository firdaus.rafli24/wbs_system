<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | <?php echo $this->lang->line('SubMenu006') ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
</head>
<body class="layout-top-nav skin-white">
<!-- Site wrapper -->
<div class="wrapper">

  <?php $this->load->view('template/navbar') ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <h1 class="text-center">Ini Dashboard</h1>
      <hr style="border-color:#dfdce3;">
    </section> -->

    <!-- Main content -->
    <section class="content" >
        <!-- <button class="btn" data-toggle="modal" data-target="#addActivity" style="background:#f7b733;margin-bottom:20px;"><i class="fa fa-plus"></i> <span>Add Activity</span></button> -->
        <!-- MAIN PAGE -->
        <div class="box box-default" style="border-top-color:#fc4a1a;">
            <div class="box-header with-border">
                <h3 class="box-title"><?php echo $this->lang->line('SubMenu006') ?></h3>
                <!-- <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div> -->
            </div>
            <div class="box-body">
              <form role="form" id="changepasswordform">
                <!-- text input -->
                <div class="form-group">
                  <label><?php echo $this->lang->line('cp_current_password') ?></label>
                  <input type="password" name="current_password" id="current_password" class="form-control" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label><?php echo $this->lang->line('cp_new_password') ?></label>
                  <input type="password" name="new_password" id="new_password" class="form-control" placeholder="Enter ...">
                </div>
                <div class="form-group">
                  <label><?php echo $this->lang->line('cp_new_password_conf') ?></label>
                  <input type="password" name="new_password_conf" id="new_password_conf" class="form-control" placeholder="Enter ...">
                </div>
                <div style="float:right;">
                  <button type="button" id="submitpassword" class="btn btn-sm btn-warning"><?php echo $this->lang->line('submit') ?></button>
                </div>

              </form>
            </div>
        </div>

    </section>
    <!-- /.Main content -->
  </div>
  <!-- /.content-wrapper -->

  <?php $this->load->view('template/footer') ?>

</div>
<!-- ./wrapper -->
<?php $this->load->view('template/localization') ?>
<?php $this->load->view('template/js') ?>
<script src="<?php echo base_url('assets/js/changepassword.js'); ?>"></script>

</body>
</html>
