<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | <?php echo $this->lang->line("Menu000"); ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
  <style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
     background-color: #fff; 
     opacity: 1;
   }
 </style>
</head>
<body class="layout-top-nav skin-white">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php $this->load->view('template/navbar') ?>

    <!-- =============================================== -->
    <div class="content-wrapper">
      <section class="content-header">
        <h1> <?php echo $this->lang->line("Menu000"); ?></h1>
      </section>

      <section class="content" >
        <div class="box">
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped table-responsive" style="table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th class="">#</th>
                  <th><?= $this->lang->line("MenuConfig01"); ?></th>
                  <th><?= $this->lang->line("MenuConfig03"); ?></th>
                  <th><?= $this->lang->line("MenuConfig02"); ?></th>
                  <th><?= $this->lang->line("MenuConfig04"); ?></th>
                  <th><?= $this->lang->line("action"); ?></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->

    <!-- Modals Edit User-->
    <div class="modal fade" id="editConfig" role="dialog" >
      <div class="modal-dialog box box-warning" style="width:50%">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><p id="titleedit"></p>Edit <?php echo $this->lang->line("Menu000"); ?></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <form role="form" id="editConfigForm">
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("MenuConfig01"); ?></label>
                <input type="text" name="config_key" id="config_key" class="form-control" required="" readonly="">
              </div>
              <div class="form-group col-lg-12">
                <input type="checkbox" name="config_enable" id="config_enable" >  <?php echo $this->lang->line("MenuConfig03"); ?>
              </div>
              <div class="form-group col-lg-6" id="value">
                <label>Custom <?php echo $this->lang->line("MenuConfig02"); ?></label>
                <input type="text" name="config_value" id="config_value" class="form-control">
              </div>
              <div class="form-group col-lg-12"></div>

              <div style="float:right;">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                <button type="button" id="button-update" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("save"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>

    <!-- /.modals -->

    <?php $this->load->view('template/footer') ?>

  </div>
  <!-- ./wrapper -->

  <?php $this->load->view('template/localization') ?>
  <?php $this->load->view('template/js') ?>
  <script src="<?php echo base_url('assets/js/menu_config.js'); ?>"></script>
</body>
</html>
