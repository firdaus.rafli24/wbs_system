
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS System</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/bootstrap/dist/css/bootstrap.min.css')?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/font-awesome/css/font-awesome.min.css') ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/Ionicons/css/ionicons.min.css') ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/dist/css/AdminLTE.css') ?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/dist/css/skins/_all-skins.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/sweetalert2/dist/sweetalert2.min.css'); ?>">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>">

  <style type="text/css">

    i{
      font-size: 22px !important;
      margin-left: 18px;
      vertical-align: middle;
      margin-right: 8px;
      display: inline-block;
      color: #707070;
    }

    input{
      border-radius: 6px !important;
    }

    button{
      margin-left: 20px !important;
      border-radius: 12px !important;
      padding-left: 28px !important;
      padding-right: 28px !important;
      text-decoration: none !important; 
      border: none !important;
    }

    #nav {
      background: rgba(255, 255, 255, 0.5) !important;
      height: 80px;
    }

    .content-wrapper {
      min-height: calc(100vh - 101px);
      background-image: url("<?php echo base_url() ?>assets/vendors/template/dist/img/workspace.png");
      z-index: 800;
    }

    .content {
      padding-top: 250px;
    }

    .main-header {
      background: transparent !important;
      border-color: transparent;
      padding-top: unset;
      padding-bottom: unset;
    }

    .navbar-brand {
      font-family: bauhaus93;
      font-weight: 500;
      color: rgba(0,153,255,0.75);
    }

    .navbar-header {
      width: unset;
    }
    .error-page{
      width: 700px;
      text-align: center;
    }

    .wrap{
      font-family: bauhaus93;
      font-weight: 500;
      font-size: 68px;
      color: #3F93CB;
      text-align: center;
    }

  </style>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition layout-top-nav">
  <div class="wrapper">

    <header class="main-header">
      <nav id="nav" class="navbar navbar-fixed-top">
        <div class="container" style="margin-top: 15px;">
          <div class="navbar-header">
            <a href="<?php echo base_url() ?>" class="navbar-brand text-blue">WBSS</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>


          <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
            <form class="navbar-form navbar-left" id="login_form" >
              <div class="form-group">
               <i class="fa fa-user fa-3"></i> 
               <input type="email" name="emailenter" id="emailenter" class="form-control" placeholder="<?php echo $this->lang->line("email"); ?>">
             </div>
             <div class="form-group">
               <i class="fa fa-lock"></i> 
               <input type="password" name="passwordenter" id="passwordenter" class="form-control" placeholder="<?php echo $this->lang->line("password"); ?>">
             </div>
             <div class="form-group">
              <button class="btn btn-primary btn-flat" id="signin" ><?php echo $this->lang->line("login_submit"); ?></button>

            </div>
          </form>
        </div>  
      </div>
    </nav>
  </header>


  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">

      <section class="content">
        <div class="error-page">

          <h1>
            <span class="wrap">IF YOU CAN DREAM IT,<br>
            YOU CAN DO IT</span>
          </h1>
        </div>
      </section>
    </div>
  </div>
  <nav id="nav" class="navbar navbar-fixed-bottom">

  </nav>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/vendors/template/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/vendors/template/bower_components/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/vendors/template/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/vendors/template/bower_components/fastclick/lib/fastclick.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/vendors/template/dist/js/adminlte.min.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('assets/vendors/template/dist/js/demo.js') ?>"></script>
<script src="<?php echo base_url('assets/vendors/template/plugins/iCheck/icheck.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/socket.io/socket.io.js');?>"></script>
<script src="<?php echo base_url('assets/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/sweetalert2/dist/sweetalert2.all.min.js'); ?>"></script>

<!-- iCheck -->
<script type="text/javascript"> 


  var base_url = "<?php echo base_url(); ?>"; 
  var ConfValue = {};

  <?php 
  $config = $this->config->config;
  foreach ($config as $key => $value) {
    if (is_array($value) == true) {
      $value = str_replace('\\\\','\\\\\\\\',$value);
      echo "ConfValue.".$key." = ".json_encode($value,true).";";    
    }else{
      $value = str_replace('\\\\','\\\\\\\\',$value);
      echo "ConfValue.".$key." = '".$value."';";  
    }   
  }

  ?>


</script>
<?php $this->load->view('template/localization') ?>
<script src="<?php echo base_url('assets/js/login.js'); ?>"></script>

<script type="text/javascript">
  // var TxtType = function(el, toRotate, period) {
  //   this.toRotate = toRotate;
  //   this.el = el;
  //   this.loopNum = 0;
  //   this.period = parseInt(period, 10) || 2000;
  //   this.txt = '';
  //   this.tick();
  //   this.isDeleting = false;
  // };

  // TxtType.prototype.tick = function() {
  //   var i = this.loopNum % this.toRotate.length;
  //   var fullTxt = this.toRotate[i];

  //   if (this.isDeleting) {
  //     this.txt = fullTxt.substring(0, this.txt.length - 1);
  //   } else {
  //     this.txt = fullTxt.substring(0, this.txt.length + 1);
  //   }

  //   this.el.innerHTML = '<span class="wrap"><b>'+this.txt+'</b></span>';

  //   var that = this;
  //   var delta = 200 - Math.random() * 100;

  //   if (this.isDeleting) { delta /= 2; }

  //   if (!this.isDeleting && this.txt === fullTxt) {
  //     delta = this.period;
  //     this.isDeleting = true;
  //   } else if (this.isDeleting && this.txt === '') {
  //     this.isDeleting = false;
  //     this.loopNum++;
  //     delta = 500;
  //   }

  //   setTimeout(function() {
  //     that.tick();
  //   }, delta);
  // };

  // window.onload = function() {
  //   var elements = document.getElementsByClassName('typewrite');
  //   for (var i=0; i<elements.length; i++) {
  //     var toRotate = elements[i].getAttribute('data-type');
  //     var period = elements[i].getAttribute('data-period');
  //     if (toRotate) {
  //       new TxtType(elements[i], JSON.parse(toRotate), period);
  //     }
  //   }

  //   var css = document.createElement("style");
  //   css.type = "text/css";
  //   css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
  //   document.body.appendChild(css);
  // };
</script>
</body>
</html>
