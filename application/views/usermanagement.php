<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | User management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
  <style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
     background-color: #fff; 
     opacity: 1;
   }
 </style>
</head>
<body class="layout-top-nav skin-white">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php $this->load->view('template/navbar') ?>

    <!-- =============================================== -->
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
          User Management
        </h1>
      </section>

      <section class="content" >
        <div class="box">
          <div class="box-header with-border">
            <button class="btn" data-toggle="modal" data-target="#addUser" data-backdrop="static" id="button-adduser" onclick="addmode()" style="background:#f7b733;"><i class="fa fa-user-plus"></i> <span><?php echo $this->lang->line("UM001"); ?></span></button>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
              title="Collapse">
              <i class="fa fa-minus"></i></button>
            </div>
          </div>

          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped table-responsive" style="table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th data-data="id" class="hidden">#</th>
                  <th data-data="username"><?php echo $this->lang->line("UM002"); ?></th>
                  <th data-data="email"><?php echo $this->lang->line("UM003"); ?></th>
                  <th data-data="user_type_name"><?php echo $this->lang->line("UM004"); ?></th>
                  <th data-data="employment_status"><?php echo $this->lang->line("UM005"); ?></th>
                  <th data-data="phone_number"><?php echo $this->lang->line("UM006"); ?></th>
                  <th data-data="wa_number"><?php echo $this->lang->line("UM007"); ?></th>
                  <th><?php echo $this->lang->line("action"); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->

    <!-- Modals -->
    <div class="modal fade" id="addUser" role="dialog" >
      <div class="modal-dialog box box-warning" style="width:50%">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><p id="title"></p></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <form role="form" id="adduserform">
              <div class="form-group" hidden>
                <input type="text" name="user_type" class="form-control">
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM008"); ?><span style="color:red;">*</span></label>
                <input type="text" name="username" class="form-control" placeholder="Enter ..." required>
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM003"); ?><span style="color:red;">*</span></label>
                <input type="email" name="email" class="form-control" placeholder="Enter ..." required>
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("password"); ?><span style="color:red;">*</span></label>
                <input type="password" name="password" class="form-control" placeholder="Enter ..." required>
              </div>
              <div class="form-group col-lg-6">
                <label><?php echo $this->lang->line("UM006"); ?><span style="color:red;">*</span></label>
                <input type="text" name="phone_number" class="form-control" placeholder="Enter ..." required>
              </div>
              <div class="form-group col-lg-6">
                <label><?php echo $this->lang->line("UM007"); ?><span style="color:red;">*</span></label>
                <input type="text" name="wa_number" class="form-control" placeholder="Enter ..."required>
              </div>

              <div class="form-group col-lg-12">
                <div class="d-inline p-2"><label><?php echo $this->lang->line("UM005"); ?><span style="color:red;">*</span></label></div>
                <div class="d-inline">
                  <label class="radio-inline"><input checked="checked" type="radio" name="optradio" id="Reguler" value="0"><?php echo $this->lang->line("UM009"); ?></label>
                  <label class="radio-inline"><input type="radio" name="optradio" id="Freelance" value="1"><?php echo $this->lang->line("UM010"); ?></label>
                </div>
              </div>

              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM011"); ?><span style="color:red;">*</span></label>
                <select name="user_type_add" id="user_type_add" class="form-control" required>
                  <option value="" disabled selected>Select</option>
                  <?php foreach($role as $r) {
                    echo '<option value="'.$r->id.'">'.$r->name.' ('.$r->description.')</option>';
                  } ?>
                </select>
              </div>
              <div class="form-group col-lg-12">
                <label class="control-label" for="foto">Digital Signature</label>
                <input type="file" name="digitalsignature" id="addDigitalSignature" class="form-control">
              </div>
              <div style="float:right;">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                <button type="button" id="button-add" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("submit"); ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- /. create -->

    <!-- Modals Edit User-->
    <div class="modal fade" id="editUser" role="dialog" >
      <div class="modal-dialog box box-warning" style="width:50%">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><p id="titleedit"></p></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <form role="form" id="edituserform">
              <div hidden>
                <input class="id" type="text" name="id" class="form-control">
                <input type="text" name="user_type" class="form-control">
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM008"); ?><span style="color:red;">*</span></label>
                <input type="text" name="usernameedit" class="form-control" placeholder="Enter ...">
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM003"); ?><span style="color:red;">*</span></label>
                <input type="email" name="emailedit" class="form-control" placeholder="Enter ...">
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("password"); ?><span style="color:red;">*</span></label>
                <input type="password" name="passwordedit" class="form-control" placeholder="Enter ...">
              </div>
              <div class="form-group col-lg-6">
                <label><?php echo $this->lang->line("UM006"); ?><span style="color:red;">*</span></label>
                <input type="text" name="phone_numberedit" class="form-control" placeholder="Enter ...">
              </div>
              <div class="form-group col-lg-6">
                <label><?php echo $this->lang->line("UM007"); ?><span style="color:red;">*</span></label>
                <input type="text" name="wa_numberedit" class="form-control" placeholder="Enter ...">
              </div>

              <div class="form-group col-lg-12">
                <div class="d-inline p-2"><label><?php echo $this->lang->line("UM005"); ?><span style="color:red;">*</span></label></div>
                <div class="d-inline">
                  <label class="radio-inline"><input type="radio" name="optradio" id="Reguleredit" value="0"><?php echo $this->lang->line("UM009"); ?></label>
                  <label class="radio-inline"><input type="radio" name="optradio" id="Freelanceedit" value="1"><?php echo $this->lang->line("UM010"); ?></label>
                </div>
              </div>

              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM011"); ?><span style="color:red;">*</span></label>
                <select name="user_type_edit" id="user_type_edit" class="form-control" required>
                  <option value="" disabled selected>Select</option>
                  <?php foreach($role as $r) {
                    echo '<option value="'.$r->id.'">'.$r->name.' ('.$r->description.')</option>';
                  } ?>
                </select>
              </div>
              <div style="float:right;">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                <button type="button" id="button-update" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("save"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>

    <div class="modal fade" id="viewUser" role="dialog" >
      <div class="modal-dialog box box-warning" style="width:50%">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><strong><?php echo $this->lang->line("UM013"); ?></strong></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form role="form">
              <!-- text input -->
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM008"); ?></label>
                <input type="text" class="form-control" name="username" placeholder="Enter ..." readonly="">
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM003"); ?></label>
                <input type="text" class="form-control" name="email" placeholder="Enter ..." readonly="">
              </div>
              <div class="form-group" style="display:none;">
                <label><?php echo $this->lang->line("password"); ?></label>
                <input type="password" class="form-control" name="password" placeholder="Enter ..." readonly="">
              </div>
              <div class="form-group col-lg-6">
                <label><?php echo $this->lang->line("UM006"); ?></label>
                <input type="text" class="form-control" name="phone_number" placeholder="Enter ..." readonly="">
              </div>
              <div class="form-group col-lg-6">
                <label><?php echo $this->lang->line("UM007"); ?></label>
                <input type="text" class="form-control" name="wa_number" placeholder="Enter ..." readonly="">
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM005"); ?></label>
                <input type="text" class="form-control" name="employment_status" placeholder="Enter ..." readonly="">
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("UM011"); ?></label>
                <input type="text" class="form-control" name="user_type" placeholder="Enter ..." readonly="">
              </div>
              <div style="float:right;">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("close"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <!-- /. view details -->

    <div class="modal fade" id="deleteUser" role="dialog" >
      <div class="modal-dialog box" style="width:30%;border-top-color:#fc4a1a;">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><strong><?php echo $this->lang->line("warning"); ?></strong></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form role="form">
              <!-- text input -->
              <div class="form-group text-center">
                <input type="hidden" name="id">
                <p><strong><?php echo $this->lang->line("del_confirm"); ?></strong></p>
              </div>
              <div class="text-center">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                <button type="button" id="button-delete" class="btn btn-sm" style="background:#fc4a1a;color:white;"><?php echo $this->lang->line("yes"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <!-- /. view details -->
    <!-- /.modals -->

    <?php $this->load->view('template/footer') ?>

  </div>
  <!-- ./wrapper -->

  <?php $this->load->view('template/localization') ?>
  <?php $this->load->view('template/js') ?>
  <script>
    function addmode() {
      document.getElementById("title").innerHTML = "<strong>Input New User Data</strong>";
    };
    function editmode() {
      document.getElementById("titleedit").innerHTML = "<strong>Update User Data</strong>";
    };

    $(document).ready(function () {
      $('.sidebar-menu').tree();
    });
    
  </script>
  <script src="<?php echo base_url('assets/js/usermanagement.js'); ?>"></script>
</body>
</html>
