<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />

  <style>
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
     background-color: #fff;
     opacity: 1;
   }
   .bar-chart-legend {
    display: inline-block;
    top: 8px;
    font-size: 10px;
  }

  .bar-chart-legend .legend-item {
    /*display: inline; */
    white-space:nowrap;

    margin-left: 5px;
  }

  .bar-chart-legend .legend-color {
    width: 20px;
    height: 12px;
    margin: 3px 5px;
    display: inline-block;
  }
  svg { overflow: visible !important; }


</style>


</head>
<body class="layout-top-nav skin-white">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php $this->load->view('template/navbar') ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header" style="margin-bottom: 150px;">
        <h1>
          Dashboard
        </h1>

        <form role="form" id="filterDashboard" style="margin-top: 10px;">
          <div class="form-group col-lg-2">
            <label><?php echo $this->lang->line("PA002"); ?></label>
            <input type="text" name="start_date" id="start_date" class="form-control datepicker" placeholder="<?php echo $this->lang->line('PA002'); ?>" value="<?php echo date('Y-m-01', strtotime(date('Y-m-d'))); ?>" readonly>
          </div>
          <div class="form-group col-lg-2">
            <label>Stop <?php echo $this->lang->line("NTF002"); ?></label>
            <input type="text" name="stop_date" id="stop_date" class="form-control datepicker" placeholder="<?php echo $this->lang->line('TF002'); ?>" value="<?php echo date('Y-m-t', strtotime(date('Y-m-d'))); ?>" readonly>
          </div>
          <div class="form-group col-lg-3">
            <label><?php echo $this->lang->line("UM004"); ?></label>
            <select name="role[]" id="role" class="form-control select2" multiple="multiple" style="width: 100%;" data-placeholder="<?php echo $this->lang->line("UM004"); ?>" selected>
              <?php foreach ($role as $key => $value): ?>
                <option value=<?= $value->id ?> ><?= $value->name ?> </option>
              <?php endforeach; ?>
            </select> <input type="checkbox" name="allRole" id="checkboxRole" > <?php echo $this->lang->line("all"); ?> <?php echo $this->lang->line("UM004"); ?>
          </div>
            <div class="form-group col-lg-3">
              <label><?php echo $this->lang->line("NTF008"); ?></label>
              <select name="user[]" id="user" class="form-control select2" multiple="multiple" style="width: 100%;" data-placeholder="<?php echo $this->lang->line("NTF008"); ?>" required>
                <!--   <?php foreach ($user as $key => $value): ?>
                    <option value=<?= $value->id ?> ><?= $value->username ?> </option>
                    <?php endforeach; ?> -->
              </select> <input type="checkbox" name="allUser" id="checkboxUser" > <?php echo $this->lang->line("all"); ?> <?php echo $this->lang->line("NTF008"); ?>
            </div>
            <div class="form-group col-lg-2">
              <label> </label>
              <button type="button" id="search" class="btn btn-primary form-control"><i class="fa fa-search"></i> <span><?php echo $this->lang->line("search"); ?></span></button>
            </div>
          </form>
            </section>

            <!-- Main content -->
            <section class="content" >

              <!-- HRD -->
              <div class="row hidden" style="margin: 0px 10px;" id="content-hrd">
                <div class="box" style="border-top-color:#337e73;">
                  <div class="box-header with-border text-center">
                    <h3 class="box-title">HRD Time Consumption</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="chart-responsive">
                      <div id="hrd_legend" class="bar-chart-legend"></div>
                      <div id="bar-chart-hrd" ></div>
                    </div>
                    <div style="margin-top: 150px;">
                      <table id="table_hrd" class="table table-bordered table-striped" style="width:1500px;table-layout: fixed; word-wrap:break-word;">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <?php foreach($project as $p) {
                              echo '<th>'.$p.'</th>';
                            } ?>
                          </tr>
                        </thead>
                        <!-- /.thead -->
                        <tbody id="target_hrd">

                        </tbody>
                        <!-- /.tbody -->
                      </table>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <hr style="border-color:#dfdce3;">
              </div>
              <!-- PG -->
              <div class="row hidden" style="margin: 0px 10px;" id="content-pg">
                <div class="box" style="border-top-color:#337e73;">
                  <div class="box-header with-border text-center">
                    <h3 class="box-title">PG Time Consumption</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="chart-responsive">
                      <div id="pg_legend" class="bar-chart-legend"></div>
                      <div id="bar-chart-pg" ></div>
                    </div>
                    <div style="margin-top: 150px;">
                      <table id="table_pg" class="table table-bordered table-striped" style="width:1500px;table-layout: fixed; word-wrap:break-word;">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <?php foreach($project as $p) {
                              echo '<th>'.$p.'</th>';
                            } ?>
                          </tr>
                        </thead>
                        <!-- /.thead -->
                        <tbody id="target_pg">

                        </tbody>
                        <!-- /.tbody -->
                      </table>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <hr style="border-color:#dfdce3;">
              </div>
              <!-- QC -->
              <div class="row hidden" style="margin: 0px 10px;" id="content-qc">
                <div class="box" style="border-top-color:#777;">
                  <div class="box-header with-border text-center">
                    <h3 class="box-title">QC Time Consumption</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="box-body chart-responsive">
                      <div id="qc_legend" class="bar-chart-legend"></div>
                      <div id="bar-chart-qc" ></div>
                    </div>
                    <div style="margin-top: 150px;">
                      <table id="table_qc" class="table table-bordered table-striped" style="width:1500px;table-layout: fixed; word-wrap:break-word;">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <?php foreach($project as $p) {
                              echo '<th>'.$p.'</th>';
                            } ?>
                          </tr>
                        </thead>
                        <!-- /.thead -->
                        <tbody id="target_qc">

                        </tbody>
                        <!-- /.tbody -->
                      </table>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
                <hr style="border-color:#dfdce3;">
              </div>
              <!-- SA -->
              <div class="row hidden" style="margin: 0px 10px;" id="content-sa">
                <div class="box" style="border-top-color:#337e73;">
                  <div class="box-header with-border text-center">
                    <h3 class="box-title">System Admin Consumption</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="chart-responsive">
                      <div id="sa_legend" class="bar-chart-legend"></div>
                      <div id="bar-chart-sa" ></div>
                    </div>
                    <div style="margin-top: 150px;">
                      <table id="table_sa" class="table table-bordered table-striped" style="width:1500px;table-layout: fixed; word-wrap:break-word;">
                        <thead>
                          <tr>
                            <th>Name</th>
                            <?php foreach($project as $p) {
                              echo '<th>'.$p.'</th>';
                            } ?>
                          </tr>
                        </thead>
                        <!-- /.thead -->
                        <tbody id="target_sa">

                        </tbody>
                        <!-- /.tbody -->
                      </table>
                    </div>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
              <!-- /.row -->

          </section>
          <!-- /.Main content -->
        </div>
        <!-- /.content-wrapper -->

        <?php $this->load->view('template/footer') ?>

      </div>
      <!-- ./wrapper -->

      <?php $this->load->view('template/localization') ?>
      <?php $this->load->view('template/js') ?>

      <script src="<?php echo base_url('assets/js/dashboard.js'); ?>"></script>
    </body>
    </html>
