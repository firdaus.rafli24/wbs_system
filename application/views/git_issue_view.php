<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | <?php echo $this->lang->line("GI"); ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
  <style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
     background-color: #fff;
     opacity: 1;
   }
 </style>

</head>
<body class="layout-top-nav skin-white">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php $this->load->view('template/navbar') ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <h1>
        User Management
        <small>(Administrator Only)</small>
      </h1>
    </section> -->

    <!-- Main content -->
    <section class="content" >

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <!-- <h3 class="box-title">List User</h3> -->
          <form class="" action="index.html" method="post">
            <div class="form-group col-lg-2">
              <label><?php echo $this->lang->line("PA002"); ?></label>
              <input type="text" name="start_date" id="start_date" class="form-control datepicker" placeholder="<?php echo $this->lang->line('PA002'); ?>" value="<?php echo date('Y-m-01', strtotime(date('Y-m-d'))); ?>" readonly>
            </div>
            <div class="form-group col-lg-2">
              <label>Stop <?php echo $this->lang->line("NTF002"); ?></label>
              <input type="text" name="stop_date" id="stop_date" class="form-control datepicker" placeholder="<?php echo $this->lang->line('TF002'); ?>" value="<?php echo date('Y-m-t', strtotime(date('Y-m-d'))); ?>" readonly>
            </div>
            <div class="form-group col-lg-3">
              <label><?php echo $this->lang->line("UM015"); ?></label>
              <select name="label[]" id="label" class="form-control select2" multiple="multiple" style="width: 100%;" data-placeholder="<?php echo $this->lang->line("UM015"); ?>" selected>
                <?php foreach ($labels as $key => $value): ?>
                  <option value=<?= "'$value'" ?>><?= $value ?> </option>
                <?php endforeach; ?>
              </select> <input type="checkbox" name="allLabel" id="checkboxLabel" > <?php echo $this->lang->line("all"); ?> <?php echo $this->lang->line("UM015"); ?>
            </div>
            <div class="form-group col-lg-2">
              <label></label>
              <button type="button" id="search" class="btn btn-primary form-control" style="margin-top: 5px;"><i class="fa fa-search"></i> <span><?php echo $this->lang->line("search"); ?></span></button>
            </div>
          </form>

          <div class="form-group col-lg-2 pull-right">
            <label></label>
            <button class="btn form-control" data-toggle="modal" data-target="#addGitIssue" data-backdrop="static" id="button-adduser"  style="background:#f7b733;margin-top: 5px;"><i class="fa fa-upload"></i> <span><?php echo $this->lang->line("GI001"); ?></span></button>
          </div>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
          </div>
        </div>

        <!-- DATATABLE -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped" style="width:auto;table-layout: fixed; word-wrap:break-word;">
            <thead>
              <tr>
                <th data-data="id"><?php echo $this->lang->line("WB021"); ?></th>
                <th data-data="project"><?php echo $this->lang->line("GI002"); ?></th>
                <th data-data="upload_username"><?php echo $this->lang->line("GI003"); ?></th>
                <th data-data="issue_id"><?php echo $this->lang->line("GI004"); ?></th>
                <th data-data="url"><?php echo $this->lang->line("GI005"); ?></th>
                <th data-data="title"><?php echo $this->lang->line("GI006"); ?></th>
                <th data-data="state"><?php echo $this->lang->line("GI007"); ?></th>
                <th data-data="description"><?php echo $this->lang->line("GI008"); ?></th>
                <th data-data="author"><?php echo $this->lang->line("GI009"); ?></th>
                <th data-data="author_username"><?php echo $this->lang->line("GI010"); ?></th>
                <th data-data="assignee"><?php echo $this->lang->line("GI011"); ?></th>
                <th data-data="assignee_username"><?php echo $this->lang->line("GI012"); ?></th>
                <th data-data="confidential"><?php echo $this->lang->line("GI013"); ?></th>
                <th data-data="locked"><?php echo $this->lang->line("GI014"); ?></th>
                <th data-data="due_date"><?php echo $this->lang->line("GI015"); ?></th>
                <th data-data="created_at"><?php echo $this->lang->line("GI016"); ?></th>
                <th data-data="updated_at"><?php echo $this->lang->line("GI017"); ?></th>
                <th data-data="closed_at"><?php echo $this->lang->line("GI018"); ?></th>
                <th data-data="milestone"><?php echo $this->lang->line("GI019"); ?></th>
                <th data-data="weight"><?php echo $this->lang->line("GI020"); ?></th>
                <th data-data="labels"><?php echo $this->lang->line("GI021"); ?></th>
                <th data-data="time_estimate"><?php echo $this->lang->line("GI022"); ?></th>
                <th data-data="time_spent"><?php echo $this->lang->line("GI023"); ?></th>
                <th><?php echo $this->lang->line("action"); ?></th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->

      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modals -->
  <div class="modal fade" id="addGitIssue" role="dialog" >
    <div class="modal-dialog modal-lg box box-warning" style="width:50%">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><p id="title">Import <?php echo $this->lang->line("GI"); ?></p></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <form role="form" id="addform"  enctype="multipart/form-data">
            <div class="form-group col-lg-6">
              <label><?php echo $this->lang->line("GI002"); ?><span style="color:red;">*</span></label>
              <select name="project_id" id="project_id" class="form-control" required="">
               <option value="" selected="" disabled="">Select <?php echo $this->lang->line("PM004"); ?></option>
               <?php foreach($project as $r) {
                 echo '<option value="'.$r->id.'">'.$r->name.'</option>';
               } ?>
             </select>
           </div>

           <div class="form-group col-lg-6">
            <label><?php echo $this->lang->line("GI000"); ?></label>
            <input type="file" name="file_csv" id="file_csv" accept=".csv" class="form-control" required>
          </div>

          <div style="float:right;">
            <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
            <button type="button" id="button-add" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("submit"); ?></button>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
<!-- /. create -->

<!-- /. view details -->

<div class="modal fade" id="deleteGitIssue" role="dialog" >
  <div class="modal-dialog box" style="width:30%;border-top-color:#fc4a1a;">
    <div class="modal-content">
      <div class="box-header with-border text-center">
        <h3 class="box-title"><strong><?php echo $this->lang->line("warning"); ?></strong></h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <form role="form">
          <!-- text input -->
          <div class="form-group text-center">
            <input type="hidden" name="id">
            <p><strong><?php echo $this->lang->line("del_confirm"); ?></strong></p>
          </div>
          <div class="text-center">
            <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
            <button type="button" id="button-delete" class="btn btn-sm" style="background:#fc4a1a;color:white;"><?php echo $this->lang->line("yes"); ?></button>
          </div>
        </form>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
<!-- /. view details -->
<!-- /.modals -->

<?php $this->load->view('template/footer') ?>

</div>
<!-- ./wrapper -->

<?php $this->load->view('template/localization') ?>
<?php $this->load->view('template/js') ?>
<script src="<?php echo base_url('assets/js/'); ?>git_issue.js"></script>
</body>
</html>
