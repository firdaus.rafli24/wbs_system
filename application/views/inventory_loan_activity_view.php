<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WBS | Inventory Loan Activity</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php $this->load->view('template/style') ?>
    <!-- CUSTOM CSS UM -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
    <style type="text/css">
        .form-control[disabled],
        .form-control[readonly],
        fieldset[disabled] .form-control {
            background-color: #fff;
            opacity: 1;
        }

        .select2-container--default.select2-container--disabled .select2-selection--multiple {
            background-color: #fff;
            cursor: default;
            opacity: 1;
        }
    </style>

</head>

<body class="layout-top-nav skin-white">
    <!-- Site wrapper -->
    <div class="wrapper">

        <?php $this->load->view('template/navbar') ?>
        <!-- =============================================== -->
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!-- <section class="content-header">
      <h1>
        User Management
        <small>(Administrator Only)</small>
      </h1>
    </section> -->

            <!-- Main content -->
            <section class="content">

                <!-- Default box -->
                <div class="box">
                    <div class="box-header with-border">
                        <button class="btn" data-toggle="modal" data-target="#addInventoryLoan" data-backdrop="static" style="background:#f7b733;"><i class="fa fa-plus"></i> <span><?php echo $this->lang->line("IL000"); ?></span></button>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fa fa-minus"></i></button>
                        </div>
                    </div>

                    <!-- DATATABLE -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped" style="width:1300px;table-layout: fixed; word-wrap:break-word;">
                            <thead>
                                <tr>
                                    <th data-data="id" class="hidden">#</th>
                                    <th data-data="start_date"><?php echo $this->lang->line("IL001"); ?></th>
                                    <th data-data="loan_duration"><?php echo $this->lang->line("IL002"); ?></th>
                                    <th data-data="username"><?php echo $this->lang->line("IL003"); ?></th>
                                    <th data-data="inventory_name"><?php echo $this->lang->line("IL004"); ?></th>
                                    <th><?php echo $this->lang->line("action"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.box -->

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Modals -->
        <div class="modal fade" id="addInventoryLoan" role="dialog">
            <div class="modal-dialog modal-lg box box-warning" style="width:60%">
                <div class="modal-content">
                    <div class="box-header with-border text-center">
                        <h3 class="box-title">
                            <p id="title">New <?php echo $this->lang->line("IL"); ?></p>
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form role="form" id="addform">
                            <div class="form-group col-lg-12">
                                <div class="alert alert-danger col-lg-12 hidden" id="alert-add">
                                    <h4><i class="icon fa fa-ban"></i> Alert</h4>
                                    <p id="message-add">as</p>
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                                <label><?php echo $this->lang->line("IL001"); ?><span style="color:red;">*</span></label>
                                <input type="text" name="start_date" class="form-control datepicker" placeholder="Enter <?php echo $this->lang->line("IL001"); ?>" required value="<?php echo date('Y-m-d'); ?>" readonly>
                            </div>
                            <div class="form-group col-lg-4">
                                <label><?php echo $this->lang->line("IL002"); ?><span style="color:red;">*</span></label>
                                <input type="number" min="1" name="loan_duration" class="form-control" placeholder="Enter" required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label><?php echo $this->lang->line("IL004"); ?><span style="color:red;">*</span></label>
                                <select name="inventory_id" class="form-control" required>
                                    <?php foreach ($get_inventory as $key => $value) : ?>
                                        <option value=<?= $value->id ?>><?= $value->inventory_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div style="float:right;">
                                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                                <button type="button" id="button-add" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("submit"); ?></button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /. create -->

        <!-- Modals Edit Loan-->
        <div class="modal fade" id="editInventoryLoan" role="dialog">
            <div class="modal-dialog modal-lg box box-warning" style="width:60%">
                <div class="modal-content">
                    <div class="box-header with-border text-center">
                        <h3 class="box-title">
                            <p id="title">Edit <?php echo $this->lang->line("IL"); ?></p>
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form role="form" id="editform">

                            <div class="form-group col-lg-12">
                                <div class="alert alert-danger col-lg-12 hidden" id="alert-edit">
                                    <h4><i class="icon fa fa-ban"></i> Alert</h4>
                                    <p id="message-edit">as</p>
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                                <label><?php echo $this->lang->line("IL001"); ?><span style="color:red;">*</span></label>
                                <input type="hidden" name="id" required>
                                <input type="text" name="start_date" class="form-control datepicker" placeholder="Enter <?php echo $this->lang->line("IL001"); ?>" required value="<?php echo date('Y-m-d'); ?>" readonly>
                            </div>
                            <div class="form-group col-lg-4">
                                <label><?php echo $this->lang->line("IL002"); ?><span style="color:red;">*</span></label>
                                <input type="number" min="1" name="loan_duration" class="form-control" placeholder="Enter" required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label><?php echo $this->lang->line("IL004"); ?><span style="color:red;">*</span></label>
                                <select name="inventory_id" class="form-control" required>
                                    <?php foreach ($get_inventory as $key => $value) : ?>
                                        <option value=<?= $value->id ?>><?= $value->inventory_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>



                            <div style="float:right;">
                                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                                <button type="button" id="button-update" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("submit"); ?></button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>

        <div class="modal fade" id="viewInventoryLoan" role="dialog">
            <div class="modal-dialog modal-lg box box-warning" style="width:60%">
                <div class="modal-content">
                    <div class="box-header with-border text-center">
                        <h3 class="box-title">
                            <p id="title">View <?php echo $this->lang->line("IL"); ?></p>
                        </h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form role="form" id="viewform">
                            <div class="form-group col-lg-12">
                                <div class="alert alert-danger col-lg-12 hidden" id="alert-edit">
                                    <h4><i class="icon fa fa-ban"></i> Alert</h4>
                                    <p id="message-edit">as</p>
                                </div>
                            </div>
                            <div class="form-group col-lg-4">
                                <label><?php echo $this->lang->line("PA002"); ?><span style="color:red;">*</span></label>
                                <input type="hidden" name="id" required>
                                <input type="text" name="start_date" class="form-control datepicker" placeholder="Enter <?php echo $this->lang->line("IL001"); ?>" required value="<?php echo date('Y-m-d'); ?>" readonly>
                            </div>
                            <div class="form-group col-lg-4">
                                <label><?php echo $this->lang->line("IL002"); ?><span style="color:red;">*</span></label>
                                <input type="number" disabled min="1" name="loan_duration" class="form-control" placeholder="Enter" required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label><?php echo $this->lang->line("PA004"); ?><span style="color:red;">*</span></label>
                                <select disabled name="inventory_id" class="form-control" required>
                                    <?php foreach ($get_inventory as $key => $value) : ?>
                                        <option value=<?= $value->id ?>><?= $value->inventory_name ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div style="float:right;">
                                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /. view details -->

        <div class="modal fade" id="deleteInventoryLoan" role="dialog">
            <div class="modal-dialog box" style="width:30%;border-top-color:#fc4a1a;">
                <div class="modal-content">
                    <div class="box-header with-border text-center">
                        <h3 class="box-title"><strong><?php echo $this->lang->line("warning"); ?></strong></h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form role="form">
                            <!-- text input -->
                            <div class="form-group text-center">
                                <input type="hidden" name="id">
                                <p><strong><?php echo $this->lang->line("del_confirm"); ?></strong></p>
                            </div>
                            <div class="text-center">
                                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                                <button type="button" id="button-delete" class="btn btn-sm" style="background:#fc4a1a;color:white;"><?php echo $this->lang->line("yes"); ?></button>
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
        <!-- /. view details -->
        <!-- /.modals -->

        <?php $this->load->view('template/footer') ?>

    </div>
    <!-- ./wrapper -->

    <?php $this->load->view('template/localization') ?>
    <?php $this->load->view('template/js') ?>

    <script>
        $(document).ready(function() {
            $('.sidebar-menu').tree()
        })
    </script>
    <script src="<?php echo base_url('assets/js/'); ?>inventory_loan_activity.js"></script>
</body>

</html>