<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | Project Active</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
  <style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
       background-color: #fff;
       opacity: 1;
   }
</style>
</head>
<body class="layout-top-nav skin-white">
    <!-- Site wrapper -->
    <div class="wrapper">

        <?php $this->load->view('template/navbar') ?>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
              <h1><?php echo $this->lang->line("PM001"); ?></h1>
            </section>

          <!-- Main content -->
          <section class="content" >

            <!-- MAIN PAGE -->
            <div class="box box-default" style="border-top-color:#fc4a1a;">
                <div class="box-header with-border">
                    <h3 class="box-title"><?php echo $this->lang->line("PM002"); ?></h3>
                    <div style="margin-top:10px;">
                        <button class="btn btn-sm btn-warning" id="addproject-btn" data-toggle="modal" data-target="#addProject" data-backdrop="static"><i class="fa fa-plus" style="margin-right:3px;"></i><?php echo $this->lang->line("PM003"); ?></button>
                    </div>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table id="table_project" class="table table-bordered table-striped table-responsive" style="table-layout: fixed; word-wrap:break-word;">
                        <thead>
                            <tr>
                                <th data-data='id'><?php echo $this->lang->line("id"); ?></th>
                                <th data-data='name'><?php echo $this->lang->line("PM004"); ?></th>
                                <th data-data='description'><?php echo $this->lang->line("UT003"); ?></th>
                                <th data-data='start_date'><?php echo $this->lang->line("PM005"); ?></th>
                                <th data-data='due_date'><?php echo $this->lang->line("PM006"); ?></th>
                                <th data-data='due_date_revised'><?php echo $this->lang->line("PM007"); ?></th>
                                <th><?php echo $this->lang->line("action"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- ./project name -->
            <div class="row">
                <div class="col-md-6">
                    <div class="box box-default" style="border-top-color:#4abdac;">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->lang->line("PM008"); ?></h3>
                            <div style="margin-top:10px;">
                                <button class="btn btn-sm btn-warning" id="addcategory-btn" data-toggle="modal" data-target="#addCat" data-backdrop="static" ><i class="fa fa-plus" style="margin-right:3px;"></i><?php echo $this->lang->line("PM009"); ?></button>
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <table id="table_category" class="table table-bordered table-striped table-responsive" style="table-layout: fixed; word-wrap:break-word;">
                                <thead>
                                    <tr>
                                        <th data-data='id'><?php echo $this->lang->line("id"); ?></th>
                                        <th data-data='name'><?php echo $this->lang->line("PM010"); ?></th>
                                        <th data-data='role'><?php echo $this->lang->line("UM004"); ?></th>
                                        <th data-data='description'><?php echo $this->lang->line("UT003"); ?></th>
                                        <th><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-default" style="border-top-color:#337e73;">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $this->lang->line("PM011"); ?></h3>
                            <div style="margin-top:10px;">
                                <button class="btn btn-sm btn-warning" data-toggle="modal" id="addstatus-btn"data-target="#addStatus" data-backdrop="static"><i class="fa fa-plus" style="margin-right:3px;"></i><?php echo $this->lang->line("PM012"); ?></button>
                            </div>
                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="box-body">
                            <table id="table_status" class="table table-bordered table-striped table-responsive" style="table-layout: fixed; word-wrap:break-word;">
                                <thead>
                                    <tr>
                                        <th data-data='id'><?php echo $this->lang->line("id"); ?></th>
                                        <th data-data='name'><?php echo $this->lang->line("PM011"); ?></th>
                                        <th data-data='description'><?php echo $this->lang->line("UT003"); ?></th>
                                        <th><?php echo $this->lang->line("action"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- /.Main content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- /.modals -->
    <?php $this->load->view('template/footer') ?>
    <?php $this->load->view('modal/modal_projectmanager') ?>


</div>
<!-- ./wrapper -->

<?php $this->load->view('template/localization') ?>
<?php $this->load->view('template/js') ?>
<script src="<?php echo base_url('assets/js/projectmanager.js'); ?>"></script>
</body>
</html>
