<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | GIT Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />

  <style>
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
     background-color: #fff;
     opacity: 1;
   }
   .bar-chart-legend {
    display: inline-block;
    top: 8px;
    font-size: 9px;
  }
  .bar-chart-legend .legend-item {
    /*display: inline; */
    white-space:nowrap;
    margin-left: 5px;
  }

  .bar-chart-legend .legend-color {
    width: 20px;
    height: 12px;
    margin: 3px 5px;
    display: inline-block;
  }
  svg { overflow: visible !important; }
  .datepicker-dropdown.datepicker-orient-bottom{
    top: 300px !important;
    left: 30px;
    z-index: 10;
    display: block;
  },
</style>


</head>
<body class="layout-top-nav skin-white">
  <!-- Site wrapper -->
  <div class="wrapper">
    <?php $this->load->view('template/navbar') ?>
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <section style="margin-top: 10px;margin-left:10px; margin-bottom:20px;">
      <h2 class="box-title">GIT Dashboard</h2>
    </section>

    <section id="tabs" class="project-tab" style="margin-top: 10px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav>
                          <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Resume</a>
                              <a class="nav-item nav-link" id="nav-project-tab" data-toggle="tab" href="#nav-project" role="tab" aria-controls="nav-project" aria-selected="false">Projects</a>
                          </div>
                      </nav>
                    </div>
                </div>
            </div>
      </section>

  <div class="tab-content" id="nav-tabContent">
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
      <section class="content-header" style="margin-bottom: 100px;">
        <form role="form" id="filterDashboard" style="margin-top: 10px;">
          <div class="form-group col-lg-2">
            <label><?php echo $this->lang->line("PA002"); ?></label>
            <input type="text" name="start_date" id="start_date" class="form-control datepicker" placeholder="<?php echo $this->lang->line('PA002'); ?>" value="<?php echo date('Y-m-01', strtotime(date('Y-m-d'))); ?>" readonly>
          </div>
          <div class="form-group col-lg-2">
            <label>Stop <?php echo $this->lang->line("NTF002"); ?></label>
            <input type="text" name="stop_date" id="stop_date" class="form-control datepicker" placeholder="<?php echo $this->lang->line('TF002'); ?>" value="<?php echo date('Y-m-t', strtotime(date('Y-m-d'))); ?>" readonly>
          </div>
          <div class="form-group col-lg-2">
            <label><?php echo $this->lang->line("UM016"); ?></label>
            <select name="project[]" id="project" class="form-control select2" multiple="multiple" style="width: 100%;" data-placeholder="<?php echo $this->lang->line("UM016"); ?>" selected>
              <?php foreach ($project as $key => $value): ?>
                <option value=<?= $value->project_id ?> ><?= $value->project ?> </option>
              <?php endforeach; ?>
            </select> <input type="checkbox" name="allProject" id="checkboxProject" > <?php echo $this->lang->line("all"); ?> <?php echo $this->lang->line("UM016"); ?>
          </div>
          <div class="form-group col-lg-2">
            <label><?php echo $this->lang->line("NTF008"); ?></label>
            <select name="user[]" id="user" class="form-control select2" multiple="multiple" style="width: 100%;" data-placeholder="<?php echo $this->lang->line("NTF008"); ?>" required>
                <?php foreach ($gitusername as $key => $value): ?>
                  <option value=<?= "'$value'" ?> ><?= $value ?> </option>
                  <?php endforeach; ?>
            </select> <input type="checkbox" name="allUser" id="checkboxUser" > <?php echo $this->lang->line("all"); ?> <?php echo $this->lang->line("NTF008"); ?>
          </div>
            <div class="form-group col-lg-2">
              <label> </label>
              <button type="button" id="search" class="btn btn-primary form-control"><i class="fa fa-search"></i> <span><?php echo $this->lang->line("search"); ?></span></button>
            </div>
          </form>
      </section>

  <!-- Main content -->
  <section class="content" >
        <!-- Level -->
        <div class="row hidden" style="margin: 0px 10px;" id="content-level">
          <div style="margin-top: 80px;">
            <h3 class="box-title"><?php echo $this->lang->line("GI026"); ?></h3>
            <table id="table_level" class="table table-bordered table-striped" style="width:1300px;table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>A</th>
                  <th>B</th>
                  <th>C</th>
                  <!-- <th>SUM</th> -->
                </tr>
              </thead>
              <!-- /.thead -->
              <tbody id="target_level">

              </tbody>
              <!-- /.tbody -->
            </table>
          </div>
          <div class="box" style="border-top-color:#337e73;">
            <div class="box-header with-border text-center">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <span style="text-align: center;"><h3><?php echo $this->lang->line("GI026"); ?></h3></span>
              <div class="chartmix">
                <div id="chart-level-y">
                  <span class="vertical-summary"><?php echo $this->lang->line("GI025"); ?></span>
                </div>
                <div id="chart-level-display" class="chart-responsive">
                  <div id="level_legend" class="bar-chart-legend"></div>
                  <div id="bar-chart-level" ></div>
                </div>
              </div>
              <div class=""style=" margin-top:410px;">
                <span style="text-align: center;"><h4><?php echo $this->lang->line("GI024"); ?></h4></span>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <hr style="border-color:#dfdce3;">
        </div>

        <!-- Label -->
        <div class="row hidden" style="margin: 0px 10px;" id="content-label">
          <div style="margin-top: 80px;">
            <h3 class="box-title"><?php echo $this->lang->line("GI028"); ?></h3>
            <table id="table_label" class="table table-bordered table-striped" style="width:1300px;table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th>Project</th>
                  <th>Sum</th>
                  <!-- <th>SUM</th> -->
                </tr>
              </thead>
              <!-- /.thead -->
              <tbody id="target_label">

              </tbody>
              <!-- /.tbody -->
            </table>
          </div>
          <div class="box" style="border-top-color:#337e73;">
            <div class="box-header with-border text-center">
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <span style="text-align: center;"><h3><?php echo $this->lang->line("GI028"); ?></h3></span>
              <div class="chartmix">
                <div id="chart-label-y">
                  <span class="vertical-summary"><?php echo $this->lang->line("GI025"); ?></span>
                </div>
                <div id="chart-label-display" class="chart-responsive">
                  <div id="label_legend" class="bar-chart-legend"></div>
                  <div id="bar-chart-label" ></div>
                </div>
              </div>
              <span style="text-align: center;"><h4><?php echo $this->lang->line("GI027"); ?></h4></span>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <hr style="border-color:#dfdce3;">
        </div>

        <!-- Kind of Bugs -->
        <div class="row hidden" style="margin: 0px 10px;" id="content-bug">
          <div style="margin-top: 80px;">
            <h3 class="box-title"><?php echo $this->lang->line("GI030"); ?></h3>
            <table id="table_bug" class="table table-bordered table-striped" style="width:1300px;table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Sum</th>
                </tr>
              </thead>
              <!-- /.thead -->
              <tbody id="target_bug">

              </tbody>
              <!-- /.tbody -->
            </table>
          </div>
          <div class="box" style="border-top-color:#337e73;">
            <div class="box-header with-border text-center">

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <span style="text-align: center;"><h3><?php echo $this->lang->line("GI030"); ?></h3></span>
              <div class="chartmix">
                <div id="chart-bug-y" class="">
                  <span class="vertical-summary"><?php echo $this->lang->line("GI025"); ?></span>
                </div>
                <div id="chart-bug-display" class="chart-responsive">
                  <div id="bug_legend" class="bar-chart-legend"></div>
                  <div id="bar-chart-bug" ></div>
                </div>
              </div>
              <div class=""style=" margin-top:480px;">
                <span style="text-align: center;"><h4><?php echo $this->lang->line("GI029"); ?></h4></span>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          <hr style="border-color:#dfdce3;">
        </div>
    </section>
  </div>
</div>
<div class="tab-content" id="nav-tabProject">
  <section class="content" id="project_section">
    <div class="tab-pane fade" id="nav-project" role="tabpanel" aria-labelledby="nav-project-tab">
      <!-- Project -->
      <form role="form" id="filterproject" style="margin-top: 10px;">
        <div class="form-group col-lg-2">
          <label><?php echo $this->lang->line("PA002"); ?></label>
          <input type="text" name="start_date_project" id="start_date_project" onchange="generateDataProject()" class="form-control datepicker" placeholder="<?php echo $this->lang->line('PA002'); ?>" value="<?php echo date('Y-m-01', strtotime(date('Y-m-d'))); ?>" readonly>
        </div>
        <div class="form-group col-lg-2">
          <label>Stop <?php echo $this->lang->line("NTF002"); ?></label>
          <input type="text" name="stop_date_project" id="stop_date_project" onchange="generateDataProject()" class="form-control datepicker" placeholder="<?php echo $this->lang->line('TF002'); ?>" value="<?php echo date('Y-m-t', strtotime(date('Y-m-d'))); ?>" readonly>
        </div>
        <div class="form-group col-lg-2">
          <label><?php echo $this->lang->line("UM016"); ?></label>
          <select name="project_select" id="project_select" class="form-control" onchange="generateDataProject()" style="width: 100%;" data-placeholder="<?php echo $this->lang->line("UM016"); ?>" selected>
            <?php foreach ($project as $key => $value): ?>
              <option value=<?= $value->project_id ?> ><?= $value->project ?> </option>
            <?php endforeach; ?>
          </select>
        </div>
      </form>
      <div class="row hidden" style="margin: 0px 10px;" id="content-project">
        <div style="margin-top: 100px;">
          <table id="table_project" class="table table-bordered table-striped" style="min-width: 2500px;table-layout: fixed; word-wrap:break-word;">
            <thead>
              <tr>
                <th style="width: 50px;">Name</th>
                <?php foreach($labels as $p) {
                  echo '<th>'.$p.'</th>';
                } ?>
                <!-- <th>SUM</th> -->
              </tr>
            </thead>
            <!-- /.thead -->
            <tbody id="target_project">

            </tbody>
            <!-- /.tbody -->
          </table>
        </div>
        <div class="box" style="border-top-color:#337e73;">
          <div class="box-header with-border text-center">
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="box" style="border-top-color:#337e73;">
            <div class="box-header with-border text-center">

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body">
              <span style="text-align: center;"><h3><?php echo $this->lang->line("GI028"); ?></h3></span>
              <div class="chartmix">
                <div id="chart-project-y" class="">
                  <span class="vertical-summary"><?php echo $this->lang->line("GI025"); ?></span>
                </div>
                <div id="chart-project-display" class="chart-responsive">
                  <div id="project_legend" class="bar-chart-legend"></div>
                  <div id="bar-chart-project" ></div>
                </div>
              </div>
              <div class=""style=" margin-top:410px;">
                <span style="text-align: center;"><h4><?php echo $this->lang->line("GI024"); ?></h4></span>
              </div>
            </div>
            <!-- <div class="box-body">
              <div style="text-align: center;margin-top:3px;"class="chart-responsive">
                <div id="project_legend2" class="bar-chart-legend"></div>
                <div id="bar-chart-project" ></div>
              </div>
            </div> -->
            <!-- /.box-body -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
        <hr style="border-color:#dfdce3;">
      </div>
    </div>
  </section>
      <!-- /.Main content -->
  </div>
      <!-- /.content-wrapper -->
      <?php $this->load->view('template/footer') ?>
  </div>
      <!-- ./wrapper -->
      <?php $this->load->view('template/localization') ?>
      <?php $this->load->view('template/js') ?>

      <script src="<?php echo base_url('assets/js/git_dashboard.js'); ?>"></script>
    </body>
    </html>
