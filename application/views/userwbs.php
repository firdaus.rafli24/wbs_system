<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | Project management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_userwbs.css'); ?>" />
  <style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
      background-color: #fffbfb;
      opacity: 1;
    }
  </style>
</head>
<body class="layout-top-nav skin-white">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php $this->load->view('template/navbar') ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1><?php echo $this->lang->line("WB001"); ?></h1>
      </section>

      <!-- Main content -->
      <section class="content" >
        <div class="row">
          <div class="form-group col-lg-2">
            <button class="btn" data-toggle="modal" data-target="#addActivity" data-backdrop="static" id="addactivity-button" style="background:#f7b733;margin-bottom:20px;"><i class="fa fa-plus"></i> <span><?php echo $this->lang->line("WB002"); ?></span></button>
          </div>

          <?php if($user_type == 2) : ?>
            <div class="form-group col-lg-2">
              <div class="d-inline">
                <label class="radio-inline"><input checked="checked" type="radio" name="optradio" value="0">Own Task</label>
                <label class="radio-inline"><input type="radio" name="optradio" value="1">All Task</label>
              </div>
            </div>
          <?php endif; ?>
        </div>

        <!-- MAIN PAGE -->

        <div class="box box-default" style="border-top-color:#fc4a1a;">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo $this->lang->line("WB003"); ?></h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
          </div>
          <div class="box-body">
            <table id="table_log" class="table table-bordered table-striped" style="width:2500px;table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th data-data="id"><?php echo $this->lang->line("WB021"); ?></th>
                  <!-- <th data-data="project"><?php echo $this->lang->line('project')?></th> -->
                  <th data-data="project"><?php echo $this->lang->line("PM004"); ?></th>
                  <th data-data="category"><?php echo $this->lang->line("PM010"); ?></th>
                  <th data-data="task_name"><?php echo $this->lang->line("WB004"); ?></th>
                  <th data-data="pic"><?php echo $this->lang->line("WB005"); ?></th>
                  <th data-data="excecutedby"><?php echo $this->lang->line("WB006"); ?></th>
                  <th data-data="start_time"><?php echo $this->lang->line("WB007"); ?></th>
                  <th data-data="stop_time"><?php echo $this->lang->line("WB008"); ?></th>
                  <th data-data="start_date"><?php echo $this->lang->line("PM005"); ?></th>
                  <th data-data="due_date"><?php echo $this->lang->line("PM006"); ?></th>
                  <th data-data="due_date_revised"><?php echo $this->lang->line("PM007"); ?></th>
                  <th data-data="task_status"><?php echo $this->lang->line("WB009"); ?></th>
                  <th data-data="estimated_hour"><?php echo $this->lang->line("WB010"); ?></th>
                  <th data-data="actual_hour"><?php echo $this->lang->line("WB011"); ?></th>
                  <th data-data="task_percentage"><?php echo $this->lang->line("WB012"); ?></th>
                  <th data-data="notes"><?php echo $this->lang->line("WB013"); ?></th>
                  <th data-data="created_date"><?php echo $this->lang->line("WB020"); ?></th>
                  <th><?php echo $this->lang->line("action"); ?></th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
      </section>
      <!-- /.Main content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- /.modals -->
    <?php $this->load->view('template/footer') ?>
    <?php $this->load->view('modal/modal_userwbs') ?>

  </div>
  <!-- ./wrapper -->

  <!-- MODAL ADD ACTIVITY -->
  <div class="modal fade" id="addActivity" role="dialog">
    <div class="modal-dialog box box-warning" style="width:90%">
      <div class="modal-content">
        <div class="box-header with-border text-center">
          <h3 class="box-title"><strong><?php echo $this->lang->line("WB014"); ?></strong></h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-dismiss="modal" id="closeformadd" data-backdrop="static"><i class="fa fa-times"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div role="form" id="templateSelection">
          <div class="form-group row col-lg-4">
            <label><?php echo $this->lang->line("WB024"); ?></label>
            <select name="select_template" id="select_template" class="form-control" >
              <option value="" selected="">Select <?php echo $this->lang->line("WB026"); ?></option>
               <!-- <?php foreach($get_template_wbs_name as $r) {
                echo '<option value="'.$r->id.'">'.$r->name.'</option>';
              } ?> -->
          </select>
          </div>
          <div class="form-group col-lg-4" style="margin-top: 25px;">
            <button class="btn btn-sm btn-danger" id="delete_template"style="font-weight:700;"><i class="fa fa-trash"></i> <?php echo $this->lang->line("WB023"); ?></button>
          </div>
          <form role="form" id="addtaskform" name="addtaskform" method="post">
            <table id="table_addActivity" class="table table-bordered table-striped" style="width:2000px;table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th><?php echo $this->lang->line("id"); ?></th>
                  <th><?php echo $this->lang->line("PM004"); ?><span style="color:red;">*</span></th>
                  <th><?php echo $this->lang->line("PM010"); ?><span style="color:red;">*</span></th>
                  <th><?php echo $this->lang->line("WB005"); ?><span style="color:red;">*</span></th>
                  <th><?php echo $this->lang->line("WB006"); ?></th>
                  <th><?php echo $this->lang->line("WB004"); ?><span style="color:red;">*</span></th>
                  <th><?php echo $this->lang->line("WB007"); ?></th>
                  <th><?php echo $this->lang->line("WB008"); ?></th>
                  <th><?php echo $this->lang->line("PM005"); ?><span style="color:red;">*</span></th>
                  <th><?php echo $this->lang->line("PM006"); ?><span style="color:red;">*</span></th>
                  <th><?php echo $this->lang->line("PM007"); ?></th>
                  <th><?php echo $this->lang->line("PM011"); ?><span style="color:red;">*</span></th>
                  <th><?php echo $this->lang->line("WB010"); ?><span style="color:red;">*</span></th>
                  <th><?php echo $this->lang->line("WB011"); ?></th>
                  <th><?php echo $this->lang->line("WB012"); ?></th>
                  <th><?php echo $this->lang->line("WB013"); ?></th>
                </tr>
              </thead>

              <tbody id="tableBody">
                <tr id="tableRow">
                  <td></td>
                  <td >
                    <select name="project_add0" id="project_add0" class="form-control">
                     <option value="" selected="" disabled="">Select <?php echo $this->lang->line("PM004"); ?></option>
                     <?php foreach($project as $r) {
                      echo '<option value="'.$r->id.'">'.$r->name.'</option>';
                    } ?>
                  </select>
                </td>
                <td>
                  <select name="category_add0" id="category_add0" class="form-control" >
                   <option value="" selected="" disabled="">Select <?php echo $this->lang->line("PM010"); ?></option>
                   <?php foreach($task_category as $r) {
                    echo '<option value="'.$r->id.'">'.$r->name.'</option>';
                  } ?>
                </select>
              </td>
              <td>
                <div class="form-group">
                  <select name="pic_add0" id="pic_add0" class="form-control select2" >
                    <option value="" selected="" disabled="">Select<?php echo $this->lang->line("WB005"); ?></option>
                   <?php foreach($pic as $r) {
                    echo '<option value="'.$r->id.'">'.$r->username.'</option>';
                  } ?>
                </select>

              </div>
            </td>
            <td>
              <div class="form-group">
                <input type="text" name="excecutedby_add0" id="excecutedby_add0" class="form-control"  value="<?php echo $username;?>" placeholder="" disabled>
              </div>
            </td>
            <td>
              <div class="form-group">
                <input name="task_name_add0" id="task_name_add0" type="text" class="form-control" placeholder="Enter ..."  maxlength="200">
              </div>
            </td>
            <td>
              <div class="input-group">
                <input name="start_time_add0" id="start_time_add0" type="text" class="form-control timepicker">
                <div class="input-group-addon icon-modal"><i class="fa fa-clock-o"></i></div>
              </div>
            </td>
            <td>
              <div class="input-group form-group">
                <input name="stop_time_add0" id="stop_time_add0"type="text" class="form-control timepicker">
                <div class="input-group-addon icon-modal"><i class="fa fa-clock-o"></i></div>
              </div>
            </td>
            <td>
              <div class="input-group date form-group">
                <input type="text" name="start_date_add0" id="start_date_add0" class="form-control pull-right datepicker startdate datepicker_add"  readonly="readonly" value="<?php echo date('Y-m-d'); ?>">
                <div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>
              </div>
            </td>
            <td>
              <div class="input-group date form-group">
                <input type="text" name="due_date_add0" id="due_date_add0" class="form-control pull-right datepicker enddate datepicker_add" readonly="readonly" value="<?php echo date('Y-m-d'); ?>">
                <div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>
              </div>
            </td>
            <td>
              <div class="input-group date form-group">
                <input type="text" name="due_date_revised_add0" id="due_date_revised_add0" class="form-control pull-right datepicker datepicker_add"  readonly="readonly" >
                <div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>
              </div>
            </td>
            <td>
              <div class="form-group">
                <select name="task_status_add0" id="task_status_add0" class="form-control" >
                  <?php foreach($task_status as $r) {
                    echo '<option value="'.$r->id.'">'.$r->name.'</option>';
                  } ?>
                </select>
              </div>
            </td>
            <td>
              <div class="form-group">
                <input name="estimated_hour_add0" id="estimated_hour_add0" type="number" class="form-control" max="12" placeholder="0.00"   min="0.00">
              </div>
            </td>
            <td>
              <div class="form-group">
                <input name="actual_hour_add0" id="actual_hour_add0" value="" type="number" class="form-control" readonly style="color: #787878;">
              </div>
            </td>
            <td>
              <div class="input-group form-group">
                <input name="task_percentage_add0" id="task_percentage_add0" type="number" class="form-control" min="0" max="100">
                <span class="input-group-addon icon-modal">%</span>
              </div>
            </td>
            <td>
              <div class="form-group">
                <textarea name="notes_add0"class="form-control" style="resize: none;height: 34px;" id="notes_add0" rows="1" placeholder="Enter ..." ></textarea>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
      <div style="margin-top:10px;">
        <div style="float:left;">
          <button class="btn btn-sm" id="addNewActivity" style="background:#fbdb99;color:#404040;font-weight:700;"><i class="fa fa-plus"></i> <?php echo $this->lang->line("WB016"); ?></button>
          <button class="btn btn-sm" id="undoActivity" style="background:#fbdb99;color:#404040;font-weight:700;"><i class="fa fa-undo"></i> <?php echo $this->lang->line("WB017"); ?></button>
        </div>
        <div style="float:right;">
          <button class="btn btn-sm btn-info" id="save_template" style="font-weight:700;"><i class="fa fa-bookmark"></i> <?php echo $this->lang->line("WB022"); ?></button>
          <button class="btn btn-sm btn-warning" id="submittask"style="font-weight:700;"><i class="fa fa-floppy-o"></i> <?php echo $this->lang->line("WB015"); ?></button>
        </div>
              <!-- <input type="submit" id='submittask' style="background:#f7b733;color:#404040;font-weight:700;" class="btn btn-sm" name="" value="">
                <label for="submittask">Submit Task</label> -->
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.modal content -->
        </div>
      </div>


      <!-- Modals -->
      <div class="modal fade" id="addTemplate" role="dialog" >
        <div class="modal-dialog modal-lg box box-warning" style="width:50%">
          <div class="modal-content">
            <div class="box-header with-border text-center">
              <h3 class="box-title"><p id="title">New <?php echo $this->lang->line("PR"); ?></p></h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" id="close-add-template" ><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" id="addformtemplate">
                <div class="form-group col-lg-12">
                  <div class="alert alert-danger col-lg-12 hidden" id="alert-add">
                    <h4><i class="icon fa fa-ban"></i> Alert</h4><p id="message-add">as</p>
                  </div>
                </div>
                <div class="form-group col-lg-12">
                  <label><?php echo $this->lang->line("WB025"); ?><span style="color:red;">*</span></label>
                  <input type="text" name="template_name" id="template_name" class="form-control" placeholder="Enter <?php echo $this->lang->line("WB025"); ?>" required>
                </div>

                <div style="float:right;">
                  <button type="button" id="cancel-add-template" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                  <button type="button" id="button-add-template" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("submit"); ?></button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
      <div class="modal fade" id="deleteTemplate" role="dialog" >
          <div class="modal-dialog box box-danger" style="width:30%;">
            <div class="modal-content">
              <div class="box-header with-border text-center">
                <h3 class="box-title"><?php echo $this->lang->line("warning"); ?></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" id="close-delete-template"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                  <div class="form-group text-center">
                    <input type="text" id="id" name="id" value="" hidden>
                    <p><?php echo $this->lang->line("del_confirm"); ?></p>
                  </div>
                  <div class="text-center">
                    <button type="button" class="btn btn-sm" id="cancel-delete-template" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                    <button type="button" id="button-delete-template" class="btn btn-sm btn-danger"><?php echo $this->lang->line("yes"); ?></button>
                  </div>
                </form>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
      </div>

      <?php $this->load->view('template/localization') ?>
      <?php $this->load->view('template/js') ?>
      <script src="<?php echo base_url('assets/js/userwbs.js'); ?>"></script>
    </body>
    </html>
