
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | Role Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
</head>
<body class="layout-top-nav skin-white">
<!-- Site wrapper -->
<div class="wrapper">

    <?php $this->load->view('template/navbar') ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1><?php echo $this->lang->line("UT001"); ?></h1>
    </section>

    <!-- Main content -->
    <section class="content" >

        <!-- MAIN PAGE -->

                <div class="box box-default" style="border-top-color:#fc4a1a;">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $this->lang->line("UM011"); ?></h3>
                        <div style="margin-top:10px;">
                            <button class="btn btn-sm" data-toggle="modal" data-target="#addRole" data-backdrop="static" style="background:#fc4a1a;color:white;"><i class="fa fa-plus" style="margin-right:3px;"></i><?php echo $this->lang->line("UT002"); ?></button>
                        </div>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <table id="table_role" class="table table-bordered table-striped" style="width:1430px;table-layout: fixed; word-wrap:break-word;">
                            <thead>
                                <tr>
                                    <th data-data='id'><?php echo $this->lang->line("id"); ?></th>
                                    <th data-data='name'><?php echo $this->lang->line("UM011"); ?></th>
                                    <th data-data='description'><?php echo $this->lang->line("UT003"); ?></th>
                                    <th><?php echo $this->lang->line("action"); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- <tr>
                                    <td>1</td>
                                    <td>Programmer</td>
                                    <td>kahdgcbfskfhniflshfnvrehgrueheslvgj</td>
                                    <td style="min-width:150px;">
                                        <button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewRole" style="background:#4abdac;margin-right:3px;"><i class="fa fa-eye"></i></button>
                                        <button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editRole" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit"></i></button>
                                        <button class="button_delete btn btn-sm" data-toggle="modal" data-target="#deleteRole" style="background:#fc4a1a;margin-right:3px;"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr> -->
                            </tbody>
                        </table>
                    </div>
                </div>

    </section>
    <!-- /.Main content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- /.modals -->
  <?php $this->load->view('template/footer') ?>
  <?php $this->load->view('modal/modal_usertype') ?>

</div>
<!-- ./wrapper -->

<?php $this->load->view('template/localization') ?>
<?php $this->load->view('template/js') ?>
<script src="<?php echo base_url('assets/js/usertype.js'); ?>"></script>

</body>
</html>
