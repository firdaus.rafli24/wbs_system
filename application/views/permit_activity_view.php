<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | Permit Activity</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
  <style type="text/css">
    .form-control[disabled],
    .form-control[readonly],
    fieldset[disabled] .form-control {
      background-color: #fff;
      opacity: 1;
    }

    .select2-container--default.select2-container--disabled .select2-selection--multiple {
      background-color: #fff;
      cursor: default;
      opacity: 1;
    }
  </style>

</head>

<body class="layout-top-nav skin-white">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php $this->load->view('template/navbar') ?>
    <!-- =============================================== -->
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <!-- <section class="content-header">
      <h1>
        User Management
        <small>(Administrator Only)</small>
      </h1>
    </section> -->

      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        <div class="box">
          <div class="box-header with-border">
            <!-- <h3 class="box-title">List User</h3> -->
            <button class="btn" data-toggle="modal" data-target="#addPermit" data-backdrop="static" style="background:#f7b733;"><i class="fa fa-user-plus"></i> <span><?php echo $this->lang->line("PA001"); ?></span></button>
            <h3 data-data="<?= $permit_quota; ?>">You Have <?= $permit_quota; ?> Permit Quota Remaining</h3>
            <input id="permitQuota" type="hidden" value="<?= $permit_quota; ?>">
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fa fa-minus"></i></button>
              <!-- <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button> -->
            </div>
          </div>

          <!-- DATATABLE -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped" style="width:1300px;table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th data-data="id" class="hidden">#</th>
                  <th data-data="user_id" class="hidden">#</th>
                  <th data-data="start_date"><?php echo $this->lang->line("PA002"); ?></th>
                  <th data-data="long"><?php echo $this->lang->line("PA003"); ?></th>
                  <th data-data="username"><?php echo $this->lang->line("PA000"); ?></th>
                  <th data-data="reason_name"><?php echo $this->lang->line("PA004"); ?></th>
                  <th data-data="to"><?php echo $this->lang->line("PA006"); ?></th>
                  <th data-data="approval"><?php echo $this->lang->line("PA010"); ?></th>
                  <th><?php echo $this->lang->line("action"); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modals -->
    <div class="modal fade" id="addPermit" role="dialog">
      <div class="modal-dialog modal-lg box box-warning" style="width:60%">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title">
              <p id="title">New <?php echo $this->lang->line("PA"); ?></p>
            </h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form role="form" id="addform">
              <div class="form-group col-lg-12">
                <div class="alert alert-danger col-lg-12 hidden" id="alert-add">
                  <h4><i class="icon fa fa-ban"></i> Alert</h4>
                  <p id="message-add">as</p>
                </div>
              </div>
              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("PA002"); ?><span style="color:red;">*</span></label>
                <input type="text" name="start_date" class="form-control datepicker" placeholder="Enter <?php echo $this->lang->line("PA002"); ?>" required value="<?php echo date('Y-m-d'); ?>" readonly>
              </div>
              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("PA003"); ?><span style="color:red;">*</span></label>
                <input type="number" min="1" name="long" class="form-control" placeholder="Enter" required>
              </div>
              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("PA004"); ?><span style="color:red;">*</span></label>
                <select name="reason_id" class="form-control" required>
                  <?php foreach ($get_reason as $key => $value) : ?>
                    <option value=<?= $value->id ?>><?= $value->reason_name ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA005"); ?><span style="color:red;">*</span></label>
                <input type="text" name="subject" class="form-control" placeholder="Enter <?php echo $this->lang->line("PA005"); ?>" required>
              </div>

              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA006"); ?><span style="color:red;">*</span></label>
                <select name="to[]" class="form-control select2" multiple="multiple" style="width: 100%;" data-placeholder="To" required>
                  <?php foreach ($get_user_to as $key => $value) : ?>
                    <option value=<?= $value->email ?>><?= $value->username ?> (<?= $value->email ?>)</option>
                  <?php endforeach; ?>
                </select>
              </div>


              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA008"); ?><span style="color:red;">*</span></label>
                <textarea cols="80" id="addCkbasic" name="description" rows="10" class="form-control" placeholder="Enter <?php echo $this->lang->line("PA008"); ?>" required></textarea>
              </div>

              <div style="float:right;">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                <button type="button" id="button-add" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("submit"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <!-- /. create -->

    <!-- Modals Edit User-->
    <div class="modal fade" id="editPermit" role="dialog">
      <div class="modal-dialog modal-lg box box-warning" style="width:60%">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title">
              <p id="title">Edit <?php echo $this->lang->line("PA"); ?></p>
            </h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form role="form" id="editform">

              <div class="form-group col-lg-12">
                <div class="alert alert-danger col-lg-12 hidden" id="alert-edit">
                  <h4><i class="icon fa fa-ban"></i> Alert</h4>
                  <p id="message-edit">as</p>
                </div>
              </div>

              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("PA002"); ?><span style="color:red;">*</span></label>
                <input type="hidden" name="id" required>
                <input type="text" name="start_date" id="start_date" class="form-control datepicker" placeholder="Enter <?php echo $this->lang->line("PA002"); ?>" required readonly>
              </div>
              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("PA003"); ?><span style="color:red;">*</span></label>
                <input type="number" min="1" name="long" class="form-control" placeholder="Enter" required>
              </div>
              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("PA004"); ?><span style="color:red;">*</span></label>
                <select name="reason_id" class="form-control" required>
                  <?php foreach ($get_reason as $key => $value) : ?>
                    <option value=<?= $value->id ?>><?= $value->reason_name ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA005"); ?><span style="color:red;">*</span></label>
                <input type="text" name="subject" class="form-control" placeholder="Enter <?php echo $this->lang->line("PA005"); ?>" required>
              </div>

              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA006"); ?><span style="color:red;">*</span></label>
                <select name="to[]" id="editSelectTo" class="form-control select2" multiple="multiple" style="width: 100%;" data-placeholder="To" required>
                  <?php foreach ($get_user_to as $key => $value) : ?>
                    <option value=<?= $value->email ?>><?= $value->username ?> (<?= $value->email ?>)</option>
                  <?php endforeach; ?>
                </select>
              </div>


              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA008"); ?><span style="color:red;">*</span></label>
                <textarea cols="80" id="editCkbasic" name="description" rows="10" class="form-control" placeholder="Enter <?php echo $this->lang->line("PA008"); ?>" required></textarea>
              </div>



              <div style="float:right;">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                <button type="button" id="button-update" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("submit"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>

    <div class="modal fade" id="viewPermit" role="dialog">
      <div class="modal-dialog modal-lg box box-warning" style="width:60%">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title">
              <p id="title">View <?php echo $this->lang->line("PA"); ?></p>
            </h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form role="form" id="viewform">
              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("PA002"); ?><span style="color:red;">*</span></label>
                <input type="hidden" name="id" required>
                <input type="text" name="start_date" id="start_date" class="form-control" placeholder="Enter <?php echo $this->lang->line("PA002"); ?>" readonly>
              </div>
              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("PA003"); ?><span style="color:red;">*</span></label>
                <input type="number" min="1" name="long" class="form-control" placeholder="Enter" readonly>
              </div>
              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("PA004"); ?><span style="color:red;">*</span></label>
                <select name="reason_id" class="form-control" disabled>
                  <?php foreach ($get_reason as $key => $value) : ?>
                    <option value=<?= $value->id ?>><?= $value->reason_name ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA005"); ?><span style="color:red;">*</span></label>
                <input type="text" name="subject" class="form-control" placeholder="Enter <?php echo $this->lang->line("PA005"); ?>" readonly>
              </div>

              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA006"); ?><span style="color:red;">*</span></label>
                <select name="to[]" id="viewSelectTo" class="form-control select2" multiple="multiple" style="width: 100%;" data-placeholder="To" required>
                  <?php foreach ($get_user_to as $key => $value) : ?>
                    <option value=<?= $value->email ?>><?= $value->username ?> (<?= $value->email ?>)</option>
                  <?php endforeach; ?>
                </select>
              </div>


              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA008"); ?><span style="color:red;">*</span></label>
                <textarea cols="80" id="viewCkbasic" name="description" rows="10" class="form-control" placeholder="Enter <?php echo $this->lang->line("PA008"); ?>" readonly></textarea>
              </div>
              <div style="float:right;">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <!-- /. view details -->

    <div class="modal fade" id="deletePermit" role="dialog">
      <div class="modal-dialog box" style="width:30%;border-top-color:#fc4a1a;">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><strong><?php echo $this->lang->line("warning"); ?></strong></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form role="form">
              <!-- text input -->
              <div class="form-group text-center">
                <input type="hidden" name="id">
                <p><strong><?php echo $this->lang->line("del_confirm"); ?></strong></p>
              </div>
              <div class="text-center">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                <button type="button" id="button-delete" class="btn btn-sm" style="background:#fc4a1a;color:white;"><?php echo $this->lang->line("yes"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <!-- /. view details -->

    <div class="modal fade" id="emailPermit" role="dialog">
      <div class="modal-dialog box" style="width:30%;border-top-color:#fc4a1a;">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><strong><?php echo $this->lang->line("confirmation"); ?></strong></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form role="form">
              <!-- text input -->
              <div class="form-group text-center">
                <input type="hidden" name="id">
                <p><strong><?php echo $this->lang->line("email_confirm"); ?></strong></p>
              </div>

              <div class="overlay hidden" id="email_loading">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="text-center">
                <button type="button" data-dismiss="modal" class="btn btn-sm btn-email" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                <button type="button" id="button-email" class="btn btn-sm btn-email" style="background:#fc4a1a;color:white;"><?php echo $this->lang->line("yes"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>

    <!-- /. view details -->
    <div class="modal fade" id="approvePermit" role="dialog">
      <div class="modal-dialog box" style="width:30%;border-top-color:#fc4a1a;">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><strong><?php echo $this->lang->line("confirmation"); ?></strong></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form role="form">
              <!-- text input -->
              <div class="form-group text-center">
                <input type="hidden" name="id">
                <p><strong><?php echo $this->lang->line("approve_confirm"); ?></strong></p>
              </div>
              <div class="text-center">
                <button type="button" value="0" class="btn btn-sm btn-approve" style="background:#777;color:beige;"><?php echo $this->lang->line("decline"); ?></button>
                <button type="button" value="1" class="btn btn-sm btn-approve" style="background:#fc4a1a;color:white;"><?php echo $this->lang->line("approve"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <!-- /. view details -->
    <!-- /.modals -->

    <?php $this->load->view('template/footer') ?>

  </div>
  <!-- ./wrapper -->

  <?php $this->load->view('template/localization') ?>
  <?php $this->load->view('template/js') ?>

  <script>
    $(document).ready(function() {
      $('.sidebar-menu').tree()
    })
  </script>

  <script>
    var ck_config = {
      toolbarGroups: [{
          "name": "basicstyles",
          "groups": ["basicstyles"]
        },
        {
          "name": "links",
          "groups": ["links"]
        },
        {
          "name": "paragraph",
          "groups": ["list", "blocks"]
        },
        {
          "name": "document",
          "groups": ["mode"]
        },
        {
          "name": "styles",
          "groups": ["styles"]
        },
        {
          "name": "about",
          "groups": ["about"]
        }
      ],
      removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar',
      removePlugins: 'elementspath',
    };

    CKEDITOR.replace('addCkbasic', ck_config);
    CKEDITOR.replace('editCkbasic', ck_config);
    CKEDITOR.replace('viewCkbasic', ck_config);
  </script>
  <script type="text/javascript">
    var button_approve = "<?= $button_approve; ?>";
    var userId = "<?= $id; ?>";
  </script>
  <script src="<?php echo base_url('assets/js/'); ?>permit_activity.js"></script>
</body>

</html>