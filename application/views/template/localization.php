<script type="text/javascript">
var appLang = {};
<?php
	$app_lang_index = array_search('wbs_lang.php', $this->lang->is_loaded);
	if($app_lang_index) unset($this->lang->is_loaded[$app_lang_index]);
	$lang = $this->lang->load('wbs_lang', '', true);

	foreach ($lang as $key => $value) {
		// A little bit cleansing of string to fit the js
		$value = str_replace("<br/>", '\n', $value);
		$value = str_replace("'", "\'", $value);
		echo "appLang.".$key." = '".$value."';";
	}

	// Load it back
	$this->lang->load('wbs_lang');
?>
</script>


