<style>
  .container {
    width: auto;
    max-width: 100%;
    padding-right: 0;
    padding-left: 0;
    margin-right: 20px;
    margin-left: 20px;
  }
</style>
<header class="main-header">
  <nav class="navbar navbar-static-top">
    <div class="container p-b-45">
      <div class="navbar-header">
        <a href="javascript:void(0);" class="navbar-brand">WBSS</a>
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
          <i class="fa fa-bars"></i>
        </button>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
        <ul class="nav navbar-nav">
          <?php foreach ($this->my_menu as $menu) { ?>
            <li class="<?php echo $menu->is_selected ? 'active ' : ''; ?><?php echo $menu->link != NULL ? "" : "dropdown"; ?>">
              <?php if ($menu->link != NULL) : ?>
                <a href="<?= site_url($menu->link); ?>">
                  <i class="fa fa-fw <?php echo $menu->icon; ?>"></i>
                  <span><?php echo lang($menu->name); ?>
                    <?php if ($menu->id == 6) : ?>
                      <span id="notif" class="badge badge-warning"></span>
                    <?php endif; ?>
                  </span>
                </a>
              <?php else : ?>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-fw <?php echo $menu->icon; ?>"></i>
                  <span><?php echo lang($menu->name); ?>
                    <?php if ($menu->id == 6) : ?>
                      <span id="notif" class="badge badge-warning"></span>
                    <?php endif; ?>
                  </span>
                  <i class="fa fa-fw fa-chevron-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <?php foreach ($menu->submenu as $submenu) { ?>
                    <li class="<?php echo $submenu->is_selected ? 'active' : ''; ?>">
                      <a href="<?= site_url($submenu->link); ?>">
                        <?php echo lang($submenu->name); ?>
                      </a>
                    </li>
                  <?php } ?>
                </ul>
              <?php endif; ?>
            </li>
          <?php } ?>
        </ul>
      </div>

      <div class="navbar-custom-menu">
        <ul class="nav">
          <li class="dropdown user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/img/abstract-user-flat-4.png'); ?>" class="user-image" alt="User Image">
              <span class="hidden-xs" id="username"><?php echo $username; ?></span>&nbsp;&nbsp;&nbsp;<i class="fa fa-fw fa-chevron-down"></i>
            </a>
            <ul class="dropdown-menu">
              <li>
                <a href="<?php echo base_url('index.php/ChangePassword') ?>"><span><?php echo $this->lang->line('SubMenu006'); ?></span></a>
              </li>
              <li>
                <a id="logout-btn" href="javascript:void(0);"><span><?php echo $this->lang->line('out'); ?></span></a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>

<input type="hidden" id="current_user_login_id" value="<?php echo $this->session->userdata('user_id'); ?>">
<input type="hidden" id="current_user_session_id" value="<?php echo $this->session->userdata('session_id'); ?>">
<input type="hidden" id="current_user_type" value="<?php echo $this->session->userdata('user_type'); ?>">