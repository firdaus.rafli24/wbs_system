
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>v1.2.0</b> 
    </div>
    <strong><?php echo $this->lang->line('cr1'); ?> &copy; 2020.</strong><?php echo $this->lang->line('cr2'); ?>
</footer>