<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="<?php echo $this->lang->line('search') ?>">
        <span class="input-group-btn">
          <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
          </button>
        </span>
      </div>
    </form>
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header"><?php echo $this->lang->line('main_nav') ?></li>
      <?php foreach($this->my_menu as $menu){?>
        <li class="<?php echo $menu->is_selected ? 'active menu-open' : '';?> treeview">
          <a href="#">
            <i class="fa <?php echo $menu->icon;?>"></i> 

            <span><?php echo lang($menu->name);?> 
            <?php if ($menu->id == 6) : ?>
              <span id="notif" class="badge badge-warning"></span>
            <?php endif; ?>
          </span> 
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <?php foreach($menu->submenu as $submenu){?>
            <li class="<?php echo $submenu->is_selected ? 'active' : '';?>"><a href="<?= site_url($submenu->link); ?>"><i class="fa fa-circle-o"></i> <?php echo lang($submenu->name);?>
            <?php if ($submenu->link == 'Notification') : ?>
              <span class="pull-right-container">
                <span class="label label-primary pull-right" id="subnotif"></span>
              </span>
            <?php endif; ?>
          </a></li>
        <?php } ?>
      </ul>
    </li>
  <?php } ?>
</ul>
</section>
<!-- /.sidebar -->
</aside>

  <!-- <script>
    $(document).ready(function(){
        /** add active class and stay opened when selected */
        var url = window.location;
        // $('ul a').filter(function() {
        //       return this.href == url;
        // }).parentsUntil(".sidebar-menu").addClass('active');
        // for treeview
        $('ul.active-menu a').filter(function() {
              return this.href == url;
        }).parentsUntil(".sidebar-menu > .active-menu").addClass('active');
    });
  </script> -->
  <script>
    $(function(){
      $('a').each(function(){
        if ($(this).prop('href') == window.location.href) {
                // $(this).addClass('active-page'); 
                $(this).parents('li').addClass('active');
                $(this).parentsUntil('.side-menu').addClass('menu-open');
              }
            });
    });
  </script>
  <style media="screen">
    ul{
      margin-left: 5px;
      list-style:none;
    }
  </style>
