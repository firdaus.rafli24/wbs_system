  <!-- Google Font -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->


  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/font-awesome/css/font-awesome.min.css'); ?>">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/Ionicons/css/ionicons.min.css'); ?>">
  <!-- Morris charts -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/morris.js/morris.css'); ?>">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/plugins/iCheck/all.css'); ?>">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css'); ?>">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css'); ?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/bootstrap-daterangepicker/daterangepicker.css'); ?>">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/plugins/timepicker/bootstrap-timepicker.min.css'); ?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/dist/css/AdminLTE.css'); ?>">
  <!-- Sweet Alert 2 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/sweetalert2/dist/sweetalert2.min.css'); ?>">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/bower_components/select2/dist/css/select2.min.css'); ?>">

  <link rel="stylesheet" href="<?php echo base_url('assets/vendors/template/dist/css/skins/_all-skins.css'); ?>">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">