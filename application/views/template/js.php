<script type="text/javascript">
  var base_url = "<?php echo base_url(); ?>";
  var ConfValue = {};
  var myConfig = {};

  <?php
  $config = $this->config->config;
  foreach ($config as $key => $value) {
    if (is_array($value) == true) {
      $value = str_replace('\\\\','\\\\\\\\',$value);
      echo "ConfValue.".$key." = ".json_encode($value,true).";";
    }else{
      $value = str_replace('\\\\','\\\\\\\\',$value);
      echo "ConfValue.".$key." = '".$value."';";
    }
  }

  foreach ($this->my_config as $key => $value) {
    $json = json_encode(['enable' => $value->config_enable, 'value' => $value->config_value]);
    echo "myConfig.".$value->config_key." = ".$json;
  }

  ?>


</script>

<!-- jQuery 3 -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap 3.3.7 -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- Morris.js charts -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/raphael/raphael.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/morris.js/morris.min.js'); ?>"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/moment/min/moment.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/bootstrap-daterangepicker/daterangepicker.js'); ?>"></script>
<!-- bootstrap datepicker -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js'); ?>"></script>
<!-- bootstrap time picker -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/plugins/timepicker/bootstrap-timepicker.min.js'); ?>"></script>
<!-- DataTables -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<!-- SlimScroll -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
<!-- FastClick -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/fastclick/lib/fastclick.js'); ?>"></script>
<!-- InputMask -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/plugins/input-mask/jquery.inputmask.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/plugins/input-mask/jquery.inputmask.date.extensions.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/plugins/input-mask/jquery.inputmask.extensions.js'); ?>"></script>
<!-- Sweet Alert 2 -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/sweetalert2/dist/sweetalert2.all.min.js'); ?>"></script>
<!-- Select2 -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/select2/dist/js/select2.full.min.js'); ?>"></script>
<!-- CK Editor -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/bower_components/ckeditor/ckeditor.js'); ?>"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/dist/js/adminlte.min.js'); ?>"></script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript" src="<?php echo base_url('assets/vendors/template/dist/js/demo.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/jquery-validation/jquery.validate.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/vendors/socket.io/socket.io.js');?>"></script>
<script src="<?php echo base_url('assets/js/wbs.js');?>"></script>
<script type="text/javascript">
  $('#logout-btn').click(function(){
    $.ajax({
      url: base_url+"index.php/Auth/logout",
      type: 'POST',
      datatype:"json",
      data: {},
      success: function (result) {
        window.location.href=base_url+"index.php/Auth";
      }
    })
  });


  $('.select').focus(function(){
    $(this).css("color", "#000");
    if ($(this).val()!=""){
      $(this).valid();
    }
  });


</script>
