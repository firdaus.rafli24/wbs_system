<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | Notification</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
  <style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
     background-color: #fff; 
     opacity: 1;
   }
   
   table#notificationTable.dataTable tbody tr:hover {
    background-color: #d8d8d8;
    cursor: pointer;
  }
  .new {
    background-color: #d8d8d8;
  }
</style>
</head>
<body class="layout-top-nav skin-white">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php $this->load->view('template/navbar') ?>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <h1>
        User Management
        <small>(Administrator Only)</small>
      </h1>
    </section> -->

    <!-- Main content -->
    <section class="content" >

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border" style="height: 50px;">
          
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
            title="Collapse">
            <i class="fa fa-minus"></i></button>
            <!-- <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button> -->
            </div>
          </div>

          <!-- DATATABLE -->
          <div class="box-body">
            <table id="notificationTable" class="table table-responsive hover" style="width:100%;table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th data-data="id" class="hidden">#</th>
                  <th data-data="date"><?php echo $this->lang->line("NTF002"); ?></th>
                  <th data-data="title"><?php echo $this->lang->line("NTF005"); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <!-- /.box-body -->

        </div>
        <!-- /.box -->

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Modals -->

    <!-- view notification -->
    <div class="modal fade" id="viewNotification" role="dialog" >
      <div class="modal-dialog modal-lg box box-warning" style="width:50%">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><p id="title"><?php echo $this->lang->line("NTF012"); ?></p></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <form role="form" id="viewform">
              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("NTF002"); ?></label>
                <input type="text" name="date" id="date_view" class="form-control datepicker" placeholder="Enter <?php echo $this->lang->line("NTF002"); ?>" required disabled>
              </div>

              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("NTF003"); ?></label>
                <select type="text" name="type_notification" id="type_notification_view" class="form-control" required disabled>
                  <option value="" selected><?php echo $this->lang->line("NTF009"); ?></option>
                  <option value="1"><?php echo $this->lang->line("NTF007"); ?></option>
                  <option value="2"><?php echo $this->lang->line("NTF008"); ?></option>
                </select>
              </div>

              <div class="form-group col-lg-4">
                <label><?php echo $this->lang->line("NTF004"); ?></label>
                <input type="text" name="category" id="category_view" class="form-control" placeholder="Enter <?php echo $this->lang->line("NTF004"); ?>" required disabled>
              </div>

              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("NTF005"); ?></label>
                <input type="text" name="title" id="title_view" class="form-control" placeholder="Enter <?php echo $this->lang->line("NTF005"); ?>" required disabled>
              </div>

              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PA008"); ?></label>
                <textarea cols="80" id="viewCkbasic" name="description" rows="10" class="form-control" placeholder="Enter <?php echo $this->lang->line("PA008"); ?>" required disabled></textarea>
              </div>

              <div style="float:right;">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <!-- /.modals -->

    <?php $this->load->view('template/footer') ?>

  </div>
  <!-- ./wrapper -->

  <?php $this->load->view('template/localization') ?>
  <?php $this->load->view('template/js') ?>
  <script>
   var ck_config = {
    toolbarGroups: [
    {"name": "basicstyles","groups": ["basicstyles"]},
    {"name": "links","groups": ["links"]},
    {"name": "paragraph","groups": ["list", "blocks"]},
    {"name": "document","groups": ["mode"]},
    {"name": "styles","groups": ["styles"]},
    {"name": "about","groups": ["about"]}
    ],
    removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar',
    removePlugins :'elementspath',
  };

  CKEDITOR.replace('viewCkbasic', ck_config);

</script>
<script src="<?php echo base_url('assets/js/'); ?>notification_user.js"></script>
</body>
</html>
