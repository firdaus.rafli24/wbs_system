<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>WBS | Permit Quota Management</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <?php $this->load->view('template/style') ?>
  <!-- CUSTOM CSS UM -->
  <link rel="stylesheet" href="<?php echo base_url('assets/css/custom_um.css'); ?>" />
  <style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
     background-color: #fff; 
     opacity: 1;
   }
   .reset{
       display: flex;
       justify-content: center;
       align-items: center;
   }
 </style>
</head>
<body class="layout-top-nav skin-white">
  <!-- Site wrapper -->
  <div class="wrapper">

    <?php $this->load->view('template/navbar') ?>

    <!-- =============================================== -->
    <div class="content-wrapper">
      <section class="content-header">
        <h1>
            <?php echo $this->lang->line("PQM") ?>
        </h1>
      </section>

      <section class="content" >
        <div class="box">
          <div class="box-header with-border">
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
              title="Collapse">
              <i class="fa fa-minus"></i></button>
            </div>
          </div>

          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped table-responsive" style="table-layout: fixed; word-wrap:break-word;">
              <thead>
                <tr>
                  <th data-data="id" class="hidden">#</th>
                  <th data-data="username"><?php echo $this->lang->line("PQM000"); ?></th>
                  <th data-data="permit_quota"><?php echo $this->lang->line("PQM001"); ?></th>
                  <th><?php echo $this->lang->line("action"); ?></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <div class="reset">
            <button class="btn btn-sm" id="btn_reset" type="button" style="background:#32C1CD;"><i style="padding-right:5px;"class="fa fa-refresh" aria-hidden="true"></i>Reset All</button>
        </div>
        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->
    <!-- Modals Edit User-->
    <div class="modal fade" id="editPermitQuota" role="dialog" >
      <div class="modal-dialog box box-warning" style="width:50%">
        <div class="modal-content">
          <div class="box-header with-border text-center">
            <h3 class="box-title"><p id="titleedit"></p></h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-dismiss="modal"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <form role="form" id="editform">
              <div hidden>
                <input class="id" type="text" name="id" class="form-control">
                <input type="text" name="user_type" class="form-control">
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PQM000"); ?><span style="color:red;">*</span></label>
                <input type="text" name="usernameEdit" class="form-control" readonly>
              </div>
              <div class="form-group col-lg-12">
                <label><?php echo $this->lang->line("PQM001"); ?><span style="color:red;">*</span></label>
                <input type="number" min="1" max="12" name="permitQuotaEdit" class="form-control" placeholder="Enter" required>
              </div>
              <div style="float:right;">
                <button type="button" data-dismiss="modal" class="btn btn-sm" style="background:#777;color:beige;"><?php echo $this->lang->line("cancel"); ?></button>
                <button type="button" id="button-update" class="btn btn-sm" style="background:#f7b733;"><?php echo $this->lang->line("save"); ?></button>
              </div>
            </form>
          </div>
          <!-- /.box-body -->
        </div>
      </div>
    </div>
    <!-- /.modals -->

    <?php $this->load->view('template/footer') ?>

  </div>
  <!-- ./wrapper -->

  <?php $this->load->view('template/localization') ?>
  <?php $this->load->view('template/js') ?>
  <script>
    function editmode() {
      document.getElementById("titleedit").innerHTML = "<strong>Update Permit Quota</strong>";
    };
    $(document).ready(function () {
      $('.sidebar-menu').tree();
    });
    
  </script>
  <script src="<?php echo base_url('assets/js/permit_quota_management.js'); ?>"></script>
</body>
</html>
