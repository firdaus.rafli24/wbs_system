<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/BaseController.php");

class Auth extends CI_Controller
{
	function __construct()
	{
		$this->auth_required = false;
		//manually assign path for controlelrs in root
		$this->path_controller = "Auth";
		$this->full_path = "Auth";

		parent::__construct();

		//load models
		$this->load->model("Ion_auth_model");
		$this->load->library('ion_auth');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
	}

	public function index()
	{
		if (!$this->ion_auth->logged_in()) {
			$this->load->view('login');
		} else {
			redirect("Home");
		}
	}

	public function login()
	{
		$response = get_ajax_response();
		$email = $this->input->post("email");
		$password = $this->input->post("password");
		$remember = (bool)$this->input->post("remember");

		$this->form_validation->set_rules('email', 'email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'password', 'required');

		if ($this->form_validation->run() === TRUE) {
			if ($this->ion_auth->login($email, $password, $remember)) {
				$session = $this->ion_auth->user()->row();

				$this->session->set_userdata([
					'user_type' => $session->user_type,
					'username' => $session->username
				]);

				$response->status = true;
				$response->user_id = $session->user_id;
				$response->user_type = $session->user_type;
				$response->username = $session->username;
				$response->email = $session->email;


				$response->message = $this->lang->line("success");
			} else {
				$response->status = false;
				$response->message = $this->lang->line("form_login_failed");
				$response->data = $email;
			}
		} else {
			$response->message = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
		}
		header("Content-Type: application/json");
		echo json_encode($response);
	}

	public function logout()
	{
		$response = get_ajax_response();
		$response->status = true;
		$response->message = $this->lang->line("success");
		// initialize_log($response);
		// Logout
		$this->ion_auth->logout();
		redirect("Auth");
		json_response($response);;
	}
}
