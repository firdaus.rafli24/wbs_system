<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Usermanagement extends BaseController {
  function __construct()
  {
    $this->auth_required = false;

    //manually assign path for controlelrs in root
    $this->path_controller = "UserManagement";
    $this->full_path = "UserManagement";

    parent::__construct();
    //load models
    // $this->load->model("Ion_auth_model");
    $this->load->model("user_mng_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');    
  }
  
  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
     $this->load->view('login');
   }
   else
   {
    $role= $this->user_mng_model->get_role();
    $user = $this->ion_auth->user()->row();
    $data = array(
     "username" => $user->username,
     "role" => $role
   );
    $this->load->view('usermanagement',$data);
  }
}

public function datatable(){
  $this->load->model('user_mng_model');

  $start    = $this->input->post('start');
  $length   = $this->input->post('length');
  $search   = $this->input->post('search[value]');
  $pageData = $this->user_mng_model->datatable($search, $length, $start);
  $allData  = $this->user_mng_model->datatable($search);

  $data = array(
   'draw'              =>  $this->input->post('draw'),
   'recordsTotal'      =>  count($allData),
   'recordsFiltered'   =>  count($pageData),
   'data' => $pageData
 );
  header('Content-Type: application/json');
  echo json_encode($data);
}
public function get()
{
  $user_id = $this->input->post("user_id");

  $this->load->model('user_mng_model');
  $get_data = $this->user_mng_model->view($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["id"]=$get_data;
   $data["message"]=$this->lang->line("get_invalid_id");
   $data["data"]=array();

 }else{
   $getAllData = $this->user_mng_model->view($user_id);
   $data["data"] = $get_data;
   $data["status"] = true;
   $data["message"]=$this->lang->line("success");
   $data["data"]=$getAllData[0];
 }

 header("Content-Type: application/json");
 echo json_encode($data);

}

public function chooseemployment($employment)
{
  if ($employment==0) {
    $therole='Reguler';
  }else {
    $therole='Freelance';
  }
  return $therole;
}

public function do_update()
{
  $response=get_ajax_response();
  $user_id = $this->input->post("user_id");

  $this->load->model('user_mng_model');
  $get_data = $this->user_mng_model->view($user_id);
  $password=$this->input->post("password");

  $getAllData= array( 'email' => $this->input->post("email"),
   'username' => $this->input->post("username"),
   'phone_number' => $this->input->post('phone_number'),
   'wa_number' => $this->input->post('wa_number'),
   'employment_status' => $this->input->post('employment_status'),
   'user_type' => $this->usertype($this->input->post('role'))
 );
  if ($password) {
    $getAllData['password']=$this->ion_auth_model->hash_password($password);
  }

  $this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[50]|edit_unique[users.email.'.$this->input->post('user_id').']',
    array('required' => 'You have not provided %s.')
  );
  $this->form_validation->set_rules('password', 'password', 'min_length[8]|max_length[22]');
  $this->form_validation->set_rules('username', 'username', 'required|max_length[100]');
  $this->form_validation->set_rules('phone_number', 'phone_number', 'required|min_length[9]|max_length[14]|numeric');
  $this->form_validation->set_rules('wa_number', 'wa_number', 'required|min_length[9]|max_length[14]|numeric');
  if(empty($get_data)){
    $data["status"] = false;
    $data["message"]=$this->lang->line("edit_invalid_id");
    $data["id"]=$user_id;
    $data["data"]=array();

    header("Content-Type: application/json");
    echo json_encode($data);
  }else{
    if ($this->form_validation->run()==TRUE) {
      $update = $this->user_mng_model->update($user_id,$getAllData);
      $data["status"] = true;
      $data["message"]=$this->lang->line("success");
      $data["data"]=$getAllData;

      header("Content-Type: application/json");
      echo json_encode($data);
    } else {
      $data["status"] = false;
      $data['message']= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
      header("Content-Type: application/json");
      echo json_encode($data);
    }
  }
}


public function do_save()
{
  $config['upload_path']="./assets/img/digital_signature";
  $config['allowed_types']='gif|jpg|png';
  $config['encrypt_name'] = TRUE;
  $this->load->library('upload',$config);

  $response = get_ajax_response();
  $this->load->model('user_mng_model');
  if($this->upload->do_upload('digital_signature')){

    $fileData = array('upload_data' => $this->upload->data());
    $fileName = $fileData['upload_data']['file_name'];    
    $password = $this->input->post("password");
    $getAllData= array( 
      'email' => $this->input->post("email"),
      'username' => $this->input->post("username"),
      'password' => $this->ion_auth_model->hash_password($password),
      'phone_number' => $this->input->post('phone_number'),
      'wa_number' => $this->input->post('wa_number'),
      'employment_status' => $this->input->post('employment_status'),
      'user_type' => $this->usertype($this->input->post('role')),
      'digital_signature' => $fileName,
      'active' => 1
    );
    $this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[users.email]|max_length[50]',
      array('required'      => 'You have not provided %s.',
        'is_unique'     => 'This %s already exists.')
    );
    $this->form_validation->set_rules('password', 'password', 'required|min_length[8]|max_length[22]|alpha_numeric');
    $this->form_validation->set_rules('username', 'username', 'required|max_length[100]');
    $this->form_validation->set_rules('phone_number', 'phone_number', 'required|min_length[9]|max_length[14]|numeric');
    $this->form_validation->set_rules('wa_number', 'wa_number', 'required|min_length[9]|max_length[14]|numeric');
  
  
    if ($this->form_validation->run()==TRUE) {
      $save = $this->user_mng_model->add($getAllData);
      $data["status"] = true;
      $data["message"]=$this->lang->line("success");
      $data["data"]=$getAllData;
    }else {
      $data["status"] = false;
      $data['message']= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
    }	    
  }else{
    $data["status"] = false;
    $data['message']= "Failed to Upload File";    
  }
  header("Content-Type: application/json");
  echo json_encode($data);
}
public function do_delete()
{
  $user_id = $this->input->post("user_id");

  $this->load->model('user_mng_model');
  $get_data = $this->user_mng_model->view($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["message"]=$this->lang->line('delete_invalid_id');
   $data["id"]=$user_id;
   $data["data"]=array();
 }else{
   $delete = $this->user_mng_model->delete($user_id);
   $data = array("user_id"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$delete;
 }
 header("Content-Type: application/json");
 echo json_encode($data);
}

public function add(){
  $response = get_ajax_response();
  $role= $this->user_mng_model->get_role();
  if(empty($role)){
    $response->status = false;
    $response->code = 400;
  }else {
    $response->status = true;
    $response->code = 200;
    $response->data = $role;
  }

  header("Content-Type: application/json");
  echo json_encode($response);
}



private function to_get_role_name($id){
  $role_name = $this->user_mng_model->get_role_name($id);
  foreach($role_name as $r) {
    $name= $r->name;
    return $name;
  }
}
private function usertype($role){
  if ($role == 2) {
    return 2;
  } else {
    return $role;
  }
}
}
