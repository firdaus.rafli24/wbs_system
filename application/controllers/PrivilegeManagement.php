<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/BaseController.php");

class PrivilegeManagement extends BaseController
{
  function __construct()
  {
    $this->auth_required = false;

    $this->path_controller = "UserManagement";
    $this->full_path = "UserManagement";

    parent::__construct();
    $this->load->model("user_mng_model");
    $this->load->model("privilege_management_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }

  public function index()
  {
    if (!$this->ion_auth->logged_in()) {
      $this->load->view('login');
    } else {
      $user = $this->ion_auth->user()->row();

      $role = $this->user_mng_model->all_role();
      $userData = $this->user_mng_model->all_user();

      $data = array(
        "username" => $user->username,
        "role" => $role,
        "user" => $userData
      );

      $this->load->view('privilege_management_view', $data);
    }
  }


  public function datatable()
  {
    header('Content-Type: application/json');
    $search   = $this->input->post('search[value]');
    $sort_col   = $this->input->post('order[0][column]');
    $sort_dir   = $this->input->post('order[0][dir]');

    if ($sort_col == 6) {
      $sort_col = 'm.id';
    } else if ($sort_col == 7) {
      $sort_col = 'sm.id';
    } else {
      $sort_col = 'm.id, sm.id';
    }

    $final = $this->privilege_management_model->datatable($search, $sort_col, $sort_dir);

    $data = array(
      'draw'              =>  $this->input->post('draw'),
      'recordsTotal'      =>  count($final),
      'recordsFiltered'   =>  count($final),
      'data' => $final
    );

    echo json_encode($data);
  }


  public function do_save()
  {
    $post = $this->input->post();

    if (isset($post['role'])) {
      foreach ($post['role'] as $key => $value) {
        $dataGroup[] = [
          'menu_id' => $post['menu_id'],
          'submenu_id' => ($post['submenu_id'] == '') ? NULL : $post['submenu_id'],
          'group_id' => $value,
          'create' => 1,
          'update' => 1,
          'delete' => 1,
          'read' => 1,
          'upload' => 1,
          'download' => 1,
        ];
      }
      $this->privilege_management_model->delete_batch_group(explode(', ', $post['pg_id']));
      $this->privilege_management_model->insert_batch_group($dataGroup);
    } else {
      $this->privilege_management_model->delete_batch_group(explode(', ', $post['pg_id']));
    }

    if (isset($post['user'])) {
      foreach ($post['user'] as $key => $value) {
        $dataUser[] = [
          'menu_id' => $post['menu_id'],
          'submenu_id' => ($post['submenu_id'] == '') ? NULL : $post['submenu_id'],
          'user_id' => $value,
          'create' => 1,
          'update' => 1,
          'delete' => 1,
          'read' => 1,
          'upload' => 1,
          'download' => 1,
        ];
      }
      $this->privilege_management_model->delete_batch_user(explode(', ', $post['pu_id']));
      $this->privilege_management_model->insert_batch_user($dataUser);
    } else {
      $this->privilege_management_model->delete_batch_user(explode(', ', $post['pu_id']));
    }

    $data["status"] = true;
    $data["message"] = $this->lang->line("success");
    header("Content-Type: application/json");
    echo json_encode($data);
  }
}
