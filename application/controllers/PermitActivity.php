<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/BaseController.php");

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require APPPATH . 'third_party/phpmailer/phpmailer/src/Exception.php';
require APPPATH . 'third_party/phpmailer/phpmailer/src/PHPMailer.php';
require APPPATH . 'third_party/phpmailer/phpmailer/src/SMTP.php';


class PermitActivity extends BaseController
{

  const MAIL_SEND       = "infinitec.indonesia@gmail.com"; // ISI EMAIL GMAIL
  const MAIL_PASSWORD   = "yonegyu1@"; // PASSWORD EMAIL GMAIL


  function __construct()
  {
    $this->auth_required = false;

    $this->path_controller = "PermitActivity";
    $this->full_path = "PermitActivity";

    parent::__construct();
    $this->load->model("user_type_model");
    $this->load->model('user_mng_model');
    $this->load->model('permit_activity_model');
    $this->load->model('notification_model');

    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }


  public function index()
  {
    if (!$this->ion_auth->logged_in()) {
      $this->load->view('login');
    } else {
      $user = $this->ion_auth->user()->row();
      $data['username']   = $user->username;
      $data['permit_quota'] = $user->permit_quota;
      $data['user_type']   = $user->user_type;
      $data['id'] = $user->id;
      $data['get_reason'] = $this->permit_activity_model->get_reason();
      $data['get_user_to'] = $this->permit_activity_model->get_user_to();
      // $data['get_user_cc'] = $this->permit_activity_model->get_user_cc();

      $data['button_approve'] = ($user->user_type == 2) ? 'visible' : 'hidden';

      $this->load->view('permit_activity_view', $data);
    }
  }

  public function datatable()
  {
    $start    = $this->input->post('start');
    $length   = $this->input->post('length');
    $search   = $this->input->post('search[value]');
    $pageData = $this->permit_activity_model->datatable($search, $length, $start);
    $allData  = $this->permit_activity_model->datatable($search);
    $data = array(
      'draw'              =>  $this->input->post('draw'),
      'recordsTotal'      =>  count($allData),
      'recordsFiltered'   =>  count($pageData),
      'data' => $pageData
    );

    header('Content-Type: application/json');
    echo json_encode($data);
  }

  public function do_save()
  {
    $response = get_ajax_response();

    $user_id = $this->session->userdata('user_id');


    $getAllData =  $this->input->post();
    $getAllData['to'] = implode(', ',  $this->input->post('to'));

    $getAllData   = array_merge($getAllData, ['user_id' => $user_id]);
    $permitLongValidation = $getAllData['long'];

    $permit_quota = $this->user_mng_model->getPermitQuota($user_id);

    if ($permit_quota > 0) {
      if($permit_quota - $permitLongValidation >= 0){
        $this->form_validation->set_rules('subject', $this->lang->line('PA005'), 'max_length[100]');
  
        if ($this->form_validation->run() == TRUE && $permit_quota > 0) {
  
          $save = $this->permit_activity_model->add($getAllData);
          $insert_id = $this->db->insert_id();
          $data["status"] = true;
          $data["message"] = $this->lang->line('success');
        } else {
          $data["status"] = false;
          $data["message"] = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));;
          $data["data"] = $getAllData;
        }
      }else{
        $data["status"] = false;
        $data["message"] = $this->lang->line('Permit_Fail2');
        $data["data"] = $getAllData;      
      }
    }else{
      $data["status"] = false;
      $data["message"] = $this->lang->line('Permit_Fail');
      $data["data"] = $getAllData;      
    }
    header("Content-Type: application/json");
    echo json_encode($data);
  }
  public function get()
  {
    $id = $this->input->post("id");

    $get_data = $this->permit_activity_model->show($id);

    if (empty($get_data)) {
      $data["status"] = false;
      $data["id"] = $get_data;
      $data["message"] = $this->lang->line('get_invalid_id');
      $data["data"] = array();

      header("Content-Type: application/json");
      echo json_encode($data);
    } else {
      $data = array("data" => $get_data);
      $data["status"] = true;
      $data["message"] = $this->lang->line('success');
      $data["data"] = $get_data;

      header("Content-Type: application/json");
      echo json_encode($data);
    }
  }
  public function do_update()
  {
    $getAllData =  $this->input->post();
    $response = get_ajax_response();
    $id = $this->input->post("id");

    $get_data = $this->permit_activity_model->show($id);
    $userId = $get_data->user_id;


    $permit_quota = $this->user_mng_model->getPermitQuota($userId);

    $permitLongValidation = $getAllData['long'];

    if($permit_quota > 0){
      if($permit_quota - $permitLongValidation >= 0){
        $this->form_validation->set_rules('subject', $this->lang->line('PA005'), 'max_length[100]');

        if (empty($get_data)) {
          $data["status"] = false;
          $data["message"] = $this->lang->line('edit_invalid_id');
          $data["id"] = $id;
          $data["data"] = array();
        } else {
          if ($this->form_validation->run() == TRUE) {
    
            $getAllData['to'] = implode(', ',  $this->input->post('to'));
    
            $update = $this->permit_activity_model->update($id, $getAllData);
            $data["status"] = true;
            $data["message"] = $this->lang->line('success');
            $data["data"] = $getAllData;
            // $this->send_email($id, $get_data);
          } else {
            $data["status"] = false;
            $data["message"] = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));;
            $data["data"] = $getAllData;
          }
        }
      }else{
        $data["status"] = false;
        $data["message"] = $this->lang->line('Permit_Fail');
        $data["data"] = $getAllData;             
      }
    }else{
      $data["status"] = false;
      $data["message"] = $this->lang->line('Permit_Fail');
      $data["data"] = $getAllData;           
    }
   
    header("Content-Type: application/json");
    echo json_encode($data);
  }

  public function do_delete()
  {
    $id = $this->input->post("id");

    $get_data = $this->permit_activity_model->show($id);
    $delete = $this->permit_activity_model->delete($id);

    if (!$delete) {
      $data["status"] = false;
      $data["message"] = $this->lang->line('delete_invalid_id');
      $data["id"] = $id;
      $data["data"] = array();

      header("Content-Type: application/json");
      echo json_encode($data);
    } else {
      $data["status"] = true;
      $data["message"] = $this->lang->line('success');
      $data["id"] = $id;
      $data["data"] = $get_data;

      header("Content-Type: application/json");
      echo json_encode($data);
    }
  }

  public function do_approve()
  {
    $id = $this->input->post("id");
    $get_data = $this->permit_activity_model->show($id);
    $long = $get_data->long;
    $user = $this->ion_auth->user()->row();
    $permit_quota = $user->permit_quota;

    if (empty($get_data) || $user->user_type != 2) {
      $data["status"] = false;
      $data["message"] = $this->lang->line('edit_invalid_id');
      $data["id"] = $id;
      $data["data"] = array();

      header("Content-Type: application/json");
      echo json_encode($data);
    } else {

      $approval_status  = $this->input->post('approval');
      $approval_name    = ($approval_status == 1) ? $this->lang->line("approve") : $this->lang->line("decline");

      $this->permit_activity_model->update($id, ['approval' => $approval_status]);
      if ($approval_status == 1) {
        $update_quota = $permit_quota - $long;
        $data = array(
          'permit_quota' => $update_quota
        );
        $this->user_mng_model->updatePermitQuota($get_data->user_id, $data);
      }
      $data["status"] = true;
      $data["message"] = $this->lang->line('success');
      header("Content-Type: application/json");
      echo json_encode($data);
    }
  }

  public function download_pdf()
  {
    $id = $this->input->get("id");
    if (!is_numeric($id)) return false;
    $data = $this->permit_activity_model->show($id);
    $approval_name    = ($data->approval == 1) ? $this->lang->line("approve") : $this->lang->line("Pending");

    $this->load->library('fpdf182/fpdf');
    $pdf = new FPDF('P', 'mm', 'A4');
    $pdf->AliasNbPages();
    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetAutoPageBreak(false);
    $pdf->SetTopMargin(6);
    $pdf->SetLeftMargin(10);
    $pdf->AddPage();

    $pdf->SetFont('Arial', 'B', 15);
    $pdf->Cell(190, 15, 'Pengajuan Cuti', 0, 1, 'C');
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 11);
    $pdf->Cell(40, 5, 'Nama', 0, 0, 'L', 0);
    $pdf->SetFont('Arial', 'B', 11);
    $pdf->Cell(30, 5, ': ' . $data->username, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 11);
    $pdf->Cell(40, 5, "Tanggal", 0, 0, 'L', 0);
    $pdf->SetFont('Arial', 'B', 11);
    $pdf->Cell(30, 5, ': ' . $data->start_date, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 11);
    $pdf->Cell(40, 5, "Lama", 0, 0, 'L', 0);
    $pdf->SetFont('Arial', 'B', 11);
    $pdf->Cell(30, 5, ': ' . $data->long, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 11);
    $pdf->Cell(40, 5, "Alasan", 0, 0, 'L', 0);
    $pdf->SetFont('Arial', 'B', 11);
    $pdf->Cell(30, 5, ': ' . $data->reason, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 11);
    $pdf->Cell(40, 5, 'Status Pengajuan', 0, 0, 'L', 0);
    $pdf->SetFont('Arial', 'B', 11);
    $pdf->Cell(30, 5, ': ' . $approval_name, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFont('Arial', '', 11);
    $pdf->Cell(0, 20, '', 0, 1, 'L');
    $pdf->MultiCell(180, 5, strip_tags($data->description), 0, 1, 'L');
    $pdf->Cell(0, 5, '', 0, 1, 'L');
    $pdf->Cell(0, 5, '', 0, 1, 'L');
    $pdf->Cell(180, 5, "Hormat Saya,", 0, 0, 'L');
    $pdf->Cell(0, 5, '', 0, 1, 'L');
    $pdf->Cell(0, 5, '', 0, 1, 'L');
    $pdf->Cell(0, 5, '', 0, 1, 'L');
    $pdf->Cell(0, 5, '', 0, 1, 'L');
    $pdf->Cell(0, 5, '', 0, 1, 'L');
    $pdf->Cell(180, 5, $data->username, 0, 0, 'L');

    $pdf->Output('permitActivity-' . $data->username . '.pdf', 'D');
  }
}
