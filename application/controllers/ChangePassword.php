<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class ChangePassword extends BaseController {
  function __construct()
  {
    $this->auth_required = false;
    //manually assign path for controlelrs in root
    $this->path_controller = "ChangePassword";
    $this->full_path = "ChangePassword";
    parent::__construct();
    //load models
    $this->load->model("Ion_auth_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }
  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
      $this->load->view('login');
    }
    else
    {
      $user = $this->ion_auth->user()->row();
      $data = array(
        "username" => $user->username
      );
      $this->load->view('changepassword',$data);
    }
  }
  public function do_change_password()
	{
		$response = get_ajax_response();

		//check session
		if (!$this->session->userdata('identity')) {
			$response->error_code = 301;
			$response->message = "restricted area";

			json_response($response);
			exit();
		}

		$identity = $this->session->userdata("identity");
    $current_password = $this->input->post("current_password");
		$new_password = $this->input->post("new_password");
		$confirm_password = $this->input->post("confirm_password");

		// validate form input
		$this->form_validation->set_rules('current_password', 'current_password', 'required|min_length[8]|max_length[22]');
		$this->form_validation->set_rules('new_password','new_password', 'required|min_length[8]|max_length[22]');
		$this->form_validation->set_rules('new_password_conf', 'new_password_conf', 'required|matches[new_password]|min_length[8]|max_length[22]');

		if ($this->form_validation->run() === TRUE)
		{
			if ($this->ion_auth->change_password($identity, $current_password, $new_password))
			{
				$response->status = true;
				$response->message = $this->lang->line("success");
			}
			else
			{
        $response->status = false;
				$response->message = strip_tags($this->ion_auth->errors());
			}
		}
		else
		{
			$response->message = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
		}
    header('Content-Type: application/json');
    echo json_encode($response);
	}

	public function check_current_password()
  {
    $identity = $this->session->userdata("identity");
    $current_password = $this->input->post("current_password");

    $password_matches = $this->ion_auth->is_match_password($identity, $current_password);
    if($password_matches){
        $response = true;
    }else{
        $response = $this->lang->line("form_curr_pass_invalid");
    }
    header('Content-Type: application/json');
    echo json_encode($response);
  }

}
