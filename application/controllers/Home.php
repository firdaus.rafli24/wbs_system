<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Home extends BaseController {
	//redirect user to page according to their role
	public function index()
	{
    	$user = $this->ion_auth->user()->row();
    	$user_group = $this->ion_auth->get_users_groups($user->id)->result();
    	//todo more flexible mechanism, avoid hardcoded values
    	if(!empty($user_group)){
    		$user_group = $user_group[0];
    		// if($user_group->id > 2){
    			//super admin
    			redirect('Dashboard');
    		// }else{
				// redirect('UserManagement');
    		// }
    	}
    	// redirect('Auth/logout');
	}
}
