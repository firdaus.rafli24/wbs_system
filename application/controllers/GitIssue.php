<?php
defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set("Asia/Bangkok");

require_once(APPPATH."controllers/BaseController.php");

class GitIssue extends BaseController {
  function __construct()
  {
    $this->auth_required = false;

    $this->path_controller = "GitIssue";
    $this->full_path = "GitIssue";

    parent::__construct();
    $this->load->model('git_issue_model');
    $this->load->model("user_wbs_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }


  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
     $this->load->view('login');
   }
   else
   {
    $user = $this->ion_auth->user()->row();
    $data['username'] = $user->username;
    $data['project']  = $this->user_wbs_model->get_project();
    $gitlabel = $this ->git_issue_model->getlabels();
    $arraygit=array();
    foreach ($gitlabel as $gi) {
      $object=$gi['labels'];
      array_push($arraygit,$object);
    }
    $str = implode (",", $arraygit);
    $str = explode (",", $str);
    $str=array_unique($str);
    $data['labels'] = $str;

    $this->load->view('git_issue_view', $data);
  }
}

public function datatable(){

 $start    = $this->input->post('start');
 $length   = $this->input->post('length');
 $search   = $this->input->post('search[value]');
 $search     = $this->input->post("search[value]");
 $start_date = $this->input->post("start_date");
 $stop_date  = $this->input->post("stop_date");
 $label  = $this->input->post("label");

 $userData = $this->git_issue_model->datatable($search, $start_date, $stop_date, $label, $length, $start);
 $allData = $this->git_issue_model->datatable($search, $start_date, $stop_date,$label);

 $data = array(
   'draw'              =>  $this->input->post('draw'),
   'recordsTotal'      =>  count($allData),
   'recordsFiltered'   =>  count($allData),
   'data' => $userData
 );

 header('Content-Type: application/json');
 echo json_encode($data);
}

public function do_save()
{

  $response = get_ajax_response();

  $getAllData =  $this->input->post();
  $file_csv       = $_FILES['file_csv']['name'];
  $file_csv_open  = $_FILES['file_csv']['tmp_name'];
  $file_size      = $_FILES['file_csv']['size'];
  $file_ekstensi  = $_FILES['file_csv']['type'];
  $file_ext = pathinfo($file_csv,PATHINFO_EXTENSION);

  $project_id = $getAllData['project_id'];
  $user_id = $this->session->userdata('user_id');

  if (empty($file_csv) || $file_size < 1 || ($file_ext !== 'csv')) {
    $data["status"]  = false;
    $data["message"] = $this->lang->line('import_invalid_csv');
  } else {
    $handle = fopen($file_csv_open,"r");
    $flag = true;
    $duplicate = 0;

    while (($row = fgetcsv($handle, 10000, ",")) != FALSE)
    {
     if($flag) {
       $flag = false;
       continue;
     }
     $check_data = $this->git_issue_model->check_data($project_id, $user_id,  $row[0]);

     if($check_data->duplicate > 0){
      $duplicate++;

      $dataDuplicate[] = [
        'project_id' => $project_id,
        'user_id_upload' => $user_id,
        'date_upload' => date('Y-m-d H:i:s'),
        'issue_id'  => $row[0],
        'url'       => $row[1],
        'title'           => $row[2],
        'state'           => $row[3],
        'description'     => $row[4],
        'author'          => $row[5],
        'author_username'   => $row[6],
        'assignee'          => $row[7],
        'assignee_username' => $row[8],
        'confidential'      => $row[9],
        'locked'            => $row[10],
        'due_date'          => $row[11],
        'created_at'        => $row[12],
        'updated_at'        => $row[13],
        'closed_at'         => $row[14],
        'milestone'         => $row[15],
        'weight'            => $row[16],
        'labels'            => $row[17],
        'time_estimate'     => $row[18],
        'time_spent'        => $row[19],
      ];


      $data["status"]   = 'warning';
      $data["message"]  = 'Duplicate '.$duplicate.' data on the same project';
      $data['duplicate']  = $dataDuplicate;
    } else {
      $dataInsert = [
        'project_id' => $project_id,
        'user_id_upload' => $user_id,
        'date_upload' => date('Y-m-d H:i:s'),
        'issue_id'  => $row[0],
        'url'       => $row[1],
        'title'           => $row[2],
        'state'           => $row[3],
        'description'     => $row[4],
        'author'          => $row[5],
        'author_username'   => $row[6],
        'assignee'          => $row[7],
        'assignee_username' => $row[8],
        'confidential'      => $row[9],
        'locked'            => $row[10],
        'due_date'          => $row[11],
        'created_at'        => $row[12],
        'updated_at'        => $row[13],
        'closed_at'         => $row[14],
        'milestone'         => $row[15],
        'weight'            => $row[16],
        'labels'            => $row[17].',',
        'time_estimate'     => $row[18],
        'time_spent'        => $row[19],
      ];

      $this->git_issue_model->insert($dataInsert);

      $data["status"]  = true;
      $data["message"] = $this->lang->line('success');
    }
  }

  fclose($handle);
}

header("Content-Type: application/json");
echo json_encode($data);
}

public function save_duplicate()
{
  $dataDuplicate =  $this->input->post('duplicate');

  foreach ($dataDuplicate as $key => $value) {
    $this->git_issue_model->delete_duplicate($value['project_id'], $value['user_id_upload'],  $value['issue_id']);
  }

  $this->git_issue_model->insert_batch($dataDuplicate);
  $data["status"]  = true;
  $data["message"] = $this->lang->line('success');

  header("Content-Type: application/json");
  echo json_encode($data);
}

public function get()
{
  $id = $this->input->post("id");

  $get_data = $this->git_issue_model->show($id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["id"]=$get_data;
   $data["message"]=$this->lang->line('get_invalid_id');
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 } else {
   $data = array("data"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$get_data;

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
public function do_update()
{
  $getAllData =  $this->input->post();
  $response=get_ajax_response();
  $id = $this->input->post("id");

  $get_data = $this->git_issue_model->show($id);

  $this->form_validation->set_rules('reason_name', $this->lang->line('PR002'), 'max_length[10]');

  if(empty($get_data)){
    $data["status"] = false;
    $data["message"]=$this->lang->line('edit_invalid_id');
    $data["id"]=$id;
    $data["data"]=array();
  } else {
    if ($this->form_validation->run()==TRUE) {
      $update = $this->git_issue_model->update($id, $getAllData);
      $data["status"] = true;
      $data["message"]=$this->lang->line('success');
      $data["data"]=$getAllData;
    } else {
      $data["status"] = false;
      $data["message"]= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
    }
  }

  header("Content-Type: application/json");
  echo json_encode($data);

}
public function do_delete()
{
  $id = $this->input->post("id");

  $get_data = $this->git_issue_model->show($id);
  $delete = $this->git_issue_model->delete($id);

  if(!$delete){
   $data["status"] = false;
   $data["message"]=$this->lang->line('delete_invalid_id');
   $data["id"]=$id;
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 }else{
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["id"]=$id;
   $data["data"]=$get_data;

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}


}
