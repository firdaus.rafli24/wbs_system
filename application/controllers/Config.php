<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Config extends BaseController {
  function __construct()
  {
    $this->auth_required = false;

    $this->path_controller = "Config";
    $this->full_path = "Config";

    parent::__construct();
    $this->load->model("menu_config_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');    
  }
  
  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
     $this->load->view('login');
   }
   else
   {
    $user = $this->ion_auth->user()->row();

    $data = array(
     "username" => $user->username,
   );

    $this->load->view('menu_config_view',$data);
  }
}


public function datatable()
{
  header('Content-Type: application/json');
  $search   = $this->input->post('search[value]');

  $final = $this->menu_config_model->datatable($search);

  $data = array(
   'draw'              =>  $this->input->post('draw'),
   'recordsTotal'      =>  count($final),
   'recordsFiltered'   =>  count($final),
   'data' => $final
 );

  echo json_encode($data);
}


public function do_save()
{
  $post = $this->input->post();
  $config_key = $post['config_key'];

  if(isset($post['config_enable'])){
    $dataUpdate = [
      'config_key' => $post['config_key'],
      'config_enable' => 1,
      'config_value' => ($post['config_value'] != '') ? $post['config_value'] : 0,
    ];
  } else {
    $dataUpdate = [
      'config_key' => $post['config_key'],
      'config_enable' => 0,
      'config_value' => NULL,
    ];
  }

  $this->menu_config_model->update($config_key, $dataUpdate );

  $data["status"] = true;
  $data["message"]=$this->lang->line("success");
  header("Content-Type: application/json");
  echo json_encode($data);
}

}
