<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Report1 extends BaseController {
  function __construct()
  {
    $this->auth_required = false;

      //manually assign path for controlelrs in root
    $this->path_controller = "Report1";
    $this->full_path = "Report1";

    parent::__construct();
      //load models
    $this->load->model("report1_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }
  
  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
      $this->load->view('login');
    }
    else
    {
      $user = $this->ion_auth->user()->row();
      $project = $this->report1_model->get_project();
      $user_group = $this->ion_auth->get_users_groups($user->id)->result()[0]->name;
      $projectData = array();

      $this->load->model('user_mng_model');
      $role = $this->report1_model->get_role();

      foreach ($project as $value) {
        array_push($projectData, $value->name);
      }

      $data = array(
        "project" => $projectData,
        "role" => $role,
        "message" => "success",
        "status" => true,
        "username" => $user->username,
        "user_group" => $user_group,
        "user_type" => $user->user_type
      );
      $this->load->view('report1',$data);
    }
  }

  public function datar1()
  {
    $post = $this->input->post();

    $search     = $this->input->post("search[value]");
    $start_date = $this->input->post("start_date");
    $stop_date  = $this->input->post("stop_date");
    $role       = $this->input->post("role");
/*    $role       = 10;
    $start_date = date('Y-m-01', strtotime(date('Y-m-d')));
    $stop_date  = date('Y-m-t', strtotime(date('Y-m-d')));
*/
    $user = $this->ion_auth->user()->row();
    $project = $this->report1_model->get_project();
    $nameCat = $this->report1_model->get_category($role, $search);
    $qchour = $this->report1_model->get_hour($role, $start_date, $stop_date);

    if(count($qchour) < 1){
      $data["status"] = false;
      $data["message"] = $this->lang->line('not_found');
      header("Content-Type: application/json");
      echo json_encode($data);
      return;
    }

    $dataLongest = $this->report1_model->get_longest_category($qchour[0]->category_id, $start_date, $stop_date);
    $longestCategory = $qchour[0]->category;


    $arrayCat = array();
    $results = array();
    $nameCatarr = array();
    $projectData = array();
    foreach ($nameCat as $value) {
      $arrayCat[$value->category] = 0;
      array_push($nameCatarr, $value->category);
    }

    foreach ($project as $value) {
      array_push($projectData, $value->name);
    }
    $projectQC = array_fill_keys($projectData, 0);
    $dataCat = array_fill_keys($nameCatarr, $projectQC);

    foreach ($qchour as $key => $value) {
      $pname = $value->name;
      if (array_key_exists($value->category, $dataCat)){
        $dataCat[$value->category][$pname] = $dataCat[$value->category][$pname] + $value->actual_sum;
      }

      if( !isset($results[$value->category]) ) {
        $results[$value->category] = 0;
      }
      $results[$value->category] += $value->actual_sum;

    }

    $actualSort = array_merge($arrayCat, $results);
    arsort($actualSort);

    $data = array(
      "datar1" => $dataCat,
      "actualSort" => $actualSort,
      "dataLongest" => $dataLongest,
      "longestCategory" => $longestCategory,
      "project" => $projectData,
      "message" => "success",
      "status" => true
    );
    header('Content-Type: application/json');
    echo json_encode($data);
  }



}
