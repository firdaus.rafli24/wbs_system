<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/BaseController.php");


class PermitQuotaManagement extends BaseController
{

  function __construct()
  {
    $this->auth_required = false;

    $this->path_controller = "PermitQuotaManagement";
    $this->full_path = "PermitQuotaManagement";

    parent::__construct();
    $this->load->model("user_mng_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }


  public function index()
  {
    if (!$this->ion_auth->logged_in()) {
      $this->load->view('login');
    } else {
      $user = $this->ion_auth->user()->row();
      $data['username']   = $user->username;

      $this->load->view('permit_quota_management_view', $data);
    }
  }

  public function datatable()
  {
    $start    = $this->input->post('start');
    $length   = $this->input->post('length');
    $search   = $this->input->post('search[value]');
    $pageData = $this->user_mng_model->datatable($search, $length, $start);
    $allData  = $this->user_mng_model->datatable($search);
    $data = array(
      'draw'              =>  $this->input->post('draw'),
      'recordsTotal'      =>  count($allData),
      'recordsFiltered'   =>  count($pageData),
      'data' => $pageData
    );

    header('Content-Type: application/json');
    echo json_encode($data);
  }
  public function get()
  {
    $id = $this->input->post("id");

    $get_data = $this->user_mng_model->view($id);

    if (empty($get_data)) {
      $data["status"] = false;
      $data["id"] = $get_data;
      $data["message"] = $this->lang->line('get_invalid_id');
      $data["data"] = array();

      header("Content-Type: application/json");
      echo json_encode($data);
    } else {
      $data = array("data" => $get_data);
      $data["status"] = true;
      $data["message"] = $this->lang->line('success');
      $data["data"] = $get_data[0];

      header("Content-Type: application/json");
      echo json_encode($data);
    }
  }
  public function do_update()
  {
    $getAllData = array(
      'permit_quota' => $this->input->post('permitQuotaEdit')
    );
    $response = get_ajax_response();
    $id = $this->input->post("id");

    $get_data = $this->user_mng_model->view($id);

    if (empty($get_data)) {
      $data["status"] = false;
      $data["message"] = $this->lang->line('edit_invalid_id');
      $data["id"] = $id;
      $data["data"] = array();
    } else {
      $update = $this->user_mng_model->updatePermitQuota($id, $getAllData);
      $data["status"] = true;
      $data["message"] = $this->lang->line('success');
      $data["data"] = $getAllData;
    }
    header("Content-Type: application/json");
    echo json_encode($data);
  }

  public function do_reset()
  {
    $response = get_ajax_response();

    $default = 12;
    $allUsersId = $this->user_mng_model->getAllId();

    $data = array();
    $userData = array();
    foreach ($allUsersId as $user) {
      $userData[] = array(
        'id' => $user['id'],
        'permit_quota' => $default
      );
    }
    
    if($userData){
      $this->user_mng_model->reset($userData);
      $data["status"] = true;
      $data["message"] = $this->lang->line('success');
      $data["data"] = $userData;
    }else{
      $data["status"] = false;
      $data["message"] = $this->lang->line('error');
      $data["data"] = $userData;
    }
  

    header("Content-Type: application/json");
    echo json_encode($data);
  }
}
