<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class GITDashboard extends BaseController {

  const HRD_ID = 2;
  const PG_ID = 3;
  const QC_ID = 4;
  const SA_ID = 5;

  function __construct()
  {
    $this->auth_required = false;
    $this->path_controller = "GITDashboard";
    $this->full_path = "GITDashboard";

    parent::__construct();
    $this->load->model('git_issue_model');
    $this->load->model("user_wbs_model");
    $this->load->model("dashboard_model");

    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }

  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
      $this->load->view('login');
    }
    else
    {
      $user = $this->ion_auth->user()->row();
      $user_group = $this->ion_auth->get_users_groups($user->id)->result()[0]->name;

      $this->load->model('user_mng_model');
      $role = $this->user_mng_model->get_role();
      $userData = $this->user_mng_model->datatable();

      $gitlabel = $this ->git_issue_model->getlabels();
      $arraygit=array();
      foreach ($gitlabel as $gi) {
          $object=$gi['labels'];
          array_push($arraygit,$object);
      }
      $str = implode (",", $arraygit);
      $str = explode (",", $str);
      $str=array_unique($str);
      sort($str);
      for ($i=1; $i <count($str) ; $i++) {
        $str[$i-1]=$str[$i];
      }

      $gitauthor = $this ->git_issue_model->getusername();
      $gitusername=array();
      foreach ($gitauthor as $gi) {
        $object=$gi['author'];
        array_push($gitusername,$object);
      }
      $gitusername=array_unique($gitusername);
      sort($gitusername);

      $project = $this ->git_issue_model->get_project();
      $projects=array_unique($project,SORT_REGULAR);

      $data = array(
        "project" => $projects,
        "role" => $role,
        "user" => $userData,
        "message" => "success",
        "status" => true,
        "username" => $user->username,
        "user_group" => $user_group,
        "user_type" => $user->user_type,
        "labels"=> $str,
        "gitusername"=> $gitusername
      );
      $this->load->view('git_dashboard_view',$data);
    }
  }

  public function changerole()
  {
    $post = $this->input->post();
    if(isset($post['id'])){
      $dataUser = $this->dashboard_model->get_user($post['id']);
      header('Content-Type: application/json');
      echo json_encode($dataUser);
    } else {
      header('Content-Type: application/json');
      echo json_encode(0);
    }
  }


  public function data_level()
  {
    $post = $this->input->post();

    $gitauthor = $this ->git_issue_model->getusername();
    $gituser=array();
    foreach ($gitauthor as $gi) {
      $object=$gi['author'];
      array_push($gituser,$object);
    }
    $gituser=array_unique($gituser);
    sort($gituser);

    if(!empty(array_intersect($post['user'],$gituser)) || $post['allUser'] == 1||(empty($post['user'][0])&&$post['allUser'] == 0)){
      $checkeduser=$post['allUser'];
      $checkedProject=$post['allproject'];
      $search     = $this->input->post("search[value]");
      $start_date = $this->input->post("start_date");
      $stop_date = $this->input->post("stop_date");
      $project = $this->input->post('project');

      $projectall = $this ->git_issue_model->get_project();
      $projectall=array_unique($projectall,SORT_REGULAR);

      if((empty($post['user'][0])&&$post['allUser'] == 0)||$checkeduser==1){
        $inUser = "1";
      } else {
        $inUser = $post['user'];
      }
      if ((empty($project[0])&&$checkedProject==0)||$checkedProject==1) {
        $projects='1';
      }else {
        $projects=$project;
      }
      $databug = $this->git_issue_model->getbug($inUser,$start_date,$stop_date,$projects);
      if (!empty($databug)) {
      foreach ($databug as $data) {
        $labels[$data['author']]="";
      }
      foreach($databug as $data){
        $labels[$data['author']]=$labels[$data['author']].','.$data['labels'];
      }
      $gitusername=array();
      foreach ($databug as $gi) {
        $object=$gi['author'];
        array_push($gitusername,$object);
      }
      $gitusername=array_unique($gitusername);
      $countgit=$this->count_git($labels,$gitusername);
      }else {
          $databug=array();
          $countgit=array();
          $gitusername=array();
      }
      $data = array(
        "count_git"=> $countgit,
        "name"=> $gitusername,
        "project" => $projectall,
        "message" => "success",
        "status" => true
      );
      header('Content-Type: application/json');
      echo json_encode($data);
    } else {
      header('Content-Type: application/json');
      echo json_encode(0);
    }
  }
  public function data_label()
  {
    $post = $this->input->post();

    $projectall = $this ->git_issue_model->get_project();
    $projectname=array();
    foreach ($projectall as $key) {
      array_push($projectname,$key->project);
      $projectid[$key->project_id]=$key->project;
    }
    $gitauthor = $this ->git_issue_model->getusername();
    $gituser=array();
    foreach ($gitauthor as $gi) {
      $object=$gi['author'];
      array_push($gituser,$object);
    }
    $gituser=array_unique($gituser);
    sort($gituser);
    $gitusername=array();

    if(!empty(array_intersect($post['user'],$gituser)) || $post['allUser'] == 1||(empty($post['user'][0])&&$post['allUser'] == 0)){

      $checkeduser=$post['allUser'];
      // $checkedlabel=$post['allLabel'];
      $checkedProject=$post['allproject'];
      $search     = $this->input->post("search[value]");
      $start_date = $this->input->post("start_date");
      $stop_date = $this->input->post("stop_date");
      $project = $this->input->post('project');

      if((empty($post['user'][0])&&$post['allUser'] == 0)||$checkeduser==1){
        $inUser = "1";
      } else {
        $inUser = $post['user'];
      }
      if ((empty($project[0])&&$checkedProject==0)||$checkedProject==1) {
        $projects='1';
      } else {
        $projects=$post['project'];;
      }
      $databug = $this->git_issue_model->getbugproject($inUser,$start_date,$stop_date,$projects);


      if (!empty($databug)) {
        $projectunique=array();
        foreach ($databug as $key) {
          $object=$key['project_id'];
          array_push($projectunique,$object);
        }
        if ($projects==1) {
          $project_detected = $this ->git_issue_model->get_project_detected($projectunique,$projectall);
        }else {
          $project_detected = $this ->git_issue_model->get_project_detected($projectunique,$projects);
        }
        $arraygit=array();
        foreach ($databug as $gi) {
          $object=$gi['project_id'];
          array_push($arraygit,$object);
        }
        $countgit = implode (",", $arraygit);
        $countgit = explode (",", $countgit);
        $countgit=array_count_values($countgit);
        $arrayproject=$this->count_git_project($countgit,$project_detected);
      }else {
        $arrayproject=0;
      }

      $data = array(
        "count_git"=> $arrayproject,
        "project" => $projectid,
        "message" => "success",
        "status" => true
      );
      header('Content-Type: application/json');
      echo json_encode($data);
    } else {
      header('Content-Type: application/json');
      echo json_encode(0);
    }
  }
  public function data_bug()
  {
    $post = $this->input->post();

    $gitauthor = $this ->git_issue_model->getusername();
    $projectall = $this ->git_issue_model->get_project();
    $projectname=array();
    foreach ($projectall as $key) {
      array_push($projectname,$key->project);
      $projectid[$key->project_id]=$key->project;
    }
    $gitauthor = $this ->git_issue_model->getusername();
    $gituser=array();
    foreach ($gitauthor as $gi) {
      $object=$gi['author'];
      array_push($gituser,$object);
    }
    $gituser=array_unique($gituser);
    sort($gituser);
    $gitusername=array();

    if(!empty(array_intersect($post['user'],$gituser)) || $post['allUser'] == 1||(empty($post['user'][0])&&$post['allUser'] == 0)){

      $checkeduser=$post['allUser'];
      $checkedProject=$post['allproject'];

      if((empty($post['user'][0])&&$post['allUser'] == 0)||$checkeduser==1){
        $inUser = "1";
      } else {
        $inUser = $post['user'];
      }
      if((empty($post['project'][0])&&$post['allproject'] == 0)||$checkedProject==1){
        $projects = "1";
      } else {
        $projects = $post['project'];
      }

      $search     = $this->input->post("search[value]");
      $start_date = $this->input->post("start_date");
      $stop_date = $this->input->post("stop_date");

      $databug = $this->git_issue_model->getbuglabel($inUser,$start_date,$stop_date,$projects);

      $arraygit=array();
      foreach ($databug as $gi) {
        $object=$gi['labels'];
        array_push($arraygit,$object);
      }
      $str = implode (",", $arraygit);
      $str = explode (",", $str);
      sort($str);
      for ($i=1; $i <count($str) ; $i++) {
        $str[$i-1]=$str[$i];
      }
      $sumgit = array_count_values($str);
      $gitlabel=array_unique($str);

      $countgit = $this->count_git_label($sumgit,$gitlabel);

      $data = array(
        "count_git"=> $countgit,
        "git" => $gitlabel,
        "message" => "success",
        "status" => true
      );
      header('Content-Type: application/json');
      echo json_encode($data);
    } else {
      header('Content-Type: application/json');
      echo json_encode(0);
    }
  }
  public function data_project()
  {
    $post = $this->input->post();
    $selectproject=$post['project_select'];

    $gitlabel = $this ->git_issue_model->getlabels();
    $arraygit=array();
    foreach ($gitlabel as $gi) {
      if ($gi['labels']!='') {
        $object=$gi['labels'];
        array_push($arraygit,$object);
      }
    }
    $str = implode (",", $arraygit);
    $str = explode (",", $str);
    $str=array_unique($str);
    sort($str);
    for ($i=1; $i <count($str) ; $i++) {
      $str[$i-1]=$str[$i];
    }

    $search     = $this->input->post("search[value]");
    $start_date = $this->input->post("start_date");
    $stop_date = $this->input->post("stop_date");

    $projectall = $this ->git_issue_model->get_project();
    $projectall=array_unique($projectall,SORT_REGULAR);

    $databug = $this->git_issue_model->getperproject($start_date,$stop_date,$selectproject);
    if (!empty($databug)) {
      foreach ($databug as $data) {
        $labels[$data['author']]="";
      }
      foreach($databug as $data){
        $labels[$data['author']]=$labels[$data['author']].','.$data['labels'];
      }      $gitusername=array();
      foreach ($databug as $gi) {
        $object=$gi['author'];
        array_push($gitusername,$object);
      }
      $gitusername=array_unique($gitusername);
      sort($gitusername);
      // var_dump($labels);
      $countgit=$this->count_git_perproject($labels,$gitusername);
    }else {
      $databug=0;
      $countgit=0;
      $str=0;
    }
      $data = array(
        "data_git" => $databug,
        "count_git"=> $countgit,
        "label"=>$str,
        "message" => "success",
        "status" => true
      );
      header('Content-Type: application/json');
      echo json_encode($data);
  }
  public function count_git($labels, $inUser)
  {
    $ar=array();
    foreach ($inUser as $key) {
      $a=explode(',',$labels[$key]);
      sort($a);
      $array[$key]=array_count_values($a);
      array_push($array[$key],$key);
    }
    return $array;
  }
  public function count_git_project($countgit,$projectall)
  {
    $ar=array();
    $color_array =array('#00a65a','#f39c12','#00c0ef','#984ea3','#00d2d5','#ff7f00','#af8d00','#7f80cd','#b3e900','#c42e60','#a65628','#f781bf','#8dd3c7','#bebada','#fb8072','#80b1d3','#fdb462','#fccde5','#bc80bd','#ffed6f','#c4eaff','#cf8c00','#1b9e77',
                        '#d95f02','#e7298a','#e6ab02','#a6761d','#0097ff','#00d067','#000000','#252525','#525252','#737373','#969696','#bdbdbd','#f43600','#4ba93b','#5779bb','#927acc','#97ee3f','#bf3947','#9f5b00','#f48758','#8caed6','#f2b94f','#eff26e',
                        '#e43872','#d9b100','#9d7a00','#698cff','#d9d9d9','#00d27e','#d06800','#009f82','#c49200','#cbe8ff','#fecddf','#c27eb6','#8cd2ce','#c4b8d9','#f883b0','#a49100','#f48800','#27d0df','#a04a9b');
    if (!empty($projectall)) {
      foreach ($projectall as $key) {
            $ar[$key->project]['id']=$key->project_id;
            $ar[$key->project]['sum']=$countgit[$key->project_id];
            $ar[$key->project]['name']=$key->project;
            $ar[$key->project]['color']=$color_array[fmod($key->project_id,60)];
        }
    }
    return $ar;
  }
  public function count_git_label($label,$alllabel)
  {
    $ar=array();
    $color_array =array('#00a65a','#f39c12','#00c0ef','#984ea3','#00d2d5','#ff7f00','#af8d00','#7f80cd','#b3e900','#c42e60','#a65628','#f781bf','#8dd3c7','#bebada','#fb8072','#80b1d3','#fdb462','#fccde5','#bc80bd','#ffed6f','#c4eaff','#cf8c00','#1b9e77',
                        '#d95f02','#e7298a','#e6ab02','#a6761d','#0097ff','#00d067','#000000','#252525','#525252','#737373','#969696','#bdbdbd','#f43600','#4ba93b','#5779bb','#927acc','#97ee3f','#bf3947','#9f5b00','#f48758','#8caed6','#f2b94f','#eff26e',
                        '#e43872','#d9b100','#9d7a00','#698cff','#d9d9d9','#00d27e','#d06800','#009f82','#c49200','#cbe8ff','#fecddf','#c27eb6','#8cd2ce','#c4b8d9','#f883b0','#a49100','#f48800','#27d0df','#a04a9b');
    $i=0;
    foreach ($alllabel as $key) {
      if ($key!='') {
        $ar[$key]['sum']=$label[$key];
        $ar[$key]['name']=$key;
        $ar[$key]['color']=$color_array[fmod($i,60)];
        $i=$i+1;
      }
    }
    return $ar;
  }
  public function count_git_perproject($labels, $inUser)
  {
    $ar=array();
    foreach ($inUser as $key) {
      $a = explode(',',$labels[$key]);
      sort($a);
      $array[$key]=array_count_values($a);
      array_push($array[$key],$key);
    }

    return $array;
  }
}
