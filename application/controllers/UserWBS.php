<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Userwbs extends BaseController {
function __construct(){
    $this->auth_required = false;

        //manually assign path for controlelrs in root
    $this->path_controller = "UserWBS";
    $this->full_path = "UserWBS";

    parent::__construct();
        //load models pasang lang
    $this->lang->load('wbs_lang');
        // $this->load->model("Ion_auth_model");
    $this->load->model("user_wbs_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }
public function index(){
    if (!$this->ion_auth->logged_in())
    {
     $this->load->view('login');
   }
   else
   {
    $user = $this->ion_auth->user()->row();
    $project= $this->user_wbs_model->get_project();
    $task_category= $this->user_wbs_model->get_category_role($this->ion_auth->user()->row()->user_type);
    $all_category= $this->user_wbs_model->get_all_category();
    $pic= $this->user_wbs_model->get_pic();
    $task_status= $this->user_wbs_model->get_status();
    $data = array(
     "username" => $user->username,
     "userid" => $user->id,
     "user_type" => $user->user_type,
     "project" => $project,
     "task_category"=>$task_category,
     "all_category"=>$all_category,
     "pic"=>$pic,
     "task_status"=>$task_status,
     "get_template_wbs_name" => $this->user_wbs_model->get_template_wbs_name()
   );
    $this->load->view('userwbs',$data);
    }
  }
public function datatable(){
  $this->load->model('user_wbs_model');

  $hrd    = $this->input->post('hrd');
  $start    = $this->input->post('start');
  $length    = $this->input->post('length');
  $search = $this->input->post('search[value]');
  $user   = $this->ion_auth->user()->row();

  if ($hrd == '1'){
    $userData = $this->user_wbs_model->datatable('', $search, $length, $start);
    $allData = $this->user_wbs_model->datatable('', $search);
  }
  else {
    $userData = $this->user_wbs_model->datatable($user->id, $search, $length, $start);
    $allData = $this->user_wbs_model->datatable($user->id, $search);
  }

  $newRowData = array();
  foreach ($userData as $key => $value) {
    $row = new StdClass();
    $row->id = $value->id;
    $row->project = $value->project;
    $row->category = $value->category;
    $row->task_name = $value->task_name;
    $row->pic = $value->pic;
    $row->excecutedby = $value->excecutedby;
    $row->start_time = $value->start_time;
    $row->stop_time = $value->stop_time;
    $row->start_date = $this->is_valid_date($value->start_date);
    $row->due_date = $this->is_valid_date($value->due_date);
    $row->due_date_revised = $this->is_valid_date($value->due_date_revised);
    $row->estimated_hour = $value->estimated_hour;
    $row->actual_hour = $value->actual_hour;
    $row->task_percentage = $value->task_percentage.'%';
    $row->task_status = $value->task_status;
    $row->notes = $value->notes;
    $row->created_date = $value->created_date;

    array_push($newRowData, $row);
  }

  $data = array(
   'draw'              =>  $this->input->post('draw'),
   'recordsTotal'      =>  count($allData),
   'recordsFiltered'   =>  count($allData),
   'data' => $newRowData
 );

  header('Content-Type: application/json');
  echo json_encode($data);
}
public function do_save(){
  $user = $this->ion_auth->user()->row();
  $post = $this->input->post();

  $datum=array();
  $row = $this->input->post('rowlength');
    for ($i=0; $i <$row; $i++) {
      $checkDate[] = [
        'start_date' =>  date('Y-m-d', strtotime($this->input->post('start_date_add'.$i))),
        'start_time' => str_pad($this->input->post("start_time_add".$i), 5, "0", STR_PAD_LEFT),
        'stop_time' => str_pad($this->input->post("stop_time_add".$i), 5, "0", STR_PAD_LEFT),
      ];

      if($checkDate[$i]['start_time'] > $checkDate[$i]['stop_time']){
        $data["status"] = false;
        $data['message'] = 'Check '.$this->lang->line("WB007").' & '.$this->lang->line("WB008");
        header("Content-Type: application/json");
        echo json_encode($data);
        return;
      }
      $object = array('start_date' => $checkDate[$i]['start_date'],
                      'start_time' => $checkDate[$i]['start_time'],
                      'stop_time' => $checkDate[$i]['stop_time']
                        );
      array_push($datum, $object);
    }
    $c_start_date = array_column($checkDate, 'start_date');
    $c_start_time = array_column($checkDate, 'start_time');
    $c_stop_time = array_column($checkDate, 'stop_time');

    $check_exist_date = $this->user_wbs_model->check_exist_date($c_start_date, $c_start_time, $c_stop_time);
    $result = $this->check_unique_time($datum,$row);

    if(($check_exist_date > 0)){
      $data["status"] = false;
      $data['message'] = 'Duplicates '.$this->lang->line("WB007").' & '.$this->lang->line("WB008") .' on the same date';
      header("Content-Type: application/json");
      echo json_encode($data);
      return;
    }elseif (!$result) {
      $data["status"] = false;
      $data['message'] = 'Duplicates '.$this->lang->line("WB007").' & '.$this->lang->line("WB008") .' on the same date';
      header("Content-Type: application/json");
      echo json_encode($data);
      return;
    }

   $response = get_ajax_response();
   $this->load->model('user_wbs_model');
   $row = $this->input->post('rowlength');
   $datainput=array();
   for ($i=0; $i <$row; $i++) {
    $rowinput= array( 'project' => $this->input->post("project_add".$i),
     'category' => $this->input->post("category_add".$i),
     'task_name' => $this->input->post("task_name_add".$i),
     'pic' => $this->input->post("pic_add".$i),
     'excecutedby' => $user->id,
     'start_time' => $this->input->post("start_time_add".$i),
     'stop_time' => $this->input->post("stop_time_add".$i),
     'start_date' => date('Y-m-d', strtotime($this->input->post('start_date_add'.$i))),
     'due_date' => date('Y-m-d', strtotime($this->input->post('due_date_add'.$i))),
     'due_date_revised' => $this->isempty_due_date_revised($this->input->post('due_date_revised_add'.$i)),
     'estimated_hour' => $this->input->post("estimated_hour_add".$i),
     'actual_hour' => $this->input->post("actual_hour_add".$i),
     'task_status' => $this->input->post("task_status_add".$i),
     'task_percentage' => $this->input->post("task_percentage_add".$i),
     'notes' => $this->input->post("notes_add".$i)
   );
    array_push($datainput,$rowinput);
  }
  for ($i=0; $i <$row; $i++) {
    $this->form_validation->set_rules('project_add'.$i, 'project_add'.$i, 'required|numeric');
    $this->form_validation->set_rules('category_add'.$i, 'category_add'.$i, 'required|numeric');
    $this->form_validation->set_rules('task_name_add'.$i, $this->lang->line("WB004"), 'trim|required|max_length[100]');
    $this->form_validation->set_rules('due_date_add'.$i, 'due_date_add'.$i, 'trim|required|callback_valid_date');
    $this->form_validation->set_rules('start_date_add'.$i, 'start_date_add'.$i, 'trim|required|callback_valid_date');
    $this->form_validation->set_rules('due_date_revised_add'.$i, 'due_date_revised_add'.$i, 'trim|callback_valid_date');
    $this->form_validation->set_rules('estimated_hour_add'.$i, 'estimated_hour_add'.$i, 'trim|required|numeric');
    $this->form_validation->set_rules('start_time_add'.$i, 'start_time_add'.$i, 'trim');
    $this->form_validation->set_rules('stop_time_add'.$i, 'stop_time_add'.$i, 'trim');
    $this->form_validation->set_rules('actual_hour_add'.$i, 'actual_hour_add'.$i, 'trim|numeric|greater_than_equal_to[0]');
    $this->form_validation->set_rules('task_percentage_add'.$i, 'task_percentage_add'.$i, 'trim|numeric|less_than_equal_to[100]|greater_than_equal_to[0]');
    $this->form_validation->set_rules('task_status_add'.$i, 'task_status_add'.$i, 'trim|numeric');
    $this->form_validation->set_rules('notes_add'.$i, $this->lang->line("WB013"), 'max_length[500]');
  }
  if ($this->form_validation->run()==TRUE) {
    $save = $this->user_wbs_model->add($datainput);
    $data["status"] = true;
    $data["message"]=$this->lang->line('success');
    $data["data"]=$datainput;

  }else {
    $data["status"] = false;
    if ($row>0) {
      $data['message'] = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
    }else {
      $data['message'] =$this->lang->line('form_row_empty');
    }
  }
  header("Content-Type: application/json");
  echo json_encode($data);
}
public function check_unique_time($datum,$row){
  $result=true;
  for ($i=0; $i < $row; $i++) {
    for ($j=0; $j <$row ; $j++) {
      if ($i!=$j) {
        if ((strtotime($datum[$i]['start_date'])==strtotime($datum[$j]['start_date']))&&(strtotime($datum[$i]['start_time'])==strtotime($datum[$j]['start_time']))&&(strtotime($datum[$i]['stop_time'])==strtotime($datum[$j]['stop_time']))) {
          $result= false;
        }else {
          $result= true;
        }
      }
    }
  }
  return $result;
}
public function get(){
  $user_id = $this->input->post("user_id");

  $this->load->model('user_wbs_model');
  $get_data = $this->user_wbs_model->view($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["id"]=$get_data;
   $data["message"]=$this->lang->line('get_invalid_id');
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 }else{
   $getAllData = $this->user_wbs_model->view($user_id);
   $projectname = $this->to_get_project_name($getAllData[0]['project']);
   $statusname = $this->to_get_task_status($getAllData[0]['task_status']);
   $categoryname = $this->to_get_category_name($getAllData[0]['category']);
   $picname = $this->to_get_user_name($getAllData[0]['pic']);
   $excecutedbyname = $this->to_get_user_name($getAllData[0]['excecutedby']);

   $getAllData[0]['projectname']=$projectname;
   $getAllData[0]['statusname']=$statusname;
   $getAllData[0]['categoryname']=$categoryname;
   $getAllData[0]['picname']=$picname;
   $getAllData[0]['excecutedbyname']=$excecutedbyname;

   $data = array("data"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$getAllData[0];

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
public function do_update(){
  $user = $this->ion_auth->user()->row();
  $response=get_ajax_response();
  $user_id = $this->input->post("user_id");
  $get_data = $this->user_wbs_model->view($user_id);

  $getAllData= array( 'project' => $this->input->post("project"),
   'category' => $this->input->post("category"),
   'task_name' => $this->input->post("task_name"),
   'pic' => $this->input->post("pic"),
   'excecutedby' =>$this->input->post("excecutedby"),
   'start_time' => $this->input->post("start_time"),
   'stop_time' => $this->input->post("stop_time"),
   'start_date' => date('Y-m-d', strtotime($this->input->post('start_date'))),
   'due_date' => date('Y-m-d', strtotime($this->input->post('due_date'))),
   'due_date_revised' => $this->isempty_due_date_revised($this->input->post('due_date_revised')),
   'estimated_hour' => $this->input->post("estimated_hour"),
   'actual_hour' => round($this->input->post("actual_hour"), 1),
   'task_percentage' => $this->input->post("task_percentage"),
   'task_status' => $this->input->post("task_status"),
   'notes' => $this->input->post("notes")
 );

  $this->form_validation->set_rules('project', 'project', 'required|numeric');
  $this->form_validation->set_rules('category', 'category', 'required|numeric');
  $this->form_validation->set_rules('pic', 'pic', 'required|numeric');
  $this->form_validation->set_rules('task_name', 'task_name', 'trim|required|max_length[50]');
  $this->form_validation->set_rules('due_date', 'due_date', 'trim|required|callback_valid_date');
  $this->form_validation->set_rules('start_date', 'start_date', 'trim|required|callback_valid_date');
  $this->form_validation->set_rules('due_date_revised', 'due_date_revised', 'trim|callback_valid_date');
  $this->form_validation->set_rules('estimated_hour', 'estimated_hour', 'trim|required|numeric');
  $this->form_validation->set_rules('start_time', 'start_time', 'trim');
  $this->form_validation->set_rules('stop_time', 'stop_time', 'trim');
  $this->form_validation->set_rules('actual_hour', 'actual_hour', 'trim|required|numeric|greater_than_equal_to[0]');
  $this->form_validation->set_rules('notes', 'notes', 'max_length[500]');
  $this->form_validation->set_rules('task_percentage', 'task_percentage', 'trim|numeric|less_than_equal_to[100]|greater_than_equal_to[0]');
  $this->form_validation->set_rules('task_status', 'task_status', 'trim|numeric');

  if(empty($get_data)){
    $data["status"] = false;
    $data["message"]=$this->lang->line('edit_invalid_id');
    $data["id"]=$user_id;
    $data["data"]=array();

    header("Content-Type: application/json");
    echo json_encode($data);
  }else{
    if ($this->form_validation->run()==TRUE) {
      $update = $this->user_wbs_model->update($user_id,$getAllData);
      $data["status"] = true;
      $data['message'] = $this->lang->line('success');
      $data["data"]=$getAllData;

      header("Content-Type: application/json");
      echo json_encode($data);
    }
    else {
      $data["status"] = false;
      $data['message'] = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));

      header("Content-Type: application/json");
      echo json_encode($data);
    }
  }
}
public function do_delete(){
  $user_id = $this->input->post("user_id");

  $this->load->model('user_wbs_model');
  $get_data = $this->user_wbs_model->view($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["message"]=$this->lang->line('delete_invalid_id');
   $data["id"]=$user_id;
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 }else{
   $delete = $this->user_wbs_model->delete($user_id);
   $data = array("user_id"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$delete;

			// $this->load->view("Auth_view",$data);
   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
private function to_get_project_name($id){
  $role_name = $this->user_wbs_model->get_project_name($id);
  foreach($role_name as $r) {
    $name= $r->name;
    return $name;
  }
}
private function to_get_category_name($id){
  $role_name = $this->user_wbs_model->get_category_name($id);
  foreach($role_name as $r) {
    $name= $r->name;
    return $name;
  }
}
private function to_get_task_status($id){
  $role_name = $this->user_wbs_model->get_task_status($id);
  foreach($role_name as $r) {
    $name= $r->name;
    return $name;
  }
}
private function to_get_user_name($id){
  $role_name = $this->user_wbs_model->get_user_name($id);
  foreach($role_name as $r) {
    $name= $r->username;
    return $name;
  }
}
public function add(){
  $response = get_ajax_response();
  $project= $this->user_wbs_model->get_project();
  $task_category= $this->user_wbs_model->get_category_role($this->ion_auth->user()->row()->user_type);
  $pic= $this->user_wbs_model->get_pic();
  $user = $this->ion_auth->user()->row()->username;
  $task_status= $this->user_wbs_model->get_status();

  $data = array(
    "project" => $project,
    "pic" => $pic,
    "excecutedby" => $user,
    "category" => $task_category,
    "status" => $task_status
  );
  if(empty($project)){
    $response->status = false;
    $response->code = 400;
  }else {
    $response->status = true;
    $response->code = 200;
    $response->data = $data;
  }

  header("Content-Type: application/json");
  echo json_encode($response);
}
public function addCategory(){
  $response = get_ajax_response();
  $all_category= $this->user_wbs_model->get_all_category();
  $task_category= $this->user_wbs_model->get_category_role($this->ion_auth->user()->row()->user_type);

  $data = array(
    "all_category" => $all_category,
    "task_category" => $task_category
  );
  $response->status = true;
  $response->code = 200;
  $response->data = $data;

  header("Content-Type: application/json");
  echo json_encode($response);
}
private function isempty_due_date_revised($input){
  if (!empty($input)) {
    return date('Y-m-d', strtotime($input));
  }
}
private function is_valid_date($input){
  if ($input < '2000-01-01') {
    return '-';
  }else{
    return date('Y-m-d', strtotime($input));
  }
}
public function valid_date($date) {
  if(!empty($date)){
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') === $date;
  }
}
private function count_actual_hour($value1,$value2){
  if ($value1>$value2) {
    return 0;
  } else {
    return (strtotime($value2)-strtotime($value1))/ 60 / 60;
  }
}
public function find_template_wbs($template_wbs_id = ''){
  if($template_wbs_id == ''){
    $data = [];
  } else {
    $data =  $this->user_wbs_model->get_template_wbs_detail($template_wbs_id);
  }
  header('Content-Type: application/json');
  echo json_encode($data);
}
public function do_save_template(){
  $user = $this->ion_auth->user()->row();
  $post = $this->input->post();

  $datum=array();
  // $template_name= array( 'name' => $this->input->post("templatename"));
  $row = $this->input->post('rowlength');
    for ($i=0; $i <$row; $i++) {
      $checkDate[] = [
        'start_date' =>  date('Y-m-d', strtotime($this->input->post('start_date_add'.$i))),
        'start_time' => str_pad($this->input->post("start_time_add".$i), 5, "0", STR_PAD_LEFT),
        'stop_time' => str_pad($this->input->post("stop_time_add".$i), 5, "0", STR_PAD_LEFT),
      ];

      if($checkDate[$i]['start_time'] > $checkDate[$i]['stop_time']){
        $data["status"] = false;
        $data['message'] = 'Check '.$this->lang->line("WB007").' & '.$this->lang->line("WB008");
        header("Content-Type: application/json");
        echo json_encode($data);
        return;
      }
      $object = array('start_date' => $checkDate[$i]['start_date'],
                      'start_time' => $checkDate[$i]['start_time'],
                      'stop_time' => $checkDate[$i]['stop_time']
                        );
      array_push($datum, $object);
    }
    $c_start_date = array_column($checkDate, 'start_date');
    $c_start_time = array_column($checkDate, 'start_time');
    $c_stop_time = array_column($checkDate, 'stop_time');

    $check_exist_date = $this->user_wbs_model->check_exist_date($c_start_date, $c_start_time, $c_stop_time);
    $result = $this->check_unique_time($datum,$row);

   $response = get_ajax_response();
   $this->load->model('user_wbs_model');
   $row = $this->input->post('rowlength');
   $datainput=array();

   $this->form_validation->set_rules('templatename', 'templatename', 'required|max_length[100]');
   for ($i=0; $i <$row; $i++) {
    $this->form_validation->set_rules('project_add'.$i, 'project_add'.$i, 'required|numeric');
    $this->form_validation->set_rules('category_add'.$i, 'category_add'.$i, 'required|numeric');
    $this->form_validation->set_rules('task_name_add'.$i, $this->lang->line("WB004"), 'trim|max_length[100]');
    $this->form_validation->set_rules('due_date_add'.$i, 'due_date_add'.$i, 'trim|callback_valid_date');
    $this->form_validation->set_rules('start_date_add'.$i, 'start_date_add'.$i, 'trim|callback_valid_date');
    $this->form_validation->set_rules('due_date_revised_add'.$i, 'due_date_revised_add'.$i, 'trim|callback_valid_date');
    $this->form_validation->set_rules('estimated_hour_add'.$i, 'estimated_hour_add'.$i, 'trim|numeric');
    $this->form_validation->set_rules('start_time_add'.$i, 'start_time_add'.$i, 'trim');
    $this->form_validation->set_rules('stop_time_add'.$i, 'stop_time_add'.$i, 'trim');
    $this->form_validation->set_rules('actual_hour_add'.$i, 'actual_hour_add'.$i, 'trim|numeric|greater_than_equal_to[0]');
    $this->form_validation->set_rules('task_percentage_add'.$i, 'task_percentage_add'.$i, 'trim|numeric|less_than_equal_to[100]|greater_than_equal_to[0]');
    $this->form_validation->set_rules('task_status_add'.$i, 'task_status_add'.$i, 'trim|numeric');
    $this->form_validation->set_rules('notes_add'.$i, $this->lang->line("WB013"), 'max_length[500]');
  }
  if ($this->form_validation->run()==TRUE) {
    $save_name = $this->save_template_name($this->input->post("templatename"),$user->id);
    for ($i=0; $i <$row; $i++) {
       $rowinput= array( 'project' => $this->input->post("project_add".$i),
        'category' => $this->input->post("category_add".$i),
        'task_name' => $this->input->post("task_name_add".$i),
        'pic' => $this->input->post("pic_add".$i),
        'excecutedby' => $user->id,
        'start_time' => $this->input->post("start_time_add".$i),
        'stop_time' => $this->input->post("stop_time_add".$i),
        'start_date' => date('Y-m-d', strtotime($this->input->post('start_date_add'.$i))),
        'due_date' => date('Y-m-d', strtotime($this->input->post('due_date_add'.$i))),
        'due_date_revised' => $this->isempty_due_date_revised($this->input->post('due_date_revised_add'.$i)),
        'estimated_hour' => $this->input->post("estimated_hour_add".$i),
        'actual_hour' => $this->input->post("actual_hour_add".$i),
        'task_status' => $this->input->post("task_status_add".$i),
        'task_percentage' => $this->input->post("task_percentage_add".$i),
        'notes' => $this->input->post("notes_add".$i),
        'template_wbs_id' => $save_name
       );
       array_push($datainput,$rowinput);
    }
    $save_template = $this->user_wbs_model->save_template_data($datainput);

    $data["status"] = true;
    $data["message"]=$this->lang->line('success');
    $data["data"]=$datainput;

  }else {
    $data["status"] = false;
    if ($row>0) {
      $data['message'] = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
    }else {
      $data['message'] =$this->lang->line('form_row_empty');
    }
  }
  header("Content-Type: application/json");
  echo json_encode($data);
}
function save_template_name($name,$user_id){
  $arrayname = array('name'=>$name,'user_id'=>$user_id);
  $id = $this->user_wbs_model->add_template_name($arrayname);
  return $id;
}
public function get_template(){
  $user_id = $this->input->post("user_id");
  $this->load->model('user_wbs_model');
  $get_data = $this->user_wbs_model->template($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["id"]=$get_data;
   $data["message"]=$this->lang->line('get_invalid_id');
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 }else{
   $getAllData = $this->user_wbs_model->template($user_id);

   $data = array("data"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$getAllData[0];

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
public function do_delete_template(){
  $id = $this->input->post("user_id");
  $this->load->model('user_wbs_model');
  $get_data = $this->user_wbs_model->template($id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["message"]=$this->lang->line('delete_invalid_id');
   $data["id"]=$id;
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 }else{
   $delete = $this->user_wbs_model->delete_template_name($id);
   $delete = $this->user_wbs_model->delete_template_detail($id);
   $data = array("id"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$delete;

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
public function get_template_data(){
  $response = get_ajax_response();
  $user = $this->ion_auth->user()->row();
  $thisuser = $user->id;
  $template= $this->user_wbs_model->get_template($thisuser);
  $user = $this->ion_auth->user()->row()->username;

  $data = array(
    "template" => $template,
    "excecutedby" => $user
  );
  if(empty($template)){
    $response->status = false;
    $response->code = 400;
  }else {
    $response->status = true;
    $response->code = 200;
    $response->data = $data;
  }

  header("Content-Type: application/json");
  echo json_encode($response);
}
}
