<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/BaseController.php");


class AssignmentLetter extends BaseController
{

  function __construct()
  {
    $this->auth_required = false;

    $this->path_controller = "AssignmentLetter";
    $this->full_path = "AssignmentLetter";

    parent::__construct();
    $this->load->model("user_type_model");
    $this->load->model("user_mng_model");
    $this->load->model('assignment_letter_model');
    // $this->load->model('notification_model');

    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }


  public function index()
  {
    if (!$this->ion_auth->logged_in()) {
      $this->load->view('login');
    } else {
      $user = $this->ion_auth->user()->row();
      $data['username']   = $user->username;
      $data['user_type']   = $user->user_type;
      $data['projects'] =  $this->assignment_letter_model->get_project();
      $data['get_user_to'] = $this->assignment_letter_model->get_user_to();

      $data['button_approve'] = ($user->user_type == 2) ? 'visible' : 'hidden';

      $this->load->view('assignment_letter_view', $data);
    }
  }

  public function datatable()
  {
    $start    = $this->input->post('start');
    $length   = $this->input->post('length');
    $search   = $this->input->post('search[value]');
    $pageData = $this->assignment_letter_model->datatable($search, $length, $start);
    $allData  = $this->assignment_letter_model->datatable($search);
    $data = array(
      'draw'              =>  $this->input->post('draw'),
      'recordsTotal'      =>  count($allData),
      'recordsFiltered'   =>  count($pageData),
      'data' => $pageData
    );

    header('Content-Type: application/json');
    echo json_encode($data);
  }

  public function do_save()
  {
    $response = get_ajax_response();

    $user_id = $this->session->userdata('user_id');

    $getAllData =  $this->input->post();
    $getAllData   = array_merge($getAllData, ['user_id' => $user_id]);

    $this->form_validation->set_rules('destination', $this->lang->line('AL005'), 'max_length[100]');
    if ($this->form_validation->run() == TRUE) {

      $save = $this->assignment_letter_model->add($getAllData);
      $insert_id = $this->db->insert_id();

      $data["status"] = true;
      $data["message"] = $this->lang->line('success');

    } else {
      $data["status"] = false;
      $data["message"] = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));;
      $data["data"] = $getAllData;
    }

    header("Content-Type: application/json");
    echo json_encode($data);
  }
  public function get()
  {
    $id = $this->input->post("id");

    $get_data = $this->assignment_letter_model->show($id);

    if (empty($get_data)) {
      $data["status"] = false;
      $data["id"] = $get_data;
      $data["message"] = $this->lang->line('get_invalid_id');
      $data["data"] = array();

      header("Content-Type: application/json");
      echo json_encode($data);
    } else {
      $data = array("data" => $get_data);
      $data["status"] = true;
      $data["message"] = $this->lang->line('success');
      $data["data"] = $get_data;

      header("Content-Type: application/json");
      echo json_encode($data);
    }
  }
  public function do_update()
  {
    $getAllData =  $this->input->post();
    $response = get_ajax_response();
    $id = $this->input->post("id");

    $get_data = $this->assignment_letter_model->show($id);

    $this->form_validation->set_rules('subject', $this->lang->line('PA005'), 'max_length[100]');

    if (empty($get_data)) {
      $data["status"] = false;
      $data["message"] = $this->lang->line('edit_invalid_id');
      $data["id"] = $id;
      $data["data"] = array();
    } else {
      if ($this->form_validation->run() == TRUE) {

        $update = $this->assignment_letter_model->update($id, $getAllData);
        $data["status"] = true;
        $data["message"] = $this->lang->line('success');
        $data["data"] = $getAllData;
        // $this->send_email($id, $get_data);
      } else {
        $data["status"] = false;
        $data["message"] = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));;
        $data["data"] = $getAllData;
      }
    }
    header("Content-Type: application/json");
    echo json_encode($data);
  }

  public function do_delete()
  {
    $id = $this->input->post("id");

    $get_data = $this->assignment_letter_model->show($id);
    $delete = $this->assignment_letter_model->delete($id);

    if (!$delete) {
      $data["status"] = false;
      $data["message"] = $this->lang->line('delete_invalid_id');
      $data["id"] = $id;
      $data["data"] = array();

      header("Content-Type: application/json");
      echo json_encode($data);
    } else {
      $data["status"] = true;
      $data["message"] = $this->lang->line('success');
      $data["id"] = $id;
      $data["data"] = $get_data;

      header("Content-Type: application/json");
      echo json_encode($data);
    }
  }
  public function download_pdf()
  {
   
    $id = $this->input->get("id");
    if (!is_numeric($id)) return false;

    $data = $this->assignment_letter_model->show($id);

    $date = formatTanggal($data->date);
    $createDate = formatTanggal($data->created_date);
  
    $image1 = "assets/img/pantona-logo-only.png";
    $image2 = "assets/img/digital_signature/$data->digital_signature";

    $this->load->library('fpdf182/fpdf');
    $pdf = new FPDF('P', 'mm', 'A4');
    $pdf->AliasNbPages();
    $pdf->SetFillColor(255, 255, 255);
    $pdf->SetAutoPageBreak(false);
    $pdf->SetTopMargin(6);
    $pdf->SetLeftMargin(20);
    $pdf->SetRightMargin(20);
    $pdf->AddPage();

    
    $pdf->Image($image1,15,9.8,23);
    $pdf->Image($image2,155,245,30);
    $pdf->SetFont('Times', 'B', 14);
    $pdf->Cell(190, 15, "PT. PANTONA Teknologi Indonesia", 0, 1, 'C');
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(200, 5, "Jl. Unpar III No.16, Sukawarna, Kec. Sukajadi, Kota Bandung, Jawa Barat 40164", 0, 1, 'C');
    $pdf->Cell(200, 5, "No. Telepon (0856) 59459986", 0, 1, 'C');
    $pdf->Ln();
    $pdf->SetLineWidth(1);
    $pdf->Line(10, 35, 200, 35);
    $pdf->Ln();
    $pdf->SetFont('Times', 'BU', 14);
    $pdf->Cell(200, 5, "Surat Tugas", 0, 1, 'C');
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(200, 50, "Yang bertanda tangan di bawah ini:", 0,1, '');
    $pdf->Cell(30, -30, 'Nama', 0, 0, 'L', 0);
    $pdf->Cell(30, -30, ': ' . $data->username, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(30,45, 'Jabatan', 0, 0, 'L', 0);
    $pdf->Cell(30,45, ': ' . $data->senderGroup, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(200, 20, "Dengan ini memberi tugas dan tanggung jawab kepada:", 0,1, '');
    $pdf->Cell(30, 0, 'Nama', 0, 0, 'L', 0);
    $pdf->Cell(30, 0, ': ' . $data->username2, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(30,15, 'Jabatan', 0, 0, 'L', 0);
    $pdf->Cell(30,15, ': ' . $data->recipientGroup, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(30, 40, "Untuk ".$data->purpose." dalam proyek ".$data->project_name.". Pada:", 0,0, '');
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(30, -10, 'Tanggal', 0, 0, 'L', 0);
    $pdf->Cell(30, -10, ': ' . $date, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(30,35, 'Tempat', 0, 0, 'L', 0);
    $pdf->Cell(30,35, ': ' . $data->destination, 0, 0, 'L', 0);
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->drawTextBox('Demikian surat tugas ini kami berikan agar dapat dilaksanakan saudara/i dengan penuh tanggung jawab.', 170, 10, 'J', 'M',false);
    // $pdf->MultiCell(180, 5, "Demikian surat tugas ini kami berikan agar dapat dilaksanakan saudara/i dengan penuh tanggung jawab.", 0, 1, 'L');
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(170,35, $createDate, 0, 0, 'R', 0);
    $pdf->Ln();
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(165,35, $data->username, 0, 0, 'R', 0);    
    $pdf->Output('AssignmentLetter-' . $data->username2 . '.pdf', 'D');
    

  }
}
