<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/BaseController.php");

class InventoryLoanList extends BaseController
{
    function __construct()
    {
        $this->auth_required = false;

        $this->path_controller = "InventoryLoanList";
        $this->full_path = "InventoryLoanList";

        parent::__construct();
        $this->load->model("user_type_model");
        $this->load->model("inventory_loan_model");

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->load->view('login');
        } else {
            $user = $this->ion_auth->user()->row();
            $data['username']   = $user->username;
            $this->load->view('inventory_loan_list_view', $data);
        }
    }

    public function datatable()
    {
        $start    = $this->input->post('start');
        $length   = $this->input->post('length');
        $search   = $this->input->post('search[value]');
        $pageData = $this->inventory_loan_model->datatable($search, $length, $start);
        $allData  = $this->inventory_loan_model->datatable($search);
        $data = array(
            'draw'              =>  $this->input->post('draw'),
            'recordsTotal'      =>  count($allData),
            'recordsFiltered'   =>  count($pageData),
            'data' => $pageData
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }
}
