<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Projectmanager extends BaseController {
  function __construct()
  {
    $this->auth_required = false;

      //manually assign path for controlelrs in root
    $this->path_controller = "ProjectManager";
    $this->full_path = "ProjectManager";

    parent::__construct();
      //load models
      // $this->load->model("Ion_auth_model");
    $this->load->model("project_mng_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

  }
  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
     $this->load->view('login');
   }
   else
   {
    $role= $this->project_mng_model->get_role();
    $user = $this->ion_auth->user()->row();
    $data = array(
     "username" => $user->username,
     "role" => $role
   );
    $this->load->view('projectmanager',$data);
  }
}

public function datatable_project(){

 $start    = $this->input->post('start');
 $length   = $this->input->post('length');
 $search   = $this->input->post('search[value]');

 $pageData = $this->project_mng_model->datatable_project($search, $length, $start);
 $allData  = $this->project_mng_model->datatable_project($search);

 $data = array(
   'draw'              =>  $this->input->post('draw'),
   'recordsTotal'      =>  count($allData),
   'recordsFiltered'   =>  count($pageData),
   'data' => $pageData
 );

 header('Content-Type: application/json');
 echo json_encode($data);
}

public function do_save_project()
{
  $response = get_ajax_response();
  $table = 'projects';
  $getAllData= array( 'name' => $this->input->post("name"),
   'description' => $this->input->post("description"),
   'start_date' => date('Y-m-d', strtotime($this->input->post('start_date'))),
   'due_date' => date('Y-m-d', strtotime($this->input->post('due_date'))),
   'due_date_revised' => $this->isempty_due_date_revised($this->input->post('due_date_revised')),
 );
  $this->form_validation->set_rules('name', 'Project Name', 'required|max_length[20]|is_unique[projects.name]',
    array('is_unique'     => 'This %s already exists.')
  );
  $this->form_validation->set_rules('description', 'description', 'max_length[100]');
  $this->form_validation->set_rules('start_date', 'start_date', 'required|isValidDate|compareToEndDate['.$this->input->post('due_date').']', 
    array('isValidDate' => 'This %s invalid date.', 'compareToEndDate' => 'Start Date must be less than or equal to Due Date.'));
  $this->form_validation->set_rules('due_date', 'due_date', 'required|isValidDate|compareToStartDate['.$this->input->post('start_date').']', 
    array('isValidDate' => 'This %s invalid date.', 'compareToStartDate' => 'Due Date must be greater than or equal to Start Date.'));
  $this->form_validation->set_rules('due_date_revised', 'due_date_revised', 'isValidDate|compareToStartDate['.$this->input->post('due_date').']', 
    array('isValidDate' => 'This %s invalid date.', 'compareToStartDate' => 'Due Date Revised must be greater than or equal to Due Date.'));


  if ($this->form_validation->run()==TRUE) {
    $save = $this->project_mng_model->add_project($getAllData);
    $data["status"] = true;
    $data["message"]=$this->lang->line('success');
    $data["data"]=$getAllData;
  }else {
    $data["status"] = false;
    $data["message"]= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
  }
  header("Content-Type: application/json");
  echo json_encode($data);
}
public function get_project()
{
  $table = 'projects';
  $user_id = $this->input->post("user_id");
  $get_data = $this->project_mng_model->view_project($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["id"]=$get_data;
   $data["message"]=$this->lang->line('get_invalid_id');
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 }else{
   $getAllData = $this->project_mng_model->view_project($user_id);
   $data = array("data"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$getAllData[0];
      // var_dump($getAllData);
      // echo $getAllData[0]['DATE(start_date)'];

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
public function do_update_project()
{
  $response=get_ajax_response();
  $user_id = $this->input->post("user_id");

  $get_data = $this->project_mng_model->view_project($user_id);
  $password=$this->input->post("password");

  $getAllData= array( 'name' => $this->input->post("name"),
   'description' => $this->input->post("description"),
   'start_date' => date('Y-m-d', strtotime($this->input->post('start_date'))),
   'due_date' => date('Y-m-d', strtotime($this->input->post('due_date'))),
   'due_date_revised' => $this->isempty_due_date_revised($this->input->post('due_date')),
 );
  $this->form_validation->set_rules('name', 'name', 'required|max_length[20]');
  $this->form_validation->set_rules('description', 'description', 'max_length[100]');
  $this->form_validation->set_rules('start_date', 'start_date', 'required|isValidDate|compareToEndDate['.$this->input->post('due_date').']', 
    array('isValidDate' => 'This %s invalid date.', 'compareToEndDate' => 'Start Date must be less than or equal to Due Date.'));
  $this->form_validation->set_rules('due_date', 'due_date', 'required|isValidDate|compareToStartDate['.$this->input->post('start_date').']', 
    array('isValidDate' => 'This %s invalid date.', 'compareToStartDate' => 'Due Date must be greater than or equal to Start Date.'));
  $this->form_validation->set_rules('due_date_revised', 'due_date_revised', 'isValidDate|compareToStartDate['.$this->input->post('due_date').']', 
    array('isValidDate' => 'This %s invalid date.', 'compareToStartDate' => 'Due Date Revised must be greater than or equal to Due Date.'));
  if(empty($get_data)){
    $data["status"] = false;
    $data["message"]=$this->lang->line('edit_invalid_id');
    $data["id"]=$user_id;
    $data["data"]=array();

    header("Content-Type: application/json");
    echo json_encode($data);
  }else{
    if ($this->form_validation->run()==TRUE) {
      $update = $this->project_mng_model->update_project($user_id,$getAllData);
      $data["status"] = true;
      $data["message"]=$this->lang->line('success');
      $data["data"]=$getAllData;
    }else {
      $data["status"] = false;
      $data["message"]= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
    }
    header("Content-Type: application/json");
    echo json_encode($data);
  }
}
public function do_delete_project()
{
  $user_id = $this->input->post("user_id");

  $get_data = $this->project_mng_model->view_project($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["message"]=$this->lang->line('delete_invalid_id');
   $data["id"]=$user_id;
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);

 }else{
   $delete = $this->project_mng_model->delete_project($user_id);
   $data = array("user_id"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');;
   $data["data"]=$delete;

			// $this->load->view("Auth_view",$data);
   header("Content-Type: application/json");
   echo json_encode($data);
 }
}

public function datatable_category(){

 $start    = $this->input->post('start');
 $length   = $this->input->post('length');
 $search   = $this->input->post('search[value]');

 $pageData = $this->project_mng_model->datatable_category($search, $length, $start);
 $allData  = $this->project_mng_model->datatable_category($search);

 $data = array(
   'draw'              =>  $this->input->post('draw'),
   'recordsTotal'      =>  count($allData),
   'recordsFiltered'   =>  count($pageData),
   'data' => $pageData
 );

 header('Content-Type: application/json');
 echo json_encode($data);
}

public function do_save_category()
{
  $response = get_ajax_response();
  $getAllData= array( 'name' => $this->input->post("name"),
   'description' => $this->input->post("description"),
   'role' => $this->input->post("role_id")
 );
  $this->form_validation->set_rules('name', 'name', 'required|max_length[100]|is_category_name_unique[task_category.name.'.$this->input->post("role_id").']');
  $this->form_validation->set_rules('description', 'description', 'max_length[100]');

  if ($this->form_validation->run()==TRUE) {
    $save = $this->project_mng_model->add_category($getAllData);
    $data["status"] = true;
    $data["message"]=$this->lang->line('success');
    $data["data"]=$getAllData;
  }else {
    $data["message"]= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
  }
  header("Content-Type: application/json");
  echo json_encode($data);
}
public function get_category()
{
  $user_id = $this->input->post("user_id");
  $get_data = $this->project_mng_model->view_category($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["id"]=$get_data;
   $data["message"]=$this->lang->line('get_invalid_id');;
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 }else{
   $getAllData = $this->project_mng_model->view_category($user_id);
   $name = $this->to_get_role_name($getAllData[0]['role']);
   $getAllData[0]['rolename']=$name;

   $data = array("data"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');;
   $data["data"]=$getAllData[0];

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
public function do_update_category()
{
  $response=get_ajax_response();
  $user_id = $this->input->post("user_id");

  $get_data = $this->project_mng_model->view_category($user_id);
  $password=$this->input->post("password");

  $getAllData= array( 'name' => $this->input->post("name"),
   'description' => $this->input->post("description"),
   'role' => $this->input->post("role_id")
 );
  $this->form_validation->set_rules('name', 'name', 'required|max_length[100]');
  $this->form_validation->set_rules('description', 'description', 'max_length[100]');

  if(empty($get_data)){
    $data["status"] = false;
    $data["message"]=$this->lang->line('edit_invalid_id');;
    $data["id"]=$user_id;
    $data["data"]=array();

    header("Content-Type: application/json");
    echo json_encode($data);
  }else{
    if ($this->form_validation->run()==TRUE) {
      $update = $this->project_mng_model->update_category($user_id,$getAllData);
      $data["status"] = true;
      $data["message"]=$this->lang->line('success');;
      $data["data"]=$getAllData;
    }else {
      $data["status"] = false;
      $data["message"]= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
    }
    header("Content-Type: application/json");
    echo json_encode($data);
  }
}
public function do_delete_category()
{
  $user_id = $this->input->post("user_id");

  $get_data = $this->project_mng_model->view_category($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["message"]=$this->lang->line('delete_invalid_id');
   $data["id"]=$user_id;
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);

 }else{
   $delete = $this->project_mng_model->delete_category($user_id);
   $data = array("user_id"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');;
   $data["data"]=$delete;

			// $this->load->view("Auth_view",$data);
   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
public function datatable_status(){
  $start    = $this->input->post('start');
  $length   = $this->input->post('length');
  $search   = $this->input->post('search[value]');

  $pageData = $this->project_mng_model->datatable_status($search, $length, $start);
  $allData  = $this->project_mng_model->datatable_status($search);

  $data = array(
   'draw'              =>  $this->input->post('draw'),
   'recordsTotal'      =>  count($allData),
   'recordsFiltered'   =>  count($pageData),
   'data' => $pageData
 );

  header('Content-Type: application/json');
  echo json_encode($data);
}

public function do_save_status()
{
  $response = get_ajax_response();
  $getAllData= array( 'name' => $this->input->post("name"),
   'description' => $this->input->post("description")
 );
  $this->form_validation->set_rules('name', 'name', 'required|max_length[20]|is_unique[task_status.name]', array('is_unique'     => 'This %s already exists.'));
  $this->form_validation->set_rules('description', 'description', 'max_length[100]');

  if ($this->form_validation->run()==TRUE) {
    $save = $this->project_mng_model->add_status($getAllData);
    $data["status"] = true;
    $data["message"]=$this->lang->line('success');
    $data["data"]=$getAllData;
  }else {
   $data["status"] = false;
   $data["message"]=strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
 }
 header("Content-Type: application/json");
 echo json_encode($data);
}
public function get_status()
{
  $user_id = $this->input->post("user_id");
  $get_data = $this->project_mng_model->view_status($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["id"]=$get_data;
   $data["message"]=$this->lang->line('get_invalid_id');
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 }else{
   $getAllData = $this->project_mng_model->view_status($user_id);
   $data = array("data"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$getAllData[0];

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
public function do_update_status()
{
  $response=get_ajax_response();
  $user_id = $this->input->post("user_id");

  $get_data = $this->project_mng_model->view_status($user_id);
  $password=$this->input->post("password");

  $getAllData= array( 'name' => $this->input->post("name"),
   'description' => $this->input->post("description"),
 );
  $this->form_validation->set_rules('name', 'name', 'required|max_length[20]');
  $this->form_validation->set_rules('description', 'description', 'max_length[100]');

  if(empty($get_data)){
    $data["status"] = false;
    $data["message"]=$this->lang->line('edit_invalid_id');
    $data["id"]=$user_id;
    $data["data"]=array();

    header("Content-Type: application/json");
    echo json_encode($data);
  }else{
    if ($this->form_validation->run()==TRUE) {
      $update = $this->project_mng_model->update_status($user_id,$getAllData);
      $data["status"] = true;
      $data["message"]=$this->lang->line('success');
      $data["data"]=$getAllData;
    }else {
      $data["status"] = false;
      $data["message"]= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
    }
    header("Content-Type: application/json");
    echo json_encode($data);
  }
}
public function do_delete_status()
{
  $user_id = $this->input->post("user_id");

  $get_data = $this->project_mng_model->view_status($user_id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["message"]=$this->lang->line('delete_invalid_id');
   $data["id"]=$user_id;
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);

 }else{
   $delete = $this->project_mng_model->delete_status($user_id);
   $data = array("user_id"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$delete;

			// $this->load->view("Auth_view",$data);
   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
function checkDateFormat($date) {
  if (preg_match("/[0-31]{2}/[0-12]{2}/[0-9]{4}/", $date)) {
    if(checkdate(substr($date, 3, 2), substr($date, 0, 2), substr($date, 6, 4)))
      return true;
    else
      return false;
  } else {
    return false;
  }
}
public function add(){
  $response = get_ajax_response();
  $role= $this->project_mng_model->get_role();
  if(empty($role)){
    $response->status = false;
    $response->code = 400;
  }else {
    $response->status = true;
    $response->code = 200;
    $response->data = $role;
  }

  header("Content-Type: application/json");
  echo json_encode($response);
}
private function to_get_role_name($id){
  $role_name = $this->project_mng_model->get_role_name($id);
  foreach($role_name as $r) {
    $name= $r->name;
    return $name;
  }
}
private function isempty_due_date_revised($input){
  if (!empty($input)) {
    return date('Y-m-d', strtotime($input));
  }
}
private function is_valid_date($input){
  if ($input < '2000-01-01') {
    return '-';
  }else{
    return date('Y-m-d', strtotime($input));
  }
}
}
