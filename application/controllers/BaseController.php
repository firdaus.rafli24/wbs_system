<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BaseController extends CI_Controller
{
  public $user;
  public $theuser;
  public $path_folder = "";
  public $path_controller = "";
  public $path_method = "";
  public $full_path = "";
  public $my_menu = array();
  public $my_config = array();
  public $user_group;
  protected $auth_required = true;
  public $session_id = "";

  function __construct()
  {
    parent::__construct();
    $this->lang->load('wbs_lang');
    $this->load->model("menu_model");
    $this->load->model("menu_config_model");
    $this->load->library('ion_auth');
    $this->load_privilege();

    $this->check_link();
    $this->auth_required - true;
    if ($this->auth_required) {
      if (!$this->ion_auth->logged_in()) {
        if ((!$this->input->is_ajax_request())) {
          redirect('Auth');
        } else {
          $response = get_ajax_response();
          $response->code = 403;
          $response->data = [];
          $response->status = false;
          $response->message = $this->lang->line('api_no_privilege');
          json_response($response);
          die();
        }
      } else {
        $this->check_privilege();
      }
    }
  }

  private function check_privilege()
  {
    $user = $this->ion_auth->user()->row();
    $user_group = $this->ion_auth->get_users_groups($user->id)->result();
    $user_group = $user_group[0];
    $menu = $this->menu_model->getMenuList();
    $controller = $this->path_folder . '/' . $this->path_controller;
    $method = $this->path_method;

    // Check Privilege
    if (!empty($menu)) {
      if (in_array($controller, $menu)) {
        $priv = $this->menu_model->getPrivilege($user_group->id, $controller)->row();
        if (empty($priv->read)) {
          $response = get_ajax_response();
          $response->code = 403;
          $response->data = [];
          $response->status = false;
          $response->message = $this->lang->line('api_no_privilege');
          json_response($response);
          die();
        }
        // If Method is Create and permission is denied
        if ($method == "save" && !$priv->create) {
          $response = get_ajax_response();
          $response->code = 403;
          $response->data = [];
          $response->status = false;
          $response->message = $this->lang->line('api_no_privilege');
          json_response($response);
          die();
          // If Method is Update and permission is denied
        } else if ($method == "update" && !$priv->update) {
          $response = get_ajax_response();
          $response->code = 403;
          $response->data = [];
          $response->status = false;
          $response->message = $this->lang->line('api_no_privilege');
          json_response($response);
          die();
          // If Method is Delete and permission is denied
        } else if ($method == "delete" && !$priv->delete) {
          $response = get_ajax_response();
          $response->code = 403;
          $response->data = [];
          $response->status = false;
          $response->message = $this->lang->line('api_no_privilege');
          json_response($response);
          die();
        }
      }
    }
  }

  public function check_link()
  {

    if (!$this->ion_auth->logged_in()) {
      redirect('Auth');
    }

    $user = $this->ion_auth->user()->row();

    $my_menu = $this->menu_model->get_menu($user->user_type, $user->id);

    $link_user = [];
    foreach ($my_menu as $key => $value) {
      array_push($link_user, $value->link_user);
    }
    array_push($link_user, 'Auth');
    array_push($link_user, 'auth');
    array_push($link_user, 'Home');
    array_push($link_user, 'home');

    $class =  $this->router->fetch_class();
    $method =  $this->router->fetch_method();

    if (!in_array($class, $link_user)) {
      $response = get_ajax_response();
      $response->code = 403;
      $response->status = false;
      $response->message = $this->lang->line('url_no_privilege');

      header('Content-Type: application/json');
      echo json_encode($response);
      exit();
    }
  }



  private function load_privilege()
  {
    if ($this->ion_auth->logged_in()) {
      $user = $this->ion_auth->user()->row();
      if (!empty($user)) {
        $user_group = $this->ion_auth->get_users_groups($user->id)->result();
        $this->user = $user;
        $this->user_group = $user_group;
        $this->session_id = $this->session->userdata('session_id');
        //only 1 group supported
        if (!empty($user_group)) {

          $user_group = $user_group[0];
          $group_id = $user_group->id;
          $my_menu = $this->menu_model->get_menu($group_id, $user->id);
          $my_submenu = $this->menu_model->get_menu($group_id, $user->id);

          //build tree
          $menu_tree = array();
          foreach ($my_menu as $value) {
            if (!array_key_exists($value->id, $menu_tree)) {
              $parent_menu = new stdClass();
              $parent_menu->id = $value->id;
              $parent_menu->name = $value->menu_name;
              $parent_menu->icon = $value->menu_icon;
              $parent_menu->is_selected = $value->link == $this->full_path;
              $parent_menu->link = $value->link;
              $parent_menu->submenu = array();

              foreach ($my_submenu as $child) {
                if ($child->menu_id == $parent_menu->id) {
                  $submenu = new stdClass();
                  $submenu->name = $child->submenu_name;
                  $submenu->link = $child->submenu_link;
                  $submenu->is_selected = $submenu->link == $this->full_path;

                  if ($submenu->is_selected) {
                    $parent_menu->is_selected = true;
                  }
                  array_push($parent_menu->submenu, $submenu);
                }
              }
              $menu_tree[$parent_menu->id] = $parent_menu;
            }
          }
          $this->my_menu = $menu_tree;
          $this->my_config = $this->menu_config_model->get_all();
        }
      } else {
        $this->ion_auth->logout();
        redirect("Auth");
      }
    }
  }
}
