<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Notification extends BaseController {
  function __construct()
  {
    $this->auth_required = false;
    
    //manually assign path for controlelrs in root
    $this->path_controller = "Notification";
    $this->full_path = "Notification";

    parent::__construct();
    $this->load->model('notification_model');
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');    
  }

  
  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
     $this->load->view('login');
   }
   else
   {
    $user = $this->ion_auth->user()->row();


    $data['username']   = $user->username;

    if(in_array($user->user_type, [3, 4, 5])){
      $this->load->view('notification_user_view', $data);
    }
    else {
      $this->load->view('notification_admin_view', $data);
    }
  }
}

public function datatable(){

  $temp = array();
  $list = $this->notification_model->get_datatable();

  if (!empty($list)) {
    foreach ($list as $result) {
      $row = new stdClass();
      $row->new = $result->new;
      $row->id = $result->id;
      $row->date = $result->date;
      $row->type_notification = ($result->type_notification == 1) ? $this->lang->line('NTF007') : $this->lang->line('NTF008');
      $row->category = $result->category;
      $row->title = $result->title;
      $row->description = $result->description;
      $row->created_by = $result->created_by;
      $temp[] = $row;
    }
  }
  $data["data"] = $temp;

  header('Content-Type: application/json');
  echo json_encode($data);
}

public function do_save()
{
  $response = get_ajax_response();

  $getAllData = array( 'date' => $this->input->post("date"),
    'type_notification' => intval($this->input->post('type_notification')),
    'category' => $this->input->post('category'),
    'title' => $this->input->post('title'),
    'description' => $this->input->post('description')
  );
  $getAllData['created_by'] = $this->ion_auth->user()->row()->id;
  $getAllData['updated_by'] = $this->ion_auth->user()->row()->id;

  $this->form_validation->set_rules('date', $this->lang->line('PR002'), 'required');
  $this->form_validation->set_rules('type_notification', $this->lang->line('NTF003'), 'required');
  $this->form_validation->set_rules('category', $this->lang->line('NTF004'), 'required');
  $this->form_validation->set_rules('title', $this->lang->line('NTF005'), 'required');
  $this->form_validation->set_rules('description', $this->lang->line('PR006'), 'required');

  if ($this->form_validation->run()==TRUE) {
    $save = $this->notification_model->add($getAllData);
    $data["status"] = true;
    $data["message"]=$this->lang->line('success');
    $data["data"]=$getAllData;
  } else {
    $data["status"] = false;
    $data["message"]= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));;
    $data["data"]=$getAllData;
  }
  
  header("Content-Type: application/json");
  echo json_encode($data);
}
public function get()
{
  $id = $this->input->post("id");

  $get_data = $this->notification_model->show($id);

  if(empty($get_data)){
   $data["status"] = false;
   $data["id"]=$get_data;
   $data["message"]=$this->lang->line('get_invalid_id');
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 } else {
   $data = array("data"=>$get_data);
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["data"]=$get_data;

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}
public function do_update()
{
  $getAllData = array( 'date' => $this->input->post("date"),
    'type_notification' => intval($this->input->post('type_notification')),
    'category' => $this->input->post('category'),
    'title' => $this->input->post('title'),
    'description' => $this->input->post('description')
  );
  $getAllData['updated_by'] = $this->ion_auth->user()->row()->id;
  $response=get_ajax_response();

  $id = $this->input->post("id");
  $get_data = $this->notification_model->show($id);

  $this->form_validation->set_rules('id', 'ID', 'required|numeric');
  $this->form_validation->set_rules('date', $this->lang->line('PR002'), 'required');
  $this->form_validation->set_rules('type_notification', $this->lang->line('NTF003'), 'required');
  $this->form_validation->set_rules('category', $this->lang->line('NTF004'), 'required');
  $this->form_validation->set_rules('title', $this->lang->line('NTF005'), 'required');
  $this->form_validation->set_rules('description', $this->lang->line('PR006'), 'required');

  if(empty($get_data)){
    $data["status"] = false;
    $data["message"]=$this->lang->line('edit_invalid_id');
    $data["id"]=$id;
    $data["data"]=array();
  } else {
    if ($this->form_validation->run()==TRUE) {
      $update = $this->notification_model->update($id, $getAllData);
      $data["status"] = true;
      $data["message"]=$this->lang->line('success');
      $data["data"]=$getAllData;
    } else {
      $data["status"] = false;
      $data["message"]= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
    }
  }

  header("Content-Type: application/json");
  echo json_encode($data);

}
public function do_delete()
{
  $id = $this->input->post("id");
  $get_data = $this->notification_model->show($id);
  $delete = $this->notification_model->delete($id);

  if(!$delete){
   $data["status"] = false;
   $data["message"]=$this->lang->line('delete_invalid_id');
   $data["id"]=$id;
   $data["data"]=array();

   header("Content-Type: application/json");
   echo json_encode($data);
 }else{
   $data["status"] = true;
   $data["message"]=$this->lang->line('success');
   $data["id"]=$id;
   $data["data"]=$get_data;

   header("Content-Type: application/json");
   echo json_encode($data);
 }
}

public function countNotif()
{
  $count = $this->notification_model->get_notif();
  echo (count($count));
}

public function viewNotification()
{
  $data = [
    'user_id' => $this->session->userdata('user_id'),
    'notification_id' => $this->input->post("id")
  ];
  $this->notification_model->addNotificationView($data);
}


}
