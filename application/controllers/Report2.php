<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Report2 extends BaseController {
  function __construct()
  {
    $this->auth_required = false;

      //manually assign path for controlelrs in root
    $this->path_controller = "Report2";
    $this->full_path = "Report2";

    parent::__construct();
      //load models
    $this->load->model("report2_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }
  
  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
      $this->load->view('login');
    }
    else
    {
      $user = $this->ion_auth->user()->row();
      $project = $this->report2_model->get_project();
      $user_group = $this->ion_auth->get_users_groups($user->id)->result()[0]->name;
      $projectData = array();

      $this->load->model('user_mng_model');
      $role = $this->report2_model->get_role();

      foreach ($project as $value) {
        array_push($projectData, $value->name);
      }

      $data = array(
        "project" => $projectData,
        "role" => $role,
        "message" => "success",
        "status" => true,
        "username" => $user->username,
        "user_group" => $user_group,
        "user_type" => $user->user_type
      );
      $this->load->view('report2',$data);
    }
  }

  public function datar2()
  {
    $post = $this->input->post();

    $search     = $this->input->post("search[value]");
    $start_date = $this->input->post("start_date");
    $stop_date  = $this->input->post("stop_date");
    $role       = $this->input->post("role");
    /*$role       = 10;
    $start_date = date('Y-m-01', strtotime(date('Y-m-d')));
    $stop_date  = date('Y-m-t', strtotime(date('Y-m-d')));*/

    $user = $this->ion_auth->user()->row();
    $project = $this->report2_model->get_project();
    $nameCat = $this->report2_model->get_user($role, $search);
    $qchour = $this->report2_model->get_hour($role, $start_date, $stop_date);

    if(count($qchour) < 1){
      $data["status"] = false;
      $data["message"] = $this->lang->line('not_found');
      header("Content-Type: application/json");
      echo json_encode($data);
      return;
    }

    $nameCatarr = array();
    $projectData = array();
    foreach ($nameCat as $value) {
      array_push($nameCatarr, $value->username);
    }

    foreach ($project as $value) {
      $arrayCat[$value->name] = 0;
      array_push($projectData, $value->name);
    }
    $projectQC = array_fill_keys($projectData, 0);
    $dataCat = array_fill_keys($nameCatarr, $projectQC);
    $dataTotalRow = [];

    foreach ($qchour as $key => $value) {
      $pname = $value->name;
      if (array_key_exists($value->username, $dataCat)){
        $dataCat[$value->username][$pname] = $dataCat[$value->username][$pname] + $value->actual_sum;
      }

      if( !isset($dataTotalRow[$pname]) ) {
        $dataTotalRow[$pname] = 0;
      }
      $dataTotalRow[$pname] += $value->actual_sum;

    }
    $dataTotalRow = array_merge($arrayCat, $dataTotalRow);
    $dataTotalRow['total'] = array_sum($dataTotalRow);

    $data = array(
      "datar2" => $dataCat,
      "dataTotalRow" => $dataTotalRow,
      "project" => $projectData,
      "message" => "success",
      "status" => true
    );
    header('Content-Type: application/json');
    echo json_encode($data);
  }



}
