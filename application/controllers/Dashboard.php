<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Dashboard extends BaseController {

  const HRD_ID = 2;
  const PG_ID = 3;
  const QC_ID = 4;
  const SA_ID = 5;

  function __construct()
  {
    $this->auth_required = false;
    $this->path_controller = "Dashboard";
    $this->full_path = "Dashboard";

    parent::__construct();
    $this->load->model("dashboard_model");
    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }
  
  public function index()
  {
    if (!$this->ion_auth->logged_in())
    {
      $this->load->view('login');
    }
    else
    {
      $user = $this->ion_auth->user()->row();
      $project = $this->dashboard_model->get_project();
      $user_group = $this->ion_auth->get_users_groups($user->id)->result()[0]->name;
      $projectData = array();

      $this->load->model('user_mng_model');
      $role = $this->user_mng_model->get_role();
      $userData = $this->user_mng_model->datatable();

      foreach ($project as $value) {
        array_push($projectData, $value->name);
      }

      $data = array(
        "project" => $projectData,
        "role" => $role,
        "user" => $userData,
        "message" => "success",
        "status" => true,
        "username" => $user->username,
        "user_group" => $user_group,
        "user_type" => $user->user_type
      );
      $this->load->view('dashboard',$data);
    }
  }

  public function changerole()
  {
    $post = $this->input->post();
    if(isset($post['id'])){
      $dataUser = $this->dashboard_model->get_user($post['id']);
      header('Content-Type: application/json');
      echo json_encode($dataUser); 
    } else {
      header('Content-Type: application/json');
      echo json_encode(0);
    }

  }

  public function data_hrd()
  {
    $post = $this->input->post();
    $hrd_id  = self::HRD_ID;

    if(in_array($hrd_id, $post['role']) || $post['allRole'] == 1){

      if( empty($post['user'][0]) ){
        $inUser = "1";
      } else {
        $inUser = $post['user'];
      }

      $this->load->model('dashboard_model');
      $search     = $this->input->post("search[value]");
      $start_date = $this->input->post("start_date");
      $stop_date = $this->input->post("stop_date");

      $user = $this->ion_auth->user()->row();
      $project = $this->dashboard_model->get_project();
      $nameHRD = $this->dashboard_model->get_name($hrd_id, $search, $inUser);
      $HRDhour = $this->dashboard_model->get_hour($start_date, $stop_date,);

      $nameHRDarr = array();
      $projectData = array();
      foreach ($nameHRD as $value) {
        array_push($nameHRDarr, $value->username);
      }
      foreach ($project as $value) {
        array_push($projectData, $value->name);
      }
      $projectHRD = array_fill_keys($projectData, 0);
      $dataHRD = array_fill_keys($nameHRDarr, $projectHRD);

      foreach ($HRDhour as $key => $value) {
        $pname = $value->name;
        if (array_key_exists($value->username, $dataHRD)){
          $dataHRD[$value->username][$pname] = $dataHRD[$value->username][$pname] + $value->actual_sum;
        }
      }

      $data = array(
        "data_hrd" => $dataHRD,
        "project" => $projectData,
        "message" => "success",
        "status" => true
      );
      header('Content-Type: application/json');
      echo json_encode($data);
    } else {
      header('Content-Type: application/json');
      echo json_encode(0);
    }
  }

  public function data_pg()
  {
    $post = $this->input->post();
    $pg_id  = self::PG_ID;

    if(in_array($pg_id, $post['role']) || $post['allRole'] == 1){

      if( empty($post['user'][0]) ){
        $inUser = "1";
      } else {
        $inUser = $post['user'];
      }

      $this->load->model('dashboard_model');
      $search     = $this->input->post("search[value]");
      $start_date = $this->input->post("start_date");
      $stop_date = $this->input->post("stop_date");

      $user = $this->ion_auth->user()->row();
      $project = $this->dashboard_model->get_project();
      $namePG = $this->dashboard_model->get_name($pg_id, $search, $inUser);
      $pghour = $this->dashboard_model->get_hour($start_date, $stop_date,);

      $namePGarr = array();
      $projectData = array();
      foreach ($namePG as $value) {
        array_push($namePGarr, $value->username);
      }
      foreach ($project as $value) {
        array_push($projectData, $value->name);
      }
      $projectPG = array_fill_keys($projectData, 0);
      $dataPG = array_fill_keys($namePGarr, $projectPG);

      foreach ($pghour as $key => $value) {
        $pname = $value->name;
        if (array_key_exists($value->username, $dataPG)){
          $dataPG[$value->username][$pname] = $dataPG[$value->username][$pname] + $value->actual_sum;
        }
      }

      $data = array(
        "data_pg" => $dataPG,
        "project" => $projectData,
        "message" => "success",
        "status" => true
      );
      header('Content-Type: application/json');
      echo json_encode($data);
    } else {
      header('Content-Type: application/json');
      echo json_encode(0);
    }
  }

  public function data_qc()
  {
    $post = $this->input->post();
    $qc_id  = self::QC_ID;

    if(in_array($qc_id, $post['role']) || $post['allRole'] == 1 ){

      if( empty($post['user'][0]) ){
        $inUser = "1";
      } else {
        $inUser = $post['user'];
      }

      $this->load->model('dashboard_model');
      $search     = $this->input->post("search[value]");
      $start_date = $this->input->post("start_date");
      $stop_date  = $this->input->post("stop_date");

      $user = $this->ion_auth->user()->row();
      $project = $this->dashboard_model->get_project();
      $nameQC = $this->dashboard_model->get_name($qc_id, $search, $inUser);
      $qchour = $this->dashboard_model->get_hour($start_date, $stop_date);

      $nameQCarr = array();
      $projectData = array();
      foreach ($nameQC as $value) {
        array_push($nameQCarr, $value->username);
      }
      foreach ($project as $value) {
        array_push($projectData, $value->name);
      }
      $projectQC = array_fill_keys($projectData, 0);
      $dataQC = array_fill_keys($nameQCarr, $projectQC);

      foreach ($qchour as $key => $value) {
        $pname = $value->name;
        if (array_key_exists($value->username, $dataQC)){
          $dataQC[$value->username][$pname] = $dataQC[$value->username][$pname] + $value->actual_sum;
        }
      }

      $data = array(
        "data_qc" => $dataQC,
        "project" => $projectData,
        "message" => "success",
        "status" => true
      );
      header('Content-Type: application/json');
      echo json_encode($data);
    } else {
      header('Content-Type: application/json');
      echo json_encode(0);
    }
  }

  public function data_sa()
  {
    $post   = $this->input->post();
    $sa_id  = self::SA_ID;

    if(in_array($sa_id, $post['role']) || $post['allRole'] == 1){

      if( empty($post['user'][0]) ){
        $inUser = "1";
      } else {
        $inUser = $post['user'];
      }

      $this->load->model('dashboard_model');
      $search     = $this->input->post("search[value]");
      $start_date = $this->input->post("start_date");
      $stop_date = $this->input->post("stop_date");

      $user = $this->ion_auth->user()->row();
      $project = $this->dashboard_model->get_project();
      $nameSA = $this->dashboard_model->get_name($sa_id, $search, $inUser);
      $sahour = $this->dashboard_model->get_hour($start_date, $stop_date,);

      $nameSAarr = array();
      $projectData = array();
      foreach ($nameSA as $value) {
        array_push($nameSAarr, $value->username);
      }
      foreach ($project as $value) {
        array_push($projectData, $value->name);
      }
      $projectSA = array_fill_keys($projectData, 0);
      $dataSA = array_fill_keys($nameSAarr, $projectSA);

      foreach ($sahour as $key => $value) {
        $pname = $value->name;
        if (array_key_exists($value->username, $dataSA)){
          $dataSA[$value->username][$pname] = $dataSA[$value->username][$pname] + $value->actual_sum;
        }
      }

      $data = array(
        "data_sa" => $dataSA,
        "project" => $projectData,
        "message" => "success",
        "status" => true
      );
      header('Content-Type: application/json');
      echo json_encode($data);
    } else {
      header('Content-Type: application/json');
      echo json_encode(0);
    }
  }

}
