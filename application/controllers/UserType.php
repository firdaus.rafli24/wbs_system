<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once(APPPATH."controllers/BaseController.php");

class Usertype extends BaseController {
  function __construct()
  {
      $this->auth_required = false;

      //manually assign path for controlelrs in root
      $this->path_controller = "UserType";
      $this->full_path = "UserType";
      parent::__construct();
      //load models
      // $this->load->model("Ion_auth_model");
      $this->load->model("user_type_model");
      $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
  }
  
  public function index()
  {
    if (!$this->ion_auth->logged_in())
		{
			$this->load->view('login');
		}
		else
		{
      $user = $this->ion_auth->user()->row();
      $theuser['username']=$user->username;
      $this->load->view('usertype', $theuser);
		}
  }
  public function datatable(){
  $this->load->model('user_type_model');
  $userData = $this->user_type_model->datatable('groups')->result();
  $newRowData = array();
  foreach ($userData as $key => $value) {
  		$row = new StdClass();
      $row->id = $value->id;
  		$row->name = $value->name;
  		$row->description = $value->description;
  		array_push($newRowData, $row);
  }

  $data = array(
  		"data" => $newRowData
  );

  header('Content-Type: application/json');
  echo json_encode($data);
  }
  public function do_save()
  {
    $response = get_ajax_response();
  	$this->load->model('user_type_model');
  	$getAllData= array( 'name' => $this->input->post("name"),
  											'description' => $this->input->post("description")
                      );
    $this->form_validation->set_rules('name', 'name', 'required|max_length[20]|is_unique[groups.name]',array('is_unique' => 'This %s already exists.'));
    $this->form_validation->set_rules('description', 'description', 'max_length[100]');

    if ($this->form_validation->run()==TRUE) {
      $save = $this->user_type_model->add($getAllData);
  		$data["status"] = true;
  		$data["message"]=$this->lang->line('success');
  		$data["data"]=$getAllData;

  		header("Content-Type: application/json");
  		echo json_encode($data);
    }else {
      $data["status"] = false;
  		$data["message"]= strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));;
  		$data["data"]=$getAllData;

  		header("Content-Type: application/json");
  		echo json_encode($data);
    }
  }
  public function get()
	{
		$user_id = $this->input->post("user_id");

		$this->load->model('user_type_model');
		$get_data = $this->user_type_model->view($user_id);

		if(empty($get_data)){
			$data["status"] = false;
			$data["id"]=$get_data;
			$data["message"]=$this->lang->line('get_invalid_id');
			$data["data"]=array();

			header("Content-Type: application/json");
			echo json_encode($data);
		}else{
			$getAllData = $this->user_type_model->view($user_id);
			$data = array("data"=>$get_data);
			$data["status"] = true;
			$data["message"]=$this->lang->line('success');
			$data["data"]=$getAllData[0];

			header("Content-Type: application/json");
			echo json_encode($data);
		}
	}
  public function do_update()
  {
    $response=get_ajax_response();
    $user_id = $this->input->post("user_id");

  	$this->load->model('user_type_model');
  	$get_data = $this->user_type_model->view($user_id);
    $password=$this->input->post("password");

    $getAllData= array( 'name' => $this->input->post("name"),
  											'description' => $this->input->post("description"),
  	                  );
    $this->form_validation->set_rules('name', 'name', 'required|max_length[20]');
    $this->form_validation->set_rules('description', 'description', 'max_length[100]');

    if(empty($get_data)){
        $data["status"] = false;
        $data["message"]=$this->lang->line('edit_invalid_id');
        $data["id"]=$user_id;
        $data["data"]=array();

        header("Content-Type: application/json");
        echo json_encode($data);
    }else{
      if ($this->form_validation->run()==TRUE) {
        $update = $this->user_type_model->update($user_id,$getAllData);
    		$data["status"] = true;
    		$data["message"]=$this->lang->line('success');
    		$data["data"]=$getAllData;

    		header("Content-Type: application/json");
    		echo json_encode($data);
      }
    }
  }
  public function do_delete()
	{
		$user_id = $this->input->post("user_id");
    $this->load->model('user_type_model');
		$get_data = $this->user_type_model->view($user_id);

		if(($user_id==1)||($user_id==2)||(empty($get_data))){
			$data["status"] = false;
			$data["message"]=$this->lang->line('delete_invalid_id');
			$data["id"]=$user_id;
			$data["data"]=array();

			header("Content-Type: application/json");
			echo json_encode($data);
		}else{
			$delete = $this->user_type_model->delete($user_id);
			$data = array("user_id"=>$get_data);
			$data["status"] = true;
			$data["message"]=$this->lang->line('success');
			$data["data"]=$delete;

			// $this->load->view("Auth_view",$data);
			header("Content-Type: application/json");
			echo json_encode($data);
		}
	}
}
