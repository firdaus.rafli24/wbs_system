<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once(APPPATH . "controllers/BaseController.php");

class InventoryManagement extends BaseController
{
    function __construct()
    {
        $this->auth_required = false;

        $this->path_controller = "InventoryManagement";
        $this->full_path = "InventoryManagement";

        parent::__construct();
        $this->load->model("user_type_model");
        $this->load->model('inventory_model');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->load->view('login');
        } else {
            $user = $this->ion_auth->user()->row();
            $data['username']   = $user->username;
            $this->load->view('inventory_management_view', $data);
        }
    }
    public function datatable()
    {
        $start    = $this->input->post('start');
        $length   = $this->input->post('length');
        $search   = $this->input->post('search[value]');
        $pageData = $this->inventory_model->datatable($search, $length, $start);
        $allData  = $this->inventory_model->datatable($search);
        $data = array(
            'draw'              =>  $this->input->post('draw'),
            'recordsTotal'      =>  count($allData),
            'recordsFiltered'   =>  count($pageData),
            'data' => $pageData
        );

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function do_save()
    {
        $response = get_ajax_response();

        $getAllData =  $this->input->post();

        $this->form_validation->set_rules('inventory_name', $this->lang->line('IM001'), 'max_length[35]');

        if ($this->form_validation->run() == TRUE) {
            $save = $this->inventory_model->add($getAllData);
            $data["status"] = true;
            $data["message"] = $this->lang->line('success');
            $data["data"] = $getAllData;
        } else {
            $data["status"] = false;
            $data["message"] = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));;
            $data["data"] = $getAllData;
        }

        header("Content-Type: application/json");
        echo json_encode($data);
    }
    public function get()
    {
        $id = $this->input->post("id");

        $get_data = $this->inventory_model->show($id);

        if (empty($get_data)) {
            $data["status"] = false;
            $data["id"] = $get_data;
            $data["message"] = $this->lang->line('get_invalid_id');
            $data["data"] = array();

            header("Content-Type: application/json");
            echo json_encode($data);
        } else {
            $data = array("data" => $get_data);
            $data["status"] = true;
            $data["message"] = $this->lang->line('success');
            $data["data"] = $get_data;

            header("Content-Type: application/json");
            echo json_encode($data);
        }
    }
    public function do_update()
    {
        $getAllData =  $this->input->post();
        $response = get_ajax_response();
        $id = $this->input->post("id");

        $get_data = $this->inventory_model->show($id);

        $this->form_validation->set_rules('inventory_name', $this->lang->line('IM001'), 'max_length[35]');

        if (empty($get_data)) {
            $data["status"] = false;
            $data["message"] = $this->lang->line('edit_invalid_id');
            $data["id"] = $id;
            $data["data"] = array();
        } else {
            if ($this->form_validation->run() == TRUE) {
                $update = $this->inventory_model->update($id, $getAllData);
                $data["status"] = true;
                $data["message"] = $this->lang->line('success');
                $data["data"] = $getAllData;
            } else {
                $data["status"] = false;
                $data["message"] = strip_tags((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));
            }
        }

        header("Content-Type: application/json");
        echo json_encode($data);
    }
    public function do_delete()
    {
        $id = $this->input->post("id");

        $get_data = $this->inventory_model->show($id);
        $delete = $this->inventory_model->delete($id);

        if (!$delete) {
            $data["status"] = false;
            $data["message"] = $this->lang->line('delete_invalid_id');
            $data["id"] = $id;
            $data["data"] = array();

            header("Content-Type: application/json");
            echo json_encode($data);
        } else {
            $data["status"] = true;
            $data["message"] = $this->lang->line('success');
            $data["id"] = $id;
            $data["data"] = $get_data;

            header("Content-Type: application/json");
            echo json_encode($data);
        }
    }
}
