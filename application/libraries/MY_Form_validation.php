<?php
class MY_Form_validation extends CI_Form_validation
{
  public function edit_unique($str, $field)
  {
    sscanf($field, '%[^.].%[^.].%[^.]', $table, $field, $id);
    $this->CI->form_validation->set_message('edit_unique', $this->CI->lang->line('form_validation_is_exist'));
    return isset($this->CI->db)
      ? ($this->CI->db->limit(1)->get_where($table, array($field => $str, 'id !=' => $id))->num_rows() === 0)
      : FALSE;
  }

  public function is_category_name_unique($str, $field)
  {
    sscanf($field, '%[^.].%[^.].%[^.]', $table, $field, $role);
    $this->CI->form_validation->set_message('is_category_name_unique', $this->CI->lang->line('form_validation_is_exist'));
    return isset($this->CI->db)
      ? ($this->CI->db->limit(1)->get_where($table, array($field => $str, 'role' => $role))->num_rows() === 0)
      : FALSE;
  }
  public function compareToEndDate($startDate, $endDate)
  {
    if (strtotime($endDate) >= strtotime($startDate)) {
      return True;
    } else {
      return False;
    }
  }
  public function compareToStartDate($endDate, $startDate)
  {
    if (strtotime($endDate) >= strtotime($startDate)) {
      return True;
    } else {
      return False;
    }
  }
  public function isValidDate($date) {
    return date('Y-m-d', strtotime($date)) === $date;
  }
}
