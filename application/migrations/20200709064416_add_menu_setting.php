<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_menu_setting extends CI_Migration {

	public function up()
	{
    	// insert data menu : configs
		$this->db->query("INSERT INTO `menu` (`id`, `menu_name`, `menu_icon`, `link`)
			VALUES
			(9, 'Menu000', 'fa-cogs', 'Config')
			");

    	// insert data privilege : configs
		$this->db->query("
			INSERT INTO `privilege` (`menu_id`, `submenu_id`, `group_id`, `create`, `update`, `delete`, `read`, `upload`, `download`)
			VALUES
			(9, NULL, 1, 1, 1, 1, 1, 1, 1);
			");

    	// drop if exist table : configs
		$this->db->query("DROP TABLE IF EXISTS configs");

    	// create table : configs
		$this->db->query("
			CREATE TABLE `configs` (
			`config_key` varchar(200) NOT NULL DEFAULT '',
			`config_enable` int(1) NOT NULL DEFAULT '1',
			`config_value` varchar(200) DEFAULT '0',
			`config_description` text
			) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;
			");

		// insert data table : configs
		$this->db->query("
			INSERT INTO `configs` (`config_key`, `config_enable`, `config_value`, `config_description`)
			VALUES
			('PAST_ACTIVITY_DATE', 1, '2', NULL);

			");
	}

	public function down()
	{

	}
}
