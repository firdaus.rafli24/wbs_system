<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_menu_change_password extends CI_Migration {

    public function up()
    {
      $this->db->query("INSERT INTO `menu` (`id`, `menu_name`, `menu_icon`, `link`)
  			VALUES
  			(10, 'Menu009', 'fa-cogs', 'ChangePassword')
  			");

      	// insert data privilege : configs
  		$this->db->query("
  			INSERT INTO `privilege` (`menu_id`, `submenu_id`, `group_id`, `create`, `update`, `delete`, `read`, `upload`, `download`)
  			VALUES
        (10, NULL, 1, 1, 1, 1, 1, 1, 1),
  			(10, NULL, 2, 1, 1, 1, 1, 1, 1),
  			(10, NULL, 3, 1, 1, 1, 1, 1, 1),
  			(10, NULL, 4, 1, 1, 1, 1, 1, 1),
  			(10, NULL, 5, 1, 1, 1, 1, 1, 1);
  			");
    }

    public function down()
    {

    }
}
