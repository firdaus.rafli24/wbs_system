<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Set_default_privilege extends CI_Migration {

	public function up()
	{
		$this->db->query("TRUNCATE TABLE privilege");
		$this->db->query("TRUNCATE TABLE privilege_user");
		$this->db->query("INSERT INTO `privilege` (`menu_id`, `submenu_id`, `group_id`, `create`, `update`, `delete`, `read`, `upload`, `download`)
			VALUES
			(8, 5, 1, 1, 1, 1, 1, 1, 1),
			(8, 6, 1, 1, 1, 1, 1, 1, 1),
			(3, NULL, 2, 1, 1, 1, 1, 1, 1),
			(4, NULL, 2, 1, 1, 1, 1, 1, 1),
			(5, 3, 2, 1, 1, 1, 1, 1, 1),
			(5, 4, 2, 1, 1, 1, 1, 1, 1),
			(6, NULL, 2, 1, 1, 1, 1, 1, 1),
			(8, 6, 2, 1, 1, 1, 1, 1, 1),
			(8, 5, 2, 1, 1, 1, 1, 1, 1),
			(8, 5, 3, 1, 1, 1, 1, 1, 1),
			(8, 6, 3, 1, 1, 1, 1, 1, 1),
			(5, 3, 3, 1, 1, 1, 1, 1, 1),
			(5, 4, 3, 1, 1, 1, 1, 1, 1),
			(6, NULL, 3, 1, 1, 1, 1, 1, 1),
			(4, NULL, 3, 1, 1, 1, 1, 1, 1),
			(8, 5, 4, 1, 1, 1, 1, 1, 1),
			(8, 6, 4, 1, 1, 1, 1, 1, 1),
			(5, 4, 4, 1, 1, 1, 1, 1, 1),
			(6, NULL, 4, 1, 1, 1, 1, 1, 1),
			(4, NULL, 4, 1, 1, 1, 1, 1, 1),
			(8, 5, 5, 1, 1, 1, 1, 1, 1),
			(8, 6, 5, 1, 1, 1, 1, 1, 1),
			(5, 4, 5, 1, 1, 1, 1, 1, 1),
			(6, NULL, 5, 1, 1, 1, 1, 1, 1),
			(4, NULL, 5, 1, 1, 1, 1, 1, 1),
			(1, NULL, 1, 1, 1, 1, 1, 1, 1),
			(1, NULL, 2, 1, 1, 1, 1, 1, 1),
			(1, NULL, 3, 1, 1, 1, 1, 1, 1),
			(1, NULL, 4, 1, 1, 1, 1, 1, 1),
			(1, NULL, 5, 1, 1, 1, 1, 1, 1),
			(2, 1, 1, 1, 1, 1, 1, 1, 1),
			(2, 1, 2, 1, 1, 1, 1, 1, 1),
			(2, 7, 1, 1, 1, 1, 1, 1, 1);
			");
	}

	public function down()
	{

	}
}
