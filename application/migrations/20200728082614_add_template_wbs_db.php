<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_template_wbs_db extends CI_Migration {

    public function up()
    {
      // create table template_wbs_name
      $this->db->query("DROP TABLE IF EXISTS template_wbs_name");
      $this->db->query("CREATE TABLE `template_wbs_name` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `name` varchar(200) NOT NULL DEFAULT '',
          `user_id` int(11) NOT NULL,
          PRIMARY KEY (`id`) USING BTREE,
          KEY `user_id` (`user_id`),
          CONSTRAINT `template_wbs_name_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;");

      // create table template_wbs_detail
      $this->db->query("DROP TABLE IF EXISTS template_wbs_detail");
      $this->db->query("CREATE TABLE `template_wbs_detail` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `template_wbs_id` int(11) NOT NULL,
          `project` int(11) NOT NULL,
          `category` int(11) NOT NULL,
          `task_name` varchar(100) DEFAULT NULL,
          `pic` int(11) NOT NULL,
          `excecutedby` int(11) NOT NULL,
          `start_time` time DEFAULT NULL,
          `stop_time` time DEFAULT NULL,
          `start_date` datetime DEFAULT NULL,
          `due_date` datetime DEFAULT NULL,
          `due_date_revised` datetime DEFAULT NULL,
          `estimated_hour` float DEFAULT NULL,
          `actual_hour` float DEFAULT NULL,
          `task_percentage` float DEFAULT NULL,
          `task_status` int(11) NOT NULL,
          `notes` text,
          `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`) USING BTREE,
          KEY `pic` (`pic`),
          KEY `excecutedby` (`excecutedby`),
          KEY `project` (`project`),
          KEY `category` (`category`),
          KEY `task_status` (`task_status`),
          KEY `template_wbs_id` (`template_wbs_id`),
          CONSTRAINT `template_wbs_detail_ibfk_1` FOREIGN KEY (`pic`) REFERENCES `users` (`id`) ON DELETE CASCADE,
          CONSTRAINT `template_wbs_detail_ibfk_2` FOREIGN KEY (`excecutedby`) REFERENCES `users` (`id`) ON DELETE CASCADE,
          CONSTRAINT `template_wbs_detail_ibfk_3` FOREIGN KEY (`project`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
          CONSTRAINT `template_wbs_detail_ibfk_4` FOREIGN KEY (`category`) REFERENCES `task_category` (`id`) ON DELETE CASCADE,
          CONSTRAINT `template_wbs_detail_ibfk_5` FOREIGN KEY (`task_status`) REFERENCES `task_status` (`id`) ON DELETE CASCADE,
          CONSTRAINT `template_wbs_detail_ibfk_6` FOREIGN KEY (`template_wbs_id`) REFERENCES `template_wbs_name` (`id`) ON DELETE CASCADE
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;");

    }

    public function down()
    {

    }
}
