<?php
defined('BASEPATH') or exit('No direct script access allowed');

class inventory_loan_model extends CI_Model
{
    public function getInventory()
    {
        $this->db->select("*");
        $this->db->from("inventory");
        return $this->db->get()->result();
    }

    public function datatable($search = '', $length = '', $start = '')
    {
        $role_id = $this->session->userdata('user_type');
        $user_id = $this->session->userdata('user_id');

        $select = "il.id, il.loan_duration, il.start_date, u.username, i.inventory_name";
        $this->db->select($select);
        $this->db->from("inventory_loan_activity as il");
        $this->db->join("inventory as i", 'il.inventory_id = i.id');
        $this->db->join("users as u", 'il.user_id = u.id');
        if ($search != '') {

            $arr_select = (explode(', ', $select));

            foreach ($arr_select as $key => $value) {

                if (strpos($value, " as ") !== FALSE) {
                    $arr_select[$key] =  strstr($value, 'as', true) . "LIKE '%" . $search . "%'";
                } else {
                    $arr_select[$key] = $value . " LIKE '%" . $search . "%'";
                }
            }

            $this->db->where("(" . implode(' OR ', $arr_select) . ")");
        }
        if($this->uri->segment(1) == "InventoryLoanActivity"){
            
            $this->db->where("il.user_id", $user_id);
        }
        if ($length != '' && $start != '') {
            $this->db->limit($length, $start);
        }

        return $this->db->get()->result();
    }

    public function add($data)
    {
        return $this->db->insert('inventory_loan_activity', $data);
    }

    public function show($id)
    {
        $this->db->select("il.*, u.username,i.inventory_name,");
        $this->db->from("inventory_loan_activity as il");
        $this->db->join("inventory as i", 'il.inventory_id = i.id');
        $this->db->join("users as u", 'il.user_id = u.id');
        $this->db->where('il.id', $id);
        return $this->db->get()->row();
    }

    public function update($id, $data)
    {
        return $this->db->where('id', $id)->update('inventory_loan_activity', $data);
    }

    public function delete($id)
    {
        return $this->db->delete('inventory_loan_activity', ['id' => $id]);
    }

}
