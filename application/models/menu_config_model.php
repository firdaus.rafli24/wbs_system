<?php
defined('BASEPATH') or exit('No direct script access allowed');

class menu_config_model extends CI_Model
{
	public function datatable($search = '')
	{
		$query = $this->db->query(" SELECT * FROM configs");
		return $query->result();
	}

	public function get_all()
	{
		return $this->db->query(" SELECT * FROM configs")->result();
	}

	public function find($config_key)
	{
		return $this->db->select("*")->from("configs")->where('config_key', $config_key)->get()->row();
	}

	public function update($config_key, $data)
	{
		return $this->db->where('config_key', $config_key)->update('configs', $data);
	}
}
