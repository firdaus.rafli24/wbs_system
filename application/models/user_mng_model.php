<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_mng_model extends CI_Model
{
  public function datatable($search = '', $length = '', $start = '')
  {

    $user_type = 1;
    if ($this->uri->segment(1) == "UserManagement") {

      $select  = "users.id, username, email, phone_number, wa_number, user_type, groups.name as user_type_name, IF(employment_status = 0,'Reguler','Freelance') as employment_status";

      $this->db->select($select);
      $this->db->from("users");
      $this->db->join("groups", "groups.id = users.user_type");
      $this->db->where("user_type !=", $user_type);
    } else {
      $select  = "users.id, username, permit_quota";

      $this->db->select($select);
      $this->db->from("users");
      $this->db->where("user_type !=", $user_type);
    }


    if ($search != '') {

      $arr_select = (explode(', ', $select));

      foreach ($arr_select as $key => $value) {

        if (strpos($value, " as ") !== FALSE) {
          $arr_select[$key] =  strstr($value, 'as', true) . "LIKE '%" . $search . "%'";
        } else {
          $arr_select[$key] = $value . " LIKE '%" . $search . "%'";
        }
      }

      $this->db->where("(" . implode(' OR ', $arr_select) . ")");
    }

    if ($length != '' && $start != '') {
      $this->db->limit($length, $start);
    }

    return $this->db->get()->result();
  }

  public function view($id)
  {
    if ($this->uri->segment(1) == "UserManagement") {

      $this->db->select("users.id,username,email,password,phone_number,wa_number,employment_status,user_type, groups.name as user_type_name");
      $this->db->from("users");
      $this->db->join("groups", "groups.id = users.user_type");
      $this->db->where("users.id", $id);
    } else {
      $this->db->select("users.id, username, permit_quota");
      $this->db->from("users");
      $this->db->where("users.id", $id);
    }

    return $this->db->get()->result_array();
  }

  public function update($user_id, $data)
  {
    $this->db->trans_start();

    $this->db->select("*");
    $this->db->from("users");
    $this->db->where('id', $user_id);
    $this->db->set($data);
    $this->db->update('users');

    $users_groups['group_id'] = $data['user_type'];

    $this->db->select("*");
    $this->db->from("users_groups");
    $this->db->where('user_id', $user_id);
    $this->db->update('users_groups', $users_groups);

    $this->db->trans_complete();

    if ($this->db->trans_status() == false) {
      return false;
    } else {
      return $user_id;
    }
  }
  public function updatePermitQuota($id, $data)
  {
    return $this->db->where('id', $id)->update('users', $data);
  }

  public function getPermitQuota($id)
  {
    $select = "permit_quota";
    $this->db->select($select);
    $this->db->from('users');
    $this->db->where('id', $id);
    return $this->db->get()->row()->permit_quota;
  }

  public function add($data)
  {
    $this->db->trans_start();
    $this->db->select("*");
    $this->db->from("users");
    $this->db->set($data);
    $this->db->insert('users');
    $id = $this->db->insert_id();

    $users_groups['user_id']  = $id;
    $users_groups['group_id'] = $data['user_type'];
    $this->db->insert('users_groups', $users_groups);
    $this->db->trans_complete();

    if ($this->db->trans_status() == false) {
      return false;
    } else {
      return $id;
    }
  }

  public function delete($user_id)
  {
    $this->db->trans_start();

    $this->db->select("*");
    $this->db->from("users");
    $this->db->where('id', $user_id);
    $this->db->delete('users');

    $this->db->where('user_id', $user_id)->delete('users_groups');
    $this->db->trans_complete();

    return $this->db->trans_status();
  }

  public function get_role()
  {
    $this->db->select("id,name, description");
    $this->db->from("groups");
    $id = 1;
    $this->db->where("id !=", $id);

    return $this->db->get()->result();
  }

  public function get_role_name($id)
  {
    $this->db->select("description");
    $this->db->from("groups");
    $this->db->where('id', $id);

    return $this->db->get()->row()->description;
  }
  public function get_user_by_group($id)
  {
    $this->db->select("id, username, email");
    $this->db->from("users");
    $this->db->where('user_type', $id);
    return $this->db->get()->result();
  }


  public function all_role()
  {
    $this->db->select("id, name, description");
    $this->db->from("groups");
    return $this->db->get()->result();
  }

  public function all_user()
  {
    $this->db->select("id,username, email");
    $this->db->from("users");
    return $this->db->get()->result();
  }

  public function getAllId()
  {
    $user_type = 1;

    $this->db->select("id");
    $this->db->from("users");
    $this->db->where("user_type!=", $user_type);

    return $this->db->get()->result_array();
  }
  public function reset($data)
  {
    return $this->db->update_batch('users', $data, 'id');
  }


  public function get_user_role($id){
    $this->db->select("g.name as group");
    $this->db->from("users");


  }

}
