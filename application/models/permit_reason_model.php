<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class permit_reason_model extends CI_Model
{

	public function datatable($search = '', $length = '', $start = ''){

		$select = "pr.id, pr.reason_name, pr.description";
		$this->db->select($select);
		$this->db->from("permit_reason as pr");

		if($search != ''){

			$arr_select = (explode(', ',$select));

			foreach ($arr_select as $key => $value) {

				if (strpos($value, " as ") !== FALSE) {
					$arr_select[$key] =  strstr($value, 'as', true)."LIKE '%".$search."%'";
				} else {
					$arr_select[$key] = $value." LIKE '%".$search."%'";
				}
			}

			$this->db->where("(".implode(' OR ', $arr_select).")");
		}

		if($length != '' && $start != ''){
			$this->db->limit($length, $start);
		}
		
		return $this->db->get()->result();
	}

	public function add($data){
		return $this->db->insert('permit_reason', $data);
	}

	public function show($id){
		$this->db->select("pr.*");
		$this->db->from("permit_reason as pr");
		$this->db->where('pr.id', $id);
		return $this->db->get()->row();
	}

	public function update($id, $data){
		return $this->db->where('id', $id)->update('permit_reason', $data);
	}

	public function delete($id)
	{
		return $this->db->delete('permit_reason', ['id' => $id]);
	}
}