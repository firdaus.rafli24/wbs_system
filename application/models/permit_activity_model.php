<?php
defined('BASEPATH') or exit('No direct script access allowed');

class permit_activity_model extends CI_Model
{
	public function get_reason()
	{
		$this->db->select("*");
		$this->db->from("permit_reason");
		return $this->db->get()->result();
	}

	public function get_user_to()
	{
		$this->db->select("u.id, u.email, u.username");
		$this->db->from("users as u");
		$this->db->join("users_groups as ug", 'ug.user_id = u.id');
		$this->db->where('ug.group_id', 2);
		return $this->db->get()->result();
	}

	public function get_user_cc()
	{
		$this->db->select("u.id, u.email, u.username");
		$this->db->from("users as u");
		$this->db->join("users_groups as ug", 'ug.user_id = u.id');
		return $this->db->get()->result();
	}

	public function datatable($search = '', $length = '', $start = '')
	{
		$role_id = $this->session->userdata('user_type');
		$user_id = $this->session->userdata('user_id');

		$select = "pa.id, pa.to, pa.cc, pa.subject, pa.description, pa.long, pa.start_date, pa.approval, pa.user_id, u.username, u.email, g.name as group, pr.reason_name";
		$this->db->select($select);
		$this->db->from("permit_activity as pa");
		$this->db->join("permit_reason as pr", 'pr.id = pa.reason_id');
		$this->db->join("users as u", 'pa.user_id = u.id');
		$this->db->join("users_groups as ug", 'ug.user_id = u.id');
		$this->db->join("groups as g", 'ug.group_id = g.id');

		if ($search != '') {

			$arr_select = (explode(', ', $select));

			foreach ($arr_select as $key => $value) {

				if (strpos($value, " as ") !== FALSE) {
					$arr_select[$key] =  strstr($value, 'as', true) . "LIKE '%" . $search . "%'";
				} else {
					$arr_select[$key] = $value . " LIKE '%" . $search . "%'";
				}
			}

			$this->db->where("(" . implode(' OR ', $arr_select) . ")");
		}

		if (!in_array($role_id, [0, 1, 2])) {
			$this->db->where("pa.user_id", $user_id);
		}

		if ($length != '' && $start != '') {
			$this->db->limit($length, $start);
		}

		return $this->db->get()->result();
	}

	public function add($data)
	{
		return $this->db->insert('permit_activity', $data);
	}

	public function show($id)
	{
		$this->db->select("pa.*, u.username, u.email, g.name as group, pr.reason_name as reason");
		$this->db->from("permit_activity as pa");
		$this->db->join("permit_reason as pr", 'pr.id = pa.reason_id');
		$this->db->join("users as u", 'pa.user_id = u.id');
		$this->db->join("users_groups as ug", 'ug.user_id = u.id');
		$this->db->join("groups as g", 'ug.group_id = g.id');
		$this->db->where('pa.id', $id);
		return $this->db->get()->row();
	}

	public function update($id, $data)
	{
		return $this->db->where('id', $id)->update('permit_activity', $data);
	}

	public function delete($id)
	{
		return $this->db->delete('permit_activity', ['id' => $id]);
	}
}
