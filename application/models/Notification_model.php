<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class notification_model extends CI_Model
{
	public function get_datatable(){

		$role_id = $this->session->userdata('user_type');
		$user_id = $this->session->userdata('user_id');

		$this->db->select("IF(nv.notification_id IS NULL, 1, 0) as new, ntf.*, u.username as created_by");
		$this->db->from("notification as ntf");
		$this->db->join("users as u", 'ntf.created_by = u.id', 'LEFT');
		$this->db->join("notification_view as nv", 'nv.notification_id = ntf.id', 'LEFT');

		if(in_array($role_id, [3, 4, 5])){
			$this->db->where( '(target_role_id = '.$role_id.' AND target_user_id = 0)');
			$this->db->or_where( '(target_role_id = 0 AND target_user_id = '.$user_id.')');
			$this->db->or_where( '(target_role_id = 0 AND target_user_id = 0)');
		}

		return $this->db->get()->result();
	}

	public function get_notif(){

		$role_id = $this->session->userdata('user_type');
		$user_id = $this->session->userdata('user_id');

		$this->db->select("ntf.*, u.username as created_by");
		$this->db->from("notification as ntf");
		$this->db->join("users as u", 'ntf.target_user_id = u.id', 'LEFT');

		if(in_array($role_id, [3, 4, 5])){
			$this->db->group_start();
			$this->db->where( '(target_role_id = '.$role_id.' AND target_user_id = 0)');
			$this->db->or_where( '(target_role_id = 0 AND target_user_id = '.$user_id.')');
			$this->db->or_where( '(target_role_id = 0 AND target_user_id = 0)');
			$this->db->group_end();
			$this->db->where("NOT EXISTS (
				SELECT *
				FROM notification_view AS nv 
				WHERE nv.user_id = ".$user_id." AND ntf.id=nv.notification_id
			)");

		}

		return $this->db->get()->result();
	}

	public function add($data){
		return $this->db->insert('notification', $data);
	}

	public function show($id){
		$this->db->select("ntf.*, u.username as created_by, u2.username as updated_by");
		$this->db->from("notification as ntf");
		$this->db->join("users as u", 'ntf.created_by = u.id', 'LEFT');
		$this->db->join("users as u2", 'ntf.updated_by = u2.id', 'LEFT');
		$this->db->where("ntf.id", $id);
		return $this->db->get()->row();
	}

	public function update($id, $data){
		return $this->db->where('id', $id)->update('notification', $data);
	}

	public function delete($id)
	{
		return $this->db->delete('notification', ['id' => $id]);
	}

	public function addNotificationView($data){
		$check = $this->db->select('nv.*')->from('notification_view as nv')->where('user_id', $data['user_id'])->where('notification_id', $data['notification_id'])->get()->num_rows();

		if($check < 1){
			$this->db->insert('notification_view', $data);
		}
	}


}