<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class project_mng_model extends CI_Model
{
  public function datatable($table){
    return $this->db->get($table);
  }

  public function datatable_project($search = '', $length = '', $start = ''){
    $select = "id, name, description, DATE(start_date) as start_date, DATE(due_date) as due_date, DATE(due_date_revised) as due_date_revised";
    $this->db->select($select);
    $this->db->from("projects");

    if($search != ''){

      $arr_select = (explode(', ',$select));

      foreach ($arr_select as $key => $value) {

        if (strpos($value, " as ") !== FALSE) {
          $arr_select[$key] =  strstr($value, 'as', true)."LIKE '%".$search."%'";
        } else {
          $arr_select[$key] = $value." LIKE '%".$search."%'";
        }
      }

      $this->db->where("(".implode(' OR ', $arr_select).")");
    }

    if($length != '' && $start != ''){
      $this->db->limit($length, $start);
    }

    return $this->db->get()->result();
  }

  public function datatable_category($search = '', $length = '', $start = ''){
    $select = "tc.id, tc.name, tc.description, g.name as role";
    $this->db->select($select);
    $this->db->from("task_category tc");
    $this->db->join("groups g", "g.id = tc.role");

    if($search != ''){

      $arr_select = (explode(', ',$select));

      foreach ($arr_select as $key => $value) {

        if (strpos($value, " as ") !== FALSE) {
          $arr_select[$key] =  strstr($value, 'as', true)."LIKE '%".$search."%'";
        } else {
          $arr_select[$key] = $value." LIKE '%".$search."%'";
        }
      }

      $this->db->where("(".implode(' OR ', $arr_select).")");
    }

    if($length != '' && $start != ''){
      $this->db->limit($length, $start);
    }

    return $this->db->get()->result();
  }


  public function datatable_status($search = '', $length = '', $start = ''){
    $select = "ts.id, ts.name, ts.description";
    $this->db->select($select);
    $this->db->from("task_status ts");

    if($search != ''){

      $arr_select = (explode(', ',$select));

      foreach ($arr_select as $key => $value) {

        if (strpos($value, " as ") !== FALSE) {
          $arr_select[$key] =  strstr($value, 'as', true)."LIKE '%".$search."%'";
        } else {
          $arr_select[$key] = $value." LIKE '%".$search."%'";
        }
      }

      $this->db->where("(".implode(' OR ', $arr_select).")");
    }

    if($length != '' && $start != ''){
      $this->db->limit($length, $start);
    }

    return $this->db->get()->result();
  }


  public function view_project($id){
    $this->db->select("id,name,description,DATE(start_date) as start_date,DATE(due_date) as due_date,DATE(due_date_revised) as due_date_revised");
    $this->db->from("projects");
    $this->db->where("id",$id);

    return $this->db->get()->result_array();
  }
  public function update_project($user_id,$data){
    $this->db->select("*");
    $this->db->from("projects");
    $this->db->where('id', $user_id);
    $this->db->set($data);
    $this->db->update('projects');
  }
  public function add_project($data){
    $this->db->select("*");
    $this->db->from("projects");
    $this->db->set($data);
    $this->db->insert('projects');
  }
  public function delete_project($user_id)
  {
    $this->db->select("*");
    $this->db->from("projects");
    $this->db->where('id', $user_id);
    $this->db->delete('projects');
  }
  public function view_category($id){
    $this->db->select("id,name,description,role");
    $this->db->from("task_category");
    $this->db->where("id",$id);

    return $this->db->get()->result_array();
  }
  public function update_category($user_id,$data){
    $this->db->select("*");
    $this->db->from("task_category");
    $this->db->where('id', $user_id);
    $this->db->set($data);
    $this->db->update('task_category');
  }
  public function add_category($data){
    $this->db->select("*");
    $this->db->from("task_category");
    $this->db->set($data);
    $this->db->insert('task_category');
  }
  public function delete_category($user_id)
  {
    $this->db->select("*");
    $this->db->from("task_category");
    $this->db->where('id', $user_id);
    $this->db->delete('task_category');
  }
  public function view_status($id){
    $this->db->select("id,name,description");
    $this->db->from("task_status");
    $this->db->where("id",$id);

    return $this->db->get()->result_array();
  }
  public function update_status($user_id,$data){
    $this->db->select("*");
    $this->db->from("task_status");
    $this->db->where('id', $user_id);
    $this->db->set($data);
    $this->db->update('task_status');
  }
  public function add_status($data){
    $this->db->select("*");
    $this->db->from("task_status");
    $this->db->set($data);
    $this->db->insert('task_status');
  }
  public function delete_status($user_id)
  {
    $this->db->select("*");
    $this->db->from("task_status");
    $this->db->where('id', $user_id);
    $this->db->delete('task_status');
  }
  public function get_role(){
    $this->db->select("id,name");
    $this->db->from("groups");
    $id=0;
    $this->db->where("id !=",$id);

    return $this->db->get()->result();
  }
  public function get_role_name($id){
    $this->db->select("name");
    $this->db->from("groups");
    $this->db->where('id', $id);

    return $this->db->get()->result();
  }
}
