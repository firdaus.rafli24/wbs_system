<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class privilege_management_model extends CI_Model
{
	public function datatable($search = '', $sort_col = '', $sort_dir = '')
	{
		$searchData = '';

		if($search != ''){
			$searchData = "WHERE (m.menu_name LIKE '%".$search."%' OR sm.submenu_name LIKE '%".$search."%' OR pg1.g_name LIKE '%".$search."%' OR pu1.u_name LIKE '%".$search."%' OR pg2.g_name LIKE '%".$search."%' OR pu2.u_name LIKE '%".$search."%') ";
		}

		$orderData = "ORDER BY ".$sort_col." ".$sort_dir;

		$query = $this->db->query(" 
			SELECT 
			m.id menu_id, m.menu_name, sm.id submenu_id, sm.submenu_name, 
			
			IF(sm.id IS NULL,
			group_concat(DISTINCT pg2.id order by pg2.id SEPARATOR ', '),
			group_concat(DISTINCT pg1.id order by pg1.id SEPARATOR ', ')) pg_id,
			IF(sm.id IS NULL,
			group_concat(DISTINCT pg2.g_id order by pg2.g_id SEPARATOR ', '),
			group_concat(DISTINCT pg1.g_id order by pg1.g_id SEPARATOR ', ')) groups_id,
			IF(sm.id IS NULL,
			group_concat(DISTINCT pg2.g_name order by pg2.g_name SEPARATOR ', '),
			group_concat(DISTINCT pg1.g_name order by pg1.g_name SEPARATOR ', ')) groups_name,
			
			IF(sm.id IS NULL,
			group_concat(DISTINCT pu2.id order by pu2.id SEPARATOR ', '),
			group_concat(DISTINCT pu1.id order by pu1.id SEPARATOR ', ')) pu_id,
			IF(sm.id IS NULL,
			group_concat(DISTINCT pu2.u_id order by pu2.u_id SEPARATOR ', '),
			group_concat(DISTINCT pu1.u_id order by pu1.u_id SEPARATOR ', ')) users_id,
			IF(sm.id IS NULL,
			group_concat(DISTINCT pu2.u_name order by pu2.u_name SEPARATOR ', '),
			group_concat(DISTINCT pu1.u_name order by pu1.u_name SEPARATOR ', ')) users_name
			
			FROM menu m
			LEFT JOIN submenu sm ON sm.menu_id = m.id
			
			LEFT JOIN ( 
			SELECT pg.id, pg.menu_id, pg.submenu_id, g.id g_id, g.name g_name FROM privilege pg
			INNER JOIN `groups` g ON g.id = pg.group_id ) pg1 ON m.id = pg1.menu_id AND sm.id = pg1.submenu_id
			
			LEFT JOIN ( 
			SELECT pg.id, pg.menu_id, pg.submenu_id, g.id g_id, g.name g_name FROM privilege pg
			INNER JOIN `groups` g ON g.id = pg.group_id ) pg2 ON m.id = pg2.menu_id AND sm.id IS NULL
			
			LEFT JOIN ( 
			SELECT pu.id, pu.menu_id, pu.submenu_id, u.id u_id, u.username u_name FROM privilege_user pu
			INNER JOIN `users` u ON u.id = pu.user_id ) pu1 ON m.id = pu1.menu_id AND sm.id = pu1.submenu_id
			
			LEFT JOIN ( 
			SELECT pu.id, pu.menu_id, pu.submenu_id, u.id u_id, u.username u_name FROM privilege_user pu
			INNER JOIN `users` u ON u.id = pu.user_id ) pu2 ON m.id = pu2.menu_id AND sm.id IS NULL

			$searchData
			
			GROUP BY sm.id, m.id

			$orderData

			");
		return $query->result();
	}


	public function pg()
	{
		$query = $this->db->query("SELECT p.menu_id, m.menu_name, sm.submenu_name, sm.id sm_id, group_concat(p.id order by p.id SEPARATOR ', ') as p_id, group_concat(g.id order by g.id SEPARATOR ', ') as groups_id, group_concat(g.name order by g.id SEPARATOR ', ') as groups_name
			FROM groups g
			LEFT JOIN privilege p on p.group_id = g.id
			JOIN menu m ON m.id = p.menu_id
			LEFT JOIN submenu sm ON sm.id = p.submenu_id
			GROUP BY p.submenu_id, p.menu_id
			ORDER BY p.menu_id, g.id");
		return $query->result();
	}

	public function pu()
	{
		$query = $this->db->query("SELECT m.id menu_id, sm.id as sm_id, group_concat(pu.id order by pu.id SEPARATOR ', ') as pu_id, group_concat(u.id order by u.id SEPARATOR ', ') as users_id, group_concat(u.username order by u.id SEPARATOR ', ') as users_name
			FROM users u
			LEFT JOIN privilege_user pu on pu.user_id = u.id
			JOIN menu m ON m.id = pu.menu_id
			LEFT JOIN submenu sm ON sm.id = pu.submenu_id
			GROUP BY pu.submenu_id, pu.menu_id
			ORDER BY pu.menu_id, u.id");
		return $query->result();
	}


	public function insert_batch_user($data){
		return $this->db->insert_batch('privilege_user', $data);
	}

	public function insert_batch_group($data){
		return $this->db->insert_batch('privilege', $data);
	}

	public function delete_batch_user($data){
		return $this->db->where_in('id', $data )->delete('privilege_user');
	}
	
	public function delete_batch_group($data){
		return $this->db->where_in('id', $data )->delete('privilege');
	}
	

}