<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class git_issue_model extends CI_Model
{

	public function datatable($search = '', $start_date, $stop_date, $label, $length = '', $start = ''){

		$this->db->select("gi.*, p.name as project, u.username upload_username");
		$this->db->from("git_issue as gi");
		$this->db->join('projects p', 'p.id = gi.project_id');
		$this->db->join('users u', 'u.id = gi.user_id_upload');

		$this->db->where('CAST(gi.date_upload AS DATE) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($stop_date)).'"');

		foreach ($label as $key => $value) {
			$this->db->like('gi.labels',$value.',');
		}

		if($search != ''){
			$where = "(gi.issue_id LIKE '%$search%' OR gi.url LIKE '%$search%' OR gi.title LIKE '%$search%' OR gi.state LIKE '%$search%' OR gi.description LIKE '%$search%' OR gi.author LIKE '%$search%' OR gi.assignee_username LIKE '%$search%' OR gi.assignee LIKE '%$search%' OR gi.author_username LIKE '%$search%' OR p.name LIKE '%$search%' OR u.username LIKE '%$search%' OR gi.confidential LIKE '%$search%'  OR gi.locked LIKE '%$search%'  OR gi.due_date LIKE '%$search%' OR gi.date_upload LIKE '%$search%' OR gi.updated_at LIKE '%$search%' OR gi.closed_at LIKE '%$search%' OR gi.milestone LIKE '%$search%' OR gi.weight LIKE '%$search%' OR gi.labels LIKE '%$search%' OR gi.time_estimate LIKE '%$search%' OR gi.time_spent LIKE '%$search%' )";
			$this->db->where($where);
		}

		if($length != '' && $start != ''){
			$this->db->limit($length, $start);
		}

		return $this->db->get()->result();
	}

	public function insert_batch($data){
		return $this->db->insert_batch('git_issue', $data);
	}

	public function check_data($project_id, $user_id,  $issue_id){
		$this->db->select("COUNT(gi.id) as duplicate");
		$this->db->from("git_issue as gi");
		$this->db->where('gi.project_id', $project_id);
		$this->db->where('gi.user_id_upload', $user_id);
		$this->db->where('gi.issue_id', $issue_id);
		return $this->db->get()->row();
	}

	public function delete_duplicate($project_id, $user_id,  $issue_id){
		return $this->db->delete('git_issue', ['project_id' => $project_id, 'user_id_upload' => $user_id, 'issue_id' => $issue_id]);
	}

	public function insert($data){
		return $this->db->insert('git_issue', $data);
	}

	public function show($id){
		$this->db->select("gi.*");
		$this->db->from("git_issue as gi");
		$this->db->where('gi.id', $id);
		return $this->db->get()->row();
	}

	public function update($id, $data){
		return $this->db->where('id', $id)->update('git_issue', $data);
	}

	public function delete($id)
	{
		return $this->db->delete('git_issue', ['id' => $id]);
	}
	public function getlabels()
	{
		$this->db->select("labels");
		$this->db->from("git_issue");

		return $this->db->get()->result_array();
	}
	public function getusername()
	{
		$this->db->select("author");
		$this->db->from("git_issue");

		return $this->db->get()->result_array();
	}
	public function get_project()
	{
		$this->db->select("gi.project_id, p.name as project");
		$this->db->from("git_issue as gi");
		$this->db->join('projects p', 'p.id = gi.project_id');

		return $this->db->get()->result();
	}
	public function get_project_detected($projectunique,$projects)
	{
		$this->db->select("gi.project_id, p.name as project");
		$this->db->from("git_issue as gi");
		$this->db->join('projects p', 'p.id = gi.project_id');
		if ($projects!=1) {
		$this->db->where_in('project_id',$projectunique);
		}
		return $this->db->get()->result();
	}

	public function getbug($author,$start_date,$stop_date,$project)
	{
		$this->db->select("author,labels,date_upload,project_id");
		$this->db->from("git_issue");

		if ($author!=1) {
			$this->db->where_in('author',$author);
		}
		if ($project!=1) {
			$this->db->where_in('project_id',$project);
		}

		$this->db->where('CAST(date_upload AS DATE) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($stop_date)).'"');

		return $this->db->get()->result_array();
	}
	public function getbugproject($author,$start_date,$stop_date,$project)
	{
		$this->db->select("project_id");
		$this->db->from("git_issue");

		if ($project!=1) {
			$this->db->where_in('project_id',$project);
		}
		if ($author!=1) {
			$this->db->where_in('author',$author);
		}

		$this->db->where('CAST(date_upload AS DATE) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($stop_date)).'"');

		return $this->db->get()->result_array();
	}
	public function getbuglabel($author,$start_date,$stop_date,$projects)
	{
		$this->db->select("labels");
		$this->db->from("git_issue");

		if ($author!=1) {
			$this->db->where_in('author',$author);
		}
		if ($projects!=1) {
			$this->db->where_in('project_id',$projects);
		}

		$this->db->where('CAST(date_upload AS DATE) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($stop_date)).'"');

		return $this->db->get()->result_array();
	}
	public function getperproject($start_date,$stop_date,$project)
	{
		$this->db->select("author,labels,date_upload,project_id");
		$this->db->from("git_issue");
		$this->db->where_in('project_id',$project);

		$this->db->where('CAST(date_upload AS DATE) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($stop_date)).'"');

		return $this->db->get()->result_array();
	}

}
