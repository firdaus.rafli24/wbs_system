<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class Report2_model extends CI_Model
{

  public function get_role(){
    $this->db->select("id,name");
    $this->db->from("groups");
    $notIn = [1];
    $this->db->where_not_in("id",$notIn);

    return $this->db->get()->result();
  }

  public function get_project(){
    $this->db->select("name, id");
    $this->db->from("projects");
    return $this->db->get()->result();
  }

  public function get_user($role, $search = ''){
   $this->db->select("username");
   $this->db->from("users");
   $this->db->where("user_type", $role);
   
   if ($search != '') {
    $this->db->like("username", $search);
  }

  return $this->db->get()->result();
}

public function get_hour($role, $start_date, $stop_date){
  $this->db->select("users.username, tc.id as category_id, projects.name, userwbs.excecutedby, userwbs.project, sum(userwbs.actual_hour) as actual_sum");
  $this->db->from("userwbs");
  $this->db->join("users", "users.id = userwbs.excecutedby");
  $this->db->join("projects", "projects.id = userwbs.project");
  $this->db->join("task_category as tc", "tc.id = userwbs.category");
  $this->db->where("tc.role", $role);
  $this->db->where('CAST(userwbs.created_date AS DATE) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($stop_date)).'"');

  $this->db->group_by("userwbs.project, userwbs.excecutedby");
  $this->db->order_by("sum(userwbs.actual_hour)", "DESC");
  return $this->db->get()->result();
}


public function get_longest_category($category, $start_date, $stop_date){
  $this->db->select("users.username, tc.name as category, tc.id as category_id, projects.name, userwbs.excecutedby, userwbs.project, sum(userwbs.actual_hour) as actual_sum");
  $this->db->from("userwbs");
  $this->db->join("users", "users.id = userwbs.pic");
  $this->db->join("projects", "projects.id = userwbs.project");
  $this->db->join("task_category as tc", "tc.id = userwbs.category");
  $this->db->where("userwbs.category", $category);
  $this->db->where('CAST(userwbs.created_date AS DATE) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($stop_date)).'"');

  $this->db->group_by("userwbs.project, userwbs.pic");
  $this->db->order_by("sum(userwbs.actual_hour)", "DESC");
  return $this->db->get()->result();
}




}
