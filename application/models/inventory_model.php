<?php
defined('BASEPATH') or exit('No direct script access allowed');

class inventory_model extends CI_Model
{
    public function datatable($search = '', $length = '', $start = '')
    {

        $select = "i.id, i.inventory_name";
        $this->db->select($select);
        $this->db->from("inventory as i");

        if ($search != '') {

            $arr_select = (explode(', ', $select));

            foreach ($arr_select as $key => $value) {

                if (strpos($value, " as ") !== FALSE) {
                    $arr_select[$key] =  strstr($value, 'as', true) . "LIKE '%" . $search . "%'";
                } else {
                    $arr_select[$key] = $value . " LIKE '%" . $search . "%'";
                }
            }

            $this->db->where("(" . implode(' OR ', $arr_select) . ")");
        }

        if ($length != '' && $start != '') {
            $this->db->limit($length, $start);
        }

        return $this->db->get()->result();
    }
    public function add($data)
    {
        return $this->db->insert('inventory', $data);
    }

    public function show($id)
    {
        $this->db->select("ir.*");
        $this->db->from("inventory as ir");
        $this->db->where('ir.id', $id);
        return $this->db->get()->row();
    }

    public function update($id, $data)
    {
        return $this->db->where('id', $id)->update('inventory', $data);
    }

    public function delete($id)
    {
        return $this->db->delete('inventory', ['id' => $id]);
    }
}
