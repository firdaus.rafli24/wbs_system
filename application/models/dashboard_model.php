<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class dashboard_model extends CI_Model
{
  public function datatable(){
    $this->db->select("project, excecutedby, actual_hour");
    $this->db->from("userwbs");
    return $this->db->get()->result();
  }

  public function get_project(){
    $this->db->select("name");
    $this->db->from("projects");
    return $this->db->get()->result();
  }

  public function get_name($role, $search = '', $user){
    $this->db->select("username");
    $this->db->from("users");
    $this->db->where("user_type", $role);

    if(is_array($user)) {
      $this->db->where_in('id', $user);
    }

    if ($search != '') {
      $this->db->like("username", $search);
    }

    return $this->db->get()->result();
  }

  public function get_hour($start_date, $stop_date){
    $this->db->select("users.username, projects.name, userwbs.excecutedby, userwbs.project, sum(userwbs.actual_hour) as actual_sum");
    $this->db->from("userwbs");
    $this->db->join("users", "users.id = userwbs.excecutedby");
    $this->db->join("projects", "projects.id = userwbs.project");
    $this->db->where('CAST(userwbs.created_date AS DATE) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($stop_date)).'"');

    $this->db->group_by("userwbs.project, userwbs.excecutedby");
    return $this->db->get()->result();
  }


  public function get_user($user){
    $this->db->select("u.id, u.email, u.username, CONCAT(u.username, ' (',g.name,')') as user");
    $this->db->from("users u");
    $this->db->join("users_groups ug", "ug.user_id = u.id");
    $this->db->join("groups g", "g.id = ug.group_id");

    if(is_array($user)) {
      $this->db->where_in('user_type', $user);
    }

    $this->db->order_by("g.id");

    return $this->db->get()->result();
  }


}
