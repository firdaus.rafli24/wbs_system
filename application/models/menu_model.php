<?php
defined('BASEPATH') or exit('No direct script access allowed');

class menu_model extends CI_Model
{

  public function get_menu($group_id, $user_id)
  {
    $query = $this->db->query("
     SELECT m.*, sm.id as submenu_id, sm.menu_id, sm.submenu_name, sm.submenu_link, pg1.group_id, pu1.user_id, IF(m.link IS NULL OR m.link = '', sm.submenu_link, m.link) link_user
     FROM menu m
     LEFT JOIN submenu sm ON sm.menu_id = m.id
     LEFT JOIN privilege pg1 ON m.id = pg1.menu_id AND sm.id = pg1.submenu_id
     LEFT JOIN privilege pg2 ON m.id = pg2.menu_id AND sm.id IS NULL
     LEFT JOIN privilege_user pu1 ON m.id = pu1.menu_id AND sm.id = pu1.submenu_id
     LEFT JOIN privilege_user pu2 ON m.id = pu2.menu_id AND sm.id IS NULL
     WHERE (pg1.group_id = $group_id OR pg2.group_id = $group_id) OR (pu1.user_id = $user_id OR pu2.user_id = $user_id)
     ORDER BY m.id ASC
     ");
    return $query->result();
  }

  public function get_menu1($group_id)
  {
    $this->db->select("menu.*,privilege.submenu_id,privilege.group_id");
    $this->db->from("menu");
    $this->db->join("privilege", "privilege.menu_id = menu.id");
    $this->db->where("privilege.group_id", $group_id);
    $this->db->order_by("menu.id", 'asc');

    return $this->db->get()->result();
  }

  public function get_submenu($group_id)
  {
    $this->db->select("submenu.*,privilege.group_id");
    $this->db->from("submenu");
    $this->db->join("privilege", "privilege.submenu_id = submenu.id");
    $this->db->where("privilege.group_id", $group_id);
    $this->db->order_by("submenu.id", 'asc');

    return $this->db->get()->result();
  }

  public function getPrivilege($group_id, $controller)
  {
    $this->db->select('p.*');
    $this->db->where('s.submenu_link', $controller);
    $this->db->where('p.group_id', $group_id);
    $this->db->join('submenu s', 's.id = p.submenu_id');
    return $this->db->get('privilege p');
  }

  public function getMenuList()
  {
    $this->db->select('GROUP_CONCAT(submenu_link) array_link', FALSE);
    $menu = $this->db->get('submenu')->row();
    if (!empty($menu->array_link)) {
      return explode(',', $menu->array_link);
    } else {
      return false;
    }
  }
}
