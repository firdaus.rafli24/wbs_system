<?php
defined('BASEPATH') or exit ('No direct script access allowed');

  class user_type_model extends CI_Model
  {
    public function datatable($table){
      $id=1;
      $this->db->where("id !=",$id);
      return $this->db->get($table);
    }
    public function view($id){
      $this->db->select("id,name,description");
      $this->db->from("groups");
      $this->db->where("id",$id);

      return $this->db->get()->result_array();
    }
    public function update($user_id,$data){
      $this->db->select("*");
      $this->db->from("groups");
      $this->db->where('id', $user_id);
      $this->db->set($data);
      $this->db->update('groups');
    }
    public function add($data){
      $this->db->trans_start();

      $this->db->select("*");
      $this->db->from("groups");
      $this->db->set($data);
      $this->db->insert('groups');

      $id = $this->db->insert_id();

      $priv = array(
         array(
            'submenu_id' => '1' ,
            'group_id' => $id ,
            'create' => '0',
            'update' => '0',
            'delete' => '0',
            'read' => '0',
            'upload' => '0',
            'download' => '1',
         ),
         array(
            'submenu_id' => '5' ,
            'group_id' => $id ,
            'create' => '1',
            'update' => '1',
            'delete' => '1',
            'read' => '1',
            'upload' => '1',
            'download' => '1',
         )
      );

      $this->db->insert_batch('privilege',$priv);
      $this->db->trans_complete();

      if($this->db->trans_status() == false){
        return false;
      } else {
        return $id;
      }
    }
    public function delete($user_id)
    {
      $this->db->select("*");
      $this->db->from("groups");
      $this->db->where('id', $user_id);
      $this->db->delete('groups');

      $this->db->where('group_id', $user_id)->delete('privilege');
      $this->db->trans_complete();

      return $this->db->trans_status();
    }
  }
