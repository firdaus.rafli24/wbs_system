<?php
defined('BASEPATH') or exit('No direct script access allowed');

class assignment_letter_model extends CI_Model
{

	public function get_project()
	{

		$this->db->select("id,name");
		$this->db->from("projects");
		return $this->db->get()->result();
	}
	public function get_user_to()
	{
		$where = "ug.group_id != '0' AND ug.group_id != '1'";
		$this->db->select("u.id, u.email, u.username");
		$this->db->from("users as u");
		$this->db->join("users_groups as ug", 'ug.user_id = u.id');
		$this->db->where($where);
		return $this->db->get()->result();
	}

	public function datatable($search = '', $length = '', $start = '')
	{
		$role_id = $this->session->userdata('user_type');
		$user_id = $this->session->userdata('user_id');

		$select = "al.id, al.destination, al.date, sender.username, receiver.username as username2, g.name as group, pj.name as project_name";
		$this->db->select($select);
		$this->db->from("assignment_letter as al");
		$this->db->join("projects as pj", 'pj.id = al.projects_id');
		$this->db->join("users as sender", 'sender.id = al.user_id ');
		$this->db->join("users as receiver", 'receiver.id = al.recipient_id ');
		$this->db->join("users_groups as ug", 'ug.user_id = sender.id');
		$this->db->join("groups as g", 'ug.group_id = g.id');

		if ($search != '') {

			$arr_select = (explode(', ', $select));

			foreach ($arr_select as $key => $value) {

				if (strpos($value, " as ") !== FALSE) {
					$arr_select[$key] =  strstr($value, 'as', true) . "LIKE '%" . $search . "%'";
				} else {
					$arr_select[$key] = $value . " LIKE '%" . $search . "%'";
				}
			}

			$this->db->where("(" . implode(' OR ', $arr_select) . ")");
		}

		if (!in_array($role_id, [0, 1, 2])) {
			$this->db->where("al.recipient_id", $user_id);
		}else{
			$this->db->where("al.user_id", $user_id);
		}

		if ($length != '' && $start != '') {
			$this->db->limit($length, $start);
		}

		return $this->db->get()->result();
	}

	public function add($data)
	{
		return $this->db->insert('assignment_letter', $data);
	}

	public function show($id)
	{
		$this->db->select("al.*, sender.username, sender.digital_signature, receiver.username as username2, g1.description as senderGroup, g2.description as recipientGroup,  pj.name as project_name");
		$this->db->from("assignment_letter as al");
		$this->db->join("projects as pj", 'pj.id = al.projects_id');
		$this->db->join("users as sender", 'al.user_id = sender.id');
		$this->db->join("users as receiver", 'al.recipient_id = receiver.id');
		$this->db->join("users_groups as ug1", 'ug1.user_id = sender.id');
		$this->db->join("users_groups as ug2", 'ug2.user_id = receiver.id');
		$this->db->join("groups as g1", 'ug1.group_id = g1.id');
		$this->db->join("groups as g2", 'ug2.group_id = g2.id');
		$this->db->where('al.id', $id);
		return $this->db->get()->row();
	}

	public function update($id, $data)
	{
		return $this->db->where('id', $id)->update('assignment_letter', $data);
	}

	public function delete($id)
	{
		return $this->db->delete('assignment_letter', ['id' => $id]);
	}
}
