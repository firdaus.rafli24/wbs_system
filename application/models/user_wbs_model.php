<?php
defined('BASEPATH') or exit ('No direct script access allowed');

class user_wbs_model extends CI_Model
{
  public function datatable($id = '', $search = '', $length = '', $start = ''){

    $where = "1=1";

    if($search != ''){
      $where = "(p.name LIKE '%$search%' OR tc.name LIKE '%$search%' OR uw.task_name LIKE '%$search%' OR u.username LIKE '%$search%' OR u.username LIKE '%$search%' OR uw.start_time LIKE '%$search%' OR uw.stop_time LIKE '%$search%' OR uw.start_date LIKE '%$search%' OR uw.due_date LIKE '%$search%' OR uw.due_date_revised LIKE '%$search%' OR uw.estimated_hour LIKE '%$search%' OR uw.actual_hour LIKE '%$search%' OR uw.task_percentage LIKE '%$search%' OR ts.name LIKE '%$search%' OR uw.notes LIKE '%$search%')";
    }

    $this->db->select("uw.id, p.name as project, tc.name as category,
     uw.task_name, u_pic.username as pic, u.username as excecutedby, DATE_FORMAT(uw.start_time, '%H:%i') as start_time, DATE_FORMAT(stop_time, '%H:%i') as stop_time, DATE(uw.start_date) as start_date, DATE(uw.due_date) as due_date, DATE(uw.due_date_revised) as due_date_revised, estimated_hour, uw.actual_hour, uw.task_percentage, ts.name as task_status, uw.notes, uw.created_date");
    $this->db->from('userwbs as uw');
    $this->db->join('projects p', 'p.id = uw.project');
    $this->db->join('task_category tc', 'tc.id = uw.category');
    $this->db->join('task_status ts', 'ts.id = uw.task_status');
    $this->db->join('users u', 'u.id = uw.excecutedby');
    $this->db->join('users u_pic', 'u_pic.id = uw.pic');

    if($id != ''){
      $this->db->where("uw.excecutedby",$id);
    }
    $this->db->where($where);
    $this->db->order_by("uw.created_date", "desc");

    if($length != '' && $start != ''){
      $this->db->limit($length, $start);
    }
    return $this->db->get()->result();
  }

  public function check_exist_date($c_start_date, $c_start_time, $c_stop_time)
  {

    $user_id = $this->session->userdata('user_id');

    $this->db->select("id");
    $this->db->from("userwbs");
    $this->db->where("excecutedby", $user_id);

    $this->db->where_in("DATE(start_date)", $c_start_date);
    $this->db->where_in("DATE_FORMAT(start_time, '%H:%i')", $c_start_time);
    $this->db->where_in("DATE_FORMAT(stop_time, '%H:%i')", $c_stop_time);
    return $this->db->get()->num_rows();

  }

  public function view($id){
    $this->db->select("id,project,category,task_name,pic,excecutedby,DATE_FORMAT(start_time, '%H:%i') as start_time, DATE_FORMAT(stop_time, '%H:%i') as stop_time,DATE(start_date) as start_date,DATE(due_date) as due_date,DATE(due_date_revised) as due_date_revised, estimated_hour,actual_hour,task_percentage,task_status,notes");
    $this->db->from("userwbs");
    $this->db->where("id",$id);

    return $this->db->get()->result_array();
  }
  public function update($user_id,$data){
    $this->db->select("*");
    $this->db->from("userwbs");
    $this->db->where('id', $user_id);
    $this->db->set($data);
    $this->db->update('userwbs');
  }
  public function add($data){
    $this->db->select("*");
    $this->db->from("userwbs");
    $this->db->insert_batch('userwbs',$data);
  }
  public function delete($user_id)
  {
    $this->db->select("*");
    $this->db->from("userwbs");
    $this->db->where('id', $user_id);
    $this->db->delete('userwbs');
  }
  public function get_project(){
    $this->db->select("id,name");
    $this->db->from("projects");
    return $this->db->get()->result();
  }
  public function get_status(){
    $this->db->select("id,name");
    $this->db->from("task_status");
    return $this->db->get()->result();
  }
  public function get_category(){
    $this->db->select("id,name");
    $this->db->from("task_category");
    return $this->db->get()->result();
  }
  public function get_pic(){
    $this->db->select("id,username");
    $this->db->from("users");
    $id=1;
    $this->db->where('id !=',$id);
    return $this->db->get()->result();
  }
  public function get_project_name($id){
    $this->db->select("name");
    $this->db->from("projects");
    $this->db->where('id', $id);

    return $this->db->get()->result();
  }
  public function get_category_name($id){
    $this->db->select("name");
    $this->db->from("task_category");
    $this->db->where('id', $id);

    return $this->db->get()->result();
  }
  public function get_task_status($id){
    $this->db->select("name");
    $this->db->from("task_status");
    $this->db->where('id', $id);

    return $this->db->get()->result();
  }
  public function get_user_name($id){
    $this->db->select("username");
    $this->db->from("users");
    $this->db->where('id', $id);

    return $this->db->get()->result();
  }
  public function get_category_role($role){
    $this->db->select("id,name");
    $this->db->from("task_category");
    if(!in_array($role, [0, 1])){
      $this->db->where('role', $role);
    }
    return $this->db->get()->result();
  }
  public function get_all_category(){
    $this->db->select("id,name");
    $this->db->from("task_category");
    return $this->db->get()->result();
  }

  public function get_template_wbs_name(){
    $user_id = $this->session->userdata('user_id');

    $this->db->select("n.*");
    $this->db->from("template_wbs_name n");
    $this->db->where("n.user_id", $user_id);

    return $this->db->get()->result();
  }

  public function get_template_wbs_detail($template_wbs_id){

    $this->db->select("d.*");
    $this->db->from("template_wbs_detail d");
    $this->db->where("d.template_wbs_id", $template_wbs_id);

    return $this->db->get()->result();
  }
  public function add_template_name($data){
    $this->db->select("*");
    $this->db->from("template_wbs_name");
    $this->db->insert('template_wbs_name', $data);

    return $this->db->insert_id();
  }
  public function save_template_data($data){
    $this->db->select("*");
    $this->db->from("template_wbs_detail");
    $this->db->insert_batch('template_wbs_detail',$data);
  }
  public function template($id){
    $this->db->select("id");
    $this->db->from("template_wbs_name");
    $this->db->where("id",$id);

    return $this->db->get()->result_array();
  }
  public function delete_template_name($id)
  {
    $this->db->select("*");
    $this->db->from("template_wbs_name");
    $this->db->where('id', $id);
    $this->db->delete('template_wbs_name');
  }
  public function delete_template_detail($id)
  {
    $this->db->select("*");
    $this->db->from("template_wbs_detail");
    $this->db->where('template_wbs_id', $id);
    $this->db->delete('template_wbs_detail');
  }
  public function get_template(){
    $user_id = $this->session->userdata('user_id');

    $this->db->select("n.*");
    $this->db->from("template_wbs_name n");
    $this->db->where("n.user_id", $user_id);

    return $this->db->get()->result();
  }
}
