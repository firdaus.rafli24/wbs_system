<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang["select"] = "Select";
$lang["success"] = "Yeay, success!";
$lang["error"] = "Error";
$lang["cancel"] = "Cancel";
$lang["submit"] = "Submit";
$lang["action"] = "Action";
$lang["id"] = "ID";
$lang["save"] = "Save Change";
$lang["close"] = "Close";
$lang["yes"] = "Yes";
$lang["confirmation"] = "Confirmation";
$lang["regards"] = "Best regards";
$lang["all"] = "All";
$lang["Permit_Fail"] = "You Couldn't Submit Permit Anymore";
$lang["Permit_Fail2"] = "You Couldn't Submit Permit With Current Long(Days) Value";

// permit
$lang["email_confirm"] = "You want to send email again ?";
$lang["approve_confirm"] = "Approval status? ";
$lang["approve_waiting"] = "Waiting for approval";
$lang["approve"] = "Approve";
$lang["decline"] = "Decline";
$lang["Pending"] = "Pending";


//delete
$lang["warning"] = "Warning !";
$lang["del_confirm"] = "Are you sure to delete this data?";
$lang["del_info"] = "You won't be able to revert this!";
$lang["del_yes"] = "Yes, delete it!";
$lang["del_cancel"] = "Cancel";
$lang["deleted"] = "Deleted!";
$lang["del_ok"] = "OK";

// duplicate
$lang["duplicate_confirm"] = "Are you sure?";
$lang["duplicate_yes"] = "Continue save";
$lang["duplicate_no"] = "No";

// form validation
$lang["form_char"] = "character";
$lang["form_requiredform"] = "This field is required!";
$lang["form_maxlength"] = "Maximum character for this field is";
$lang["form_minlength"] = "Minimum character for this field is";
$lang["form_min"] = "Minimum value for this field is";
$lang["form_max"] = "Maximum value for this field is";
$lang["form_is_match_password"] = "Confirmation password should matched to the new password."; //all
$lang["form_login_failed"] = "Can't login with this account. Make sure your email and password are correct."; //all
$lang["form_email_invalid"] = "The email field must contain a valid email address!";
$lang["form_date_invalid"] = "Please enter a valid date.";
$lang["form_number_invalid"] = "Please enter a valid number.";
$lang["form_row_empty"] = "Minimum 1 row to submit";
$lang["form_curr_pass_invalid"] = "Current password is invalid, check it out!";
$lang["form_pass_invalid"] = "Please input alphanumeric character";
$lang["change_password_invalid"] = "Failed to change password, please try again later!";
$lang["delete_invalid_id"] = "Failed to delete, The id is not found";
$lang["edit_invalid_id"] = "Failed to edit, The id is not found";
$lang["get_invalid_id"] = "Failed to get data, The id is not found";
$lang["import_invalid_csv"] = "Failed to import data, check your CSV file";
$lang["not_found"] = "No data found";


//Menu
$lang["Menu001"] = "Dashboard"; //hrd + user
$lang["Menu002"] = "User"; //hrd + sa + user(view only)
$lang["Menu003"] = "Project"; //hrd
$lang["Menu004"] = "Activity";
$lang["Menu005"] = "Permit App"; //hrd + sa + user
$lang["Menu006"] = "Assignment Letter"; //hrd + sa + user
$lang["Menu007"] = "Inventory"; //hrd + sa + user
$lang["Menu008"] = "Notification";
$lang["Menu009"] = "Git Issue";
$lang["Menu010"] = "Report";
$lang["Menu011"] = "Change Password"; //hrd
$lang["Menu000"] = "Config"; //hrd


//Sub Menu
$lang["SubMenu001"] = "Dashboard"; //hrd + user
$lang["SubMenu002"] = "User Management"; //hrd + sa + user(view only)
$lang["SubMenu003"] = "Role Management"; //hrd + sa + user(view only)
$lang["SubMenu004"] = "Project Management"; //hrd
$lang["SubMenu005"] = "User WBS"; //hrd
$lang["SubMenu006"] = "Change Password"; //all
$lang["SubMenu007"] = "Permit Application"; //all
$lang["SubMenu008"] = "Permit Reason Management"; //all
$lang["SubMenu009"] = "Notification";
$lang["SubMenu010"] = "Sign Out";
$lang["SubMenuReport1"] = "Report 1";
$lang["SubMenuReport2"] = "Report 2";
$lang["SubMenu00202"] = "Privilege Management"; //super user
$lang["subMenu_GITIssue"] = "GIT Report";
$lang["subMenu_GITDashboard"] = "GIT Dashboard";
$lang["subMenu_AssignmentLetter001"] = "Assignment Letter Application"; // hrd + user
$lang["subMenu_Inventory001"] = "Inventory Management"; //hrd
$lang["subMenu_Inventory002"] = "Inventory Loan Application"; // hrd + user
$lang["subMenu_Inventory003"] = "Loan List"; // hrd + user
$lang["subMenu_Permit003"] = "Permit Quota Management"; // hrd


//api error
$lang["api_no_privilege"] = "Cannot access this API";
$lang["url_no_privilege"] = "No direct access allowed";

//Change Password
$lang["cp_current_password"] = "Current Password"; //all
$lang["cp_new_password"] = "New Password"; //all
$lang["cp_new_password_conf"] = "New Password Confirmation"; //all

//Login page
$lang["login_title"] = "Work Breakdown Structure";
$lang["login_instruction"] = "Sign in to start your session";
$lang["login_remember"] = "Remember me";
$lang["login_submit"] = "Sign In";
$lang["login_forgot_password"] = "I forgot my password";
$lang["email"] = "Email";
$lang["password"] = "Password";

//navbar
$lang["out"] = "Sign Out";

//sidebar
$lang["main_nav"] = "MAIN NAVIGATION";
$lang["search"] = "Search...";

//footer
$lang["cr1"] = "Copyright";
$lang["cr2"] = " All rights reserved.";

//User Management page
$lang["UM001"] = "Add User";
$lang["UM002"] = "Name";
$lang["UM003"] = "Email Address";
$lang["UM004"] = "Role";
$lang["UM005"] = "Employment Status";
$lang["UM006"] = "Phone Number";
$lang["UM007"] = "Whatsapp Number";
$lang["UM008"] = "Username";
$lang["UM009"] = "Reguler";
$lang["UM010"] = "Freelance";
$lang["UM011"] = "Role/Position";
$lang["UM012"] = "Input New User Data";
$lang["UM013"] = "User Details";
$lang["UM014"] = "User Management";
$lang["UM015"] = "Label";
$lang["UM016"] = "Project";

//User Type
$lang["UT001"] = "User Type Management";
$lang["UT002"] = "Add Role";
$lang["UT003"] = "Description";
$lang["UT004"] = "Add New User Role";
$lang["UT005"] = "Role Name";
$lang["UT006"] = "User Role Details";
$lang["UT007"] = "Edit User Role";

//Project Management
$lang["PM001"] = "Project Manager";
$lang["PM002"] = "On Going Projects";
$lang["PM003"] = "Add Project";
$lang["PM004"] = "Project";
$lang["PM005"] = "Start Date";
$lang["PM006"] = "Due Date";
$lang["PM007"] = "Due Date (Revised)";
$lang["PM008"] = "Task Category";
$lang["PM009"] = "Add Category";
$lang["PM010"] = "Category";
$lang["PM011"] = "Task Status";
$lang["PM012"] = "Add Status";
$lang["PM013"] = "Add New Project";
$lang["PM014"] = "Project Name";
$lang["PM015"] = "Add New Category";
$lang["PM016"] = "Task Category Name";
$lang["PM017"] = "Add New Task Status";
$lang["PM018"] = "Task Status Name";
$lang["PM019"] = "Project Details";
$lang["PM020"] = "Edit Project";
$lang["PM021"] = "Task Category Details";
$lang["PM022"] = "Edit Category";
$lang["PM023"] = "Task Status Details";
$lang["PM024"] = "Edit Task Status";

//User WBS
$lang["WB001"] = "Project Log";
$lang["WB002"] = "Add Activity";
$lang["WB003"] = "Task Log";
$lang["WB004"] = "Task Name";
$lang["WB005"] = "PIC";
$lang["WB006"] = "Executed By";
$lang["WB007"] = "Start Time";
$lang["WB008"] = "Stop Time";
$lang["WB009"] = "Status";
$lang["WB010"] = "Estimated Hours";
$lang["WB011"] = "Actual Hours";
$lang["WB012"] = "Task Percentage";
$lang["WB013"] = "Notes";
$lang["WB014"] = "Add Todays Activity";
$lang["WB015"] = "Submit Task";
$lang["WB016"] = "Add More Task";
$lang["WB017"] = "Delete Last Row";
$lang["WB018"] = "Activity/Task Details";
$lang["WB019"] = "Edit Activity/Task";
$lang["WB020"] = "Created Date";
$lang["WB021"] = "No.";
$lang["WB022"] = "Save Template";
$lang["WB023"] = "Delete Template";
$lang["WB024"] = "Template WBS";
$lang["WB025"] = "Template Name";
$lang["WB026"] = "Template";


//Permit Activity page
$lang["PA"] = "Permit Activity";
$lang["PA000"] = "User";
$lang["PA001"] = "Add Permit";
$lang["PA002"] = "Start Date";
$lang["PA003"] = "Long <i>(day)</i>";
$lang["PA004"] = "Reason";
$lang["PA005"] = "Subject";
$lang["PA006"] = "To";
$lang["PA007"] = "CC";
$lang["PA008"] = "Description";
$lang["PA009"] = "Email Send";
$lang["PA010"] = "Approval";



//Permit Reason page
$lang["PR"] = "Permit Reason";
$lang["PR001"] = "Add Reason";
$lang["PR002"] = "Name";
$lang["PR003"] = "Description";

//Notification page
$lang["NTF"] = "Notification";
$lang["NTF001"] = "Add Notification";
$lang["NTF002"] = "Date";
$lang["NTF003"] = "Type Notification";
$lang["NTF004"] = "Category";
$lang["NTF005"] = "Title";
$lang["NTF006"] = "Description";
//
$lang["NTF007"] = "System";
$lang["NTF008"] = "User";
$lang["NTF009"] = "Select Type Notification";
$lang["NTF010"] = "Created By";
$lang["NTF011"] = "Edit Notification";
$lang["NTF012"] = "View Notification";
$lang["NTF013"] = "Created Date";
$lang["NTF014"] = "Updated By";
$lang["NTF015"] = "Updated Date";

//Git Issue page
$lang["GI"] = "Git Issue";
$lang["GI000"] = "File CSV";
$lang["GI001"] = "Import CSV";
$lang["GI002"] = "Project";
$lang["GI003"] = "User Upload";
$lang["GI004"] = "Issue ID";
$lang["GI005"] = "URL";
$lang["GI006"] = "Title";
$lang["GI007"] = "State";
$lang["GI008"] = "Description";
$lang["GI009"] = "Author";
$lang["GI010"] = "Author Username";
$lang["GI011"] = "Assignee";
$lang["GI012"] = "Assignee Username";
$lang["GI013"] = "Confidential";
$lang["GI014"] = "Locked";
$lang["GI015"] = "Due Date";
$lang["GI016"] = "Created At (UTC)";
$lang["GI017"] = "Updated At (UTC)";
$lang["GI018"] = "Closed At (UTC)";
$lang["GI019"] = "Milestone";
$lang["GI020"] = "Weight";
$lang["GI021"] = "Labels";
$lang["GI022"] = "Time Estimate";
$lang["GI023"] = "Time Spent";
$lang["GI024"] = "QC Name";
$lang["GI025"] = "Summary";
$lang["GI026"] = "Found Issues based on Level";
$lang["GI027"] = "Projects";
$lang["GI028"] = "Found Issues based on Project";
$lang["GI029"] = "Label Name";
$lang["GI030"] = "Found Issues based on Labels";

//Report page
$lang["RE001"] = "Report 1";
$lang["RE002"] = "Report 2";

//Privilege Management page
$lang["PriMan01"] = "Menu";
$lang["PriMan02"] = "Sub Menu";
$lang["PriMan03"] = "Group";
$lang["PriMan04"] = "User";

//Privilege Management page
$lang["MenuConfig01"] = "Key";
$lang["MenuConfig02"] = "Value";
$lang["MenuConfig03"] = "Enable";
$lang["MenuConfig04"] = "Description";


//Inventory Management Page
$lang["IM"] = "Inventory";
$lang["IM000"] = "Add Inventory";
$lang["IM001"] = "Inventory Name";


//Inventory Loan Activity Page
$lang["IL"] = "Inventory Loan";
$lang["IL000"] = "Add Inventory Loan";
$lang["IL001"] = "Start Date";
$lang["IL002"] = "Long <i>(day)</i>";
$lang["IL003"] = "User";
$lang["IL004"] = "Inventory Name";

//Permit Quota Management
$lang["PQM"] = "Permit Quota Management";
$lang["PQM000"] = "Username";
$lang["PQM001"] = "Permit Quota";

//Assignment Letter Activity
$lang["AL"] = "Assignment Letter";
$lang["AL000"] = "Add Assignment Letter";
$lang["AL001"] = "Date";
$lang["AL002"] = "User";
$lang["AL003"] = "Project";
$lang["AL004"] = "To";
$lang["AL005"] = "Destination";
$lang["AL006"] = "Approval";
$lang["AL007"] = "Purpose";
