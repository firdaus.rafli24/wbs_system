<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function formatTanggal($date){

    $months = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    $year = substr($date, 0, 4);
	$month = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);

	$result = $tgl . " " . $months[(int)$month-1] . " ". $year;		
	return($result);

  }
