var currDate = new Date();

var datatable;
$(document).ready(function() {
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    startDate : currDate,
  });
  datatable = $('#notificationTable').DataTable({
    scrollX: true,
    "ajax": {
      url: base_url+"index.php/Notification/datatable",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"1%", className: "hidden"},
    {targets: 1, width:"5%"},
    {targets: 2, width:"5%"},
    {targets: 3, width:"5%"},
    {targets: 4, width:"10%"},
    {targets: 5, width:"9%"},
    {
      targets: 6,
      sortable: false,
      width: "5%",
      render: function (data, type, row) {
        var viewButton = '<button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewNotification" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;
        var editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editNotification" data-backdrop="static" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
        var deleteButton = '<button class="button_delete btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deleteNotification" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
        return '<div class="text-center" style="min-width:150px">'+viewButton+editButton+deleteButton+'</div>';
      }
    }
    ],
    drawCallback: function(data, type, row) {
      $(".button_view").click(function(e){
        var id = $(this).data('id');
        toview(id);
      })
      $(".button_edit").click(function(e){
        var id = $(this).data('id');
        toedit(id);
      })
      $(".button_delete").click(function(e){
        var id = $(this).data('id');
        todelete(id);
      })
    }
  });

  datatable.on('draw', function () {
    $('td').addClass('wrap-text')
  });
});

function toview(id){
  $.ajax({
    url: base_url+"index.php/Notification/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#viewNotification');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="date"]').val(result.data.date);
        modal.find('[name="type_notification"]').val(result.data.type_notification);
        modal.find('[name="category"]').val(result.data.category);
        modal.find('[name="title"]').val(result.data.title);
        CKEDITOR.instances.viewCkbasic.setData( result.data.description );
        modal.find('[name="created_by"]').val(result.data.created_by);
        modal.find('[name="created_date"]').val(result.data.created_date);
        modal.find('[name="updated_by"]').val(result.data.updated_by);
        modal.find('[name="updated_date"]').val(result.data.updated_date);
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

function toedit(id){
  $.ajax({
    url: base_url+"index.php/Notification/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editNotification');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="date"]').val(result.data.date);
        modal.find('[name="type_notification"]').val(result.data.type_notification);
        modal.find('[name="category"]').val(result.data.category);
        modal.find('[name="title"]').val(result.data.title);
        CKEDITOR.instances.editCkbasic.setData( result.data.description );
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

$(document).ready(function(){
  $("#button-edit").on('click',function(e) {
    e.preventDefault();
    var isFormValid = $("#editform").valid();
    var jsonPost = $("form#editform").serializeArray();
    jsonPost.push({name: 'description', value: CKEDITOR.instances.editCkbasic.getData()});
    if(isFormValid){
      $.ajax({
        url: base_url+"index.php/Notification/do_update",
        type: 'POST',
        data : jsonPost,
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#editNotification');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: response.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});

$('#button-addnotification').click(function () {
  $('#addform').trigger('reset'); // Reset form
  $('#addform').validate().resetForm();
  $("#addform").find('.validate_error').removeClass("validate_error");
  CKEDITOR.instances.addCkbasic.setData( '' );
});

$(document).ready(function(){
  $("#button-add").on('click',function(e) {
    e.preventDefault();
    var isFormValid = $("#addform").valid();
    var jsonPost = $("form#addform").serializeArray();
    jsonPost.push({name: 'description', value: CKEDITOR.instances.addCkbasic.getData()});
    if (isFormValid) {
      $.ajax({
        url: base_url+"index.php/Notification/do_save",
        type: 'POST',
        data : jsonPost,
        datatype:"json",
        success: function(result){
          if (result.status) {
            socket.emit('sm_send_notif_permit', {});
            countNotif();
            var modal = $('#addNotification');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});

function todelete(id){
  Swal.fire({
    title: appLang.del_confirm,
    html: appLang.del_info,
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    cancelButtonText: appLang.del_cancel,
    confirmButtonText: appLang.del_yes,
    reverseButtons : true,
  }).then((result) => {
    if (result.value) {
      $.ajax({
        url: base_url+"index.php/Notification/do_delete",
        type: 'POST',
        datatype:"json",
        data: {id: id},
        success: function (result) {
          if (result.status) {
            datatable.ajax.reload();
            Swal.fire({
              title: appLang.deleted,
              icon: 'success',
              html: result.message,
              confirmButtonText: appLang.del_ok
            });
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  })

}


