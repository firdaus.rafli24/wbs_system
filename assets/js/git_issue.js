var datatable;
$(document).ready(function() {

  $("#checkboxLabel").prop("checked", true);
  $("#label").prop("disabled", true).trigger('change');

  $('#label').select2();
  $("#checkboxLabel").click(function(){
    $("#label").find('option').prop("selected",false);
    $("#label").prop("disabled", ($(this).is(':checked'))).trigger('change');
    $("#checkboxLabel").prop("checked", ($(this).is(':checked')));
  });


  $("#search").click(function(e){
    datatable.ajax.reload();
  });

  $("#start_date").datepicker({autoclose: true,format: 'yyyy-mm-dd', setEndDate : new Date($("#stop_date").val()),
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#stop_date').datepicker('setStartDate', new Date(selected.date.valueOf()));
  });

  $("#stop_date").datepicker({autoclose: true,format: 'yyyy-mm-dd',setStartDate : new Date($("#start_date").val()),}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', new Date(selected.date.valueOf()));
  });

datatable = $('#example1').DataTable({
  scrollX: true,serverSide: true,paginate: true,
  "ajax": {
    url: base_url+"index.php/GitIssue/datatable",
    type: 'POST',
    data: function (d) {
      d.start_date = $('#start_date').val();
      d.stop_date = $('#stop_date').val();
      d.label = ($('#label option:selected').length > 0) ? $('#label').val() : [''];
      d.allRole = $("#checkboxLabel").is(":checked") ? 1 : 0;
      console.log(d);
    }
  },
  columnDefs: [
  {targets: 0, width:"3%", className: "text-center", render: function (data, type, row, meta) {
    return meta.row + meta.settings._iDisplayStart + 1;
  }},
  {targets: 1, width:"5%"},
  {targets: 2, width:"10%"},
  {
    targets: 23,
    sortable: false,
    width: "5%",
    render: function (data, type, row) {
      var deleteButton = '<button class="button_delete btn btn-danger btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deleteGitIssue" data-backdrop="static">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
      return '<div class="text-center" style="min-width:100px">'+deleteButton+'</div>';
    }
  }
  ],
  drawCallback: function(data, type, row) {
    $(".button_delete").click(function(e){
      var id = $(this).data('id');
      todelete(id);
    })
  }
});

datatable.on('draw', function () {
  $('td').addClass('wrap-text')
});
});

$(document).ready(function(){
  // $('#example1').DataTable({"iDisplayLength": 100, "search": {regex: true}}).column(1).search("backlog|Stretch|Solid|NIR", true, false ).draw();
});

$(document).ready(function(){
  $("#button-add").on('click',function(e) {
    e.preventDefault();
    var jsonPost = new FormData($("#addform")[0]);
    var isFormValid = $("#addform").valid();

    $.ajax({
      url: base_url+"index.php/GitIssue/do_save",
      type: 'POST',  data : jsonPost,contentType: false,
      processData: false,
      success: function(result){
        console.log(result);
        if (result.status === 'warning') {
          Swal.fire({
            title: appLang.duplicate_confirm,
            text: result.message,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: appLang.duplicate_no,
            confirmButtonText: appLang.duplicate_yes,
            reverseButtons : true,
          }).then((response) => {
            if (response.value) {
              $.ajax({
                url: base_url+"index.php/GitIssue/save_duplicate",
                type: 'POST',  data : { duplicate : result.duplicate }, datatype:"json",
                success: function(result2){
                  if (result2.status) {
                    var modal = $('#addGitIssue');
                    modal.modal('hide');
                    datatable.ajax.reload();
                  }
                }
              });
            } else {
              var modal = $('#addGitIssue');
              modal.modal('hide');
              datatable.ajax.reload();
            }
          })
        }
        else if (result.status) {
          var modal = $('#addGitIssue');
          modal.modal('hide');
          datatable.ajax.reload();
        }else {
          $("#alert-add").removeClass('hidden');
          $("#message-add").text(result.message);
        }
      }
    });
  });
});

function todelete(id){
  $.ajax({
    url: base_url+"index.php/GitIssue/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deleteGitIssue');
        modal.find('[name="id"]').val(result.data.id);
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

$('#button-delete').click(function(){
  var modal= $("#deleteGitIssue");
  var id = modal.find('[name="id"]').val();
  $.ajax({
    url: base_url+"index.php/GitIssue/do_delete",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatable.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
})

$('.modal').on('hidden.bs.modal', function () {
  $('form').each(function(){
    $(this).trigger('reset');
    $(this).validate().resetForm();
    $(this).find('.validate_error').removeClass('validate_error');
  });

});
