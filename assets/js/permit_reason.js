var datatable;
$(document).ready(function() {
  datatable = $('#example1').DataTable({
    scrollX: true,serverSide: true,processing: true,
    "ajax": {
      url: base_url+"index.php/PermitReason/datatable",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"1%", className: "hidden"},
    {targets: 1, width:"5%"},
    {targets: 2, width:"5%"},
    {
      targets: 3,
      sortable: false,
      width: "5%",
      render: function (data, type, row) {
        var viewButton = '<button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewPermit" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;
        var editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editPermit" data-backdrop="static" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
        var deleteButton = '<button class="button_delete btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deletePermit" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
        return '<div class="text-center" style="min-width:100px">'+viewButton+editButton+deleteButton+'</div>';
      }
    }
    ],
    drawCallback: function(data, type, row) {
      $(".button_view").click(function(e){
        var id = $(this).data('id');
        toview(id);
      })
      $(".button_edit").click(function(e){
        var id = $(this).data('id');
        toedit(id);
      })
      $(".button_delete").click(function(e){
        var id = $(this).data('id');
        todelete(id);
      })
    }
  });

  datatable.on('draw', function () {
    $('td').addClass('wrap-text')
  });
});

function toview(id){
  $.ajax({
    url: base_url+"index.php/PermitReason/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#viewPermit');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="reason_name"]').val(result.data.reason_name);
        modal.find('[name="description"]').val(result.data.description);
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

function toedit(id){
  $.ajax({
    url: base_url+"index.php/PermitReason/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editPermit');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="reason_name"]').val(result.data.reason_name);
        modal.find('[name="description"]').val(result.data.description);
        $("#alert-edit").addClass('hidden');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

$(document).ready(function(){
  $("#button-update").on('click',function(e) {
    e.preventDefault();
    var isFormValid = $("#editform").valid();
    var jsonPost = $("form#editform").serializeArray();
    if(isFormValid){
      $.ajax({
        url: base_url+"index.php/PermitReason/do_update",
        type: 'POST',
        data : jsonPost,
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#editPermit');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            $("#alert-edit").removeClass('hidden');
            $("#message-edit").text(response.message);
          }
        }
      });
    }
  });
});
$(document).ready(function(){
  $("#button-adduser").on('click',function(e) {
    e.preventDefault();
    var modal= $("#addPermit");
    modal.find('[name="reason_name"]').val('');
    modal.find('[name="description"]').val('');
  });
});



$(document).ready(function(){
  $("#button-add").on('click',function(e) {
    e.preventDefault();
    var isFormValid = $("#addform").valid();
    var jsonPost = $("form#addform").serializeArray();
    if (isFormValid) {
      $.ajax({
        url: base_url+"index.php/PermitReason/do_save",
        type: 'POST',
        data : jsonPost,
        datatype:"json",
        success: function(result){
          if (result.status) {
            var modal = $('#addPermit');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            $("#alert-add").removeClass('hidden');
            $("#message-add").text(result.message);
          }
        }
      });
    }
  });
});

function todelete(id){
  $.ajax({
    url: base_url+"index.php/PermitReason/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deletePermit');
        modal.find('[name="id"]').val(result.data.id);
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

$('#button-delete').click(function(){
  var modal= $("#deletePermit");
  var id = modal.find('[name="id"]').val();
  $.ajax({
    url: base_url+"index.php/PermitReason/do_delete",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatable.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
})
