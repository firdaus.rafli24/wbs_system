var currDate = new Date();
var datatable;
$(document).ready(function() {
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    startDate : currDate,
  });
  datatable = $('#example1').DataTable({
    scrollX: true,serverSide: true,processing: true,
    "ajax": {
      url: base_url+"index.php/AssignmentLetter/datatable",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"1%", className: "hidden"},
    {targets: 1, width:"7%"},
    {targets: 2, width:"7%"},
    {targets: 3, width:"15%"},
    {targets: 4, width:"12%"},
    {targets: 5, width:"14%"},
    {
      targets: 6,
      sortable: false,
      width: "250px",
      render: function (data, type, row) {
        var downloadPdf = '';
        var viewButton = '';
        var editButton = '';
        var deleteButton = '';

        downloadPdf = '<button class="button_download_pdf btn btn-sm" data-toggle="modal" data-target="#downloadPdfPermit" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-download">'+'</i>'+'</button>' ;
        viewButton = '<button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewAssignmentLetter" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;

        if(user_type == 2 ){
          editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editAssignmentLetter" data-backdrop="static" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
          deleteButton = '<button class="button_delete btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deleteAssignmentLetter" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
        }else{
          deleteButton = '';
          editButton = '';
        }
     
        return '<div class="text-center" style="min-width:100px">'+downloadPdf+viewButton+editButton+deleteButton+'</div>';
      }
    }
    ],
    drawCallback: function(data, type, row) {
      $(".button_view").click(function(e){
        var id = $(this).data('id');
        toview(id);
      })
      $(".button_edit").click(function(e){
        var id = $(this).data('id');
        toedit(id);
      })
      $(".button_delete").click(function(e){
        var id = $(this).data('id');
        todelete(id);
      })
      $(".button_email").click(function(e){
        var id = $(this).data('id');
        toemail(id);
      })
      $(".button_approve").click(function(e){
        var id = $(this).data('id');
        toapprove(id);
      })
      $(".button_download_pdf").click(function(e){
        var id = $(this).data('id');
        window.location = base_url+"index.php/AssignmentLetter/download_pdf?id=" + id;
      })
    }
  });

  datatable.on('draw', function () {
    $('td').addClass('wrap-text')
  });


  $('.select2').select2()
  $('.select2-search__field').css("width", "auto");

});

function toview(id){
  $.ajax({
    url: base_url+"index.php/AssignmentLetter/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#viewAssignmentLetter');
        modal.find('[name="date"]').val(result.data.date);
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="projects_id"]').val(result.data.projects_id);
        modal.find('[name="purpose"]').val(result.data.purpose);
        modal.find('[name="destination"]').val(result.data.destination);

        var dataTo = result.data.recipient_id;
        $('#viewSelectTo').val(dataTo).change();
        $('#viewSelectTo').attr('disabled','disabled');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

function toedit(id){
  $.ajax({
    url: base_url+"index.php/AssignmentLetter/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editAssignmentLetter');
        var dataTo = result.data.recipient_id;
        modal.find('[name="date"]').val(result.data.date);
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="projects_id"]').val(result.data.projects_id);
        modal.find('[name="purpose"]').val(result.data.purpose);
        modal.find('[name="destination"]').val(result.data.destination);
        $("#alert-edit, .error").addClass('hidden');

        $('#editSelectTo').val(dataTo).change();
        // $('#editSelectCc').val(dataCc).change();
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}


$(document).ready(function(){
  $("#button-update").on('click',function(e) {
    e.preventDefault();
    var isFormValid = $("#editform").valid();
    var jsonPost = $("form#editform").serializeArray();
    if(isFormValid){
      $.ajax({
        url: base_url+"index.php/AssignmentLetter/do_update",
        type: 'POST',
        data : jsonPost,
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#editAssignmentLetter');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            $("#alert-edit").removeClass('hidden');
            $("#message-edit").text(response.message);
          }
        }
      });
    }
  });
});

$(document).ready(function(){
  $("#button-add").on('click',function(e) {
    e.preventDefault();
    var isFormValid = $("#addform").valid();
    var jsonPost = $("form#addform").serializeArray();
    if (isFormValid) {
      $.ajax({
        url: base_url+"index.php/AssignmentLetter/do_save",
        type: 'POST',
        data : jsonPost,
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#addAssignmentLetter');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            $("#alert-add").removeClass('hidden');
            $("#message-add").text(response.message);
          }
        }
      });
    }
  });
});
function todelete(id){
  $.ajax({
    url: base_url+"index.php/AssignmentLetter/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deleteAssignmentLetter');
        modal.find('[name="id"]').val(result.data.id);
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

function toapprove(id){
  $.ajax({
    url: base_url+"index.php/assignment_letter/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#approvePermit');
        modal.find('[name="id"]').val(result.data.id);
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

$('#button-delete').click(function(){
  var modal= $("#deleteAssignmentLetter");
  var id = modal.find('[name="id"]').val();
  $.ajax({
    url: base_url+"index.php/AssignmentLetter/do_delete",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatable.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
})

$('form').each(function(){
  $(this).validate({
    errorClass: "validate_error",
    rules: {
      date: {
        required: true,
        maxlength: 100
      },
      destination:{
        required: true,
        maxlength: 100
      }
    },
    messages: {
      date: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 100 '+appLang.form_char,
      },
      destination: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 100 '+appLang.form_char,
      },
    }
  });
});



$('.modal').on('hidden.bs.modal', function () {
  $('form').each(function(){
    $(this).trigger('reset');
    $(this).validate().resetForm();
    $(this).find('.validate_error').removeClass('validate_error');
  });

  $(".select2").trigger("change");
  $('.select2-search__field').css("width", "auto");

});
