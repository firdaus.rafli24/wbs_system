var socket = io.connect( ConfValue.node_url+':'+ConfValue.node_port );

$(document).ready(function(){

  socket.on('connect_error', function(e){ 
    Swal.fire({
      title: 'Error!',
      html: 'Connection Nodejs error',
      icon: 'error',
      confirmButtonText: 'OK'
    }).then((result) => {
      if (result.value) {
        window.location.reload();
      }
    });
  });


  $("#signin").on('click',function(e) {
    e.preventDefault();

    var emailenter = $("#emailenter").val();
    var passwordenter = $("#passwordenter").val();
    var text = $(this).text();
    $.ajax({
      url: base_url+"index.php/Auth/login",
      type: "POST",
      data:{
        email: emailenter,
        password: passwordenter
      },
      datatype: "json",
      success: function(response){
        if (response.status) {
          socket.emit('login_success', response);
          window.location.href=base_url+"index.php/Home";
        }else {
          Swal.fire({
            title: 'Error!',
            html: response.message,
            icon: 'error',
            confirmButtonText: 'OK'
          });

        }
      }
    });
  });
});

