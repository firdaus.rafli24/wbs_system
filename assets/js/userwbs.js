var currDate = new Date();
var startDate = new Date();

var past_date = myConfig.PAST_ACTIVITY_DATE.value;

if( myConfig.PAST_ACTIVITY_DATE.enable == 0 || myConfig.PAST_ACTIVITY_DATE.value < 0 || myConfig.PAST_ACTIVITY_DATE.value == ''){
  past_date = 0;
}
var strDate = new Date().toISOString().slice(0,10);
startDate.setDate(startDate.getDate() - past_date);
$(document).ready(function () {
  var icount=0;
  defaultdisplay(icount);
});
$('.datepicker_add').datepicker({
  format: 'yyyy-mm-dd',
  autoclose: true,
  startDate : startDate,
})
.on('changeDate', function () {
  $("input#"+$(this).attr('id')).removeClass('error');
  $("label#"+$(this).attr('id')+"-error").remove();
});
var alerts = function() {
  return {
    showError: function(message) {
      $(".error").html(message);
      $(".error").removeClass('d-none');
    },
    hideError: function() {
      $(".error").html("");
      $(".error").addClass('d-none');
    }
  }
}();
var datatableLog;
var datatableAddActivity;
$(document).ready(function () {
  $("#addtaskform").validate();
  addRule(0);
  $('.sidebar-menu').tree();

  $('.select2').select2()
  $('.select2-search__field').css("width", "auto");

  datatableLog = $('#table_log').DataTable({
    scrollX: true,serverSide: true,paginate: true,
    pageLength: 10,
    autoWidth: false,
    info: false,
    scrollY: '150vh',
    scrollCollapse: true,
    "order": [[17, 'desc']],
    "ajax": {
      url: base_url+"index.php/UserWBS/datatable",type: 'POST',
      data: function (d) {
        d.hrd =  $('input:radio[name="optradio"]:checked').val();
        addcategory(d.hrd);
      }
    },
    columnDefs: [
    {targets: 0, width:"3%", className: "text-center", render: function (data, type, row, meta) {
      return meta.row + meta.settings._iDisplayStart + 1;
    }},
    {targets: 1, width:"5%", className: "text-center"},
    {targets: 2, width:"10%"},
    {targets: 3, width:"10%"},
    {targets: 4, width:"10%"},
    {targets: 5, width:"10%"},
    {targets: 6, width:"5%", className: "text-center"},
    {targets: 7, width:"5%", className: "text-center"},
    {targets: 8, width:"5%", className: "text-center"},
    {targets: 9, width:"5%", className: "text-center"},
    {targets: 10, width:"5%", className: "text-center"},
    {targets: 11, width:"5%", className: "text-center"},
    {targets: 12, width:"5%", className: "text-center"},
    {targets: 13, width:"5%", className: "text-center"},
    {targets: 14, width:"5%", className: "text-center"},
    {targets: 15, width:"9%", "render": function ( data, type, row ) { return "<textarea name='customTextArea' readonly>"+data+"</textarea>"; }  },
    {targets: 16, width:"6%", className: "text-center"},
    {targets: 17, width:"8%", sortable: false,
    render: function (data, type, row) {
      var viewButton = '<button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewTask" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;
      var editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editTask"" data-backdrop="static" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
      var deleteButton = '<button class="button_delete btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deleteTask" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
      return '<div class="text-center" style="min-width:100px">'+viewButton+editButton+deleteButton+'</div>';
    }
  }
  ],
  drawCallback: function(data, type, row) {
    $(".button_view").click(function(e){
      var id = $(this).data('id');
      toview(id);
    })
    $(".button_edit").click(function(e){
      var id = $(this).data('id');
      toedit(id);
      })
    $(".button_delete").click(function(e){
      var id = $(this).data('id');
      todelete(id);
    })
    var tx = document.getElementsByName("customTextArea");
    for (var i = 0; i < tx.length; i++) {
      if (tx[i].value == '') {
        tx[i].setAttribute("style", "height:0px;resize:none;border:0px;background: transparent;");
      } else {
        tx[i].setAttribute("style", "min-height:80px;max-height:110px;resize:none;border:0px;background: transparent;");
      }
      tx[i].addEventListener("input", OnInput, false);
    }
  }
});

  datatableAddActivity = $('#table_addActivity').DataTable({
    scrollX: true,
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"2%", className: "hidden"},
    {targets: 1, width:"5%"},
    {targets: 2, width:"13%"},
    {targets: 3, width:"5%"},
    {targets: 4, width:"5%"},
    {targets: 5, width:"8%"},
    {targets: 6, width:"6%"},
    {targets: 7, width:"6%"},
    {targets: 8, width:"6%"},
    {targets: 9, width:"6%"},
    {targets: 10, width:"6%"},
    {targets: 11, width:"6%"},
    {targets: 12, width:"6%"},
    {targets: 13, width:"6%"},
    {targets: 14, width:"6%"},
    {targets: 15, width:"10%"},
    {targets: '_all', sortable:false}
    ]
  });

  $("input:radio[name=optradio]").click(function() {
    datatableLog.ajax.reload();
  });
  //Date picker
  $('.datepicker').datepicker({
    autoclose: true,
    startDate : startDate,
  });

  function OnInput(e) {
    this.style.height = "auto";
    this.style.height = (this.scrollHeight) + "px";
  }

  //Timepicker
  $('.timepicker').timepicker({
    showInputs: false,
    showSeconds: false,
    showMeridian: false,
    defaultTime: false
  });

  document.getElementById("addNewActivity").addEventListener("click", moreActivity);

  function moreActivity(e) {
    e.preventDefault();
    var count = $('#addtaskform tbody tr').length;

    var strDate = new Date().toISOString().slice(0,10);
    // var strDate = new Date();

    if (e.clientX != 0) {
      // var itm = document.getElementById("tableRow"); //yang akan dicopy
      // document.getElementById("tableBody").appendChild(cln); //target paste
      getDropdown('project','category','pic','excecutedby','task_status','add',count);
      output = '<tr id="row_'+count+'">';
      output += '<td>'+
      '<select name="project_add'+count+'" id="project_add'+count+'" class="form-control" >'+
      '</select>'+
      '</td>';
      output+= '<td>'+
      '<select name="category_add'+count+'" id="category_add'+count+'" class="form-control" >'+
      '<option value="" selected="" disabled="">Pilih</option>'+
      '</select>'+
      '</td>';
      output+= '<td>'+
      '<select name="pic_add'+count+'" id="pic_add'+count+'" class="form-control select2" >'+
      '<option value="" selected="" disabled="">Pilih</option>'+
      '</select>'+
      '</td>';
      output+= '<td>'+
      '<input type="text" name="excecutedby_add'+count+'" id="excecutedby_add'+count+'" class="form-control"  value="" placeholder="" disabled>'+
      '</td>';
      output+='<td>'+
      '<div class="form-group">'+
      '<input name="task_name_add'+count+'" id="task_name_add'+count+'" type="text" class="form-control" placeholder="Enter ...">'+
      '</div>'+
      '</td>';
      output+=  '<td>'+
      '<div class="input-group">'+
      '<input name="start_time_add'+count+'" id="start_time_add'+count+'" type="text" class="form-control timepicker">'+
      '<div class="input-group-addon icon-modal"><i class="fa fa-clock-o"></i></div>'+
      '  </div>'+
      '</td>';
      output+='<td>'+
      '<div class="input-group form-group">'+
      '<input name="stop_time_add'+count+'" id="stop_time_add'+count+'"type="text" class="form-control timepicker">'+
      '<div class="input-group-addon icon-modal"><i class="fa fa-clock-o"></i></div>'+
      '</div>'+
      '</td>';
      output+='<td>'+
      '<div class="input-group date form-group">'+
      '<input type="text" name="start_date_add'+count+'" id="start_date_add'+count+'" class="form-control pull-right datepicker" value="'+strDate+'">'+
      '<div class="input-group-addon icon-modal" ><i class="fa fa-calendar"></i></div>'+
      '</div>'+
      '</td>';
      output+='<td>'+
      '<div class="input-group date form-group">'+
      '<input type="text" name="due_date_add'+count+'" id="due_date_add'+count+'" class="form-control pull-right datepicker" value="'+strDate+'">'+
      '<div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>'+
      '</div>'+
      '</td>';
      output+='<td>'+
      '<div class="input-group date form-group">'+
      '<input type="text" name="due_date_revised_add'+count+'" id="due_date_revised_add'+count+'" class="form-control pull-right datepicker" >'+
      '<div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>'+
      '</div>'+
      '</td>';
      output+= '<td>'+
      '<select name="task_status_add'+count+'" id="task_status_add'+count+'" class="form-control">'+
      '</select>'+
      '</td>';
      output+='<td>'+
      '<div class="form-group">'+
      '<input name="estimated_hour_add'+count+'" id="estimated_hour_add'+count+'" type="number" class="form-control" placeholder="0.00" >'+
      '</div>'+
      '</td>';
      output+='<td>'+
      '<div class="form-group">'+
      '<input name="actual_hour_add'+count+'" id="actual_hour_add'+count+'" type="number" class="form-control" readonly style="color: #787878;">'+
      '</div>'+
      '</td>';
      output+='<td>'+
      '<div class="input-group form-group">'+
      '<input name="task_percentage_add'+count+'" id="task_percentage_add'+count+'" type="number" class="form-control">'+
      '<span class="input-group-addon icon-modal">%</span>'+
      '</div>'+
      '</td>';
      output+='<td>'+
      '<div class="form-group">'+
      '<textarea name="notes_add'+count+'"class="form-control" style="resize: none;height: 34px;" id="notes_add'+count+'" rows="1" placeholder="Enter ..." maxlength="500"></textarea>'+
      '</div>'+
      '</td>';
      $('#tableBody').append(output);
      addRule(count);

      $('.select2').select2()
      $('.select2-search__field').css("width", "auto");

      $("#start_date_add"+count).datepicker({
        todayBtn:  1,
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate : startDate,
      }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date);
        $('#due_date_add'+count).datepicker('setStartDate', minDate);
        $('#start_date_add'+count).removeClass('error');
        $('#start_date_add'+count+'-error').remove();
      });

      $("#due_date_add"+count).datepicker({
        format: 'yyyy-mm-dd',
        startDate : startDate,
        autoclose: true,
      })
      .on('changeDate', function (selected) {
        var maxDate = new Date(selected.date);
        var minDate = new Date(selected.date);
        $('#due_date_revised_add'+count).datepicker('setStartDate', minDate);
        $('#start_date_add'+count).datepicker('setEndDate', maxDate);
        $('#due_date_add'+count).removeClass('error');
        $('#due_date_add'+count+'-error').remove();
      });

      $("#due_date_revised_add"+count).datepicker({
        autoclose: true,
        format: 'yyyy-mm-dd',
        startDate : startDate,
      })
      .on('changeDate', function (selected) {
        var maxDate = new Date(selected.date);
        $('#due_date_add'+count).datepicker('setEndDate', maxDate);
      });

      $("#stop_time_add"+count).timepicker({
        defaultTime: 'value',
        minuteStep: 1,
        disableFocus: true,
        template: 'dropdown',
        showMeridian:false
      });
      $("#start_time_add"+count).timepicker({
        defaultTime: 'value',
        minuteStep: 1,
        disableFocus: true,
        template: 'dropdown',
        showMeridian:false
      });
    }
    calculateActualHour(count);
  }
  $('#addactivity-button').click(function (e) {
    e.preventDefault();
    getDropdownTemplateData();
    $('#addtaskform').trigger('reset');
    $("#addtaskform").find('.error').removeClass('.error');
    getAll('add');
    var parent = document.getElementById("tableBody");
    var rowlength = $('#addtaskform tbody tr').length;
    if (rowlength>0) {
      for (var i = 0; i < rowlength; i++) {
        parent.removeChild(parent.lastElementChild);
      }
      count = 0;
    }
    moreActivity(e);
  });
  document.getElementById("undoActivity").addEventListener("click", undoLastRow);

  function undoLastRow(e) {
    e.preventDefault();
    var parent = document.getElementById("tableBody");
    if($('#tableBody tr').length > 1){
      parent.removeChild(parent.lastElementChild);
      count = count - 1;
    }
  }
  $("#submittask").on('click',function(e) {
    e.preventDefault();
    var rowlength = $('#addtaskform tbody tr').length;
    var form_data = $('#addtaskform').serializeArray();
    var isFormValid = $("#addtaskform").valid();
    if (isFormValid) {
      form_data.push({name:'rowlength',value:rowlength});
      socket.emit('sm_checkvalidasi', form_data);
      $.ajax({
        url: base_url+"index.php/UserWBS/do_save",
        type: 'POST',
        data: form_data,
        datatype:"json",
        success: function(result){
          if (result.status) {
            var modal = $('#addActivity');
            modal.modal('hide');
            datatableLog.ajax.reload();
            $('#start_date_add0').datepicker('setDate', 'today');
            $('#due_date_add0').datepicker('setDate', 'today');
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });

  $("#cancel-add-template, #close-add-template").on('click',function() {
    var modal1 = $('#addTemplate');
    modal1.modal('hide');
  });
  $("#cancel-delete-template, #close-delete-template").on('click',function() {
    var modal1 = $('#deleteTemplate');
    modal1.modal('hide');
  });
  $("#save_template").on('click',function(e) {
    e.preventDefault();
    var modal = $('#addTemplate');
    modal.modal('show');
    modal.find('[name="template_name"]').val('');
  });
  $("#button-add-template").on('click',function(e) {
    e.preventDefault();
    var rowlength = $('#addtaskform tbody tr').length;
    var form_data = $('#addtaskform').serializeArray();

    var modal = $('#addTemplate');
    var templatename = modal.find('[name="template_name"]').val();

    var isFormValid = $("#addformtemplate").valid();
    if (isFormValid) {
      form_data.push({name:'rowlength',value:rowlength});
      form_data.push({name:'templatename',value:templatename});
      socket.emit('sm_checkvalidasi', form_data);
      $.ajax({
        url: base_url+"index.php/UserWBS/do_save_template",
        type: 'POST',
        data: form_data,
        datatype:"json",
        success: function(result){
          if (result.status) {
            getDropdownTemplateData();
            var modal2 = $('#addTemplate');
            modal2.modal('hide');
            var parent = document.getElementById("tableBody");
            var rowlength = $('#addtaskform tbody tr').length;
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
  $('#button-delete-template').click(function(e){
    e.preventDefault();
    var modal= $("#deleteTemplate");
    var id = modal.find('[name="id"]').val();
    data: {user_id:id};
    $.ajax({
      url: base_url+"index.php/UserWBS/do_delete_template",
      type: 'POST',
      datatype:"json",
      data: {user_id: id},
      success: function (result) {
        if (result.status) {
          getDropdownTemplateData();
          var modal2 = $('#deleteTemplate');
          modal2.modal('hide');
          var parent = document.getElementById("tableBody");
          var rowlength = $('#addtaskform tbody tr').length;
          // if (rowlength>0) {
          //   for (var i = 0; i < rowlength; i++) {
          //     parent.removeChild(parent.lastElementChild);
          //   }
          //   count = 0;
          // }
          // moreActivity(e);
        }else {
          Swal.fire({
            title: 'Error!',
            html: result.message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        }
      }
    })
  });
});
$(document).ready(function(){
  $("#delete_template").on("click", function (e) {
    e.preventDefault();
    var id= $('#select_template').val();
    if (id!='') {
      $.ajax({
        url: base_url+"index.php/UserWBS/get_template",
        type: 'POST',
        datatype:"json",
        data: {user_id: id},
        success: function (result) {
          if (result.status) {
            var modal = $('#deleteTemplate');
            modal.modal('show');
            modal.find('[name="id"]').val(result.data.id);
          }
        }
      });
    }
  });
});
function getDropdownTemplateData() {
  $.ajax({
    url: base_url+"index.php/UserWBS/get_template_data",
    type: "POST",
    success: function (res) {
      var el1 = $("#select_template");
      var el2 = $("#excecutedby_add0");
      el1.empty();
      if (res.status) {
        el1.append($("<option></option>").attr({"value":'', "selected":'', "disabled":''}).text('Select Template'));
        $.each(res.data.template, function (count,template) {
          el1.append($("<option></option>").attr("value", template.id).text(template.name));
        });
        // el2.val(res.data.excecutedby);
      }
    }
  });
};

$(document).ready(function(){
  $("#select_template").on("change", function () {
    $.get(base_url+"index.php/UserWBS/find_template_wbs/"+$(this).val(),function (result){
      var selected_option = [];
      if(result.length > 0){
        $("#tableBody").empty();
        $.each(result, function (index, value) {
          count = index;
          selected_option.push({
            'project_add': value.project,
            'category_add': value.category,
            'pic_add': value.pic,
            'task_status_add': value.task_status,
          });
          getDropdownTemplate('project','category','pic','excecutedby','task_status','add',count, selected_option);
          output = '<tr id="row_'+count+'">';
          output += '<td>'+
          '<select name="project_add'+count+'" id="project_add'+count+'" class="form-control" >'+
          '</select>'+
          '</td>';
          output+= '<td>'+
          '<select name="category_add'+count+'" id="category_add'+count+'" class="form-control" >'+
          '</select>'+
          '</td>';
          output+= '<td>'+
          '<select name="pic_add'+count+'" id="pic_add'+count+'" class="form-control select2" >'+
          '</select>'+
          '</td>';
          output+= '<td>'+
          '<input type="text" name="excecutedby_add'+count+'" id="excecutedby_add'+count+'" class="form-control"  value="" placeholder="" disabled>'+
          '</td>';
          output+='<td>'+
          '<div class="form-group">'+
          '<input name="task_name_add'+count+'" id="task_name_add'+count+'" type="text" class="form-control" placeholder="Enter ...">'+
          '</div>'+
          '</td>';
          output+=  '<td>'+
          '<div class="input-group">'+
          '<input name="start_time_add'+count+'" id="start_time_add'+count+'" type="text" class="form-control timepicker">'+
          '<div class="input-group-addon icon-modal"><i class="fa fa-clock-o"></i></div>'+
          '  </div>'+
          '</td>';
          output+='<td>'+
          '<div class="input-group form-group">'+
          '<input name="stop_time_add'+count+'" id="stop_time_add'+count+'"type="text" class="form-control timepicker">'+
          '<div class="input-group-addon icon-modal"><i class="fa fa-clock-o"></i></div>'+
          '</div>'+
          '</td>';
          output+='<td>'+
          '<div class="input-group date form-group">'+
          '<input type="text" name="start_date_add'+count+'" id="start_date_add'+count+'" class="form-control pull-right datepicker" value="'+strDate+'">'+
          '<div class="input-group-addon icon-modal" ><i class="fa fa-calendar"></i></div>'+
          '</div>'+
          '</td>';
          output+='<td>'+
          '<div class="input-group date form-group">'+
          '<input type="text" name="due_date_add'+count+'" id="due_date_add'+count+'" class="form-control pull-right datepicker" value="'+strDate+'">'+
          '<div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>'+
          '</div>'+
          '</td>';
          output+='<td>'+
          '<div class="input-group date form-group">'+
          '<input type="text" name="due_date_revised_add'+count+'" id="due_date_revised_add'+count+'" class="form-control pull-right datepicker" >'+
          '<div class="input-group-addon icon-modal"><i class="fa fa-calendar"></i></div>'+
          '</div>'+
          '</td>';
          output+= '<td>'+
          '<select name="task_status_add'+count+'" id="task_status_add'+count+'" class="form-control">'+
          '</select>'+
          '</td>';
          output+='<td>'+
          '<div class="form-group">'+
          '<input name="estimated_hour_add'+count+'" id="estimated_hour_add'+count+'" type="number" class="form-control" placeholder="0.00" >'+
          '</div>'+
          '</td>';
          output+='<td>'+
          '<div class="form-group">'+
          '<input name="actual_hour_add'+count+'" id="actual_hour_add'+count+'" type="number" class="form-control" readonly style="color: #787878;">'+
          '</div>'+
          '</td>';
          output+='<td>'+
          '<div class="input-group form-group">'+
          '<input name="task_percentage_add'+count+'" id="task_percentage_add'+count+'" type="number" class="form-control">'+
          '<span class="input-group-addon icon-modal">%</span>'+
          '</div>'+
          '</td>';
          output+='<td>'+
          '<div class="form-group">'+
          '<textarea name="notes_add'+count+'"class="form-control" style="resize: none;height: 34px;" id="notes_add'+count+'" rows="1" placeholder="Enter ..." maxlength="500"></textarea>'+
          '</div>'+
          '</td>';

          $('#tableBody').append(output);
          $('#task_name_add'+count).val(value.task_name);
          $('#start_time_add'+count).val(value.start_time);
          $('#stop_time_add'+count).val(value.stop_time);
          if (value.estimated_hour>0) {
            $('#estimated_hour_add'+count).val(value.estimated_hour);
          }
          $('#task_percentage_add'+count).val(value.task_percentage);
          defaultdisplay(count);
        });
        $('.select2').select2()
        $('.select2-search__field').css("width", "auto");
      }
    });
  });
});
function defaultdisplay(count) {
  $("#start_date_add"+count).datepicker({
    todayBtn:  1,
    autoclose: true,
    format: 'yyyy-mm-dd',
    startDate : startDate,
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date);
    $('#due_date_add'+count).datepicker('setStartDate', minDate);
    $('#start_date_add'+count).removeClass('error');
    $('#start_date_add'+count+'-error').remove();
  });

  $("#due_date_add"+count).datepicker({
    format: 'yyyy-mm-dd',
    startDate : startDate,
    autoclose: true,
  })
  .on('changeDate', function (selected) {
    var maxDate = new Date(selected.date);
    var minDate = new Date(selected.date);
    $('#due_date_revised_add'+count).datepicker('setStartDate', minDate);
    $('#start_date_add'+count).datepicker('setEndDate', maxDate);
    $('#due_date_add'+count).removeClass('error');
    $('#due_date_add'+count+'-error').remove();
  });

  $("#due_date_revised_add"+count).datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    startDate : startDate,
  })
  .on('changeDate', function (selected) {
    var maxDate = new Date(selected.date);
    $('#due_date_add'+count).datepicker('setEndDate', maxDate);
  });

  $("#stop_time_add"+count).timepicker({
    defaultTime: 'value',
    minuteStep: 1,
    disableFocus: true,
    template: 'dropdown',
    showMeridian:false
  });
  $("#start_time_add"+count).timepicker({
    defaultTime: 'value',
    minuteStep: 1,
    disableFocus: true,
    template: 'dropdown',
    showMeridian:false
  });
  calculateActualHour(count);
}

function calculateActualHour(count) {
  for (var i = 0; i < count+1; i++) {
    $("#stop_time_add"+count).change(function () {
      var start = $( "#start_time_add"+count).val();
      var newstart =  new Date("1970-1-1 " + start);
      var stop = $( "#stop_time_add"+count).val();
      var newstop = new Date("1970-1-1 " + stop);
      var diff = (newstop-newstart) / 1000 / 60 / 60;
      var n = diff.toFixed(1);
      n = (n < 0) ? 24 - (n * -1) : n;
      $( "#actual_hour_add"+count ).text(n);
      $( "#actual_hour_add"+count ).val(n);
    }).change();
    $("#start_time_add"+count).change(function () {
      var start = $( "#start_time_add"+count).val();
      var newstart =  new Date("1970-1-1 " + start);
      var stop = $( "#stop_time_add"+count).val();
      var newstop = new Date("1970-1-1 " + stop);
      var diff = (newstop-newstart) / 1000 / 60 / 60;
      var n = diff.toFixed(1);
      n = (n < 0) ? 24 - (n * -1) : n;
      $( "#actual_hour_add"+count ).text(n);
      $( "#actual_hour_add"+count ).val(n);
    }).change();
  }
}

function addRule(count) {
  $('select[name="project_add'+count+'"]').rules("add", {
    required: true,
    messages: {
      required: appLang.form_requiredform
    }
  });
  $('select[name="category_add'+count+'"]').rules("add", {
    required: true,
    messages: {
      required: appLang.form_requiredform
    }
  });
  $('select[name="pic_add'+count+'"]').rules("add", {
    required: true,
    messages: {
      required: appLang.form_requiredform
    }
  });
  $('input[name="start_date_add'+count+'"]').rules("add", {
    required: true,
    messages: {
      required: appLang.form_requiredform
    }
  });
  $('input[name="due_date_add'+count+'"]').rules("add", {
    required: true,
    messages: {
      required: appLang.form_requiredform
    }
  });
  $('input[name="task_status_add'+count+'"]').rules("add", {
    required: true,
    messages: {
      required: appLang.form_requiredform
    }
  });
  $('input[name="task_name_add'+count+'"]').rules("add", {
    required: true,
    maxlength: 100,
    messages: {
      required: appLang.form_requiredform,
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char
    }
  });
  $('input[name="estimated_hour_add'+count+'"]').rules("add", {
    required: true,
    max: 100,
    min: 0,
    messages: {
      required: appLang.form_requiredform,
      max: appLang.form_max+' 100',
      min: appLang.form_min+' 0'
    }
  });
  $('input[name="task_percentage_add'+count+'"]').rules("add", {
    max: 100,
    min: 0,
    messages: {
      max: appLang.form_max+' 100',
      min: appLang.form_min+' 0'
    }
  });
  $('input[name="notes_add'+count+'"]').rules("add", {
    maxlength: 500,
    messages: {
      maxlength: appLang.form_maxlength+' 500 '+appLang.form_char
    }
  });
}
function getAll(view) {
  $.ajax({
    url: base_url+"index.php/UserWBS/add",
    type: "POST",
    success: function (res) {
      var el = $("#project_" + view);
      el.empty();
      el.append($("<option disabled selected></option>").attr("value", 0).text('Select'));
      if (res.status) {
        $.each(res.data, function (index, project) {
          el.append($("<option></option>").attr("value", project.id).text(project.name));
        });
      }
    }
  });
};

function getDropdown(id1,id2,id3,id4,id5,view,count) {
  $.ajax({
    url: base_url+"index.php/UserWBS/add",
    type: "POST",
    success: function (res) {
      var el1 = $("#"+id1+"_"+view+count);
      var el2 = $("#"+id2+"_"+view+count);
      var el3 = $("#"+id3+"_"+view+count);
      var el4 = $("#"+id4+"_"+view+count);
      var el5 = $("#"+id5+"_"+view+count);
      el1.empty();
      el2.empty();
      el3.empty();
      el4.empty();
      el5.empty();
      if (res.status) {
        el1.append($("<option></option>").attr({"value":'', "selected":'', "disabled":''}).text('Select Project'));
        $.each(res.data.project, function (count,project) {
          el1.append($("<option></option>").attr("value", project.id).text(project.name));
        });
        el2.append($("<option></option>").attr({"value":'', "selected":'', "disabled":''}).text('Select Category'));
        $.each(res.data.category, function (count,category) {
          el2.append($("<option></option>").attr("value", category.id).text(category.name));
        });
        el3.append($("<option></option>").attr({"value":'', "selected":'', "disabled":''}).text('Select PIC'));
        $.each(res.data.pic, function (count,pic) {
          el3.append($("<option></option>").attr("value", pic.id).text(pic.username));
        });
        el4.val(res.data.excecutedby);
        $.each(res.data.status, function (count,status) {
          el5.append($("<option></option>").attr("value", status.id).text(status.name));
        });
      }
    }
  });
};
function todelete(id){
  $.ajax({
    url: base_url+"index.php/UserWBS/get",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deleteTask');
        modal.find('[name="id"]').val(result.data.id);
      }
    }
  });
}
$('#button-delete-task').click(function(){
  var modal= $("#deleteTask");
  var id = modal.find('[name="id"]').val();
  data: {user_id:id};
  $.ajax({
    url: base_url+"index.php/UserWBS/do_delete",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal=$('#deleteTask');
        modal.modal('hide');
        datatableLog.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: response.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
});
function toview(id){
  $.ajax({
    url: base_url+"index.php/UserWBS/get",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#viewTask');
        modal.find('[name="project"]').val(result.data.projectname);
        modal.find('[name="category"]').val(result.data.categoryname);
        modal.find('[name="picid"]').val(result.data.pic);
        modal.find('[name="excecutedbyid"]').val(result.data.excecutedby);
        modal.find('[name="pic"]').val(result.data.picname);
        modal.find('[name="excecutedby"]').val(result.data.excecutedbyname);
        modal.find('[name="task_name"]').val(result.data.task_name);
        modal.find('[name="start_time"]').val(result.data.start_time);
        modal.find('[name="stop_time"]').val(result.data.stop_time);
        modal.find('[name="start_date"]').val(result.data.start_date);
        modal.find('[name="due_date"]').val(result.data.due_date);
        modal.find('[name="due_date_revised"]').val(result.data.due_date_revised);
        modal.find('[name="estimated_hour"]').val(result.data.estimated_hour);
        modal.find('[name="actual_hour"]').val(result.data.actual_hour);
        modal.find('[name="task_percentage"]').val(result.data.task_percentage);
        modal.find('[name="task_status"]').val(result.data.statusname);
        modal.find('[name="notes"]').val(result.data.notes);
        modal.modal('show');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}
function addcategory(hrd) {
    $.ajax({
      url: base_url+"index.php/UserWBS/addCategory",
      type: "POST",
      success: function (res) {
        var el1 = $("#category_edit");
        el1.empty();
        if (res.status) {
          if (hrd==1) {
            $.each(res.data.all_category, function (count,all_category) {
              el1.append($("<option></option>").attr("value", all_category.id).text(all_category.name));
            });
          }else{
            $.each(res.data.task_category, function (count,task_category) {
              el1.append($("<option></option>").attr("value", task_category.id).text(task_category.name));
            });
          }
        }
      }
    });
};
function toedit(id){
  $('#edittaskform').trigger('reset');
  $('#edittaskform').validate().resetForm();
  $("#edittaskform").find('.validate_error').removeClass('validate_error');
  $.ajax({
    url: base_url+"index.php/UserWBS/get",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editTask');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="project"]').val(result.data.project);
        modal.find('[name="category"]').val(result.data.category);
        modal.find('[name="picid"]').val(result.data.pic);
        modal.find('[name="excecutedbyid"]').val(result.data.excecutedby);
        modal.find('[name="pic"]').val(result.data.pic);
        modal.find('[name="excecutedby"]').val(result.data.excecutedbyname);
        modal.find('[name="task_name"]').val(result.data.task_name);
        modal.find('[name="start_time"]').val(result.data.start_time);
        modal.find('[name="stop_time"]').val(result.data.stop_time);
        modal.find('[name="start_date"]').val(result.data.start_date);
        modal.find('[name="due_date"]').val(result.data.due_date);
        modal.find('[name="due_date_revised"]').val(result.data.due_date_revised);
        modal.find('[name="estimated_hour"]').val(result.data.estimated_hour);
        modal.find('[name="actual_hour"]').val(result.data.actual_hour);
        modal.find('[name="task_percentage"]').val(result.data.task_percentage);
        modal.find('[name="task_status"]').val(result.data.task_status);
        modal.find('[name="notes"]').val(result.data.notes);
        modal.modal('show');
        var el1 = $("#category_edit");

      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}
$("#edittaskform").validate({
  errorClass: "validate_error",
  rules: {
    project: {
      required: true
    },
    category: {
      required: true
    },
    pic:{
      required: true,
    },
    excecutedby:{
      required: true,
    },
    task_name:{
      required: true,
      maxlength:100
    },
    start_time:{
      required: true,
    },
    stop_time:{
      required: true,
    },
    start_date:{
      required: true,
      date: true
    },
    due_date:{
      required: true,
      date: true
    },
    due_date_revised:{
      date: true
    },
    task_status:{
      required: true,
    },
    estimated_hour:{
      required: true,
      number:true,
      min: 0
    },
    task_percentage:{
      // required: true,
      number:true,
      min: 0,
      max:100
    },
    notes:{
      maxlength: 500,
    }
  },
  messages: {
    required: appLang.form_requiredform,
    task_name:{
      maxlength:appLang.form_maxlength+' 100 '+appLang.form_char
    },
    estimated_hour:{
      min: appLang.form_min+' 0 '
    },
    task_percentage:{
      min: appLang.form_min+' 0%',
      max: appLang.form_max+' 100%',
    },
    notes:{
      maxlength: appLang.form_maxlength+' 300 '+appLang.form_char
    }
  }
});
$(document).ready(function(){
  $("#closeformadd").on('click',function(e) {
    e.preventDefault();
    $("#addtaskform").trigger("reset");
    $("#addtaskform").validate().resetForm();
    datatableLog.ajax.reload();
    $('#start_date_add0').datepicker('setDate', 'today');
    $('#due_date_add0').datepicker('setDate', 'today');
  });
});
function resetCalendar() {
  $('#start_date_add0').datepicker('setDate', new Date);
  $('#due_date_add0').datepicker('setDate', new Date);
}
$(document).ready(function(){
  $("#button-update-task").on('click',function(e) {
    e.preventDefault();
    var modal= $("#editTask");
    var id= modal.find('[name="id"]').val();
    var project = modal.find('[name="project"]').val();
    var category = modal.find('[name="category"]').val();
    var pic = modal.find('[name="pic"]').val();
    var excecutedby = modal.find('[name="excecutedbyid"]').val();
    var task_name = modal.find('[name="task_name"]').val();
    var start_time = modal.find('[name="start_time"]').val();
    var stop_time = modal.find('[name="stop_time"]').val();
    var start_date = modal.find('[name="start_date"]').val();
    var due_date= modal.find('[name="due_date"]').val();
    var due_date_revised = modal.find('[name="due_date_revised"]').val();
    var estimated_hour = modal.find('[name="estimated_hour"]').val();
    var diff = ( new Date("1970-1-1 " + stop_time) - new Date("1970-1-1 " + start_time) ) / 1000 / 60 / 60;
    diff = (diff < 0) ? 24 - (diff * -1) : diff;
    var actual_hour = diff;

    var task_percentage = modal.find('[name="task_percentage"]').val();
    var task_status = modal.find('[name="task_status"]').val();
    var notes = modal.find('[name="notes"]').val();
    modal.find('[name="notes"]').val();
    var form = $('#edittaskform');
    var valid = form.valid()
    if(valid){
      $.ajax({
        url: base_url+"index.php/UserWBS/do_update",
        type: 'POST',
        data: {user_id: id,
          project: project,
          category: category,
          pic:pic,
          excecutedby:excecutedby,
          task_name:task_name,
          start_time:start_time,
          stop_time:stop_time,
          start_date:start_date,
          due_date:due_date,
          due_date_revised:due_date_revised,
          estimated_hour:estimated_hour,
          actual_hour:actual_hour,
          task_percentage:task_percentage,
          task_status:task_status,
          notes: notes
        },
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#editTask');
            modal.modal('hide');
            datatableLog.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: response.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});
$(document).ready(function(){
  $("#sdatepicker").datepicker({
    todayBtn:  1,
    autoclose: true,
    startDate : startDate,
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date);
    $('#edatepicker').datepicker('setStartDate', minDate);
  });
  $("#edatepicker").datepicker({
    startDate : startDate,
  })
  .on('changeDate', function (selected) {
    var maxDate = new Date(selected.date);
    $('#sdatepicker').datepicker('setEndDate', maxDate);
  });

  $("#edatepicker").datepicker({
    startDate : startDate,
    autoclose: true,
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date);
    $('#edatepicker_rev').datepicker('setStartDate', minDate);
  });
  $("#edatepicker_rev").datepicker()
  .on('changeDate', function (selected) {
    var maxDate = new Date(selected.date);
    $('#edatepicker').datepicker('setEndDate', maxDate);
  });
});

jQuery.extend(jQuery.validator.messages, {
  required: appLang.form_requiredform,
  email: appLang.form_email_invalid,
  date: appLang.form_date_invalid,
  number: appLang.form_number_invalid,
  maxlength: jQuery.validator.format(appLang.form_maxlength),
  minlength: jQuery.validator.format(appLang.form_minlength),
  max: jQuery.validator.format(appLang.form_max),
  min: jQuery.validator.format(appLang.form_min)
});

$('#editTask').on('show.bs.modal', function () {
  $("#sdatepicker").datepicker("setEndDate", new Date($('#edatepicker').val()) );
  $("#edatepicker").datepicker("setStartDate", new Date($('#sdatepicker').val()) );
  $("#edatepicker_rev").datepicker("setStartDate", new Date($('#edatepicker').val()) );
});

// $('.modal').on('hidden.bs.modal', function () {
//   $('form').each(function(){
//     $(this).trigger('reset');
//     $(this).validate().resetForm();
//   });
//   $(".select2").trigger("change");
//   $('.select2-search__field').css("width", "auto");
// });

function getDropdownTemplate(id1,id2,id3,id4,id5,view,count,selected) {
  $.ajax({
    url: base_url+"index.php/UserWBS/add",
    type: "POST",
    success: function (res) {
      var el1 = $("#"+id1+"_"+view+count);
      var el2 = $("#"+id2+"_"+view+count);
      var el3 = $("#"+id3+"_"+view+count);
      var el4 = $("#"+id4+"_"+view+count);
      var el5 = $("#"+id5+"_"+view+count);
      el1.empty();
      el2.empty();
      el3.empty();
      el4.empty();
      el5.empty();
      if (res.status) {
        el1.append($("<option></option>").attr({"value":'', "selected":'', "disabled":''}).text('Select Project'));
        $.each(res.data.project, function (count,project) {
          el1.append($("<option></option>").attr("value", project.id).text(project.name));
        });
        el2.append($("<option></option>").attr({"value":'', "selected":'', "disabled":''}).text('Select Category'));
        $.each(res.data.category, function (count,category) {
          el2.append($("<option></option>").attr("value", category.id).text(category.name));
        });
        el3.append($("<option></option>").attr({"value":'', "selected":'', "disabled":''}).text('Select PIC'));
        $.each(res.data.pic, function (count,pic) {
          el3.append($("<option></option>").attr("value", pic.id).text(pic.username));
        });
        el4.val(res.data.excecutedby);
        $.each(res.data.status, function (count,status) {
          el5.append($("<option></option>").attr("value", status.id).text(status.name));
        });
        $.each(selected, function (i, v) {
          var j = i;
          $('#project_add'+j).val(selected[i].project_add);
          $('#category_add'+j).val(selected[i].category_add);
          $('#pic_add'+j).val(selected[i].pic_add);
          $('#task_status_add'+j).val(selected[i].task_status_add);
        });
      }
    }
  });
};
$(document).keydown(function(objEvent) {
    if (objEvent.keyCode == 9) {  //tab pressed
        objEvent.preventDefault(); // stops its action
    }
});
