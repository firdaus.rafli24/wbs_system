var datatableQc;
var datatablePg;
var datatable;
var currDate = new Date();
var color_array = ['#00a65a','#f39c12','#00c0ef','#984ea3','#00d2d5','#ff7f00','#af8d00','#7f80cd','#b3e900','#c42e60','#a65628','#f781bf','#8dd3c7','#bebada','#fb8072','#80b1d3','#fdb462','#fccde5','#bc80bd','#ffed6f','#c4eaff','#cf8c00','#1b9e77','#d95f02','#e7298a','#e6ab02','#a6761d','#0097ff','#00d067','#000000','#252525','#525252','#737373','#969696','#bdbdbd','#f43600','#4ba93b','#5779bb','#927acc','#97ee3f','#bf3947','#9f5b00','#f48758','#8caed6','#f2b94f','#eff26e','#e43872','#d9b100','#9d7a00','#698cff','#d9d9d9','#00d27e','#d06800','#009f82','#c49200','#cbe8ff','#fecddf','#c27eb6','#8cd2ce','#c4b8d9','#f883b0','#a49100','#f48800','#27d0df','#a04a9b'];


$(document).ready(function () {
  $('.sidebar-menu').tree();

  $("#checkboxRole, #checkboxUser").prop("checked", true);
  $("#role, #user").prop("disabled", true).trigger('change');

  generateDataHrd();
  generateDataPg();
  generateDataQc();
  generateDataSa();


  $('#role').select2();
  $('#user').select2();

  $("#checkboxRole").click(function(){
    $("#role").find('option').prop("selected",false);
    $("#role").prop("disabled", ($(this).is(':checked'))).trigger('change');
    $("#checkboxUser").prop("checked", ($(this).is(':checked')));
    $("#user").prop("disabled", ($(this).is(':checked'))).trigger('change');
  });

  $("#checkboxUser").click(function(){
    $("#user").find('option').prop("selected",false);
    $("#user").prop("disabled", ($(this).is(':checked'))).trigger('change');
  });


  $("#start_date").datepicker({autoclose: true,format: 'yyyy-mm-dd', setEndDate : new Date($("#stop_date").val()),
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#stop_date').datepicker('setStartDate', new Date(selected.date.valueOf()));
  });

  $("#stop_date").datepicker({autoclose: true,format: 'yyyy-mm-dd',setStartDate : new Date($("#start_date").val()),}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', new Date(selected.date.valueOf()));
  });


$("#role").on("change", function () {
  $("#user").html('');
  $.ajax({
    url: base_url+"index.php/Dashboard/changerole",
    type: 'POST',
    datatype:"json",
    data: {id: $(this).val()},
    success: function (result) {
      if(result != 0){
        var arr = []
        $.each(result, function (index, value) {
          arr.push({id: value.id,text: value.user});
        });
        $("#user").html('').select2({data: arr});
      }
    }
  });
});

$("#search").click(function(e){
  generateDataHrd();
  generateDataPg();
  generateDataQc();
  generateDataSa();
});
});

function generateDataHrd() {

  if($("#table_hrd").length > 0 && $("#bar-chart-hrd").length > 0) {
    $('#table_hrd').DataTable().destroy();
    datatableQc = $('#table_hrd').DataTable({
      scrollX: true,serverSide: true,autoWidth: false,paging:false,ordering: false,info: false,
      ajax: {
        url: base_url+"index.php/Dashboard/data_hrd",
        type: 'POST',
        data: function (d) {
          d.start_date = $('#start_date').val();
          d.stop_date = $('#stop_date').val();
          d.role = ($('#role option:selected').length > 0) ? $('#role').val() : [''];
          d.user = ($('#user option:selected').length > 0) ? $('#user').val() : [''];
          d.allRole = $("#checkboxRole").is(":checked") ? 1 : 0;
          d.allUser = $("#checkboxUser").is(":checked") ? 1 : 0;
        },
        success: function(data) {
          if(data != 0){
            $("#content-hrd").removeClass('hidden');
            $("#bar-chart-hrd").empty();
            $('#hrd_legend').html('');
            $('#target_hrd').html('');

            var newRow='';
            var name=Object.keys(data.data_hrd);
            var a=Object.values(data.project);
            var ptime=Object.values(data.data_hrd);

            for (var i=0; i<ptime.length; i++){
              ptime[i]["name"] = name[i];
            }

            var hrd_chart = Morris.Bar({
              element: 'bar-chart-hrd',
              resize: true,
              data: ptime,
              xkey: 'name',
              ykeys: a,
              labels: a,
              hideHover: 'always',
              xLabelAngle: 75,
              stacked: true,
              colors: color_array,
              barColors : color_array,
              hideHover: 'auto',
              hoverCallback: function (index, options, content, row) {
                var idx = 0;
                var legendItem;
                $('#hrd_legend').html('');
                delete row['name'];
                $.each(row, function (n, v) {
                  var val = v.toFixed(1);
                  legendItem = $('<span class="legend-item" id="'+n+'"></span>').text(n +' ('+ val +')').prepend('<span class="legend-color">&nbsp;</span>');
                  legendItem.find('span').css('backgroundColor', hrd_chart.options.colors[idx++]);
                  $('#hrd_legend').append(legendItem)
                });
              }
            });

            hrd_chart.options.labels.forEach(function(label, i) {
              var legendItem = $('<span class="legend-item" id="'+label+'"></span>').text( label).prepend('<span class="legend-color">&nbsp;</span>');
              legendItem.find('span').css('backgroundColor', hrd_chart.options.colors[i]);
              $('#hrd_legend').append(legendItem)
            });

            for (var i=0;i<name.length;i++){
              newRow = '<tr>'+'<td>'+ name[i] +'</td>';
              for (var j=0;j<a.length;j++){
                var num_hrd = Object.values(data.data_hrd)[i][a[j]];
                if(num_hrd > 0 ){
                  newRow += '<td class="text-center">'+ num_hrd.toFixed(1) +'</td>';
                } else {
                  newRow += '<td class="text-center" style="color:#C0C0C0">'+ num_hrd +'</td>';
                }
              }
              $('#target_hrd').append(newRow);
            }
            $('.odd').css('display','none');
          }  else {
            $("#content-hrd").addClass('hidden');
          }
        }
      }
    });
  }
}

function generateDataQc() {

  if($("#table_qc").length > 0 && $("#bar-chart-qc").length > 0) {
    $('#table_qc').DataTable().destroy();
    datatableQc = $('#table_qc').DataTable({
      scrollX: true,serverSide: true,autoWidth: false,paging:false,ordering: false,info: false,
      ajax: {
        url: base_url+"index.php/Dashboard/data_qc",
        type: 'POST',
        data: function (d) {
          d.start_date = $('#start_date').val();
          d.stop_date = $('#stop_date').val();
          d.role = ($('#role option:selected').length > 0) ? $('#role').val() : [''];
          d.user = ($('#user option:selected').length > 0) ? $('#user').val() : [''];
          d.allRole = $("#checkboxRole").is(":checked") ? 1 : 0;
          d.allUser = $("#checkboxUser").is(":checked") ? 1 : 0;
        },
        success: function(data) {
          if(data != 0){
            $("#content-qc").removeClass('hidden');
            $("#bar-chart-qc").empty();
            $('#qc_legend').html('');
            $('#target_qc').html('');


            var newRow='';
            var name=Object.keys(data.data_qc);
            var a=Object.values(data.project);
            var ptime=Object.values(data.data_qc);

            for (var i=0; i<ptime.length; i++){
              ptime[i]["name"] = name[i];
            }
            var qc_chart = Morris.Bar({
              element: 'bar-chart-qc',
              resize: true,
              data: ptime,
              xkey: 'name',
              ykeys: a,
              labels: a,
              hideHover: 'always',
              xLabelAngle: 75,
              stacked: true,
              colors: color_array,
              barColors : color_array,
              hideHover: 'auto',
              hoverCallback: function (index, options, content, row) {
                var idx = 0;
                var legendItem;
                $('#qc_legend').html('');
                delete row['name'];
                $.each(row, function (n, v) {
                  var val = v.toFixed(1);
                  legendItem = $('<span class="legend-item" id="'+n+'"></span>').text(n +' ('+ val +')').prepend('<span class="legend-color">&nbsp;</span>');
                  legendItem.find('span').css('backgroundColor', qc_chart.options.colors[idx++]);
                  $('#qc_legend').append(legendItem)
                });
              }
            });

            qc_chart.options.labels.forEach(function(label, i) {
              var legendItem = $('<span class="legend-item" id="'+label+'"></span>').text( label).prepend('<span class="legend-color">&nbsp;</span>');
              legendItem.find('span').css('backgroundColor', qc_chart.options.colors[i]);
              $('#qc_legend').append(legendItem)
            });

            for (var i=0;i<name.length;i++){
              newRow = '<tr>'+'<td>'+ name[i] +'</td>';
              for (var j=0;j<a.length;j++){
                var num_qc = Object.values(data.data_qc)[i][a[j]];
                if(num_qc > 0 ){
                  newRow += '<td class="text-center">'+ num_qc.toFixed(1) +'</td>';
                } else {
                  newRow += '<td class="text-center" style="color:#C0C0C0">'+ num_qc +'</td>';
                }
              }
              $('#target_qc').append(newRow);
            }
            $('.odd').css('display','none');
          }  else {
            $("#content-qc").addClass('hidden');
          }
        }
      }
    });
  }
}

function generateDataPg() {

  if($("#table_pg").length > 0 && $("#bar-chart-pg").length > 0) {
    $('#table_pg').DataTable().destroy();
    datatableQc = $('#table_pg').DataTable({
      scrollX: true,serverSide: true,autoWidth: false,paging:false,ordering: false,info: false,
      ajax: {
        url: base_url+"index.php/Dashboard/data_pg",
        type: 'POST',
        data: function (d) {
          d.start_date = $('#start_date').val();
          d.stop_date = $('#stop_date').val();
          d.role = ($('#role option:selected').length > 0) ? $('#role').val() : [''];
          d.user = ($('#user option:selected').length > 0) ? $('#user').val() : [''];
          d.allRole = $("#checkboxRole").is(":checked") ? 1 : 0;
          d.allUser = $("#checkboxUser").is(":checked") ? 1 : 0;
        },
        success: function(data) {
          if(data != 0){
            $("#content-pg").removeClass('hidden');
            $("#bar-chart-pg").empty();
            $('#pg_legend').html('');
            $('#target_pg').html('');

            var newRow='';
            var name=Object.keys(data.data_pg);
            var a=Object.values(data.project);
            var ptime=Object.values(data.data_pg);
            console.log(data.data_pg);
            for (var i=0; i<ptime.length; i++){
              ptime[i]["name"] = name[i];
            }

            var pg_chart = Morris.Bar({
              element: 'bar-chart-pg',
              resize: true,
              data: ptime,
              xkey: 'name',
              ykeys: a,
              labels: a,
              xLabelAngle: 75,
              stacked: true,
              colors: color_array,
              barColors : color_array,
              hideHover: 'auto',
              hoverCallback: function (index, options, content, row) {
                var idx = 0;
                var legendItem;
                $('#pg_legend').html('');
                delete row['name'];
                $.each(row, function (n, v) {
                  var val = v.toFixed(1);
                  legendItem = $('<span class="legend-item" id="'+n+'"></span>').text(n +' ('+ val +')').prepend('<span class="legend-color">&nbsp;</span>');
                  legendItem.find('span').css('backgroundColor', pg_chart.options.colors[idx++]);
                  $('#pg_legend').append(legendItem)
                });
              }
            });

            pg_chart.options.labels.forEach(function(label, i) {
              var legendItem = $('<span class="legend-item" id="'+label+'"></span>').text( label).prepend('<span class="legend-color">&nbsp;</span>');
              legendItem.find('span').css('backgroundColor', pg_chart.options.colors[i]);

              $('#pg_legend').append(legendItem)
            });

            for (var i=0;i<name.length;i++){
              newRow = '<tr>'+'<td>'+ name[i] +'</td>';
              for (var j=0;j<a.length;j++){
                var num_pg = Object.values(data.data_pg)[i][a[j]];
                if(num_pg > 0 ){
                  newRow += '<td class="text-center">'+ num_pg.toFixed(1) +'</td>';
                } else {
                  newRow += '<td class="text-center" style="color:#C0C0C0">'+ num_pg +'</td>';
                }
              }
              $('#target_pg').append(newRow);
            }
            $('.odd').css('display','none');
          }  else {
            $("#content-pg").addClass('hidden');
          }
        }
      }
    });
  }
}

function generateDataSa() {

  if($("#table_sa").length > 0 && $("#bar-chart-sa").length > 0) {
    $('#table_sa').DataTable().destroy();
    datatableQc = $('#table_sa').DataTable({
      scrollX: true,serverSide: true,autoWidth: false,paging:false,ordering: false,info: false,
      ajax: {
        url: base_url+"index.php/Dashboard/data_sa",
        type: 'POST',
        data: function (d) {
          d.start_date = $('#start_date').val();
          d.stop_date = $('#stop_date').val();
          d.role = ($('#role option:selected').length > 0) ? $('#role').val() : [''];
          d.user = ($('#user option:selected').length > 0) ? $('#user').val() : [''];
          d.allRole = $("#checkboxRole").is(":checked") ? 1 : 0;
          d.allUser = $("#checkboxUser").is(":checked") ? 1 : 0;
        },
        success: function(data) {
          if(data != 0){
            $("#content-sa").removeClass('hidden');
            $("#bar-chart-sa").empty();
            $('#sa_legend').html('');
            $('#target_sa').html('');

            var newRow='';
            var name=Object.keys(data.data_sa);
            var a=Object.values(data.project);
            var ptime=Object.values(data.data_sa);

            for (var i=0; i<ptime.length; i++){
              ptime[i]["name"] = name[i];
            }

            var sa_chart = Morris.Bar({
              element: 'bar-chart-sa',
              resize: true,
              data: ptime,
              xkey: 'name',
              ykeys: a,
              labels: a,
              hideHover: 'always',
              xLabelAngle: 75,
              stacked: true,
              colors: color_array,
              barColors : color_array,
              hideHover: 'auto',
              hoverCallback: function (index, options, content, row) {
                var idx = 0;
                var legendItem;
                $('#sa_legend').html('');
                delete row['name'];
                $.each(row, function (n, v) {
                  var val = v.toFixed(1);
                  legendItem = $('<span class="legend-item" id="'+n+'"></span>').text(n +' ('+ val +')').prepend('<span class="legend-color">&nbsp;</span>');
                  legendItem.find('span').css('backgroundColor', sa_chart.options.colors[idx++]);
                  $('#sa_legend').append(legendItem)
                });
              }
            });

            sa_chart.options.labels.forEach(function(label, i) {
              var legendItem = $('<span class="legend-item" id="'+label+'"></span>').text( label).prepend('<span class="legend-color">&nbsp;</span>');
              legendItem.find('span').css('backgroundColor', sa_chart.options.colors[i]);
              $('#sa_legend').append(legendItem)
            });

            for (var i=0;i<name.length;i++){
              newRow = '<tr>'+'<td>'+ name[i] +'</td>';
              for (var j=0;j<a.length;j++){
                var num_sa = Object.values(data.data_sa)[i][a[j]];
                if(num_sa > 0 ){
                  newRow += '<td class="text-center">'+ num_sa.toFixed(1) +'</td>';
                } else {
                  newRow += '<td class="text-center" style="color:#C0C0C0">'+ num_sa +'</td>';
                }
              }
              $('#target_sa').append(newRow);
            }
            $('.odd').css('display','none');
          }  else {
            $("#content-sa").addClass('hidden');
          }
        }
      }
    });
  }
}
