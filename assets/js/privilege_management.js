var datatable;
var badge = ['red', 'blue', 'green', 'yellow', 'purple', 'teal', 'aqua'];

$(document).ready(function() {

	datatable = $('#example1').DataTable({
		"order": [[ 4, "asc" ]],
		scrollX: true,serverSide: true,paginate: false,
		pageLength: 25, info:false,
		autoWidth: false,
		"ajax": {
			url: base_url+"index.php/PrivilegeManagement/datatable",
			type: 'POST'
		},
		columns: [
		{data: 'pg_id', orderable: false, width:"5%", render: function (data, type, row, meta) {
			return meta.row + meta.settings._iDisplayStart + 1;
		}},
		{data: 'pu_id', className: "hidden"},
		{data: 'groups_id', className: "hidden"},
		{data: 'users_id', className: "hidden"},
		{data: 'menu_id', className: "hidden"},
		{data: 'submenu_id', className: "hidden"},
		{data: 'menu_name', render: function (data, type, row) {
			if ( data === null ) {
				return '';
			} else {
				return appLang[data];
			}
		}},
		{data: 'submenu_name', render: function (data, type, row) {
			if ( data === null ) {
				return '';
			} else {
				return appLang[data];
			}
		}},
		{data: 'groups_name', orderable: false, render: function (data, type, row) {
			if ( data !== null ) {
				data = data.split(', ').map((w, i) => '<small class="label bg-'+badge[i]+'">'+w+'</small>').join(' ')
				return data;
			} else {
				return '';
			}
		}},
		{data: 'users_name', orderable: false, render: function (data, type, row) {
			if ( data !== null ) {
				data = data.split(', ').map((w, i) => '<small class="label bg-'+badge[i]+'">'+w+'</small>').join(' ')
				return data;
			} else {
				return '';
			}
		}},
		{data: 'menu_id', orderable: false, width:"10%", render: function (data, type, row) {
			return '<div class="text-center"><button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editPrivilege" data-backdrop="static" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button></div>';
		}},
		],
		drawCallback: function(data, type, row) {

			$(".button_edit").click(function(e){
				var data = datatable.row($(this).closest('tr')).data();

				var modal = $('#editPrivilege');
				modal.find('[name="pg_id"]').val(data.pg_id);
				modal.find('[name="pu_id"]').val(data.pu_id);
				modal.find('[name="groups_id"]').val(data.groups_id);
				modal.find('[name="users_id"]').val(data.users_id);
				modal.find('[name="menu_id"]').val(data.menu_id);
				modal.find('[name="submenu_id"]').val(data.submenu_id);
				modal.find('[name="menu_name"]').val(appLang[data.menu_name]);
				modal.find('[name="submenu_name"]').val(appLang[data.submenu_name]);
				if(data.groups_id !== null){
					( data.groups_id.includes(',') ) ? $('#role').val(data.groups_id.split(', ')).change() : $('#role').val(data.groups_id).change();
				}
				if(data.users_id !== null){
					( data.users_id.includes(',') )	? $('#user').val(data.users_id.split(', ')).change() : $('#user').val(data.users_id).change();
				}
			})
		}
	});

	$('.select2').select2()
	$('.select2-search__field').css("width", "auto");

	$("#button-update").on('click',function(e) {
		e.preventDefault();
		var isFormValid = $("#editPrivilegeForm").valid();
		var jsonPost = new FormData($("#editPrivilegeForm")[0] );

		$.ajax({
			url: base_url+"index.php/PrivilegeManagement/do_save",
			type: 'POST',
			data : jsonPost,
			datatype:"json",
			processData: false,
			contentType: false,
			success: function(response){
				if (response.status) {
					$('#editPrivilege').modal('hide');
					datatable.ajax.reload();
				}else {
					Swal.fire({
						title: 'Error!',
						html: response.message,
						icon: 'error',
						confirmButtonText: 'OK'
					});
				}
			}
		});
	});

	$('.modal').on('hidden.bs.modal', function () {
		$('form').each(function(){
			$(this).trigger('reset');
			$(this).validate().resetForm();
		});

		$(".select2").trigger("change");
		$('.select2-search__field').css("width", "auto");

	});

});
