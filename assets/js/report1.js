var datatableQc;
var datatablePg;
var datatable;
var currDate = new Date();
var color_array = ['#00a65a','#f39c12','#00c0ef','#984ea3','#00d2d5','#ff7f00','#af8d00','#7f80cd','#b3e900','#c42e60','#a65628','#f781bf','#8dd3c7','#bebada','#fb8072','#80b1d3','#fdb462','#fccde5','#bc80bd','#ffed6f','#c4eaff','#cf8c00','#1b9e77','#d95f02','#e7298a','#e6ab02','#a6761d','#0097ff','#00d067','#000000','#252525','#525252','#737373','#969696','#bdbdbd','#f43600','#4ba93b','#5779bb','#927acc','#97ee3f','#bf3947','#9f5b00','#f48758','#8caed6','#f2b94f','#eff26e','#e43872','#d9b100','#9d7a00','#698cff','#d9d9d9','#00d27e','#d06800','#009f82','#c49200','#cbe8ff','#fecddf','#c27eb6','#8cd2ce','#c4b8d9','#f883b0','#a49100','#f48800','#27d0df','#a04a9b'];


$(document).ready(function () {
  $('.sidebar-menu').tree();

  generateDataR1();

  $("#start_date").datepicker({autoclose: true,format: 'yyyy-mm-dd', setEndDate : new Date($("#stop_date").val()),
}).on('changeDate', function (selected) {
  var minDate = new Date(selected.date.valueOf());
  $('#stop_date').datepicker('setStartDate', new Date(selected.date.valueOf()));
});

$("#stop_date").datepicker({autoclose: true,format: 'yyyy-mm-dd',setStartDate : new Date($("#start_date").val()),}).on('changeDate', function (selected) {
  var minDate = new Date(selected.date.valueOf());
  $('#start_date').datepicker('setEndDate', new Date(selected.date.valueOf()));
});


$("#search").click(function(e){
  generateDataR1();
});
});


function generateDataR1() {
  $.ajax({
    url: base_url+"index.php/Report1/datar1",
    type: 'POST',  datatype:"json",
    data: {
      start_date : $('#start_date').val(),
      stop_date : $('#stop_date').val(),
      role :  $('#role').val(),
    },
    success: function(data) {
      if(data.status){
        $("#content-r1").removeClass('hidden');
        $("#bar-chart").empty();
        $('#bar-legend').html('');
        $('#target_body_a').html('');
        $('#target_body_b').html('');

        var name  =  Object.keys(data.datar1);
        var label     =  Object.values(data.project);
        var ptime =  Object.values(data.datar1);

        for (var i=0; i<ptime.length; i++){
          ptime[i]["name"] = name[i];
        }

        var bar_chart = Morris.Bar({
          element: 'bar-chart',
          resize: true,
          data: ptime,
          xkey: 'name',
          ykeys: label, 
          labels: label, 
          stacked: true,
          barRatio: 0.4,
          hideHover: 'auto',
          xLabelAngle: 75,
          colors: color_array,
          barColors : color_array,
        });


        bar_chart.options.labels.forEach(function(label, i) {
          var legendItem = $('<span class="legend-item"></span>').text( label).prepend('<span class="legend-color">&nbsp;</span>');
          legendItem.find('span').css('backgroundColor', bar_chart.options.colors[i]);
          $('#bar-legend').append(legendItem)
        });

        var newRowA='';
        var noA = 1;
        var jmlA = 0;
        $.each(data.actualSort, function(key, val) { 
          numA = noA++;
          jmlA += Number(val);
          if(numA == 1){
            newRowA = '<tr style="background:#FFD700;">'+'<td>'+ numA +'</td><td>'+key+'</td>'+'</td><td>'+val.toFixed(1)+'</td></tr>';
          } else {
            newRowA = '<tr>'+'<td>'+ numA +'</td><td>'+key+'</td>'+'</td><td>'+val.toFixed(1)+'</td></tr>';
          }
          $('#target_body_a').append(newRowA);
        });
        $('#table-a:last-child').append('<tr style="background: #D3D3D3;"><td colspan="2">Total</td><td>'+jmlA.toFixed(1)+'</td></tr>');


        var newRowB='';
        $('#longestCategori').html('<b>'+data.longestCategory+'</b>');
        var noB = 1;
        var jmlB = 0;
        $.each(data.dataLongest, function(key, val) { 
          numB = noB++;
          jmlB += Number(val.actual_sum);
          newRowB = '<tr>'+'<td>'+ numB +'</td><td>'+val.username+'</td><td>'+val.name+'</td><td>'+parseFloat(val.actual_sum).toFixed(1)+'</td></tr>';
          $('#target_body_b').append(newRowB);
        });

        $('#table-b:last-child').append('<tr style="background: #D3D3D3;"><td colspan="3">Total</td><td>'+jmlB.toFixed(1)+'</td></tr>');

      }  else {
        $("#content-r1").addClass('hidden');
        Swal.fire({
          title: 'Information',
          html: data.message,
          icon: 'info',
          confirmButtonText: 'OK'
        });
      }
    }
  });

}




