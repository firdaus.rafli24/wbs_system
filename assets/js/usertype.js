var datatable;
$(document).ready(function () {
  $('.sidebar-menu').tree();

  datatable = $('#table_role').DataTable({
    scrollX: true,
    "ajax": {
      url: base_url+"index.php/UserType/datatable",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"1%", className: "hidden"},
    {targets: 1, width:"29%",},
    {targets: 2, width:"35%"},
    {
      targets: 3,
      sortable: false,
      width: "35%",
      render: function (data, type, row) {
        var viewButton = '<button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewRole" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;
        var editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editRole"" data-backdrop="static" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
        var deleteButton = '<button class="button_delete btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deleteRole" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
        return '<div class="text-center" style="min-width:100px">'+viewButton+editButton+deleteButton+'</div>';
      }
    }],
    drawCallback: function(data, type, row) {
      $(".button_view").click(function(e){
        var id = $(this).data('id');
        toview(id);
            // console.log(id);
          })
      $(".button_edit").click(function(e){
        var id = $(this).data('id');
        toedit(id);
            // console.log(id);
          })
      $(".button_delete").click(function(e){
        var id = $(this).data('id');
        todelete(id);
      })
    }
  });

    //Date picker
    $('.datepicker').datepicker({
      autoclose: true
    });

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    });
  });
var alerts = function() {
  return {
    showError: function(message) {
      $("#error_container").html(message);
      $("#error_container").removeClass('d-none');
    },
    hideError: function() {
      $("#error_container").html("");
      $("#error_container").addClass('d-none');
    }
  }
}();
$("#addroleform").validate({
  errorClass: "validate_error",
  rules: {
    name: {
      required: true,
      maxlength: 20
    },
    description: {
      maxlength: 100,
    }
  },
  messages: {
    name: {
      required: appLang.form_requiredform,
      maxlength: appLang.form_maxlength+' 20 '+appLang.form_char,
    },
    description: {
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char,
    }
  }
});
$(document).ready(function(){
  $("#submitrole").on('click',function(e) {
    e.preventDefault();
    var modal= $("#addRole");
    var name = modal.find('[name="name"]').val();
    var description = modal.find('[name="description"]').val();
    var text = $(this).text();
    var isFormValid = $("#addroleform").valid();
    if (isFormValid) {
      $.ajax({
        url: base_url+"index.php/UserType/do_save",
        type: 'POST',
        data: {name:name, description:description},
        datatype:"json",
        success: function(result){
          if (result.status) {
            var modal = $('#addRole');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});
function toview(id){
  $.ajax({
    url: base_url+"index.php/UserType/get",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#viewRole');
        modal.find('[name="name"]').val(result.data.name);
        modal.find('[name="description"]').val(result.data.description);
        modal.modal('show');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}
function toedit(id){
  $.ajax({
    url: base_url+"index.php/UserType/get",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editRole');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="name"]').val(result.data.name);
        modal.find('[name="description"]').val(result.data.description);
        modal.modal('show');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}
$("#editroleform").validate({
  errorClass: "validate_error",
  rules: {
    name: {
      required: true,
      maxlength: 20
    },
    description: {
      maxlength: 100,
    }
  },
  messages: {
    name: {
      required: appLang.form_requiredform,
      maxlength: appLang.form_maxlength+' 20 '+appLang.form_char,
    },
    description: {
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char,
    }
  }
});
$(document).ready(function(){
  $("#button-update-role").on('click',function(e) {
    e.preventDefault();
    var modal= $("#editRole");
    var id = modal.find('[name="id"]').val();
    var name = modal.find('[name="name"]').val();
    var description = modal.find('[name="description"]').val();
    var text = $(this).text();
    var form = $('#editroleform');
    var valid = form.valid()
    if(valid){
      $.ajax({
        url: base_url+"index.php/UserType/do_update",
        type: 'POST',
        data: {user_id:id, name: name, description:description},
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#editRole');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});
function todelete(id){
  $.ajax({
    url: base_url+"index.php/UserType/get",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deleteRole');
        modal.find('[name="id"]').val(result.data.id);
      }
    }
  });
}
$('#button-delete-role').click(function(){
  var modal= $("#deleteRole");
  var id = modal.find('[name="id"]').val();
  data: {user_id:id};
  $.ajax({
    url: base_url+"index.php/UserType/do_delete",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deleteRole');
        modal.modal('hide');
        datatable.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
});


$('.modal').on('hidden.bs.modal', function () {
  $('form').each(function(){
    $(this).trigger('reset');
    $(this).validate().resetForm();
    $(this).find('.validate_error').removeClass('validate_error');
  });

});


