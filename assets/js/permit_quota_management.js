var currDate = new Date();
var datatable;
$(document).ready(function() {
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    startDate : currDate,
  });
  datatable = $('#example1').DataTable({
    scrollX: true,serverSide: true,processing: true,
    "ajax": {
      url: base_url+"index.php/PermitQuotaManagement/datatable",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"1%", className: "hidden"},
    {targets: 1, width:"5%"},
    {targets: 2, width:"5%"},
    {
      targets: 3,
      sortable: false,
      width: "5%",
      render: function (data, type, row) {
        var editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editPermitQuota" data-backdrop="static" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
        return '<div class="text-center" style="min-width:100px">'+editButton+'</div>';
      }
    }
    ],
    drawCallback: function(data, type, row) {
      $(".button_edit").click(function(e){
        var id = $(this).data('id');
        toedit(id);
      })
    }
  });

  datatable.on('draw', function () {
    $('td').addClass('wrap-text')
  });


  $('.select2').select2();
  $('.select2-search__field').css("width", "auto");

});
function toedit(id){
  $.ajax({
    url: base_url+"index.php/PermitQuotaManagement/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editPermitQuota');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="usernameEdit"]').val(result.data.username);
        modal.find('[name="permitQuotaEdit"]').val(result.data.permit_quota);
        $("#alert-edit, .error").addClass('hidden');

      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}


$(document).ready(function(){
  $("#button-update").on('click',function(e) {
    e.preventDefault();
    var isFormValid = $("#editform").valid();
    var jsonPost = $("form#editform").serializeArray();
    if(isFormValid){
      $.ajax({
        url: base_url+"index.php/PermitQuotaManagement/do_update",
        type: 'POST',
        data : jsonPost,
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#editPermitQuota');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            $("#alert-edit").removeClass('hidden');
            $("#message-edit").text(response.message);
          }
        }
      });
    }
  });
});
$('form').each(function(){
  $(this).validate({
    errorClass: "validate_error",
    ignore: [],
    rules: {
      permitQuotaEdit: {
        required: true,
        min: 0,
        max: 12,
        number: true
      }
    },
    messages: {
        permitQuotaEdit: {
        required: appLang.form_requiredform,
        min: appLang.form_min+' 0 ',
        max: appLang.form_max+' 12 ',
      },
    }
  });
});

$('#btn_reset').on('click',function(){
    $.ajax({
        url:base_url+"index.php/PermitQuotaManagement/do_reset",
        type:"POST",
        datatype:"json",
        success:function(response){
          if(response.status){
            Swal.fire({
              icon: 'success',
              title: 'Permit Quota Has Been Resetted',
              showConfirmButton: false,
              timer: 1500
            });
            datatable.ajax.reload();
          }else{
            Swal.fire({
              icon: 'error',
              title: 'Permit Quota Failed to Reset',
              showConfirmButton: false,
              timer: 1500
            });
          }
        }
    });
});



$('.modal').on('hidden.bs.modal', function () {
  $(".select2").trigger("change");
  $('.select2-search__field').css("width", "auto");

});
