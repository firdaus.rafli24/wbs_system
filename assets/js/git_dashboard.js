var datatablelevel;
var datatablelabel;
var datatable;
var currDate = new Date();
var color_array = ['#00a65a','#f39c12','#00c0ef','#984ea3','#00d2d5','#ff7f00','#af8d00','#7f80cd','#b3e900','#c42e60','#a65628','#f781bf','#8dd3c7','#bebada','#fb8072','#80b1d3','#fdb462','#fccde5','#bc80bd','#ffed6f','#c4eaff','#cf8c00','#1b9e77','#d95f02','#e7298a','#e6ab02','#a6761d','#0097ff','#00d067','#000000','#252525','#525252','#737373','#969696','#bdbdbd','#f43600','#4ba93b','#5779bb','#927acc','#97ee3f','#bf3947','#9f5b00','#f48758','#8caed6','#f2b94f','#eff26e','#e43872','#d9b100','#9d7a00','#698cff','#d9d9d9','#00d27e','#d06800','#009f82','#c49200','#cbe8ff','#fecddf','#c27eb6','#8cd2ce','#c4b8d9','#f883b0','#a49100','#f48800','#27d0df','#a04a9b'];

$(document).ready(function () {
  $('.sidebar-menu').tree();
  $("#nav-home-tab").click();
  $("#nav-home-tab").click(function(){
      $("#nav-tabContent").show();
      $("#nav-tabProject").hide();
      $("#project_section").hide();
      $("#nav-project-tab").removeClass('active');
      $("#nav-home-tab").addClass('active');
    });

  $("#nav-project-tab").click(function(){
      $("#nav-tabContent").hide();
      $("#nav-tabProject").show();
      $("#project_section").show();
      $("#nav-home-tab").removeClass('active');
      $("#nav-project-tab").addClass('active');
    });

  $("#checkboxProject").prop("checked", true);
  $("#project").prop("disabled", true).trigger('change');

  $("#checkboxLabel").prop("checked", true);
  $("#label").prop("disabled", true).trigger('change');

  $("#checkboxUser").prop("checked", true);
  $("#user").prop("disabled", true).trigger('change');

  $('#label').select2();
  $('#user').select2();
  $('#project').select2();


  $("#checkboxLabel").click(function(){
    $("#label").find('option').prop("selected",false);
    $("#label").prop("disabled", ($(this).is(':checked'))).trigger('change');
    $("#checkboxLabel").prop("checked", ($(this).is(':checked')));
  });

  $("#checkboxUser").click(function(){
    $("#user").find('option').prop("selected",false);
    $("#user").prop("disabled", ($(this).is(':checked'))).trigger('change');
    $("#checkboxUser").prop("checked", ($(this).is(':checked')));
  });

  $("#checkboxProject").click(function(){
    $("#project").find('option').prop("selected",false);
    $("#project").prop("disabled", ($(this).is(':checked'))).trigger('change');
    $("#checkboxProject").prop("checked", ($(this).is(':checked')));
  });


  generateDataLevel();
  generateDataLabel();
  generateDataBug();
  generateDataProject();

  $("#start_date").datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    setEndDate : new Date($("#stop_date").val()),
    // container: '.container',
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#stop_date').datepicker('setStartDate', new Date(selected.date.valueOf()));
  });
  $("#stop_date").datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    setStartDate : new Date($("#start_date").val()),
    // container: '.container',
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#start_date').datepicker('setEndDate', new Date(selected.date.valueOf()));
  });
  $("#start_date_project").datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    setEndDate : new Date($("#stop_date").val()),
    // container: '.container',
  }).on('changeDate', function (selected) {
  var minDate = new Date(selected.date.valueOf());
  $('#stop_date_project').datepicker('setStartDate', new Date(selected.date.valueOf()));
  });
  $("#stop_date_project").datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    setStartDate : new Date($("#start_date").val()),
    // container: '.container',
  }).on('changeDate', function (selected) {
  var minDate = new Date(selected.date.valueOf());
  $('#start_date_project').datepicker('setEndDate', new Date(selected.date.valueOf()));
  });
  $(document).on('show', $('.datepicker').datepicker(), function() {
    $('.datepicker').removeClass('datepicker-orient-top');
    $('.datepicker').addClass('datepicker-orient-bottom');
  });

$("#table_level_filter").addClass('hidden');
$("#table_label_filter").addClass('hidden');
$("#table_bug_filter").addClass('hidden');
$("#table_project_filter").addClass('hidden');


$("#search").click(function(e){
  generateDataLevel();
  generateDataLabel();
  generateDataBug();
  generateDataProject();
});
});
(function() {
  var $, MyMorris;

  MyMorris = window.MyMorris = {};
  $ = jQuery;

  MyMorris = Object.create(Morris);

  MyMorris.Bar.prototype.defaults["labelTop"] = false;

  MyMorris.Bar.prototype.drawLabelTop = function(xPos, yPos, text) {
    var label;
    return label = this.raphael.text(xPos, yPos, text).attr('font-size', this.options.gridTextSize).attr('font-family', this.options.gridTextFamily).attr('font-weight', this.options.gridTextWeight).attr('fill', this.options.gridTextColor);
  };

  MyMorris.Bar.prototype.drawSeries = function() {
    var barWidth, bottom, groupWidth, idx, lastTop, left, leftPadding, numBars, row, sidx, size, spaceLeft, top, ypos, zeroPos;
    groupWidth = this.width / this.options.data.length;
    numBars = this.options.stacked ? 1 : this.options.ykeys.length;
    barWidth = (groupWidth * this.options.barSizeRatio - this.options.barGap * (numBars - 1)) / numBars;
    if (this.options.barSize) {
      barWidth = Math.min(barWidth, this.options.barSize);
    }
    spaceLeft = groupWidth - barWidth * numBars - this.options.barGap * (numBars - 1);
    leftPadding = spaceLeft / 2;
    zeroPos = this.ymin <= 0 && this.ymax >= 0 ? this.transY(0) : null;
    return this.bars = (function() {
      var _i, _len, _ref, _results;
      _ref = this.data;
      _results = [];
      for (idx = _i = 0, _len = _ref.length; _i < _len; idx = ++_i) {
        row = _ref[idx];
        lastTop = 0;
        _results.push((function() {
          var _j, _len1, _ref1, _results1;
          _ref1 = row._y;
          _results1 = [];
          for (sidx = _j = 0, _len1 = _ref1.length; _j < _len1; sidx = ++_j) {
            ypos = _ref1[sidx];
            if (ypos !== null) {
              if (zeroPos) {
                top = Math.min(ypos, zeroPos);
                bottom = Math.max(ypos, zeroPos);
              } else {
                top = ypos;
                bottom = this.bottom;
              }
              left = this.left + idx * groupWidth + leftPadding;
              if (!this.options.stacked) {
                left += sidx * (barWidth + this.options.barGap);
              }
              size = bottom - top;
              if (this.options.verticalGridCondition && this.options.verticalGridCondition(row.x)) {
                this.drawBar(this.left + idx * groupWidth, this.top, groupWidth, Math.abs(this.top - this.bottom), this.options.verticalGridColor, this.options.verticalGridOpacity, this.options.barRadius, row.y[sidx]);
              }
              if (this.options.stacked) {
                top -= lastTop;
              }
              this.drawBar(left, top, barWidth, size, this.colorFor(row, sidx, 'bar'), this.options.barOpacity, this.options.barRadius);
              _results1.push(lastTop += size);

              if (this.options.labelTop && !this.options.stacked) {
                label = this.drawLabelTop((left + (barWidth / 2)), top - 10, row.y[sidx]);
                textBox = label.getBBox();
                _results.push(textBox);
              }
            } else {
              _results1.push(null);
            }
          }
          return _results1;
        }).call(this));
      }
      return _results;
    }).call(this);
  };
}).call(this);
function generateDataLevel() {
  if($("#table_level").length > 0 && $("#bar-chart-level").length > 0) {
    $('#table_level').DataTable().destroy();
    datatablelevel = $('#table_level').DataTable({
      scrollX: true,serverSide: true,autoWidth: false,paging:false,ordering: false,info: false, search:false,
      ajax: {
        url: base_url+"index.php/GITDashboard/data_level",
        type: 'POST',
        data: function (d) {
          d.start_date = $('#start_date').val();
          d.stop_date = $('#stop_date').val();
          d.user = ($('#user option:selected').length > 0) ? $('#user').val() : [''];
          d.project = ($('#project option:selected').length > 0) ? $('#project').val() : [''];
          d.allproject = $("#checkboxProject").is(":checked") ? 1 : 0;
          d.allUser = $("#checkboxUser").is(":checked") ? 1 : 0;
        },
        success: function(data) {
          if(data.count_git != 0){
            $("#content-level").removeClass('hidden');
            $("#bar-chart-level").empty();
            $('#level_legend').html('');
            $('#target_level').html('');

            var newRow='';
            var name=Object.keys(data.count_git);
            var label = ["A","B","C"];
            var a=Object.values(label);
            var ptime=Object.values(data.count_git);

            for (var i=0; i<ptime.length; i++){
              ptime[i][0] = name[i];
            }

            var level_chart = Morris.Bar({
              element: 'bar-chart-level',
              resize: true,
              data: ptime,
              xkey: 0,
              ykeys: a,
              labels: a,
              hideHover: 'false',
              xLabelAngle: 45,
              stacked: true,
              colors: color_array,
              labelTop: true,
              barColors : color_array,
              hideHover: 'auto',
              hoverCallback: function (index, options, content, row) {
                var idx = 0;
                var legendItem;
                $('#level_legend').html('');
                delete row[0];
                delete row[''];
                $.each(row, function (n, v) {
                  if (n=="A"||n=="B"||n=="C") {
                    legendItem = $('<span class="legend-item" id="'+n+'"></span>').text(n +' ('+ v +')').prepend('<span class="legend-color">&nbsp;</span>');
                    if (n=="A") {
                      legendItem.find('span').css('backgroundColor', level_chart.options.colors[0]);
                    }else if (n=="B") {
                      legendItem.find('span').css('backgroundColor', level_chart.options.colors[1]);
                    }else if (n=="C") {
                      legendItem.find('span').css('backgroundColor', level_chart.options.colors[2]);
                    }
                    $('#level_legend').append(legendItem);
                  }
                });
              }
            });

            level_chart.options.labels.forEach(function(label, i) {
              var legendItem = $('<span class="legend-item" id="'+label+'"></span>').text(label).prepend('<span class="legend-color">&nbsp;</span>');
              legendItem.find('span').css('backgroundColor', level_chart.options.colors[i]);
              $('#level_legend2').append(legendItem)
            });

            for (var i=0;i<name.length;i++){
              newRow = '<tr>'+'<td>'+ name[i] +'</td>';
              for (var j=0;j<a.length;j++){
                var num_hrd = Object.values(data.count_git)[i][a[j]];
                if (num_hrd === undefined) {
                    num_hrd = 0;
                }
                if(num_hrd > 0 ){
                  newRow += '<td class="text-center">'+ num_hrd +'</td>';
                } else {
                  newRow += '<td class="text-center" style="color:#C0C0C0">'+ num_hrd +'</td>';
                }
              }
              $('#target_level').append(newRow);
            }
            $('.odd').css('display','none');
          }  else {
            $("#content-level").addClass('hidden');
          }
        }
      }
    });
  }
}
function generateDataLabel() {
  if($("#table_label").length > 0 && $("#bar-chart-label").length > 0) {
    $('#table_label').DataTable().destroy();
    datatablelabel = $('#table_label').DataTable({
      scrollX: true,serverSide: true,autoWidth: false,paging:false,ordering: false,info: false,
      ajax: {
        url: base_url+"index.php/GITDashboard/data_label",
        type: 'POST',
        data: function (d) {
          d.start_date = $('#start_date').val();
          d.stop_date = $('#stop_date').val();
          d.label = ($('#label option:selected').length > 0) ? $('#label').val() : [''];
          d.user = ($('#user option:selected').length > 0) ? $('#user').val() : [''];
          d.project = ($('#project option:selected').length > 0) ? $('#project').val() : [''];
          d.allproject = $("#checkboxProject").is(":checked") ? 1 : 0;
          d.allUser = $("#checkboxUser").is(":checked") ? 1 : 0;
          d.allLabel = $("#checkboxLabel").is(":checked") ? 1 : 0;
        },
        success: function(data) {
          if(data.count_git != 0){
            $("#content-label").removeClass('hidden');
            $("#bar-chart-label").empty();
            $('#label_legend').html('');
            $('#target_label').html('');

            var newRow='';
            var name=Object.keys(data.count_git);
            var a=Object.values(data.project);
            var ptime=Object.values(data.count_git);
            var sum=Object.values(data.count_git);

            for (var i=0; i<ptime.length; i++){
              ptime[i]["name"] = name[i];
            }

            var label_chart = Morris.Bar({
              element: 'bar-chart-label',
              resize: true,
              data:ptime,
              xkey:'name',
              ykeys:['sum'],
              labels: a,
              xLabelAngle: 60,
              stacked: false,
              colors: color_array,
              labelTop: true,
              barColors :function (row, series, type) {
                var idx=0;
                for (var i = 0; i < ptime.length; i++) {
                  if (row.label==ptime[i]['name']) {
                    return ptime[i]['color'];
                  }
                }
              },
              hideHover: 'auto',
              hoverCallback: function (index, options, content, row) {
                var idx = 0;
                var legendItem;
                $('#label_legend').html('');
                delete row['id'];
                delete row['name'];
                delete row['color'];
                $.each(row, function (n, v) {
                  legendItem = $('<span class="legend-item" id="'+n+'"></span>').text('Number of Issues' +' = '+ v ).prepend('<span class="legend-color">&nbsp;</span>');
                  legendItem.find('span').css('backgroundColor', '#ffffff');
                  $('#label_legend').append(legendItem);
                });
              }
            });
            for (var i=0;i<ptime.length;i++){
                var nama =  Object.values(ptime)[i]['name'];
                newRow = '<tr>'+'<td>'+ nama +'</td>';
                for (var j=0;j<1;j++){
                  var num_hrd = Object.values(ptime)[i]['sum'];
                  if (num_hrd === undefined) {
                      num_hrd = 0;
                  }
                  if(num_hrd > 0 ){
                    newRow += '<td class="text-center">'+ num_hrd +'</td>';
                  } else {
                    newRow += '<td class="text-center" style="color:#C0C0C0">'+ num_hrd +'</td>';
                  }
                }
                $('#target_label').append(newRow);
              }
            $('.odd').css('display','none');
          }  else {
            $("#content-label").addClass('hidden');
          }
        }
      }
    });
  }
}
function generateDataBug() {
  if($("#table_bug").length > 0 && $("#bar-chart-bug").length > 0) {
    $('#table_bug').DataTable().destroy();
    datatablebug = $('#table_bug').DataTable({
      scrollX: true,serverSide: true,autoWidth: false,paging:false,ordering: false,info: false,
      ajax: {
        url: base_url+"index.php/GITDashboard/data_bug",
        type: 'POST',
        data: function (d) {
          d.start_date = $('#start_date').val();
          d.stop_date = $('#stop_date').val();
          d.label = ($('#label option:selected').length > 0) ? $('#label').val() : [''];
          d.user = ($('#user option:selected').length > 0) ? $('#user').val() : [''];
          d.project = ($('#project option:selected').length > 0) ? $('#project').val() : [''];
          d.allproject = $("#checkboxProject").is(":checked") ? 1 : 0;
          d.allUser = $("#checkboxUser").is(":checked") ? 1 : 0;
          d.allLabel = $("#checkboxLabel").is(":checked") ? 1 : 0;
        },
        success: function(data) {
          if(data.count_git != 0){
            $("#content-bug").removeClass('hidden');
            $("#bar-chart-bug").empty();
            $('#bug_legend').html('');
            $('#target_bug').html('');

            var newRow='';
            var name=Object.keys(data.count_git);
            var a=Object.values(data.git);
            var ptime=Object.values(data.count_git);
            var sum=Object.values(data.count_git);
            // var color = ['#ff7f00'];

            for (var i=0; i<ptime.length; i++){
              ptime[i]["name"] = name[i];
            }

            var bug_chart = Morris.Bar({
              element: 'bar-chart-bug',
              resize: true,
              data:ptime,
              xkey:'name',
              ykeys:['sum'],
              labels: ['name'],
              xLabelAngle: 75,
              stacked: false,
              colors: color_array,
              labelTop: true,
              barColors :function (row, series, type) {
                var idx=0;
                for (var i = 0; i < ptime.length; i++) {;
                  if (row.label==ptime[i]['name']) {
                    return ptime[i]['color'];
                  }
                }
              },
              hideHover: 'auto',
              hoverCallback: function (index, options, content, row) {
                var idx = 0;
                var legendItem;
                $('#bug_legend').html('');
                delete row['id'];
                delete row['name'];
                delete row['color'];
                $.each(row, function (n, v) {
                    legendItem = $('<span class="legend-item" id="'+n+'"></span>').text('Number of Issues' +' = '+ v ).prepend('<span class="legend-color">&nbsp;</span>');
                    legendItem.find('span').css('backgroundColor', '#ffffff');
                    $('#bug_legend').append(legendItem);
                });
              }
            });

            for (var i=0;i<ptime.length;i++){
                var nama =  Object.values(ptime)[i]['name'];
                if (nama!="") {
                  newRow = '<tr>'+'<td>'+nama+'</td>';
                  for (var j=0;j<1;j++){
                    var num_hrd = Object.values(ptime)[i]['sum'];
                    if (num_hrd === undefined) {
                        num_hrd = 0;
                    }
                    if(num_hrd > 0 ){
                      newRow += '<td class="text-center">'+ num_hrd +'</td>';
                    } else {
                      newRow += '<td class="text-center" style="color:#C0C0C0">'+ num_hrd +'</td>';
                    }
                  }
                }
              $('#target_bug').append(newRow);
            }
            $('.odd').css('display','none');
          }  else {
            $("#content-bug").addClass('hidden');
          }
        }
      }
    });
  }
}
function generateDataProject() {
  if($("#table_project").length > 0 && $("#bar-chart-project").length > 0) {
    $('#table_project').DataTable().destroy();
    datatablelevel = $('#table_project').DataTable({
        scrollX: true,serverSide: true,autoWidth: false,paging:false,ordering: false,info: false,
        ajax: {
        url: base_url+"index.php/GITDashboard/data_project",
        type: 'POST',
        data: function (d) {
          d.start_date = $('#start_date_project').val();
          d.stop_date = $('#stop_date_project').val();
          d.project_select = $('#project_select').val();
        },
        success: function(data) {
          if(data.data_git != 0){
            $("#content-project").removeClass('hidden');
            $("#bar-chart-project").empty();
            $('#project_legend').html('');
            $('#target_project').html('');

            var newRow='';
            var name=Object.keys(data.count_git);
            var a=Object.values(data.label);
            var ptime=Object.values(data.count_git);

            for (var i=0; i<ptime.length; i++){
              ptime[i][0] = name[i];
            }

            var project_chart = Morris.Bar({
              element: 'bar-chart-project',
              resize: true,
              data: ptime,
              xkey: 0,
              ykeys: a,
              labels: a,
              hideHover: 'false',
              xLabelAngle: 45,
              stacked: true,
              colors: color_array,
              barColors : color_array,
              hideHover: 'auto',
              hoverCallback: function (index, options, content, row) {
                var idx = 0;
                var legendItem;
                $('#project_legend').html('');
                delete row[0];
                delete row[''];

                $.each(row, function (n, v) {
                  if (n!="") {
                  legendItem = $('<span class="legend-item" id="'+n+'"></span>').text(n +' ('+ v +')').prepend('<span class="legend-color">&nbsp;</span>');
                  legendItem.find('span').css('backgroundColor', project_chart.options.colors[idx++]);
                  $('#project_legend').append(legendItem);
                  }
                });
              }
            });
            project_chart.options.labels.forEach(function(label, i) {
                var legendItem = $('<span class="legend-item" id="'+label+'"></span>').text( label).prepend('<span class="legend-color">&nbsp;</span>');
                legendItem.find('span').css('backgroundColor', project_chart.options.colors[i]);
                $('#project_legend2').append(legendItem);
            });

            for (var i=0;i<name.length;i++){
              newRow = '<tr>'+'<td>'+ name[i] +'</td>';
              for (var j=0;j<a.length;j++){
                var num_hrd = Object.values(data.count_git)[i][a[j]];
                if (num_hrd === undefined) {
                    num_hrd = 0;
                }
                if(num_hrd > 0 ){
                  newRow += '<td class="text-center">'+ num_hrd +'</td>';
                } else {
                  newRow += '<td class="text-center" style="color:#C0C0C0">'+ num_hrd +'</td>';
                }
              }
              $('#target_project').append(newRow);
            }
            $('.odd').css('display','none');
          }  else {
            $("#content-project").addClass('hidden');
          }
        }
      }
    });
  }
}
