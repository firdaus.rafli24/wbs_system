var datatable;
var badge = ['red', 'blue', 'green', 'yellow', 'purple', 'teal', 'aqua'];

$(document).ready(function() {

	$("#config_enable").click(function(){
		if ($(this).is(':checked')){
			$('#value').removeClass('hidden');
			$('#config_value').prop("disabled", false);
		} else {
			$('#value').addClass('hidden');
			$('#config_value').prop("disabled", true);
		}
	});

	datatable = $('#example1').DataTable({
		"order": [[ 4, "asc" ]],
		scrollX: true,serverSide: true,paginate: false,
		pageLength: 25, info:false,searching :false,
		autoWidth: false,
		"ajax": {
			url: base_url+"index.php/Config/datatable",
			type: 'POST'
		},
		columns: [
		{data: 'config_key', orderable: false, width:"5%", render: function (data, type, row, meta) {
			return meta.row + meta.settings._iDisplayStart + 1;
		}},
		{data: 'config_key'},

		{data: 'config_enable', orderable: false, render: function (data, type, row) {
			if ( data == '1' ) {
				return '<small class="label bg-green">Enable</small>';
			} else {
				return '<small class="label bg-yellow">Disable</small>';
			}
		}},

		{data: 'config_value'},
		{data: 'config_description'},
		
		{data: 'menu_id', orderable: false, width:"10%", render: function (data, type, row) {
			return '<div class="text-center"><button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editConfig" data-backdrop="static" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button></div>';
		}},
		],
		drawCallback: function(data, type, row) {

			$(".button_edit").click(function(e){
				var data = datatable.row($(this).closest('tr')).data();

				var modal = $('#editConfig');

				var checked = false;
				if(data.config_enable == '1'){
					checked = true;
				}

				modal.find('[name="config_key"]').val(data.config_key);
				modal.find('[name="config_enable"]').prop("checked",  checked );
				modal.find('#value').toggleClass('hidden',  !checked);
				modal.find('[name="config_value"]').val(data.config_value).prop('disabled', !checked);
			})
		}
	});

	$('.select2').select2()
	$('.select2-search__field').css("width", "auto");

	$("#button-update").on('click',function(e) {
		e.preventDefault();
		var isFormValid = $("#editConfigForm").valid();
		var jsonPost = new FormData($("#editConfigForm")[0] );

		$.ajax({
			url: base_url+"index.php/Config/do_save",
			type: 'POST',
			data : jsonPost,
			datatype:"json",
			processData: false,
			contentType: false,
			success: function(response){
				if (response.status) {
					$('#editConfig').modal('hide');
					datatable.ajax.reload();
				}else {
					Swal.fire({
						title: 'Error!',
						html: response.message,
						icon: 'error',
						confirmButtonText: 'OK'
					});
				}
			}
		});
	});



	$('.modal').on('hidden.bs.modal', function () {
		$('form').each(function(){
			$(this).trigger('reset');
			$(this).validate().resetForm();
		});

		$(".select2").trigger("change");
		$('.select2-search__field').css("width", "auto");

	});





});




