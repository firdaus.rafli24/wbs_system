var currDate = new Date();
var datatable;
$(document).ready(function() {
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    startDate : currDate,
  });
  datatable = $('#example1').DataTable({
    scrollX: true,serverSide: true,processing: true,
    "ajax": {
      url: base_url+"index.php/PermitActivity/datatable",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"1%", className: "hidden"},
    {targets: 1, width:"1%", className: "hidden"},
    {targets: 2, width:"7%"},
    {targets: 3, width:"7%"},
    {targets: 4, width:"15%"},
    {targets: 5, width:"12%"},
    {targets: 6, width:"14%"},
    {targets: 7, width:"10%", className: "text-center", render:function (value) {
      if(value == 1) { return "<i class='fa fa-check' style='color:green' >" } else { return "<i class='fa fa-times' style='color:red'>" };
    }},
    {
      targets: 8,
      sortable: false,
      width: "250px",
      render: function (data, type, row) {
        var downloadPdf = '<button class="button_download_pdf btn btn-sm" data-toggle="modal" data-target="#downloadPdfPermit" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-download">'+'</i>'+'</button>' ;
        var viewButton = '<button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewPermit" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;
        var deleteButton = '<button class="button_delete btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deletePermit" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
        
        if (button_approve=='visible' && row.approval == 0) {
          var approvalButton = '<button class="button_approve btn btn-sm btn-info" data-toggle="modal" data-target="#approvePermit" data-backdrop="static" data-id="'+row.id+'" style="margin-right:3px;visibility:'+button_approve+'">'+'<i class="fa fa-check-circle">'+'</i>'+'</button>' ;
        } else {
          var approvalButton = '';
        }
        if(userId == row.user_id && row.approval == 0){
          var editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editPermit" data-backdrop="static" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
        }else{
          var editButton = '';
        }
        
        return '<div class="text-center" style="min-width:100px">'+downloadPdf+approvalButton+viewButton+editButton+deleteButton+'</div>';
      }
    }
    ],
    drawCallback: function(data, type, row) {
      $(".button_view").click(function(e){
        var id = $(this).data('id');
        toview(id);
      })
      $(".button_edit").click(function(e){
        var id = $(this).data('id');
        toedit(id);
      })
      $(".button_delete").click(function(e){
        var id = $(this).data('id');
        todelete(id);
      })
      $(".button_email").click(function(e){
        var id = $(this).data('id');
        toemail(id);
      })
      $(".button_approve").click(function(e){
        var id = $(this).data('id');
        toapprove(id);
      })
      $(".button_download_pdf").click(function(e){
        var id = $(this).data('id');
        window.location = base_url+"index.php/PermitActivity/download_pdf?id=" + id;
      })
    }
  });

  datatable.on('draw', function () {
    $('td').addClass('wrap-text')
  });


  $('.select2').select2()
  $('.select2-search__field').css("width", "auto");

});

function toview(id){
  $.ajax({
    url: base_url+"index.php/PermitActivity/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#viewPermit');
        $('#viewCkbasic').val(result.data.description);
        modal.find('[name="start_date"]').val(result.data.start_date);
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="long"]').val(result.data.long);
        modal.find('[name="subject"]').val(result.data.subject);

        var dataTo = result.data.to.split(', ');
        var dataCc = result.data.cc.split(', ');
        $('#viewSelectTo').val(dataTo).change();
        $('#viewSelectTo').attr('disabled','disabled');

        CKEDITOR.instances.viewCkbasic.setData( result.data.description );
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

function toedit(id){
  $.ajax({
    url: base_url+"index.php/PermitActivity/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editPermit');
        var dataTo = result.data.to.split(', ');
        var dataCc = result.data.cc.split(', ');

        $('#editCkbasic').val(result.data.description);
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="start_date"]').val(result.data.start_date);
        modal.find('[name="long"]').val(result.data.long);
        modal.find('[name="subject"]').val(result.data.subject);
        $("#alert-edit, .error").addClass('hidden');

        $('#editSelectTo').val(dataTo).change();
        CKEDITOR.instances.editCkbasic.setData( result.data.description );

      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}


$(document).ready(function(){
  $("#button-update").on('click',function(e) {
    e.preventDefault();
    var isFormValid = $("#editform").valid();
    var jsonPost = $("form#editform").serializeArray();
    jsonPost.push({name: 'description', value: CKEDITOR.instances.editCkbasic.getData()});
    if(isFormValid){
      $.ajax({
        url: base_url+"index.php/PermitActivity/do_update",
        type: 'POST',
        data : jsonPost,
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#editPermit');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            $("#alert-edit").removeClass('hidden');
            $("#message-edit").text(response.message);
          }
        }
      });
    }
  });
});

$(document).ready(function(){
  $("#button-add").on('click',function(e) {
    e.preventDefault();
    var isFormValid = $("#addform").valid();
    var jsonPost = $("form#addform").serializeArray();
    var permit_quota = $("#permitQuota").val();
    jsonPost.push({name: 'description', value: CKEDITOR.instances.addCkbasic.getData()});
    if (isFormValid) {
      $.ajax({
        url: base_url+"index.php/PermitActivity/do_save",
        type: 'POST',
        data : jsonPost,
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#addPermit');
            modal.modal('hide');
            datatable.ajax.reload();
          }else {
            $("#alert-add").removeClass('hidden');
            $("#message-add").text(response.message);
          }
        }
      });
    }
  });
});
function todelete(id){
  $.ajax({
    url: base_url+"index.php/PermitActivity/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deletePermit');
        modal.find('[name="id"]').val(result.data.id);
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

function toemail(id){
  $.ajax({
    url: base_url+"index.php/PermitActivity/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#emailPermit');
        modal.find('[name="id"]').val(result.data.id);
        $(".btn-email").removeClass('disabled');
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

function toapprove(id){
  $.ajax({
    url: base_url+"index.php/PermitActivity/get",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#approvePermit');
        modal.find('[name="id"]').val(result.data.id);
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

$('#button-email').click(function(){
  var modal= $("#emailPermit");
  var id = modal.find('[name="id"]').val();
  $(".btn-email").addClass('disabled');

  $.ajax({
    url: base_url+"index.php/PermitActivity/do_email",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatable.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
        $('#emailPermit').modal('hide');
      }
    }
  })
})


$('.btn-approve').click(function(){
  var modal= $("#approvePermit");
  var id = modal.find('[name="id"]').val();
  $.ajax({
    url: base_url+"index.php/PermitActivity/do_approve",
    type: 'POST',
    datatype:"json",
    data: {id: id, approval: $(this).val(), },
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatable.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
})

$('#button-delete').click(function(){
  var modal= $("#deletePermit");
  var id = modal.find('[name="id"]').val();
  $.ajax({
    url: base_url+"index.php/PermitActivity/do_delete",
    type: 'POST',
    datatype:"json",
    data: {id: id},
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatable.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
})

$('form').each(function(){
  $(this).validate({
    errorClass: "validate_error",
    ignore: [],
    rules: {
      subject: {
        required: true,
        maxlength: 100
      },
      description: {
        required: true,
      },
      description: {
        required: function(textarea) {
          CKEDITOR.instances[textarea.id].updateElement();
          var editorcontent = textarea.value.replace(/<[^>]*>/gi, '');
          return editorcontent.length === 0;
        }
      },
      long: {
        required: true,
        min: 0,
        max: 10,
        number: true
      },
    },
    messages: {
      subject: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 100 '+appLang.form_char,
      },
      description: {
        required: appLang.form_requiredform,
      },
      long: {
        required: appLang.form_requiredform,
        min: appLang.form_min+' 1 ',
        max: appLang.form_max+' 99 ',
      },
    }
  });
});



$('.modal').on('hidden.bs.modal', function () {
  $('form').each(function(){
    $(this).trigger('reset');
    $(this).validate().resetForm();
    $(this).find('.validate_error').removeClass('validate_error');
    CKEDITOR.instances.addCkbasic.setData('')
    CKEDITOR.instances.editCkbasic.setData('')
    CKEDITOR.instances.viewCkbasic.setData('')
  });

  $(".select2").trigger("change");
  $('.select2-search__field').css("width", "auto");

});
