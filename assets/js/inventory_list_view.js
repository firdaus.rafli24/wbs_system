$(document).ready(function(){
    datatable = $('#example1').DataTable({
        scrollX: true, serverSide: true, processing: true,
        "ajax": {
          url: base_url + "index.php/InventoryLoanList/datatable",
          type: 'POST'
        },
        autoWidth: false,
        columnDefs: [
          { targets: 0, width: "1%", className: "hidden" },
          { targets: 1, width: "7%" },
          { targets: 2, width: "7%" },
          { targets: 3, width: "15%" },
          { targets: 4, width: "12%" },
        ]
      });
      datatable.on('draw', function () {
        $('td').addClass('wrap-text')
      });
    
    
      $('.select2').select2()
      $('.select2-search__field').css("width", "auto");
});