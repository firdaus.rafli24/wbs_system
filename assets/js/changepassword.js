$(document).ready(function(){
  $("#submitpassword").on('click',function(e) {
    e.preventDefault();
    var current_password = $("#current_password").val();
    var new_password = $("#new_password_conf").val();
    var new_password_conf = $("#new_password_conf").val();

    var text = $(this).text();
    var isFormValid = $("#changepasswordform").valid();
    if (isFormValid) {
    $.ajax({
      url: base_url+"index.php/ChangePassword/do_change_password",
      type: 'POST',
      data: {current_password:current_password, new_password:new_password, new_password_conf:new_password_conf},
      datatype:"json",
      success: function(result){
        if (result.status) {
          $('#changepasswordform').trigger('reset');
          $('#changepasswordform').validate().resetForm();
          $('.validate_error').removeClass("validate_error");
          Swal.fire({
            title: 'Success!',
            html: result.message,
            icon: 'success',
            confirmButtonText: 'OK'
          });
        }else {
          Swal.fire({
            title: 'Error!',
            html: result.message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        }
      }
    });
  }
  });
});
var validator = $("#changepasswordform").validate({
                errorClass: "validate_error",
                rules: {
                  current_password: {
                    required: true,
                    normalizer: function( value ) {
                        return $.trim( value );
                    },
                    maxlength: 22,
                    minlength: 8,
                    regex: "^[A-Za-z0-9\\-_!#$%()*+-.\\/:?@\\[\\]\\\\\\`{}~ ]+$",
                    remote: {
                        async: false,
                        url:  base_url+'index.php/ChangePassword/check_current_password',
                        type: "POST"
                    }
                  },
                  new_password: {
                    required: true,
                    normalizer: function( value ) {
                        return $.trim( value );
                    },
                    maxlength: 22,
                    minlength: 8,
                    regex: "^[A-Za-z0-9\\-_!#$%()*+-.\\/:?@\\[\\]\\\\\\`{}~ ]+$",
                  },
                  new_password_conf:{
                    required: true,
                    normalizer: function( value ) {
                        return $.trim( value );
                    },
                    maxlength: 22,
                    minlength: 8,
                    equalTo: "#new_password",
                    regex: "^[A-Za-z0-9\\-_!#$%()*+-.\\/:?@\\[\\]\\\\\\`{}~ ]+$",
                  }
                },
                messages: {
                  required: appLang.form_requiredform,
                  current_password: {
                    minlength: appLang.form_minlength+' 8 '+appLang.form_char,
                    maxlength: appLang.form_maxlength+' 22 '+appLang.form_char,
                    regex: appLang.form_curr_pass_invalid,
                    remote: appLang.UI019E05
                  },
                  new_password: {
                    minlength: appLang.form_minlength+' 8 '+appLang.form_char,
                    maxlength: appLang.form_maxlength+' 22 '+appLang.form_char,
                    regex: appLang.form_pass_invalid
                  },
                  new_password_conf:{
                    minlength: appLang.form_minlength+' 8 '+appLang.form_char,
                    maxlength: appLang.form_maxlength+' 22 '+appLang.form_char,
                    regex: appLang.form_pass_invalid,
                    equalTo: appLang.form_is_match_password
                  }
                }
              });

$.validator.addMethod(
    "regex",
    function (value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    }, appLang.form_pass_invalid
);
