var currDate = new Date();
var datatable;
$(document).ready(function () {
  $('.datepicker').datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true,
    startDate: currDate,
  });
  datatable = $('#example1').DataTable({
    scrollX: true, serverSide: true, processing: true,
    "ajax": {
      url: base_url + "index.php/InventoryLoanActivity/datatable",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
      { targets: 0, width: "1%", className: "hidden" },
      { targets: 1, width: "7%" },
      { targets: 2, width: "7%" },
      { targets: 3, width: "15%" },
      { targets: 4, width: "12%" },
      {
        targets: 5,
        sortable: false,
        width: "5%",
        render: function (data, type, row) {
          var viewButton = '<button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewInventoryLoan" data-backdrop="static" data-id="' + row.id + '" style="background:#4abdac;margin-right:3px;">' + '<i class="fa fa-eye">' + '</i>' + '</button>';
          var editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editInventoryLoan" data-backdrop="static" data-id="' + row.id + '" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>';
          var deleteButton = '<button class="button_delete btn btn-sm" data-id="' + row.id + '" data-toggle="modal" data-target="#deleteInventoryLoan" data-backdrop="static" style="background:#fc4a1a;">' + '<i class="fa fa-trash">' + '</i>' + '</button>';
          return '<div class="text-center" style="min-width:100px">' + viewButton + editButton + deleteButton + '</div>';
        }
      }
    ],
    drawCallback: function (data, type, row) {
      $(".button_view").click(function (e) {
        var id = $(this).data('id');
        toview(id);
      })
      $(".button_edit").click(function (e) {
        var id = $(this).data('id');
        toedit(id);
      })
      $(".button_delete").click(function (e) {
        var id = $(this).data('id');
        todelete(id);
      })
    }
  });

  datatable.on('draw', function () {
    $('td').addClass('wrap-text')
  });


  $('.select2').select2()
  $('.select2-search__field').css("width", "auto");

});

function toview(id) {
  $.ajax({
    url: base_url + "index.php/InventoryLoanActivity/get",
    type: 'POST',
    datatype: "json",
    data: { id: id },
    success: function (result) {
      if (result.status) {
        var modal = $('#viewInventoryLoan');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="start_date"]').val(result.data.start_date);
        modal.find('[name="loan_duration"]').val(result.data.loan_duration);
        modal.find('[name="inventory_id"]').val(result.data.inventory_id);

        CKEDITOR.instances.viewCkbasic.setData(result.data.description);
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}

function toedit(id) {
  $.ajax({
    url: base_url + "index.php/InventoryLoanActivity/get",
    type: 'POST',
    datatype: "json",
    data: { id: id },
    success: function (result) {
      if (result.status) {
        var modal = $('#editInventoryLoan');

        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="start_date"]').val(result.data.start_date);
        modal.find('[name="loan_duration"]').val(result.data.loan_duration);
        modal.find('[name="inventory_id"]').val(result.data.inventory_id);
        $("#alert-edit, .error").addClass('hidden');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}


$(document).ready(function () {
  $("#button-update").on('click', function (e) {
    e.preventDefault();
    var isFormValid = $("#editform").valid();
    var jsonPost = $("form#editform").serializeArray();
    if (isFormValid) {
      $.ajax({
        url: base_url + "index.php/InventoryLoanActivity/do_update",
        type: 'POST',
        data: jsonPost,
        datatype: "json",
        success: function (response) {
          if (response.status) {
            var modal = $('#editInventoryLoan');
            modal.modal('hide');
            datatable.ajax.reload();
          } else {
            $("#alert-edit").removeClass('hidden');
            $("#message-edit").text(response.message);
          }
        }
      });
    }
  });
});

$(document).ready(function () {
  $("#button-add").on('click', function (e) {
    e.preventDefault();
    var isFormValid = $("#addform").valid();
    var jsonPost = $("form#addform").serializeArray();
    if (isFormValid) {
      $.ajax({
        url: base_url + "index.php/InventoryLoanActivity/do_save",
        type: 'POST',
        data: jsonPost,
        datatype: "json",
        success: function (response) {
          if (response.status) {
            var modal = $('#addInventoryLoan');
            modal.modal('hide');
            datatable.ajax.reload();
          } else {
            $("#alert-add").removeClass('hidden');
            $("#message-add").text(response.message);
          }
        }
      });
    }
  });
});
function todelete(id) {
  $.ajax({
    url: base_url + "index.php/InventoryLoanActivity/get",
    type: 'POST',
    datatype: "json",
    data: { id: id },
    success: function (result) {
      if (result.status) {
        var modal = $('#deleteInventoryLoan');
        modal.find('[name="id"]').val(result.data.id);
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}
$('#button-delete').click(function () {
  var modal = $("#deleteInventoryLoan");
  var id = modal.find('[name="id"]').val();
  $.ajax({
    url: base_url + "index.php/InventoryLoanActivity/do_delete",
    type: 'POST',
    datatype: "json",
    data: { id: id },
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatable.ajax.reload();
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
})

$('form').each(function () {
  $(this).validate({
    errorClass: "validate_error",
    ignore: [],
    rules: {
      start_date: {
        required: true,
      },
      loan_duration: {
        required: true,
        min: 1,
        max: 99,
        number: true
      },
    },
    messages: {
      start_date: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength + ' 100 ' + appLang.form_char,
      },
      loan_duration: {
        required: appLang.form_requiredform,
        min: appLang.form_min + ' 1 ',
        max: appLang.form_max + ' 99 ',
      },
    }
  });
});



$('.modal').on('hidden.bs.modal', function () {
  $('form').each(function () {
    $(this).trigger('reset');
    $(this).validate().resetForm();
    $(this).find('.validate_error').removeClass('validate_error');
  });

  $(".select2").trigger("change");
  $('.select2-search__field').css("width", "auto");

});
