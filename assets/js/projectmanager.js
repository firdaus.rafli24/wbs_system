
var datatable1;
var datatableCategory;
var datatablesStatus;
$(document).ready(function () {
  $('.sidebar-menu').tree();

  datatable1 = $('#table_project').DataTable({
    scrollX: true,serverSide: true,processing: true,
    "ajax": {
      url: base_url+"index.php/ProjectManager/datatable_project",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"5%", className: "hidden"},
    {targets: 1, width:"15%", className: "text-center"},
    {targets: 2, width:"20%"},
    {targets: 3, width:"15%", className: "text-center"},
    {targets: 4, width:"15%", className: "text-center"},
    {targets: 5, width:"15%", className: "text-center"},
    {
      targets: 6,
      sortable: false,
      width: "15%",
      render: function (data, type, row) {
        var viewButton = '<button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewProject" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;
        var editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editProject"" data-backdrop="static" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
        var deleteButton = '<button class="button_delete btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deleteProject" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
        return '<div class="text-center" style="min-width:100px">'+viewButton+editButton+deleteButton+'</div>';
      }
    }],
    drawCallback: function(data, type, row) {
      $(".button_view").click(function(e){
        var id = $(this).data('id');
        toview(id);
          // console.log(id);
        })
      $(".button_edit").click(function(e){
        var id = $(this).data('id');
        toedit(id);
          // console.log(id);
        })
      $(".button_delete").click(function(e){
        var id = $(this).data('id');
        todelete(id);
      })
    }
  });
  datatable1.on('draw', function () {
    $('td').addClass('wrap-text')
  });
  datatableCategory = $('#table_category').DataTable({
    scrollX: true,serverSide: true,processing: true,
    "ajax": {
      url: base_url+"index.php/ProjectManager/datatable_category",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"5%", className: "hidden"},
    {targets: 1, width:"30%"},
    {targets: 2, width:"15%", className: "text-center"},
    {targets: 3, width:"30%"},
    {
      targets: 4,
      sortable: false,
      width: "20%",
      render: function (data, type, row) {
        var viewButton2 = '<button class="button_view2 btn btn-sm" data-toggle="modal" data-target="#viewCat" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;
        var editButton2 = '<button class="button_edit2 btn btn-sm" data-toggle="modal" data-target="#editCat" data-backdrop="static" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
        var deleteButton2 = '<button class="button_delete2 btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deleteCat" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
        return '<div class="text-center" style="min-width:100px">'+viewButton2+editButton2+deleteButton2+'</div>';
      }
    }],
    drawCallback: function(data, type, row) {
      $(".button_view2").click(function(e){
        var id = $(this).data('id');
        toview2(id);
            // console.log(id);
          })
      $(".button_edit2").click(function(e){
        getRole('edit');
        var id = $(this).data('id');
        toedit2(id);
            // console.log(id);
          })
      $(".button_delete2").click(function(e){
        var id = $(this).data('id');
        todelete2(id);
      })
    }
  });
  datatableCategory.on('draw', function () {
    $('td').addClass('wrap-text')
  });

  datatablesStatus = $('#table_status').DataTable({
    scrollX: true,serverSide: true,processing: true,
    "ajax": {
      url: base_url+"index.php/ProjectManager/datatable_status",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"5%", className: "hidden"},
    {targets: 1, width:"30%", className: "text-center"},
    {targets: 2, width:"35%"},
    {
      targets: 3,
      sortable: false,
      width: "30%",
      render: function (data, type, row) {
        var viewButton3 = '<button class="button_view3 btn btn-sm" data-toggle="modal" data-backdrop="static" data-target="#viewStatus" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;
        var editButton3 = '<button class="button_edit3 btn btn-sm" data-toggle="modal" data-backdrop="static" data-target="#editStatus" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
        var deleteButton3 = '<button class="button_delete3 btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deleteStatus" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
        return '<div class="text-center" style="min-width:100px">'+viewButton3+editButton3+deleteButton3+'</div>';
      }
    }],
    drawCallback: function(data, type, row) {
      $(".button_view3").click(function(e){
        var id = $(this).data('id');
        toview3(id);
          // console.log(id);
        })
      $(".button_edit3").click(function(e){
        var id = $(this).data('id');
        toedit3(id);
          // console.log(id);
        })
      $(".button_delete3").click(function(e){
        var id = $(this).data('id');
        todelete3(id);
      })
    }
  });
  datatablesStatus.on('draw', function () {
    $('td').addClass('wrap-text')
  });

});


var alerts = function() {
  return {
    showError: function(message) {
      $("#error_container").html(message);
      $("#error_container").removeClass('d-none');
    },
    hideError: function() {
      $("#error_container").html("");
      $("#error_container").addClass('d-none');
    }
  }
}();
$(document).ready(function(){
  $("#addproject-btn").on('click',function(e) {
    e.preventDefault();
    $('#addprojectform').trigger('reset');
    $('#addprojectform').validate().resetForm();
    $('.validate_error').removeClass("validate_error");
  });
});
$("#addprojectform").validate({
  errorClass: "validate_error",
  rules: {
    name: {
      required: true,
      maxlength: 20
    },
    description: {
      maxlength: 100,
    },
    start_date:{
      required: true,
    },
    due_date:{
      required: true,
    }
  },
  messages: {
    name: {
      required: appLang.form_requiredform,
      maxlength: appLang.form_maxlength+' 20 '+appLang.form_char,
    },
    description: {
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char
    },
    start_date:{
      required: appLang.form_requiredform
    },
    due_date:{
      required: appLang.form_requiredform
    }
  }
});
$('#addcategory-btn').click(function () {
  getRole('add');
  $('#addcatform').trigger('reset');
  $('#addcatform').validate().resetForm();
  $('.validate_error').removeClass("validate_error");
});
function getRole(view) {
  $.ajax({
    url: base_url+"index.php/ProjectManager/add",
    type: "POST",
    success: function (res) {
      var el = $("#role_id" + view);
      el.empty();
      el.append($("<option disabled selected></option>").attr("value", 0).text('Select'));
      if (res.status) {
        $.each(res.data, function (index, role) {
          el.append($("<option></option>").attr("value", role.id).text(role.name));
        });
      }
    }
  });
};
$(document).ready(function(){
  $("#button-add-project").on('click',function(e) {
    e.preventDefault();
    var modal= $("#addProject");
    var name = modal.find('[name="name"]').val();
    var description = modal.find('[name="description"]').val();
    var start_date = modal.find('[name="start_date"]').val();
    var due_date = modal.find('[name="due_date"]').val();
    var due_date_revised = modal.find('[name="due_date_revised"]').val();
    var text = $(this).text();
    var isFormValid = $("#addprojectform").valid();
    if (isFormValid) {
      $.ajax({
        url: base_url+"index.php/ProjectManager/do_save_project",
        type: 'POST',
        data: {name:name, description:description, start_date:start_date,due_date:due_date, due_date_revised:due_date_revised},
        datatype:"json",
        success: function(result){
          if (result.status) {
            var modal = $('#addProject');
            modal.modal('hide');
            datatable1.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});
function toview(id){
  $.ajax({
    url: base_url+"index.php/ProjectManager/get_project",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#viewProject');
        modal.find('[name="name"]').val(result.data.name);
        modal.find('[name="description"]').val(result.data.description);
        modal.find('[name="start_date"]').val(is_valid_date(result.data.start_date));
        modal.find('[name="due_date"]').val(is_valid_date(result.data.due_date));
        modal.find('[name="due_date_revised"]').val(is_valid_date(result.data.due_date_revised));
        modal.modal('show');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
};
function toedit(id){
  $('#editprojectform').trigger('reset');
  $('#editprojectform').validate().resetForm();
  $('.validate_error').removeClass("validate_error");
  $.ajax({
    url: base_url+"index.php/ProjectManager/get_project",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editProject');

        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="name"]').val(result.data.name);
        modal.find('[name="description"]').val(result.data.description);
        modal.find('[name="start_date"]').val(result.data.start_date);
        modal.find('[name="due_date"]').val(result.data.due_date);
        modal.find('[name="due_date_revised"]').val(result.data.due_date_revised);
        modal.modal('show');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
};
$("#editprojectform").validate({
  errorClass: "validate_error",
  rules: {
    name: {
      required: true,
      maxlength: 20
    },
    description: {
      maxlength: 100,
    },
    start_date:{
      required: true,
    },
    due_date:{
      required: true,
    }
  },
  messages: {
    name: {
      required: appLang.form_requiredform,
      maxlength: appLang.form_maxlength+' 20 '+appLang.form_char,
    },
    description: {
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char
    },
    start_date:{
      required: appLang.form_requiredform
    },
    due_date:{
      required: appLang.form_requiredform
    }
  }
});
$(document).ready(function(){
  $("#button-update-project").on('click',function(e) {
    e.preventDefault();
    var modal= $("#editProject");
    var id = modal.find('[name="id"]').val();
    var name = modal.find('[name="name"]').val();
    var description = modal.find('[name="description"]').val();
    var start_date = modal.find('[name="start_date"]').val();
    var due_date = modal.find('[name="due_date"]').val();
    var due_date_revised = modal.find('[name="due_date_revised"]').val();
    var text = $(this).text();
    var form = $('#editprojectform');
    var valid = form.valid()
    if(valid){
      $.ajax({
        url: base_url+"index.php/ProjectManager/do_update_project",
        type: 'POST',
        data: {user_id:id,name:name, description:description, start_date:start_date,due_date:due_date, due_date_revised:due_date_revised},
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#editProject');
            modal.modal('hide');
            datatable1.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: response.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});
function todelete(id){
  $.ajax({
    url: base_url+"index.php/ProjectManager/get_project",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deleteProject');
        modal.find('[name="id"]').val(result.data.id);      }
      }
    });
};
$('#button-delete-project').click(function(){
  var modal= $("#deleteProject");
  var id = modal.find('[name="id"]').val();
  data: {user_id:id};
  $.ajax({
    url: base_url+"index.php/ProjectManager/do_delete_project",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatable1.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
});
$(document).ready(function(){
  $("#button-add-cat").on('click',function(e) {
    e.preventDefault();
    var modal= $("#addCat");
    var name = modal.find('[name="name"]').val();
    var description = modal.find('[name="description"]').val();
    var role_id_add = modal.find('[name="role_id_add"]').val();
    var text = $(this).text();
    var isFormValid = $("#addcatform").valid();
    if (isFormValid) {
      $.ajax({
        url: base_url+"index.php/ProjectManager/do_save_category",
        type: 'POST',
        data: {name:name, description:description, role_id: role_id_add},
        datatype:"json",
        success: function(result){
          if (result.status) {
            var modal = $('#addCat');
            modal.modal('hide');
            datatableCategory.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});
$("#addcatform").validate({
  errorClass: "validate_error",
  rules: {
    name: {
      required: true,
      maxlength: 50
    },
    description: {
      maxlength: 100,
    },
    role_id_add:{
      required: true,
    }
  },
  messages: {
    name: {
      required: appLang.form_requiredform,
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char,
    },
    description: {
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char
    },
    role_id_add:{
      required: appLang.form_requiredform
    }
  }
});
$("#editcatform").validate({
  errorClass: "validate_error",
  rules: {
    name_cat: {
      required: true,
      maxlength: 100
    },
    description_cat: {
      maxlength: 100,
    },
    role_id_edit:{
      required: true,
    }
  },
  messages: {
    name_cat: {
      required: appLang.form_requiredform,
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char,
    },
    description_cat: {
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char
    },
    role_id_edit:{
      required: appLang.form_requiredform
    }
  }
});
function toview2(id){
  $.ajax({
    url: base_url+"index.php/ProjectManager/get_category",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        console.log(result.data);
        var modal = $('#viewCat');
        modal.find('[name="name"]').val(result.data.name);
        modal.find('[name="description"]').val(result.data.description);
        modal.find('[name="role"]').val(result.data.rolename);
        modal.modal('show');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
};
function toedit2(id){
  $('#editcatform').trigger('reset');
  $('#editcatform').validate().resetForm();
  $('.validate_error').removeClass("validate_error");
  $.ajax({
    url: base_url+"index.php/ProjectManager/get_category",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editCat');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="name_cat"]').val(result.data.name);
        modal.find('[name="description_cat"]').val(result.data.description);
        modal.find('[name="role_id_edit"]').val(result.data.role);
        modal.modal('show');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
};
$(document).ready(function(){
  $("#button-edit-cat").on('click',function(e) {
    e.preventDefault();
    var modal= $("#editCat");
    var id = modal.find('[name="id"]').val();
    var name = modal.find('[name="name_cat"]').val();
    var description = modal.find('[name="description_cat"]').val();
    var role_id_edit = modal.find('[name="role_id_edit"]').val();
    var text = $(this).text();
    var isFormValid = $("#editcatform").valid();
    if (isFormValid) {
      $.ajax({
        url: base_url+"index.php/ProjectManager/do_update_category",
        type: 'POST',
        data: {user_id:id, name:name, description:description, role_id: role_id_edit},
        datatype:"json",
        success: function(result){
          if (result.status) {
            var modal = $('#editCat');
            modal.modal('hide');
            datatableCategory.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});
function todelete2(id){
  $.ajax({
    url: base_url+"index.php/ProjectManager/get_category",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deleteCat');
        modal.find('[name="id"]').val(result.data.id);      }
      }
    });
};
$('#button-delete-cat').click(function(){
  var modal= $("#deleteCat");
  var id = modal.find('[name="id"]').val();
  data: {user_id:id};
  $.ajax({
    url: base_url+"index.php/ProjectManager/do_delete_category",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatableCategory.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
});
$(document).ready(function(){
  $("#addstatus-btn").on('click',function(e) {
    e.preventDefault();
    $('#addstatusform').trigger('reset');
    $('#addstatusform').validate().resetForm();
    $('.validate_error').removeClass("validate_error");
  });
});
$("#addstatusform").validate({
  errorClass: "validate_error",
  rules: {
    name: {
      required: true,
      maxlength: 20
    },
    description: {
      maxlength: 100,
    }
  },
  messages: {
    name: {
      required: appLang.form_requiredform,
      maxlength: appLang.form_maxlength+' 20 '+appLang.form_char,
    },
    description: {
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char
    }
  }
});
$(document).ready(function(){
  $("#button-add-status").on('click',function(e) {
    e.preventDefault();
    var modal= $("#addStatus");
    var name = modal.find('[name="name"]').val();
    var description = modal.find('[name="description"]').val();
    var text = $(this).text();
    var isFormValid = $("#addstatusform").valid();
    if (isFormValid) {
      $.ajax({
        url: base_url+"index.php/ProjectManager/do_save_status",
        type: 'POST',
        data: {name:name, description:description},
        datatype:"json",
        success: function(result){
          if (result.status) {
            var modal = $('#addStatus');
            modal.modal('hide');
            datatablesStatus.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});
function toview3(id){
  $.ajax({
    url: base_url+"index.php/ProjectManager/get_status",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#viewStatus');
        modal.find('[name="name"]').val(result.data.name);
        modal.find('[name="description"]').val(result.data.description);
        modal.modal('show');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
};
function toedit3(id){
  $('#editstatusform').trigger('reset');
  $('#editstatusform').validate().resetForm();
  $('.validate_error').removeClass("validate_error");
  $.ajax({
    url: base_url+"index.php/ProjectManager/get_status",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editStatus');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="name"]').val(result.data.name);
        modal.find('[name="description"]').val(result.data.description);
        modal.modal('show');
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
};
$("#editstatusform").validate({
  errorClass: "validate_error",
  rules: {
    name: {
      required: true,
      maxlength: 20
    },
    description: {
      maxlength: 100,
    }
  },
  messages: {
    name: {
      required: appLang.form_requiredform,
      maxlength: appLang.form_maxlength+' 20 '+appLang.form_char,
    },
    description: {
      maxlength: appLang.form_maxlength+' 100 '+appLang.form_char
    }
  }
});
$(document).ready(function(){
  $("#button-edit-status").on('click',function(e) {
    e.preventDefault();
    var modal= $("#editStatus");
    var id = modal.find('[name="id"]').val();
    var name = modal.find('[name="name"]').val();
    var description = modal.find('[name="description"]').val();
    var text = $(this).text();
    var form = $('#editstatusform');
    var valid = form.valid()
    if(valid){
      $.ajax({
        url: base_url+"index.php/ProjectManager/do_update_status",
        type: 'POST',
        data: {user_id:id, name: name, description:description},
        datatype:"json",
        success: function(response){
          if (response.status) {
            var modal = $('#editStatus');
            modal.modal('hide');
            datatablesStatus.ajax.reload();
          }else {
            Swal.fire({
              title: 'Error!',
              html: response.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
    }
  });
});
function todelete3(id){
  $.ajax({
    url: base_url+"index.php/ProjectManager/get_status",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#deleteStatus');
        modal.find('[name="id"]').val(result.data.id);      }
      }
    });
};
$('#button-delete-status').click(function(){
  var modal= $("#deleteStatus");
  var id = modal.find('[name="id"]').val();
  data: {user_id:id};
  $.ajax({
    url: base_url+"index.php/ProjectManager/do_delete_status",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        modal.modal('hide');
        datatablesStatus.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
});
function is_valid_date($input){
  if (($input < '2000-01-01')) {
    return '-';
  }else{
    return $input;
  }
}
$(document).ready(function(){
  $("#start_date_add").datepicker({
    todayBtn:  1,
    autoclose: true,
    format: 'yyyy-mm-dd',
    startDate : new Date(),
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#due_date_add').datepicker('setStartDate', minDate);
  });

  $("#due_date_add").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  })
  .on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#start_date_add').datepicker('setEndDate', maxDate);
  });

  $("#due_date_add").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
  }).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#due_date_revised_add').datepicker('setStartDate', minDate);
  });

  $("#due_date_revised_add").datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
  })
  .on('changeDate', function (selected) {
    var maxDate = new Date(selected.date.valueOf());
    $('#due_date_add').datepicker('setEndDate', maxDate);
  });
});


$('#editProject').on('show.bs.modal', function () {
  $("#start_date_edit").datepicker("setEndDate", new Date($('#due_date_edit').val()) );
  $("#due_date_edit").datepicker("setStartDate", new Date($('#start_date_edit').val()) );
  $("#due_date_revised_edit").datepicker("setStartDate", new Date($('#due_date_edit').val()) );
});


$('.modal').on('hidden.bs.modal', function () {
  $('form[role="form"]').validate().resetForm();
  $('form[role="form"]').trigger('reset');
});