var socket = io.connect( ConfValue.node_url+':'+ConfValue.node_port );
var userId = $('#current_user_login_id').val();


$(document).ready(function() {
	countNotif();

	socket.on('connect_error', function(e){ 
		Swal.fire({
			title: 'Error!',
			html: 'Connection Nodejs error',
			icon: 'error',
			confirmButtonText: 'OK'
		}).then((result) => {
			if (result.value) {
				window.location.reload();
			}
		});
	});

	
});



socket.on('connect', () => {

	socket.emit('login_check', {
		userId: userId,
		sessionId: $('#current_user_session_id').val(),
		userType: $('#current_user_type').val()
	});

	socket.on('login_session', (data) => {
		// console.log(data);
	});
	socket.on('logout_direct', (data) => {
		window.location.href = base_url+'index.php/Auth/logout';
	});

	socket.on('cm_send_notif_permit', function() {
		countNotif();
	});

	socket.on('cm_send_notif_view', (data) => {
		countNotif();
	});

	socket.on('cm_checkvalidasi', (data) => {
		console.log(data);
	});


});


function countNotif() {
	$.ajax({
		url: base_url+"index.php/Notification/countNotif",
		type: 'GET',
		success: function(response){
			$('#notif, #subnotif').text(response);
			$('#notificationTable').DataTable().ajax.reload();
			// datatable.ajax.reload();
		}
	});
}






