var alerts = function() {
  return {
    showError: function(message) {
      $("#error_container").html(message);
      $("#error_container").removeClass('d-none');
    },
    hideError: function() {
      $("#error_container").html("");
      $("#error_container").addClass('d-none');
    }
  }
}();
var datatable;
$(document).ready(function() {
  datatable = $('#example1').DataTable({
    scrollX: true,serverSide: true,processing: true,
    "ajax": {
      url: base_url+"index.php/UserManagement/datatable",
      type: 'POST'
    },
    autoWidth: false,
    columnDefs: [
    {targets: 0, width:"1%", className: "hidden"},
    {targets: 1, width:"20%"},
    {targets: 2, width:"10%"},
    {targets: 3, width:"10%"},
    {targets: 4, width:"10%"},
    {targets: 5, width:"14%"},
    {targets: 6, width:"14%"},
    {
      targets: 7,
      sortable: false,
      width: "15%",
      render: function (data, type, row) {
        var viewButton = '<button class="button_view btn btn-sm" data-toggle="modal" data-target="#viewUser" data-backdrop="static" data-id="'+row.id+'" style="background:#4abdac;margin-right:3px;">'+'<i class="fa fa-eye">'+'</i>'+'</button>' ;
        var editButton = '<button class="button_edit btn btn-sm" data-toggle="modal" data-target="#editUser" data-backdrop="static" onclick="editmode()" data-id="'+row.id+'" style="background:#f7b733;margin-right:3px;"><i class="fa fa-edit" ></i></button>' ;
        var deleteButton = '<button class="button_delete btn btn-sm" data-id="'+row.id+'" data-toggle="modal" data-target="#deleteUser" data-backdrop="static" style="background:#fc4a1a;">'+'<i class="fa fa-trash">'+'</i>'+'</button>' ;
        return '<div class="text-center" style="min-width:100px">'+viewButton+editButton+deleteButton+'</div>';
      }
    }],
    drawCallback: function(data, type, row) {
      $(".button_view").click(function(e){
        var id = $(this).data('id');
        toview(id);
      })
      $(".button_edit").click(function(e){
        var id = $(this).data('id');
        toedit(id);
      })
      $(".button_delete").click(function(e){
        var id = $(this).data('id');
        todelete(id);
      })
    }
  });

  datatable.on('draw', function () {
    $('td').addClass('wrap-text')
  });
});
function toview(id){
  $.ajax({
    url: base_url+"index.php/UserManagement/get",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#viewUser');
        modal.find('[name="username"]').val(result.data.username);
        modal.find('[name="email"]').val(result.data.email);
            // modal.find('[name="password"]').val(result.data.password);
            modal.find('[name="phone_number"]').val(result.data.phone_number);
            modal.find('[name="wa_number"]').val(result.data.wa_number);
            chooseemployment(result.data.employment_status);
            modal.find('[name="employment_status"]').val($theemployment);
            modal.find('[name="user_type"]').val(result.data.user_type_name);
            modal.modal('show');
          } else {
            Swal.fire({
              title: 'Error!',
              html: result.message,
              icon: 'error',
              confirmButtonText: 'OK'
            });
          }
        }
      });
}

function chooseemployment($employment)
{
  if ($employment==0) {
    $theemployment='Reguler';
  }else {
    $theemployment='Freelance';
  }
  return $theemployment;
}

function toedit(id){
  $('#edituserform').trigger('reset');
  $('#edituserform').validate().resetForm();
  $('.validate_error').removeClass("validate_error");
  $.ajax({
    url: base_url+"index.php/UserManagement/get",
    type: 'POST',
    datatype:"json",
    data: {user_id: id},
    success: function (result) {
      if (result.status) {
        var modal = $('#editUser');
        modal.find('[name="id"]').val(result.data.id);
        modal.find('[name="usernameedit"]').val(result.data.username);
        modal.find('[name="emailedit"]').val(result.data.email);
        modal.find('[name="phone_numberedit"]').val(result.data.phone_number);
        modal.find('[name="wa_numberedit"]').val(result.data.wa_number);
        selectemployment(result.data.employment_status);
        modal.find('[name="user_type_edit"]').val(result.data.user_type);
      } else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  });
}
function selectrole(role){
  var modal = $('#editUser');
  if (role==0){
    modal.find("#PGedit").prop("selected", true);
  }else if (role==1) {
    modal.find("#QCedit").prop("selected", true);
  }else if (role==2) {
    modal.find("#SAedit").prop("selected", true);
  } else{
    modal.find("#HRedit").prop("selected", true);  }
  }
  function selectemployment(employment_status) {
    var modal = $('#editUser');
    if (employment_status==0) {
      modal.find("#Reguleredit").prop("checked", true);
    }else if(employment_status==1){
      modal.find("#Freelanceedit").prop("checked", true);
    }
  }
  $(document).ready(function(){
    $("#button-update").on('click',function(e) {
      e.preventDefault();
      var modal= $("#editUser");
      var id = modal.find('[name="id"]').val();
      var username = modal.find('[name="usernameedit"]').val();
      var email = modal.find('[name="emailedit"]').val();
      var password = modal.find('[name="passwordedit"]').val();
      var phone_number = modal.find('[name="phone_numberedit"]').val();
      var wa_number = modal.find('[name="wa_numberedit"]').val();
      var employment_status = modal.find('[name="optradio"]:checked').val();
      var user_type_edit = modal.find('[name="user_type_edit"]').val();
      var text = $(this).text();
      var form = $('#edituserform');
      var valid = form.valid()
      if(valid){
        $.ajax({
          url: base_url+"index.php/UserManagement/do_update",
          type: 'POST',
          data: {user_id: id, email: email, password: password, username:username,phone_number:phone_number, wa_number:wa_number, employment_status:employment_status, role:user_type_edit},
          datatype:"json",
          success: function(response){
            if (response.status) {
              var modal = $('#editUser');
              modal.modal('hide');
              datatable.ajax.reload();
            }else {
              Swal.fire({
                title: 'Error!',
                html: response.message,
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
          }
        });
      }
    });
  });
  $(document).ready(function(){
    $("#button-adduser").on('click',function(e) {
      e.preventDefault();
      $('#adduserform').trigger('reset');
      $('#adduserform').validate().resetForm();
      $('.validate_error').removeClass("validate_error");
    });
  });

  $(document).ready(function(){
    $("#button-add").on('click',function(e) {
      e.preventDefault();
      var modal= $("#addUser");
      var username = modal.find('[name="username"]').val();
      var email = modal.find('[name="email"]').val();
      var password = modal.find('[name="password"]').val();
      var phone_number = modal.find('[name="phone_number"]').val();
      var wa_number = modal.find('[name="wa_number"]').val();
      var employment_status = modal.find('[name="optradio"]:checked').val();
      var user_type_add = modal.find('[name="user_type_add"]').val();
      // var digital_signature = modal.find('[name="digitalsignature"]').val();
      var text = $(this).text();
      var isFormValid = $("#adduserform").valid();
      if (isFormValid) { 
        var fd = new FormData();
        var files;
        files = $('#addDigitalSignature')[0].files;
        fd.append('digital_signature',files[0]);
        fd.append('email',email);
        fd.append('password',password);
        fd.append('username',username);
        fd.append('phone_number',phone_number);
        fd.append('wa_number',wa_number);
        fd.append('employment_status',employment_status);
        fd.append('role',user_type_add);
        // fd.append("digital_signature",digital_signature);
        $.ajax({
          url: base_url+"index.php/UserManagement/do_save",
          type: 'POST',
          data: fd,
          processData: false,
          contentType: false,         
          success: function(response){
            if (response.status) {
              var modal = $('#addUser');
              modal.modal('hide');
              datatable.ajax.reload();
            }else {
              Swal.fire({
                title: 'Error!',
                html: response.message,
                icon: 'error',
                confirmButtonText: 'OK'
              });
            }
          }
        });
      }
    });
  });
  function todelete(id){
    $.ajax({
      url: base_url+"index.php/UserManagement/get",
      type: 'POST',
      datatype:"json",
      data: {user_id: id},
      success: function (result) {
        if (result.status) {
          var modal = $('#deleteUser');
          modal.find('[name="id"]').val(result.data.id);
        }else {
          Swal.fire({
            title: 'Error!',
            html: result.message,
            icon: 'error',
            confirmButtonText: 'OK'
          });
        }
      }
    });
  }
  $('#button-delete').click(function(){
    var modal= $("#deleteUser");
    var id = modal.find('[name="id"]').val();
    data: {user_id:id};
    $.ajax({
      url: base_url+"index.php/UserManagement/do_delete",
      type: 'POST',
      datatype:"json",
      data: {user_id: id},
      success: function (result) {
        if (result.status) {
        // window.alert("deleted");
        modal.modal('hide');
        datatable.ajax.reload();
      }else {
        Swal.fire({
          title: 'Error!',
          html: result.message,
          icon: 'error',
          confirmButtonText: 'OK'
        });
      }
    }
  })
  })
  $("#adduserform").validate({
    errorClass: "validate_error",
    rules: {
      phone_number: {
        required: true,
        number: true,
        maxlength: 14,
        minlength: 9,
      },
      email: {
        required: true,
        maxlength: 50,
        email: true
      },
      username: {
        required: true,
        maxlength: 100
      },
      wa_number: {
        required: true,
        number: true,
        maxlength: 14,
        minlength: 9,
      },
      password: {
        required: true,
        minlength: 8,
        maxlength: 22,
      }
    },
    messages: {
      phone_number: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 14 '+appLang.form_char,
        minlength: appLang.form_minlength+' 9 '+appLang.form_char,
        number: appLang.form_number_invalid
      },
      email: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 50 '+appLang.form_char,
        email: appLang.form_email_invalid
      },
      username: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 100 '+appLang.form_char,
      },
      wa_number: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 14 '+appLang.form_char,
        minlength: appLang.form_minlength+' 9 '+appLang.form_char,
        number: appLang.form_number_invalid
      },
      password: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 22 '+appLang.form_char,
        minlength: appLang.form_minlength+' 8 '+appLang.form_char,
      }
    }
  });
  $("#edituserform").validate({
    errorClass: "validate_error",
    rules: {
      phone_numberedit: {
        required: true,
        number: true,
        maxlength: 14,
        minlength: 9,
      },
      emailedit: {
        required: true,
        maxlength: 50,
        email: true
      },
      usernameedit: {
        required: true,
        maxlength: 100
      },
      wa_numberedit: {
        required: true,
        number: true,
        maxlength: 14,
        minlength: 9,
      },
      passwordedit: {
        minlength: 8,
        maxlength: 22,
      }
    },
    messages: {
      phone_numberedit: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 14 '+appLang.form_char,
        minlength: appLang.form_minlength+' 9 '+appLang.form_char,
        number: appLang.form_number_invalid
      },
      emailedit: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 50 '+appLang.form_char,
        email: appLang.form_email_invalid
      },
      usernameedit: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 100 '+appLang.form_char,
      },
      wa_numberedit: {
        required: appLang.form_requiredform,
        maxlength: appLang.form_maxlength+' 14 '+appLang.form_char,
        minlength: appLang.form_minlength+' 9 '+appLang.form_char,
        number: appLang.form_number_invalid
      },
      passwordedit: {
        maxlength: appLang.form_maxlength+' 22 '+appLang.form_char,
        minlength: appLang.form_minlength+' 8 '+appLang.form_char,
      }
    }
  });

