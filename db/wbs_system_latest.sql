-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2021 at 01:14 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wbs_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `assignment_letter`
--

CREATE TABLE `assignment_letter` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `projects_id` int(11) NOT NULL,
  `recipient_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `date` date NOT NULL,
  `purpose` varchar(100) NOT NULL,
  `destination` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assignment_letter`
--

INSERT INTO `assignment_letter` (`id`, `user_id`, `projects_id`, `recipient_id`, `created_date`, `date`, `purpose`, `destination`) VALUES
(27, 2, 1, 4, '2021-11-14', '2021-11-14', 'Melakukan Requirement Gathering', 'Jl. Waruga Jaya No.20');

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `config_key` varchar(200) NOT NULL DEFAULT '',
  `config_enable` int(1) NOT NULL DEFAULT 1,
  `config_value` varchar(200) DEFAULT '0',
  `config_description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`config_key`, `config_enable`, `config_value`, `config_description`) VALUES
('PAST_ACTIVITY_DATE', 1, '2', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `git_issue`
--

CREATE TABLE `git_issue` (
  `id` int(10) UNSIGNED NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id_upload` int(11) NOT NULL,
  `date_upload` datetime NOT NULL DEFAULT current_timestamp(),
  `issue_id` int(20) UNSIGNED NOT NULL,
  `url` text NOT NULL,
  `title` text NOT NULL,
  `state` varchar(50) DEFAULT '',
  `description` text DEFAULT NULL,
  `author` varchar(150) NOT NULL DEFAULT '',
  `author_username` varchar(100) NOT NULL DEFAULT '',
  `assignee` varchar(150) DEFAULT '',
  `assignee_username` varchar(100) DEFAULT '',
  `confidential` varchar(15) DEFAULT '',
  `locked` varchar(15) DEFAULT '',
  `due_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `milestone` text DEFAULT NULL,
  `weight` varchar(100) NOT NULL,
  `labels` text DEFAULT NULL,
  `time_estimate` double NOT NULL DEFAULT 0,
  `time_spent` double NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `git_issue`
--

INSERT INTO `git_issue` (`id`, `project_id`, `user_id_upload`, `date_upload`, `issue_id`, `url`, `title`, `state`, `description`, `author`, `author_username`, `assignee`, `assignee_username`, `confidential`, `locked`, `due_date`, `created_at`, `updated_at`, `closed_at`, `milestone`, `weight`, `labels`, `time_estimate`, `time_spent`) VALUES
(1, 1, 1, '2020-07-07 14:46:01', 64, 'https://gitlab.com/ikhsan.jtk10/wbsapp/-/issues/64', 'Login - Create menu forgot password di menu Login', 'Open', '![image](/uploads/9b25c6f0f02c069858bb683446ad39ca/image.png)', 'ari restu ardian', 'imachu', 'Arroe Tanasa', 'arroe.tanasa', 'No', 'No', '0000-00-00 00:00:00', '2020-06-18 06:56:22', '2020-06-18 06:56:52', '0000-00-00 00:00:00', '', '', 'CHANGE REQUEST,LOGIN,TO DO', 0, 0),
(2, 1, 1, '2020-07-07 14:46:01', 104, 'https://gitlab.com/ikhsan.jtk10/wbsapp/-/issues/104', 'permit application - Email sending belum jalan', 'Open', 'di menu add permit dan resend email', 'ari restu ardian', 'imachu', 'Arroe Tanasa', 'arroe.tanasa', 'No', 'No', '0000-00-00 00:00:00', '2020-06-22 09:05:06', '2020-06-25 02:26:00', '0000-00-00 00:00:00', '', '', 'A,BUG,PERMIT APPLICATION,TO DO,USER HRD', 0, 0),
(3, 1, 1, '2020-07-07 14:46:01', 115, 'https://gitlab.com/ikhsan.jtk10/wbsapp/-/issues/115', 'Add Activity - perhitungan actual hours masih salah', 'Open', '![bandicam_2020-06-23_10-55-35-232](/uploads/84cc90abddb73a519ef1bb8294edd56a/bandicam_2020-06-23_10-55-35-232.mp4)\r\n\r\nPreKondisi\r\n1. start time  23:00\r\n2. stopt time  01:00\r\n3. start date  23-06-2020\r\n4. end date    24-06-2020\r\nResult actual hours (-22,)\r\nekspektasi :\r\nhasil actual hours harusnya 2 jam kerja', 'ari restu ardian', 'imachu', 'Puguh Jayadi', 'puguhjayadi', 'No', 'No', '0000-00-00 00:00:00', '2020-06-23 04:00:59', '2020-06-25 12:57:19', '0000-00-00 00:00:00', '', '', 'A,ACTIVITY,BUG,FOR TESTING,TO DO,USER,USER HRD', 0, 0),
(4, 1, 1, '2020-07-07 14:46:01', 120, 'https://gitlab.com/ikhsan.jtk10/wbsapp/-/issues/120', 'Dashboard - selain PG dan QC, user yang lain tidak muncul di dashboard', 'Open', '![bandicam_2020-06-23_14-40-31-818](/uploads/74e4f27e9a0327256af4850630c0d043/bandicam_2020-06-23_14-40-31-818.mp4)\r\n\r\nNote :\r\nHRD, Sysadmin, dan role position yang lain chartnya tidak muncul di dashboard\r\n\r\nExpectation:\r\nHarusnya HRD, Sysadmin dan User dashboardnya tetap muncul di semua user..', 'ari restu ardian', 'imachu', 'Puguh Jayadi', 'puguhjayadi', 'No', 'No', '0000-00-00 00:00:00', '2020-06-23 07:45:02', '2020-06-26 03:17:21', '0000-00-00 00:00:00', '', '', 'A,BUG,DASHBOARD,TO DO,USER,USER HRD', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'SU', 'Super User'),
(2, 'HRD', 'Human Resources Development'),
(3, 'PG', 'Programmer'),
(4, 'QC', 'Quality Control'),
(5, 'SA', 'System Admin');

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `inventory_name` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `inventory_name`) VALUES
(1, 'iPad Pro 2020'),
(4, 'Iphone 7 Plus'),
(5, 'Raspberry pI');

-- --------------------------------------------------------

--
-- Table structure for table `inventory_loan_activity`
--

CREATE TABLE `inventory_loan_activity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `loan_duration` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `inventory_loan_activity`
--

INSERT INTO `inventory_loan_activity` (`id`, `user_id`, `inventory_id`, `start_date`, `loan_duration`) VALUES
(3, 3, 1, '2021-09-04', 3),
(8, 2, 1, '2021-09-10', 4),
(9, 2, 4, '2021-09-10', 3),
(10, 2, 1, '2021-09-10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(120) DEFAULT NULL,
  `menu_icon` varchar(120) DEFAULT NULL,
  `link` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `menu_icon`, `link`) VALUES
(12, 'Menu001', 'fa-bar-chart', 'Dashboard'),
(13, 'Menu002', 'fa-users', NULL),
(14, 'Menu003', 'fa-folder-open', 'ProjectManager'),
(15, 'Menu004', 'fa-file-text', 'UserWBS'),
(16, 'Menu005', 'fa-envelope', ''),
(17, 'Menu006', 'fa-envelope', ''),
(18, 'Menu007', 'fa-cubes', ''),
(19, 'Menu008', 'fa-comment', 'Notification'),
(20, 'Menu009', 'fa-code-fork', 'GitIssue'),
(21, 'Menu010', 'fa-file', '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(20200728082614);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `type_notification` int(11) NOT NULL,
  `type_target` int(11) NOT NULL,
  `target_role_id` int(11) NOT NULL,
  `target_user_id` int(11) NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT current_timestamp(),
  `updated_date` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `date`, `type_notification`, `type_target`, `target_role_id`, `target_user_id`, `category`, `title`, `description`, `created_by`, `updated_by`, `created_date`, `updated_date`) VALUES
(1, '2021-09-02', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-09-02 08:28:35', '2021-09-02 08:28:35'),
(2, '2021-09-08', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-09-06</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Cuti Idul Fitri</p>\n', NULL, NULL, '2021-09-08 14:03:38', '2021-09-08 14:03:38'),
(3, '2021-09-08', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-09-06</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Cuti Idul Fitri</p>\n', NULL, NULL, '2021-09-08 14:16:51', '2021-09-08 14:16:51'),
(4, '2021-09-08', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-09-06</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Cuti Idul Fitri</p>\n', NULL, NULL, '2021-09-08 14:16:53', '2021-09-08 14:16:53'),
(5, '2021-09-10', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-09-10 10:37:06', '2021-09-10 10:37:06'),
(6, '2021-09-10', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-09-10 10:37:09', '2021-09-10 10:37:09'),
(7, '2021-11-07', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-09-06</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Cuti Idul Fitri</p>\n', NULL, NULL, '2021-11-08 00:50:31', '2021-11-08 00:50:31'),
(8, '2021-11-07', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-09-06</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Cuti Idul Fitri</p>\n', NULL, NULL, '2021-11-08 00:50:36', '2021-11-08 00:50:36'),
(9, '2021-11-07', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-11-08 00:54:28', '2021-11-08 00:54:28'),
(10, '2021-11-07', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-09-06</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Cuti Idul Fitri</p>\n', NULL, NULL, '2021-11-08 00:54:36', '2021-11-08 00:54:36'),
(11, '2021-11-08', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-10-25</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Libur Om</p>\n', NULL, NULL, '2021-11-08 10:12:53', '2021-11-08 10:12:53'),
(12, '2021-11-08', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-10-25</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Libur Om</p>\n', NULL, NULL, '2021-11-08 10:12:57', '2021-11-08 10:12:57'),
(13, '2021-11-08', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-10-25</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Libur Om</p>\n', NULL, NULL, '2021-11-08 10:12:59', '2021-11-08 10:12:59'),
(14, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-10-25</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Libur Om</p>\n', NULL, NULL, '2021-11-09 11:17:43', '2021-11-09 11:17:43'),
(15, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-10-25</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Libur Om</p>\n', NULL, NULL, '2021-11-09 11:17:45', '2021-11-09 11:17:45'),
(16, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-10-25</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Libur Om</p>\n', NULL, NULL, '2021-11-09 11:17:47', '2021-11-09 11:17:47'),
(17, '2021-11-09', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-11-09 11:20:13', '2021-11-09 11:20:13'),
(18, '2021-11-09', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-11-09 11:20:18', '2021-11-09 11:20:18'),
(19, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-10-25</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Libur Om</p>\n', NULL, NULL, '2021-11-09 11:20:35', '2021-11-09 11:20:35'),
(20, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-10-25</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Libur Om</p>\n', NULL, NULL, '2021-11-09 11:21:12', '2021-11-09 11:21:12'),
(21, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-10-25</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Libur Om</p>\n', NULL, NULL, '2021-11-09 11:22:08', '2021-11-09 11:22:08'),
(22, '2021-11-09', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-11-09 11:22:33', '2021-11-09 11:22:33'),
(23, '2021-11-09', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-11-09 11:25:33', '2021-11-09 11:25:33'),
(24, '2021-11-09', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-11-09 11:25:34', '2021-11-09 11:25:34'),
(25, '2021-11-09', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-11-09 11:27:05', '2021-11-09 11:27:05'),
(26, '2021-11-09', 1, 0, 0, 2, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2020-07-06</p>\r\n    <p>Long <i>(day)</i> : 1</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>asdasdas</p>\n', NULL, NULL, '2021-11-09 11:27:41', '2021-11-09 11:27:41'),
(27, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:31:02', '2021-11-09 11:31:02'),
(28, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:31:08', '2021-11-09 11:31:08'),
(29, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:32:29', '2021-11-09 11:32:29'),
(30, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:32:55', '2021-11-09 11:32:55'),
(31, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:34:32', '2021-11-09 11:34:32'),
(32, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:34:44', '2021-11-09 11:34:44'),
(33, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:37:10', '2021-11-09 11:37:10'),
(34, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:37:15', '2021-11-09 11:37:15'),
(35, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:41:53', '2021-11-09 11:41:53'),
(36, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:41:55', '2021-11-09 11:41:55'),
(37, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:42:56', '2021-11-09 11:42:56'),
(38, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:42:59', '2021-11-09 11:42:59'),
(39, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:43:39', '2021-11-09 11:43:39'),
(40, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:43:43', '2021-11-09 11:43:43'),
(41, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:44:25', '2021-11-09 11:44:25'),
(42, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:44:27', '2021-11-09 11:44:27'),
(43, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Decline', '<p>Permit : <b>Decline</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:45:32', '2021-11-09 11:45:32'),
(44, '2021-11-09', 1, 0, 0, 3, 'Permit', 'Permit Sakit : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-09</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Sakit</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-09 11:45:47', '2021-11-09 11:45:47'),
(45, '2021-11-13', 1, 0, 0, 3, 'Permit', 'Permit Cuti : Approve', '<p>Permit : <b>Approve</b></p>\r\n    <p>Start Date : 2021-11-13</p>\r\n    <p>Long <i>(day)</i> : 2</p>\r\n    <p>Reason : Cuti</p>\r\n    <hr><p>Cuti Cuti Cuti Cuti Cuti Cuti</p>\n', NULL, NULL, '2021-11-13 09:10:48', '2021-11-13 09:10:48');

-- --------------------------------------------------------

--
-- Table structure for table `notification_view`
--

CREATE TABLE `notification_view` (
  `user_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `notification_view`
--

INSERT INTO `notification_view` (`user_id`, `notification_id`) VALUES
(3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `permit_activity`
--

CREATE TABLE `permit_activity` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `long` int(11) NOT NULL,
  `to` varchar(255) NOT NULL DEFAULT '',
  `cc` varchar(255) DEFAULT '',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `email_send` int(11) NOT NULL DEFAULT 0,
  `approval` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `permit_activity`
--

INSERT INTO `permit_activity` (`id`, `user_id`, `reason_id`, `start_date`, `long`, `to`, `cc`, `subject`, `description`, `email_send`, `approval`) VALUES
(21, 3, 2, '2021-11-13', 2, 'hrd1@mail.com', 'qc1@mail.com', 'Cuti Idul Fitri', '<p>Cuti Cuti Cuti Cuti Cuti Cuti</p>\n', 0, 1),
(24, 3, 1, '2021-11-15', 1, 'hrd1@mail.com', '', 'Cuti Idul Fitri', '<p>AAAAAAAAAAAAAAAA</p>\n', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `permit_reason`
--

CREATE TABLE `permit_reason` (
  `id` int(11) NOT NULL,
  `reason_name` varchar(120) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `permit_reason`
--

INSERT INTO `permit_reason` (`id`, `reason_name`, `description`) VALUES
(1, 'Sakit', NULL),
(2, 'Cuti', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `privilege`
--

CREATE TABLE `privilege` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `submenu_id` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `create` int(2) NOT NULL,
  `update` int(2) NOT NULL,
  `delete` int(2) NOT NULL,
  `read` int(2) NOT NULL,
  `upload` int(2) NOT NULL,
  `download` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `privilege`
--

INSERT INTO `privilege` (`id`, `menu_id`, `submenu_id`, `group_id`, `create`, `update`, `delete`, `read`, `upload`, `download`) VALUES
(44, 21, NULL, 1, 1, 1, 1, 1, 1, 1),
(45, 21, NULL, 1, 1, 1, 1, 1, 1, 1),
(46, 14, NULL, 2, 1, 1, 1, 1, 1, 1),
(47, 15, NULL, 2, 1, 1, 1, 1, 1, 1),
(48, 16, 2, 2, 1, 1, 1, 1, 1, 1),
(49, 16, 3, 2, 1, 1, 1, 1, 1, 1),
(51, 21, 8, 2, 1, 1, 1, 1, 1, 1),
(52, 21, 9, 2, 1, 1, 1, 1, 1, 1),
(53, 21, 8, 3, 1, 1, 1, 1, 1, 1),
(54, 21, 9, 3, 1, 1, 1, 1, 1, 1),
(55, 16, 2, 3, 1, 1, 1, 1, 1, 1),
(56, 16, 3, 3, 1, 1, 1, 1, 1, 1),
(58, 15, NULL, 3, 1, 1, 1, 1, 1, 1),
(59, 21, 8, 4, 1, 1, 1, 1, 1, 1),
(60, 21, 9, 4, 1, 1, 1, 1, 1, 1),
(61, 16, 2, 4, 1, 1, 1, 1, 1, 1),
(63, 15, NULL, 4, 1, 1, 1, 1, 1, 1),
(64, 21, 8, 5, 1, 1, 1, 1, 1, 1),
(65, 21, 9, 5, 1, 1, 1, 1, 1, 1),
(66, 16, 3, 5, 1, 1, 1, 1, 1, 1),
(68, 15, NULL, 5, 1, 1, 1, 1, 1, 1),
(69, 12, NULL, 1, 1, 1, 1, 1, 1, 1),
(70, 12, NULL, 2, 1, 1, 1, 1, 1, 1),
(71, 12, NULL, 3, 1, 1, 1, 1, 1, 1),
(72, 12, NULL, 4, 1, 1, 1, 1, 1, 1),
(73, 12, NULL, 5, 1, 1, 1, 1, 1, 1),
(74, 13, 1, 1, 1, 1, 1, 1, 1, 1),
(75, 13, 1, 2, 1, 1, 1, 1, 1, 1),
(76, 13, 10, 1, 1, 1, 1, 1, 1, 1),
(85, 18, 5, 2, 1, 1, 1, 1, 1, 1),
(86, 18, 6, 2, 1, 1, 1, 1, 1, 1),
(87, 18, 6, 3, 1, 1, 1, 1, 1, 1),
(88, 18, 6, 4, 1, 1, 1, 1, 1, 1),
(89, 18, 7, 2, 1, 1, 1, 1, 1, 1),
(90, 18, 7, 3, 1, 1, 1, 1, 1, 1),
(91, 18, 7, 4, 1, 1, 1, 1, 1, 1),
(93, 16, 11, 2, 1, 1, 1, 1, 1, 1),
(95, 17, 4, 2, 1, 1, 1, 1, 1, 1),
(96, 17, 4, 3, 1, 1, 1, 1, 1, 1),
(97, 17, 4, 4, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `privilege_user`
--

CREATE TABLE `privilege_user` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `submenu_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `create` int(2) NOT NULL,
  `update` int(2) NOT NULL,
  `delete` int(2) NOT NULL,
  `read` int(2) NOT NULL,
  `upload` int(2) NOT NULL,
  `download` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `privilege_user`
--

INSERT INTO `privilege_user` (`id`, `menu_id`, `submenu_id`, `user_id`, `create`, `update`, `delete`, `read`, `upload`, `download`) VALUES
(1, 20, NULL, 4, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `due_date_revised` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `start_date`, `due_date`, `due_date_revised`) VALUES
(1, 'AWA', 'asdasda', '2020-06-05 00:00:00', '2020-06-11 00:00:00', '2020-06-11 00:00:00'),
(2, 'PB', 'adsdada', '2020-04-01 00:00:00', '2020-05-31 00:00:00', '2020-05-31 00:00:00'),
(3, 'AWD', '', '2020-06-01 00:00:00', '2020-08-31 00:00:00', NULL),
(4, 'ADC', 'sadsadas', '2020-06-01 00:00:00', '2020-08-31 00:00:00', '2020-08-31 00:00:00'),
(5, 'DSN', '', '2020-05-06 00:00:00', '2020-08-31 00:00:00', '2020-08-31 00:00:00'),
(6, 'ELM', '', '2020-06-01 00:00:00', '2020-08-31 00:00:00', NULL),
(7, 'AWP', '', '2020-06-01 00:00:00', '2020-08-31 00:00:00', NULL),
(8, 'AWV', '', '2020-06-02 00:00:00', '2020-08-30 00:00:00', NULL),
(9, 'PASOL', '', '2020-06-02 00:00:00', '2020-08-30 00:00:00', NULL),
(10, 'DPRO', '', '2020-05-31 00:00:00', '2020-08-30 00:00:00', NULL),
(11, 'ADCUM', '', '2020-05-31 00:00:00', '2020-08-24 00:00:00', NULL),
(12, 'ADOM', '', '2020-06-01 00:00:00', '2020-09-02 00:00:00', NULL),
(13, 'JAMBUL', '', '2020-06-01 00:00:00', '2020-08-25 00:00:00', NULL),
(14, 'WBSAPP', '', '2020-05-11 00:00:00', '2020-07-31 00:00:00', NULL),
(15, 'HP Clinic', '', '2020-06-01 00:00:00', '2020-08-31 00:00:00', NULL),
(16, 'STANDARD', '', '2020-05-31 00:00:00', '2020-08-01 00:00:00', NULL),
(17, 'MAINT SERVER', '', '2020-05-01 00:00:00', '2020-07-31 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `submenu`
--

CREATE TABLE `submenu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `submenu_name` varchar(120) DEFAULT NULL,
  `submenu_link` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `submenu`
--

INSERT INTO `submenu` (`id`, `menu_id`, `submenu_name`, `submenu_link`) VALUES
(1, 13, 'SubMenu002', 'UserManagement'),
(2, 16, 'SubMenu007', 'PermitActivity'),
(3, 16, 'SubMenu008', 'PermitReason'),
(4, 17, 'subMenu_AssignmentLetter001', 'AssignmentLetter'),
(5, 18, 'subMenu_Inventory001', 'InventoryManagement'),
(6, 18, 'subMenu_Inventory002', 'InventoryLoanActivity'),
(7, 18, 'subMenu_Inventory003', 'InventoryLoanList'),
(8, 21, 'SubMenuReport1', 'Report1'),
(9, 21, 'SubMenuReport2', 'Report 2'),
(10, 13, 'SubMenu00202', 'PrivilegeManagement'),
(11, 16, 'subMenu_Permit003', 'PermitQuotaManagement');

-- --------------------------------------------------------

--
-- Table structure for table `task_category`
--

CREATE TABLE `task_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `task_category`
--

INSERT INTO `task_category` (`id`, `name`, `role`, `description`) VALUES
(1, 'Check Permit', 2, ''),
(2, 'Bug#4 (Your Bugs)', 3, ''),
(3, 'Bug#3 (Your Bugs)', 3, ''),
(4, 'Bug#2 (Your Bugs)', 3, ''),
(5, 'Bug#1 (Your Bugs)', 3, ''),
(6, 'Meeting Coordination PG', 3, ''),
(7, 'Training PG', 3, ''),
(8, 'Issue Found (By QC)', 3, ''),
(9, 'Installation', 3, ''),
(10, 'Feedback (Not Your Task Before)', 3, ''),
(11, 'Bug Fixing (Not Your Bug)', 3, ''),
(12, 'Learning Project', 3, ''),
(13, 'Daily Meeting', 3, ''),
(14, 'Briefing Task', 3, ''),
(15, 'Feedback (Your Task Before)', 3, ''),
(16, 'Change Request', 3, ''),
(17, 'New Task', 3, ''),
(18, 'Bug#5 (Your Bugs)', 3, ''),
(19, 'Revise Doc Test Tamper', 4, ''),
(20, 'Test Rare Case (Initiative)', 4, ''),
(21, 'Test Tamper', 4, ''),
(22, 'Testing based on Doc Scenario Test', 4, ''),
(23, 'Testing based on Doc Tamper Test', 4, ''),
(24, 'Testing based on Doc Test Feature & GUI', 4, ''),
(25, 'Testing based on Doc Test Rare Case', 4, ''),
(26, 'Training', 4, ''),
(27, 'Revise Doc Test Scenario', 4, ''),
(28, 'Update Doc Test Feature & GUI', 4, ''),
(29, 'Update Doc Test Rare Case', 4, ''),
(30, 'Update Doc Test Scenario', 4, ''),
(31, 'Update Doc Test Tamper', 4, ''),
(32, 'Translate Document', 4, ''),
(33, 'Cek/Verifikasi Issue from Japan', 4, ''),
(34, 'Support PG or QC for Server', 4, ''),
(35, 'Cek Git Fortesting', 4, ''),
(36, 'Revise Doc Test Rare Case', 4, ''),
(37, 'Revise Doc Test Feature & GUI', 4, ''),
(38, 'Review Doc Wireframe (SS appli)', 4, ''),
(39, 'Review Doc Test Tamper', 4, ''),
(40, 'Review Doc Test Scenario', 4, ''),
(41, 'Review Doc Test Rare Case', 4, ''),
(42, 'Review Doc Test Feature & GUI', 4, ''),
(43, 'Meeting Coordination', 4, ''),
(44, 'Learning', 4, ''),
(45, 'Create Doc Wireframe (SS appli)', 4, ''),
(46, 'Create Doc Test Tamper', 4, ''),
(47, 'Create Doc Test Scenario', 4, ''),
(48, 'Create Doc Test Rare Case', 4, ''),
(49, 'Create Doc Test Feature & GUI', 4, ''),
(50, 'Briefing Project/Task', 4, ''),
(51, 'Setup Server', 5, ''),
(52, 'Update Server', 5, ''),
(53, 'Random Testing', 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `task_status`
--

CREATE TABLE `task_status` (
  `id` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `task_status`
--

INSERT INTO `task_status` (`id`, `name`, `description`) VALUES
(1, 'Done', ''),
(2, 'Feedback', ''),
(3, 'Progress', ''),
(4, 'Open', '');

-- --------------------------------------------------------

--
-- Table structure for table `template_wbs_detail`
--

CREATE TABLE `template_wbs_detail` (
  `id` int(11) NOT NULL,
  `template_wbs_id` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `task_name` varchar(100) DEFAULT NULL,
  `pic` int(11) NOT NULL,
  `excecutedby` int(11) NOT NULL,
  `start_time` time DEFAULT NULL,
  `stop_time` time DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `due_date_revised` datetime DEFAULT NULL,
  `estimated_hour` float DEFAULT NULL,
  `actual_hour` float DEFAULT NULL,
  `task_percentage` float DEFAULT NULL,
  `task_status` int(11) NOT NULL,
  `notes` text DEFAULT NULL,
  `created_date` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `template_wbs_name`
--

CREATE TABLE `template_wbs_name` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_type` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(255) NOT NULL,
  `permit_quota` int(11) NOT NULL,
  `digital_signature` varchar(100) DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `token` varchar(256) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT current_timestamp(),
  `updated_date` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `employment_status` int(11) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `wa_number` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type`, `username`, `email`, `password`, `permit_quota`, `digital_signature`, `ip_address`, `activation_selector`, `activation_code`, `token`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `created_date`, `updated_date`, `employment_status`, `phone_number`, `wa_number`) VALUES
(1, 1, 'Super User', 'super@user.com', '$2y$10$55Rg25fFOjRmwTZyOP4WE.vCbygl27tCF3mlZP1QtmQ2iYLgpNC/e', 12, NULL, '127.0.0.1', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1268889823, 1636968666, 1, '2020-05-05 11:01:15', '2021-11-15 16:31:06', 1, '882888888', '882888888'),
(2, 2, 'Fery Bayu Aji', 'hrd1@mail.com', '$2y$10$n.Bqzpm3h5LsEyX1suw/hOtNuuDWgCPOhrIDp7HI8j/9rxMxuDi9m', 12, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1636972735, 1, '2020-07-03 13:20:07', '2021-11-15 17:38:55', 0, '08988892506', '08988892506'),
(3, 3, 'Rafli Firdaus', 'pg1@mail.com', '$2y$10$BbsBcMu75QYO2qwqu5q20uCAeP3Uw.VUDF3M/oDbTxvkjj3bFNxLK', 0, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1636969643, 1, '2020-07-03 15:52:06', '2021-11-15 16:47:23', 0, '08988892506', '08988892506'),
(4, 4, 'qc satu', 'qc1@mail.com', '$2y$10$kpXbYg/ARDibXHgabcTxN.TxekdQ5wRmbi91FT37otcqPaT945oF.', 12, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1636967283, 1, '2020-07-03 16:18:13', '2021-11-15 16:08:03', 0, '08988892506', '08988892506'),
(5, 5, 'sa satu', 'sa1@mail.com', '$2y$10$ZVFl7Z7ezuKvFeFH38C5J.UZkBpG9qNfVDIflRpPq2PX7iuajBZH.', 12, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1636968657, 1, '2020-07-03 16:18:43', '2021-11-15 16:30:57', 0, '08988892506', '08988892506'),
(6, 3, 'pg2@mail.com', 'pg2@mail.com', '$2y$10$Ue7uYxHGLzVWhhqQ430lbez.iJzQAPEiDDWr7/zjc0a.jLw/NNZTm', 12, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1630912461, 1, '2020-07-07 13:23:40', '2021-11-09 11:24:43', 0, '08988892506', '08988892506'),
(7, 3, 'Pg tiga', 'pg3@mail.com', '$2y$10$Wd6xZXdcAVOb6cjg9sphku8fsw6wuQJ9SLYGdozsGzaULv0/ULI7m', 12, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, '2021-09-06 14:12:07', '2021-11-09 11:24:43', 1, '085950558091', '085950558091');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 3),
(7, 7, 3);

-- --------------------------------------------------------

--
-- Table structure for table `userwbs`
--

CREATE TABLE `userwbs` (
  `id` int(11) NOT NULL,
  `project` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `task_name` varchar(100) DEFAULT NULL,
  `pic` int(11) NOT NULL,
  `excecutedby` int(11) NOT NULL,
  `start_time` time DEFAULT NULL,
  `stop_time` time DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `due_date_revised` datetime DEFAULT NULL,
  `estimated_hour` float DEFAULT NULL,
  `actual_hour` float DEFAULT NULL,
  `task_percentage` float DEFAULT NULL,
  `task_status` int(11) NOT NULL,
  `notes` text DEFAULT NULL,
  `created_date` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `userwbs`
--

INSERT INTO `userwbs` (`id`, `project`, `category`, `task_name`, `pic`, `excecutedby`, `start_time`, `stop_time`, `start_date`, `due_date`, `due_date_revised`, `estimated_hour`, `actual_hour`, `task_percentage`, `task_status`, `notes`, `created_date`) VALUES
(1, 1, 2, '111', 3, 3, '05:00:00', '10:00:00', '2020-07-08 00:00:00', '2020-07-08 00:00:00', '2020-07-08 00:00:00', 2, 5, 100, 1, 'Add Todays Activity - ada fitur yang unlock , dipegang oleh adminnya\n\nAdd Todays Activity - ada fitur yang unlock , dipegang oleh adminnya\n\nAdd Todays Activity - ada fitur yang unlock , dipegang oleh adminnya\n\nAdd Todays Activity - ada fitur yang unlock , dipegang oleh adminnya', '2020-07-08 21:07:45'),
(2, 2, 2, '222', 3, 3, '04:00:00', '05:00:00', '2020-07-08 00:00:00', '2020-07-08 00:00:00', NULL, 1, 1, 0, 1, 'Add Todays Activity - ada fitur yang unlock , dipegang oleh adminnya', '2020-07-08 21:10:23'),
(3, 2, 2, '222', 3, 3, '04:00:00', '05:00:00', '2020-07-08 00:00:00', '2020-07-08 00:00:00', NULL, 1, 1, 0, 1, 'Add Todays Activity - ada fitur yang unlock , dipegang oleh adminnya', '2020-07-08 21:10:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assignment_letter`
--
ALTER TABLE `assignment_letter`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`projects_id`),
  ADD KEY `projects_id` (`projects_id`),
  ADD KEY `recipient_id` (`recipient_id`);

--
-- Indexes for table `git_issue`
--
ALTER TABLE `git_issue`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `project_id` (`project_id`),
  ADD KEY `user_id_create` (`user_id_upload`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory_loan_activity`
--
ALTER TABLE `inventory_loan_activity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`inventory_id`),
  ADD KEY `inventory_id` (`inventory_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `notification_view`
--
ALTER TABLE `notification_view`
  ADD KEY `user_id` (`user_id`) USING BTREE,
  ADD KEY `notification_id` (`notification_id`) USING BTREE;

--
-- Indexes for table `permit_activity`
--
ALTER TABLE `permit_activity`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `reason_id` (`reason_id`) USING BTREE,
  ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- Indexes for table `permit_reason`
--
ALTER TABLE `permit_reason`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `privilege`
--
ALTER TABLE `privilege`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `submenu_id` (`submenu_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `privilege_user`
--
ALTER TABLE `privilege_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `submenu_id` (`submenu_id`),
  ADD KEY `group_id` (`user_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `submenu`
--
ALTER TABLE `submenu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `task_category`
--
ALTER TABLE `task_category`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `role` (`role`);

--
-- Indexes for table `task_status`
--
ALTER TABLE `task_status`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `template_wbs_detail`
--
ALTER TABLE `template_wbs_detail`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `pic` (`pic`),
  ADD KEY `excecutedby` (`excecutedby`),
  ADD KEY `project` (`project`),
  ADD KEY `category` (`category`),
  ADD KEY `task_status` (`task_status`),
  ADD KEY `template_wbs_id` (`template_wbs_id`);

--
-- Indexes for table `template_wbs_name`
--
ALTER TABLE `template_wbs_name`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `user_type` (`user_type`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `userwbs`
--
ALTER TABLE `userwbs`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD KEY `pic` (`pic`),
  ADD KEY `excecutedby` (`excecutedby`),
  ADD KEY `project` (`project`),
  ADD KEY `category` (`category`),
  ADD KEY `task_status` (`task_status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assignment_letter`
--
ALTER TABLE `assignment_letter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `git_issue`
--
ALTER TABLE `git_issue`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `inventory_loan_activity`
--
ALTER TABLE `inventory_loan_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `permit_activity`
--
ALTER TABLE `permit_activity`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `permit_reason`
--
ALTER TABLE `permit_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `privilege`
--
ALTER TABLE `privilege`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `privilege_user`
--
ALTER TABLE `privilege_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `submenu`
--
ALTER TABLE `submenu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `task_category`
--
ALTER TABLE `task_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `task_status`
--
ALTER TABLE `task_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `template_wbs_detail`
--
ALTER TABLE `template_wbs_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `template_wbs_name`
--
ALTER TABLE `template_wbs_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `userwbs`
--
ALTER TABLE `userwbs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assignment_letter`
--
ALTER TABLE `assignment_letter`
  ADD CONSTRAINT `assignment_letter_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `assignment_letter_ibfk_2` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `assignment_letter_ibfk_3` FOREIGN KEY (`recipient_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `git_issue`
--
ALTER TABLE `git_issue`
  ADD CONSTRAINT `git_issue_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `git_issue_ibfk_2` FOREIGN KEY (`user_id_upload`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `inventory_loan_activity`
--
ALTER TABLE `inventory_loan_activity`
  ADD CONSTRAINT `inventory_loan_activity_ibfk_1` FOREIGN KEY (`inventory_id`) REFERENCES `inventory` (`id`),
  ADD CONSTRAINT `inventory_loan_activity_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `notification_view`
--
ALTER TABLE `notification_view`
  ADD CONSTRAINT `notification_view_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notification_view_ibfk_2` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permit_activity`
--
ALTER TABLE `permit_activity`
  ADD CONSTRAINT `permit_activity_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permit_activity_ibfk_4` FOREIGN KEY (`reason_id`) REFERENCES `permit_reason` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `privilege`
--
ALTER TABLE `privilege`
  ADD CONSTRAINT `privilege_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `privilege_ibfk_2` FOREIGN KEY (`submenu_id`) REFERENCES `submenu` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `privilege_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `privilege_user`
--
ALTER TABLE `privilege_user`
  ADD CONSTRAINT `privilege_user_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `privilege_user_ibfk_2` FOREIGN KEY (`submenu_id`) REFERENCES `submenu` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `privilege_user_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `submenu`
--
ALTER TABLE `submenu`
  ADD CONSTRAINT `submenu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `task_category`
--
ALTER TABLE `task_category`
  ADD CONSTRAINT `task_category_ibfk_1` FOREIGN KEY (`role`) REFERENCES `groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `template_wbs_detail`
--
ALTER TABLE `template_wbs_detail`
  ADD CONSTRAINT `template_wbs_detail_ibfk_1` FOREIGN KEY (`pic`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `template_wbs_detail_ibfk_2` FOREIGN KEY (`excecutedby`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `template_wbs_detail_ibfk_3` FOREIGN KEY (`project`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `template_wbs_detail_ibfk_4` FOREIGN KEY (`category`) REFERENCES `task_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `template_wbs_detail_ibfk_5` FOREIGN KEY (`task_status`) REFERENCES `task_status` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `template_wbs_detail_ibfk_6` FOREIGN KEY (`template_wbs_id`) REFERENCES `template_wbs_name` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `template_wbs_name`
--
ALTER TABLE `template_wbs_name`
  ADD CONSTRAINT `template_wbs_name_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_type`) REFERENCES `groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userwbs`
--
ALTER TABLE `userwbs`
  ADD CONSTRAINT `userwbs_ibfk_1` FOREIGN KEY (`pic`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userwbs_ibfk_2` FOREIGN KEY (`excecutedby`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userwbs_ibfk_3` FOREIGN KEY (`project`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userwbs_ibfk_4` FOREIGN KEY (`category`) REFERENCES `task_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userwbs_ibfk_5` FOREIGN KEY (`task_status`) REFERENCES `task_status` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
