# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.26)
# Database: wbs_system
# Generation Time: 2020-07-03 09:41:29 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table git_issue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `git_issue`;

CREATE TABLE `git_issue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `user_id_upload` int(11) NOT NULL,
  `date_upload` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `issue_id` int(20) unsigned NOT NULL,
  `url` text NOT NULL,
  `title` text NOT NULL,
  `state` varchar(50) DEFAULT '',
  `description` text,
  `author` varchar(150) NOT NULL DEFAULT '',
  `author_username` varchar(100) NOT NULL DEFAULT '',
  `assignee` varchar(150) DEFAULT '',
  `assignee_username` varchar(100) DEFAULT '',
  `confidential` varchar(15) DEFAULT '',
  `locked` varchar(15) DEFAULT '',
  `due_date` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `milestone` text,
  `weight` varchar(100) NOT NULL,
  `labels` text,
  `time_estimate` double NOT NULL DEFAULT '0',
  `time_spent` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `project_id` (`project_id`),
  KEY `user_id_create` (`user_id_upload`),
  CONSTRAINT `git_issue_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `git_issue_ibfk_2` FOREIGN KEY (`user_id_upload`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;



# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `name`, `description`)
VALUES
	(1,'SU','Super User'),
	(2,'HRD','Human Resources Development'),
	(3,'PG','Programmer'),
	(4,'QC','Quality Control'),
	(5,'SA','System Admin');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table login_attempts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;



# Dump of table menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(120) DEFAULT NULL,
  `menu_icon` varchar(120) DEFAULT NULL,
  `link` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;

INSERT INTO `menu` (`id`, `menu_name`, `menu_icon`, `link`)
VALUES
	(1,'Menu001','fa-bar-chart','Dashboard'),
	(2,'Menu002','fa-users',NULL),
	(3,'Menu003','fa-folder-open','ProjectManager'),
	(4,'Menu004','fa-file-text','UserWBS'),
	(5,'Menu005','fa-envelope',NULL),
	(6,'Menu006','fa-comment','Notification'),
	(7,'Menu007','fa-code-fork','GitIssue'),
	(8,'Menu008','fa-file','');

/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`version`)
VALUES
	(20200619010705);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table notification
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification`;

CREATE TABLE `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `type_notification` int(11) NOT NULL,
  `type_target` int(11) NOT NULL,
  `target_role_id` int(11) NOT NULL,
  `target_user_id` int(11) NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;



# Dump of table notification_view
# ------------------------------------------------------------

DROP TABLE IF EXISTS `notification_view`;

CREATE TABLE `notification_view` (
  `user_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `notification_id` (`notification_id`) USING BTREE,
  CONSTRAINT `notification_view_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `notification_view_ibfk_2` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;



# Dump of table permit_activity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permit_activity`;

CREATE TABLE `permit_activity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `long` int(11) NOT NULL,
  `to` varchar(255) NOT NULL DEFAULT '',
  `cc` varchar(255) NOT NULL DEFAULT '',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `email_send` int(11) NOT NULL DEFAULT '0',
  `approval` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `reason_id` (`reason_id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  CONSTRAINT `permit_activity_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permit_activity_ibfk_4` FOREIGN KEY (`reason_id`) REFERENCES `permit_reason` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;



# Dump of table permit_reason
# ------------------------------------------------------------

DROP TABLE IF EXISTS `permit_reason`;

CREATE TABLE `permit_reason` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason_name` varchar(120) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

LOCK TABLES `permit_reason` WRITE;
/*!40000 ALTER TABLE `permit_reason` DISABLE KEYS */;

INSERT INTO `permit_reason` (`id`, `reason_name`, `description`)
VALUES
	(1,'Sakit',NULL),
	(2,'Cuti',NULL);

/*!40000 ALTER TABLE `permit_reason` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table privilege
# ------------------------------------------------------------

DROP TABLE IF EXISTS `privilege`;

CREATE TABLE `privilege` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `submenu_id` int(11) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `create` int(2) NOT NULL,
  `update` int(2) NOT NULL,
  `delete` int(2) NOT NULL,
  `read` int(2) NOT NULL,
  `upload` int(2) NOT NULL,
  `download` int(2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  KEY `submenu_id` (`submenu_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `privilege_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE,
  CONSTRAINT `privilege_ibfk_2` FOREIGN KEY (`submenu_id`) REFERENCES `submenu` (`id`) ON DELETE CASCADE,
  CONSTRAINT `privilege_ibfk_3` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `privilege` WRITE;
/*!40000 ALTER TABLE `privilege` DISABLE KEYS */;

INSERT INTO `privilege` (`id`, `menu_id`, `submenu_id`, `group_id`, `create`, `update`, `delete`, `read`, `upload`, `download`)
VALUES
	(1,1,NULL,1,1,1,1,1,1,1),
	(2,2,1,1,1,1,1,1,1,1),
	(3,2,2,1,1,1,1,1,1,1),
	(4,7,NULL,1,1,1,1,1,1,1),
	(5,8,5,1,1,1,1,1,1,1),
	(6,8,6,1,1,1,1,1,1,1),
	(7,1,NULL,2,1,1,1,1,1,1),
	(8,2,1,2,1,1,1,1,1,1),
	(9,2,2,2,1,1,1,1,1,1),
	(10,3,NULL,2,1,1,1,1,1,1),
	(11,4,NULL,2,1,1,1,1,1,1),
	(12,5,3,2,1,1,1,1,1,1),
	(13,5,4,2,1,1,1,1,1,1),
	(14,6,NULL,2,1,1,1,1,1,1),
	(15,7,NULL,2,1,1,1,1,1,1),
	(16,8,6,2,1,1,1,1,1,1),
	(17,8,5,2,1,1,1,1,1,1),
	(18,8,5,3,1,1,1,1,1,1),
	(19,7,NULL,3,1,1,1,1,1,1),
	(20,8,6,3,1,1,1,1,1,1),
	(21,5,3,3,1,1,1,1,1,1),
	(22,5,4,3,1,1,1,1,1,1),
	(23,6,NULL,3,1,1,1,1,1,1),
	(24,1,NULL,3,1,1,1,1,1,1),
	(25,4,NULL,3,1,1,1,1,1,1),
	(26,7,NULL,4,1,1,1,1,1,1),
	(27,8,5,4,1,1,1,1,1,1),
	(28,8,6,4,1,1,1,1,1,1),
	(29,5,4,4,1,1,1,1,1,1),
	(30,5,4,4,1,1,1,1,1,1),
	(31,6,NULL,4,1,1,1,1,1,1),
	(32,1,NULL,4,1,1,1,1,1,1),
	(33,4,NULL,4,1,1,1,1,1,1),
	(34,7,NULL,5,1,1,1,1,1,1),
	(35,8,5,5,1,1,1,1,1,1),
	(36,8,6,5,1,1,1,1,1,1),
	(37,5,4,5,1,1,1,1,1,1),
	(38,5,4,5,1,1,1,1,1,1),
	(39,6,NULL,5,1,1,1,1,1,1),
	(40,1,NULL,5,1,1,1,1,1,1),
	(41,4,NULL,5,1,1,1,1,1,1);

/*!40000 ALTER TABLE `privilege` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `due_date_revised` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;

INSERT INTO `projects` (`id`, `name`, `description`, `start_date`, `due_date`, `due_date_revised`)
VALUES
	(1,'AWA','asdasda','2020-06-05 00:00:00','2020-06-11 00:00:00','2020-06-11 00:00:00'),
	(2,'PB','adsdada','2020-04-01 00:00:00','2020-05-31 00:00:00','2020-05-31 00:00:00'),
	(3,'AWD','','2020-06-01 00:00:00','2020-08-31 00:00:00',NULL),
	(4,'ADC','sadsadas','2020-06-01 00:00:00','2020-08-31 00:00:00','2020-08-31 00:00:00'),
	(5,'DSN','','2020-05-06 00:00:00','2020-08-31 00:00:00','2020-08-31 00:00:00'),
	(6,'ELM','','2020-06-01 00:00:00','2020-08-31 00:00:00',NULL),
	(7,'AWP','','2020-06-01 00:00:00','2020-08-31 00:00:00',NULL),
	(8,'AWV','','2020-06-02 00:00:00','2020-08-30 00:00:00',NULL),
	(9,'PASOL','','2020-06-02 00:00:00','2020-08-30 00:00:00',NULL),
	(10,'DPRO','','2020-05-31 00:00:00','2020-08-30 00:00:00',NULL),
	(11,'ADCUM','','2020-05-31 00:00:00','2020-08-24 00:00:00',NULL),
	(12,'ADOM','','2020-06-01 00:00:00','2020-09-02 00:00:00',NULL),
	(13,'JAMBUL','','2020-06-01 00:00:00','2020-08-25 00:00:00',NULL),
	(14,'WBSAPP','','2020-05-11 00:00:00','2020-07-31 00:00:00',NULL),
	(15,'HP Clinic','','2020-06-01 00:00:00','2020-08-31 00:00:00',NULL),
	(16,'STANDARD','','2020-05-31 00:00:00','2020-08-01 00:00:00',NULL),
	(17,'MAINT SERVER','','2020-05-01 00:00:00','2020-07-31 00:00:00',NULL);

/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table submenu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `submenu`;

CREATE TABLE `submenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `submenu_name` varchar(120) DEFAULT NULL,
  `submenu_link` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  CONSTRAINT `submenu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

LOCK TABLES `submenu` WRITE;
/*!40000 ALTER TABLE `submenu` DISABLE KEYS */;

INSERT INTO `submenu` (`id`, `menu_id`, `submenu_name`, `submenu_link`)
VALUES
	(1,2,'SubMenu002','UserManagement'),
	(2,2,'SubMenu003','UserType'),
	(3,5,'SubMenu007','PermitActivity'),
	(4,5,'SubMenu008','PermitReason'),
	(5,8,'SubMenuReport1','Report1'),
	(6,8,'SubMenuReport2','Report2');

/*!40000 ALTER TABLE `submenu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table task_category
# ------------------------------------------------------------

DROP TABLE IF EXISTS `task_category`;

CREATE TABLE `task_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `role` (`role`),
  CONSTRAINT `task_category_ibfk_1` FOREIGN KEY (`role`) REFERENCES `groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

LOCK TABLES `task_category` WRITE;
/*!40000 ALTER TABLE `task_category` DISABLE KEYS */;

INSERT INTO `task_category` (`id`, `name`, `role`, `description`)
VALUES
	(1,'Check Permit',2,''),
	(2,'Bug#4 (Your Bugs)',3,''),
	(3,'Bug#3 (Your Bugs)',3,''),
	(4,'Bug#2 (Your Bugs)',3,''),
	(5,'Bug#1 (Your Bugs)',3,''),
	(6,'Meeting Coordination PG',3,''),
	(7,'Training PG',3,''),
	(8,'Issue Found (By QC)',3,''),
	(9,'Installation',3,''),
	(10,'Feedback (Not Your Task Before)',3,''),
	(11,'Bug Fixing (Not Your Bug)',3,''),
	(12,'Learning Project',3,''),
	(13,'Daily Meeting',3,''),
	(14,'Briefing Task',3,''),
	(15,'Feedback (Your Task Before)',3,''),
	(16,'Change Request',3,''),
	(17,'New Task',3,''),
	(18,'Bug#5 (Your Bugs)',3,''),
	(19,'Revise Doc Test Tamper',4,''),
	(20,'Test Rare Case (Initiative)',4,''),
	(21,'Test Tamper',4,''),
	(22,'Testing based on Doc Scenario Test',4,''),
	(23,'Testing based on Doc Tamper Test',4,''),
	(24,'Testing based on Doc Test Feature & GUI',4,''),
	(25,'Testing based on Doc Test Rare Case',4,''),
	(26,'Training',4,''),
	(27,'Revise Doc Test Scenario',4,''),
	(28,'Update Doc Test Feature & GUI',4,''),
	(29,'Update Doc Test Rare Case',4,''),
	(30,'Update Doc Test Scenario',4,''),
	(31,'Update Doc Test Tamper',4,''),
	(32,'Translate Document',4,''),
	(33,'Cek/Verifikasi Issue from Japan',4,''),
	(34,'Support PG or QC for Server',4,''),
	(35,'Cek Git Fortesting',4,''),
	(36,'Revise Doc Test Rare Case',4,''),
	(37,'Revise Doc Test Feature & GUI',4,''),
	(38,'Review Doc Wireframe (SS appli)',4,''),
	(39,'Review Doc Test Tamper',4,''),
	(40,'Review Doc Test Scenario',4,''),
	(41,'Review Doc Test Rare Case',4,''),
	(42,'Review Doc Test Feature & GUI',4,''),
	(43,'Meeting Coordination',4,''),
	(44,'Learning',4,''),
	(45,'Create Doc Wireframe (SS appli)',4,''),
	(46,'Create Doc Test Tamper',4,''),
	(47,'Create Doc Test Scenario',4,''),
	(48,'Create Doc Test Rare Case',4,''),
	(49,'Create Doc Test Feature & GUI',4,''),
	(50,'Briefing Project/Task',4,''),
	(51,'Setup Server',5,''),
	(52,'Update Server',5,''),
	(53,'Random Testing',5,'');

/*!40000 ALTER TABLE `task_category` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table task_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `task_status`;

CREATE TABLE `task_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

LOCK TABLES `task_status` WRITE;
/*!40000 ALTER TABLE `task_status` DISABLE KEYS */;

INSERT INTO `task_status` (`id`, `name`, `description`)
VALUES
	(1,'Done',''),
	(2,'Feedback',''),
	(3,'Progress',''),
	(4,'Open','');

/*!40000 ALTER TABLE `task_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `token` varchar(256) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `employment_status` int(11) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `wa_number` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_type` (`user_type`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_type`) REFERENCES `groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `user_type`, `username`, `email`, `password`, `ip_address`, `activation_selector`, `activation_code`, `token`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `created_date`, `updated_date`, `employment_status`, `phone_number`, `wa_number`)
VALUES
	(1,1,'Super User','super@user.com','$2y$10$55Rg25fFOjRmwTZyOP4WE.vCbygl27tCF3mlZP1QtmQ2iYLgpNC/e','127.0.0.1',NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,1268889823,1593750020,1,'2020-05-05 11:01:15','2020-07-03 11:20:44',1,'882888888','882888888'),
	(2,2,'hrd satu','hrd1@mail.com','$2y$10$n.Bqzpm3h5LsEyX1suw/hOtNuuDWgCPOhrIDp7HI8j/9rxMxuDi9m','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1593766728,1,'2020-07-03 13:20:07','2020-07-03 15:58:48',0,'08988892506','08988892506'),
	(3,3,'pg satu','pg1@mail.com','$2y$10$BbsBcMu75QYO2qwqu5q20uCAeP3Uw.VUDF3M/oDbTxvkjj3bFNxLK','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1593766351,1,'2020-07-03 15:52:06','2020-07-03 15:52:31',0,'08988892506','08988892506'),
	(4,4,'qc satu','qc1@mail.com','$2y$10$kpXbYg/ARDibXHgabcTxN.TxekdQ5wRmbi91FT37otcqPaT945oF.','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1593767956,1,'2020-07-03 16:18:13','2020-07-03 16:19:16',0,'08988892506','08988892506'),
	(5,5,'sa satu','sa1@mail.com','$2y$10$ZVFl7Z7ezuKvFeFH38C5J.UZkBpG9qNfVDIflRpPq2PX7iuajBZH.','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,1593768128,1,'2020-07-03 16:18:43','2020-07-03 16:22:08',0,'08988892506','08988892506');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

LOCK TABLES `users_groups` WRITE;
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`)
VALUES
	(1,1,1),
	(2,2,2),
	(3,3,3),
	(4,4,4),
	(5,5,5);

/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table userwbs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `userwbs`;

CREATE TABLE `userwbs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `task_name` varchar(100) DEFAULT NULL,
  `pic` int(11) NOT NULL,
  `excecutedby` int(11) NOT NULL,
  `start_time` time DEFAULT NULL,
  `stop_time` time DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `due_date` datetime DEFAULT NULL,
  `due_date_revised` datetime DEFAULT NULL,
  `estimated_hour` float DEFAULT NULL,
  `actual_hour` float DEFAULT NULL,
  `task_percentage` float DEFAULT NULL,
  `task_status` int(11) NOT NULL,
  `notes` text,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pic` (`pic`),
  KEY `excecutedby` (`excecutedby`),
  KEY `project` (`project`),
  KEY `category` (`category`),
  KEY `task_status` (`task_status`),
  CONSTRAINT `userwbs_ibfk_1` FOREIGN KEY (`pic`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userwbs_ibfk_2` FOREIGN KEY (`excecutedby`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userwbs_ibfk_3` FOREIGN KEY (`project`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userwbs_ibfk_4` FOREIGN KEY (`category`) REFERENCES `task_category` (`id`) ON DELETE CASCADE,
  CONSTRAINT `userwbs_ibfk_5` FOREIGN KEY (`task_status`) REFERENCES `task_status` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
